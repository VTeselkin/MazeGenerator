﻿using UnityEngine;
using UnityEditor;

public class CreateClassAsset : MonoBehaviour
{

	[MenuItem("AutoBuilder/Create/GPSettings")]
	public static void CreateGPAsset()
	{
		ScriptableObjectUtility.CreateAsset<GPPlatformBuildSettings>();
	}

	[MenuItem("AutoBuilder/Create/IOSSettings")]
	public static void CreateIOSAsset()
	{
		ScriptableObjectUtility.CreateAsset<IOSPlatformBuildSettings>();
	}
}
