﻿using System;
using System.IO;
using UnityEditor;
using UnityEngine;

public class AutoBuilder : MonoBehaviour
{

	[MenuItem("AutoBuilder/Maze GooglePlay")]
	private static void PerformAndroidBuildDev()
	{
		GPPlatformBuildSettings currentSettings = Resources.Load<GPPlatformBuildSettings>("GPPlatformBuildSettings");
		PlayerSettings.SetScriptingDefineSymbolsForGroup(
			BuildTargetGroup.Android, "PLATFORM_ANDROID");

		PlayerSettings.companyName = currentSettings.CompanyName;
		PlayerSettings.productName = currentSettings.ProductName;
		PlayerSettings.applicationIdentifier = currentSettings.ApplicationIdentifier;
		PlayerSettings.bundleVersion = currentSettings.BundleVersion;
		PlayerSettings.Android.bundleVersionCode = currentSettings.BundleVersionCode;
		PlayerSettings.Android.keystoreName = currentSettings.KeystoreName;
		PlayerSettings.keyaliasPass = currentSettings.KeyAliasPass;
		PlayerSettings.Android.keyaliasName = currentSettings.KeyAliasName;
		PlayerSettings.keystorePass = currentSettings.KeystorePass;
		PlayerSettings.Android.useAPKExpansionFiles = currentSettings.UseAPKExpansionFiles;
		PlayerSettings.Android.minSdkVersion = AndroidSdkVersions.AndroidApiLevel16;
		
		EditorUserBuildSettings.SwitchActiveBuildTarget(BuildTargetGroup.Android, BuildTarget.Android);
		EditorUserBuildSettings.androidBuildSystem = AndroidBuildSystem.Gradle;
		EditorUserBuildSettings.development = currentSettings.Development;
		EditorUserBuildSettings.allowDebugging = currentSettings.Development;
		EditorUserBuildSettings.connectProfiler = currentSettings.Development;
		string devText = "";
		var option = BuildOptions.None;
		if (currentSettings.Development)
		{
			devText = "_Dev";
			option = BuildOptions.AutoRunPlayer;
		}
		else
		{
			EditorUserBuildSettings.buildAppBundle = true;
		}

		var buildFile = "Builds/Maze_GP" + devText + "_" + DateTime.Now.ToString("yyyy_MM_dd_HHmmss") + ".apk";
		BuildPipeline.BuildPlayer(GetScenePaths(BuildTarget.Android), GetOutputPath(buildFile), BuildTarget.Android, option);
	}

	[MenuItem("AutoBuilder/Maze IOS")]
	private static void PerformIOSBuild()
	{
		IOSPlatformBuildSettings currentSettings = Resources.Load<IOSPlatformBuildSettings>("IOSPlatformBuildSettings");
		PlayerSettings.SetScriptingDefineSymbolsForGroup(BuildTargetGroup.iOS, "PLATFORM_IOS");

		PlayerSettings.companyName = currentSettings.CompanyName;
		PlayerSettings.productName = currentSettings.ProductName;
		PlayerSettings.applicationIdentifier = currentSettings.ApplicationIdentifier;
		PlayerSettings.bundleVersion = currentSettings.BundleVersion;

		EditorUserBuildSettings.SwitchActiveBuildTarget(BuildTargetGroup.iOS, BuildTarget.iOS);
		EditorUserBuildSettings.development = currentSettings.Development;
		EditorUserBuildSettings.allowDebugging = currentSettings.Development;
		EditorUserBuildSettings.connectProfiler = currentSettings.Development;

		PlayerSettings.iOS.scriptCallOptimization = ScriptCallOptimizationLevel.SlowAndSafe;
		PlayerSettings.SetApiCompatibilityLevel(BuildTargetGroup.iOS, ApiCompatibilityLevel.NET_2_0_Subset);
		PlayerSettings.iOS.targetOSVersionString = currentSettings.TargetOSVersionString;

		BuildPipeline.BuildPlayer(GetScenePaths(BuildTarget.iOS), "app_ios", BuildTarget.iOS, BuildOptions.None);

	}

	private static string[] GetScenePaths(BuildTarget platform)
	{
		var scenes = new string[EditorBuildSettings.scenes.Length];

		for (var i = 0; i < scenes.Length; i++)
			scenes[i] = EditorBuildSettings.scenes[i].path;

		return scenes;
	}

	private static string GetOutputPath(string defaultPath)
	{
		const string outputPathCommandArg = "-outputPath";
		var commandLineArgs = Environment.GetCommandLineArgs();
		var outputPathIndex = -1;
		for (var i = 0; i < commandLineArgs.Length; i++)
		{
			if (commandLineArgs[i].Equals(outputPathCommandArg))
			{
				outputPathIndex = i;
			}
		}
		var outputPath = defaultPath;
		if (outputPathIndex >= 0 && outputPathIndex < commandLineArgs.Length - 1)
		{
			outputPath = commandLineArgs[outputPathIndex + 1];
		}
		CheckFilePathForWriting(outputPath);
		return outputPath;
	}

	private static void CheckFilePathForWriting(string filePath)
	{
		if (!filePath.Contains("/"))
		{
			return;
		}
		filePath = filePath.Substring(0, filePath.LastIndexOf('/'));
		if (!Directory.Exists(filePath))
		{
			Directory.CreateDirectory(filePath);
		}
	}
}
