using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class NotificationManager
{
#if UNITY_ANDROID && !UNITY_EDITOR
	private static AndroidJavaObject playerActivityContext = null;
	private static List<int> currentNotificationIDs = new List<int>();
#endif
    //private static string PREF_KEY = "ELANPlayerPrefsKey";



    public static void sendParametrizedNotification(Notification notification)
    {
#if UNITY_ANDROID && !UNITY_EDITOR
		// Obtain unity context
        if(playerActivityContext == null) {
			AndroidJavaClass actClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        	playerActivityContext = actClass.GetStatic<AndroidJavaObject>("currentActivity");
		}
		
        // Set notification within java plugin
        AndroidJavaClass pluginClass = new AndroidJavaClass("com.CFM.ELAN.ELANManager");
		
        if (pluginClass != null) {
			//Debug.Log("FireNotification --> " + notification.fullClassName );
            pluginClass.CallStatic("FireNotification", playerActivityContext, notification.title,notification.message,notification.getDelay(),notification.ID, notification.fullClassName,notification.useSound,notification.soundName,notification.useVibration,notification.getRepetition(), notification.localType, notification.image);
			if(notification.getDelay() > 0) { // NEW forgot to add this to be able to cancel notification
				currentNotificationIDs.Add (notification.ID);
			}
       }
#endif
    }





    [System.Obsolete("sendParametrizedNotification (ELANNotification notification)", true)]
    public static int SendNotification(string title, string message, long delay)
    {
#if UNITY_ANDROID && !UNITY_EDITOR
		//if(currentNotificationIDs.Count == 0) currentNotificationIDs.AddRange(GetIntArray());
		// Obtain unity context
        if(playerActivityContext == null) {
			AndroidJavaClass actClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        	playerActivityContext = actClass.GetStatic<AndroidJavaObject>("currentActivity");
		}
		
        // Set notification within java plugin
        AndroidJavaClass pluginClass = new AndroidJavaClass("com.CFM.ELAN.ELANManager");
        int nID = (int)(Time.time*1000) + (int)Random.Range (0,int.MaxValue/2);
        if (pluginClass != null) {
			pluginClass.CallStatic("FireNotification", playerActivityContext, title,message,delay,nID);
			if(delay > 0) {
				currentNotificationIDs.Add (nID);
			}
        }
		return nID;
#endif
        return -1;
    }
    [System.Obsolete("sendParametrizedNotification (ELANNotification notification)", true)]
    public static void ScheduleRepeatingNotification(string title, string message, long delay, long rep)
    {
#if UNITY_ANDROID && !UNITY_EDITOR
		// Obtain unity context
        if(playerActivityContext == null) {
			AndroidJavaClass actClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        	playerActivityContext = actClass.GetStatic<AndroidJavaObject>("currentActivity");
		}
        // Set notification within java plugin
        AndroidJavaClass pluginClass = new AndroidJavaClass("com.CFM.ELAN.ELANManager");
        if (pluginClass != null) {
            pluginClass.CallStatic("ScheduleRepeatingNotification", playerActivityContext,title,message,delay,rep);
        }
#endif
    }

    public static void CancelRepeatingNotification()
    {
#if UNITY_ANDROID && !UNITY_EDITOR
		// Obtain unity context
        if(playerActivityContext == null) {
			AndroidJavaClass actClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        	playerActivityContext = actClass.GetStatic<AndroidJavaObject>("currentActivity");
		}
        // Set notification within java plugin
        AndroidJavaClass pluginClass = new AndroidJavaClass("com.CFM.ELAN.ELANManager");
        if (pluginClass != null) {
            pluginClass.CallStatic("CancelRepeatingNotification", playerActivityContext);
        }
#endif
    }

    public static void CancelAllNotifications()
    {
#if UNITY_ANDROID && !UNITY_EDITOR
		//currentNotificationIDs.AddRange(GetIntArray());
		if(currentNotificationIDs.Count == 0) return;
		List<int> copy = new List<int>();
		copy.AddRange (currentNotificationIDs);
		foreach(int nID in copy)
            CancelLocalNotification(nID);
		//SetIntArray(currentNotificationIDs.ToArray());
		CancelRepeatingNotification();
#endif
    }

    public static void CancelLocalNotification(int nID)
    {
#if UNITY_ANDROID && !UNITY_EDITOR
		// Obtain unity context
        if(playerActivityContext == null) {
			AndroidJavaClass actClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        	playerActivityContext = actClass.GetStatic<AndroidJavaObject>("currentActivity");
		}
        // Set notification within java plugin
        AndroidJavaClass pluginClass = new AndroidJavaClass("com.CFM.ELAN.ELANManager");
        if (pluginClass != null) {
			pluginClass.CallStatic("CancelNotification", playerActivityContext,nID);
			currentNotificationIDs.Remove (nID);
        }
#endif


    }
    // NEW not being called anywhere!
#if UNITY_ANDROID && !UNITY_EDITOR
	public static void addNotificationId(int nID){
		if ( currentNotificationIDs != null )currentNotificationIDs.Add(nID);
	}
#endif
}
