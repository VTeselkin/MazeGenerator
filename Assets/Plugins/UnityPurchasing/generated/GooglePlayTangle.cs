#if UNITY_ANDROID || UNITY_IPHONE || UNITY_STANDALONE_OSX || UNITY_TVOS
// WARNING: Do not modify! Generated file.

namespace UnityEngine.Purchasing.Security {
    public class GooglePlayTangle
    {
        private static byte[] data = System.Convert.FromBase64String("/AnuA0beo/LZOCJtlMfIrJhjmbCCVLsEcq1ItW3sFV5pXUNxRPrc/SPYN15Shxvm8y3kblSYC8KqpM+d1Wyd2b0xswrnUeJoD+UQhvEGOiH4PKa5HESEtnRQzHdFPpdNmYZ8rGCov1AsPpsC1ShQQamIbpJOCVkJRWT1VDAnCbltldvzUIfm3sYzaCq0v84IMaCvtMLnyl7mEJ3y0Mue14o/8/NgHvOcU9Yo3DQ04Dr/lAa9+sZUJ6zOEzYTIazsYl4cKk6wOBZa+MaLgX1WbN4t1dTpJqDlwRA86G+4X7alhkwe2fv61NN6pPmtSmuq2Wvoy9nk7+DDb6FvHuTo6Ojs6epr6Obp2Wvo4+tr6OjpS5KL6pd6RYsX2ZLu9Iu/euvq6Ono");
        private static int[] order = new int[] { 12,13,4,3,9,5,6,8,8,11,10,11,12,13,14 };
        private static int key = 233;

        public static readonly bool IsPopulated = true;

        public static byte[] Data() {
        	if (IsPopulated == false)
        		return null;
            return Obfuscator.DeObfuscate(data, order, key);
        }
    }
}
#endif
