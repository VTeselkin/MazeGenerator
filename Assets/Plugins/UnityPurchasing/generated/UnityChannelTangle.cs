#if UNITY_ANDROID || UNITY_IPHONE || UNITY_STANDALONE_OSX || UNITY_TVOS
// WARNING: Do not modify! Generated file.

namespace UnityEngine.Purchasing.Security {
    public class UnityChannelTangle
    {
        private static byte[] data = System.Convert.FromBase64String("TM/Bzv5Mz8TMTM/Pzmy1rM2wXWJIn3iRgqFrOf7c3fP0XYPeim1MjWJD0nMXAC6eSrL81HegwfnhFE8N/kzP7P7DyMfkSIZIOcPPz8/Lzs193+GsplpxS/kK8vPOAYfC5jcbz9suySRh+YTV/h8FSrPg74u/RL6XR4+YdwsZvCXyD3dmjq9JtWkufi6TmOkvFoeIk+XA7XnBN7rV9+y58AT/EHl1oDzB1ArDSXO/LOWNg+i63eFzAIvpNBE0BovLRXk7DWmXHzGtGNTURznUu3TxD/sTE8cd2LMhmqVznCNVim+SSssyeU56ZFZj3fva3xuBnjtjo5FTd+tQYhmwar6hW4vyS7r+mhaULcB2xU8owjeh1iEdBqww/rXJ06yYXczNz87P");
        private static int[] order = new int[] { 1,2,6,6,10,12,12,8,10,11,10,13,13,13,14 };
        private static int key = 206;

        public static readonly bool IsPopulated = true;

        public static byte[] Data() {
        	if (IsPopulated == false)
        		return null;
            return Obfuscator.DeObfuscate(data, order, key);
        }
    }
}
#endif
