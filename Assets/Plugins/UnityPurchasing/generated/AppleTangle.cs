#if UNITY_ANDROID || UNITY_IPHONE || UNITY_STANDALONE_OSX || UNITY_TVOS
// WARNING: Do not modify! Generated file.

namespace UnityEngine.Purchasing.Security {
    public class AppleTangle
    {
        private static byte[] data = System.Convert.FromBase64String("nMWElpaQiICWxYSGhoCVkYSLhoDtzuPk4ODi5+Tz+42RkZWW38rKkm78bDscrokQ4k7H1ecN/dsdtew21WfhXtVn5kZF5ufk5+fk59Xo4+ztu9Vn5PTj5rD4xeFn5O3VZ+Th1eHj9uewttT21fTj5rDh7/bvpJWVxYSLgcWGgJeRjIOMhoSRjIqLxZVNOZvH0C/AMDzqM44xR8HG9BJESbxC4OyZ8qWz9PuRNlJuxt6iRjCKlYmAxbeKipHFpqTV+/Lo1dPV0ddwe5/pQaJuvjHz0tYuIeqoK/GMNLeAiYyEi4aAxYqLxZGNjJbFhoCXVNW9Cb/h12mNVmr4O4CWGoK7gFnFioPFkY2AxZGNgIvFhJWViYyGhIHQxvCu8Lz4VnESE3l7KrVfJL214+aw+Ovh8+HxzjWMonGT7BsRjmjBBw40UpU66qAEwi8UiJ0IAlDy8s9jrWMS6OTk4ODl1YfU7tXs4+awn9Vn5JPV6+PmsPjq5OQa4eHm5+Tz1fHj5rDh5vbopJWViYDFt4qKkYuBxYaKi4GMkYyKi5bFioPFkJaA2MOCxW/WjxLoZyo7DkbKHLaPvoHK1WQm4+3O4+Tg4OLn59VkU/9kVk5GlHeitrAkSsqkVh0eBpUoA0apxaak1Wfkx9Xo4+zPY61jEujk5OSgm/qpjrVzpGwhkYfu9WakYtZvZJGNipeMkZzU89Xx4+aw4eb26KSVh4mAxZaRhIuBhJeBxZGAl4iWxYRl8c41jKJxk+wbEY5oy6VDEqKomtbTv9WH1O7V7OPmsOHj9uewttT203ypyJ1SCGl+ORaSfheTN5LVqiSJgMWsi4bL1MPVwePmsOHu9viklSz8lxC46zCaun4XwOZfsGqouOgUycWGgJeRjIOMhoSRgMWViomMhpziCZjcZm62xTbdIVRaf6rvjhrOGeDl5mfk6uXVZ+Tv52fk5OUBdEzsUv5YdqfB988i6vhTqHm7hi2uZfLo4+zPY61jEujk5ODg5eZn5OTluTzTmiRisDxCfFzXpx49MJR7m0S3w9XB4+aw4e72+KSVlYmAxaaAl5FqlmSFI/6+7Mp3Vx2hrRWF3XvwEPp0PvuitQ7gCLucYcgO00eyqbAJjIOMhoSRjIqLxaSQkY2Kl4yRnNRbEZZ+CzeB6i6cqtE9R9scnRqOLculQxKiqJrtu9X64+aw+Mbh/dXz0NfU0dXW07/y6NbQ1dfV3NfU0dWsPZN61vGARJJxLMjn5uTl5EZn5Op42BbOrM3/LRsrUFzrPLv5My7Y49Xq4+aw+Pbk5Brh4NXm5OQa1fglhtaSEt/iybMOP+rE6z9flvyqUGfk5ePsz2OtYxKGgeDk1WQX1c/jmqRNfRw0L4N5wY70NUZeAf7PJvqRjIOMhoSRgMWHnMWEi5zFlYSXkYJq7VHFEi5JycWKlVPa5NVpUqYqlYmAxaaAl5GMg4yGhJGMiovFpJBQ30gR6uvld+5UxPPLkTDZ6D6H85KSy4SVlYmAy4aKiMqElZWJgIaE1fTj5rDh7/bvpJWViYDFrIuGy9T6YGZg/nzYotIXTH6la8kxVHX3PZeEhpGMhoDFlpGEkYCIgIuRlsvVtU9vMD8BGTXs4tJVkJDE");
        private static int[] order = new int[] { 40,35,16,40,33,16,45,54,52,45,18,38,26,41,20,30,53,35,28,26,43,56,37,24,41,25,53,42,32,52,48,40,53,44,51,43,59,47,58,53,55,48,58,50,55,54,58,47,52,57,51,51,53,56,55,56,59,59,59,59,60 };
        private static int key = 229;

        public static readonly bool IsPopulated = true;

        public static byte[] Data() {
        	if (IsPopulated == false)
        		return null;
            return Obfuscator.DeObfuscate(data, order, key);
        }
    }
}
#endif
