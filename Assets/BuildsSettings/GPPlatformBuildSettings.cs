﻿using UnityEngine;

public class GPPlatformBuildSettings : ScriptableObject
{

	public string CompanyName;
	public string ProductName;
	public string ApplicationIdentifier;
	public string BundleVersion;
	public int BundleVersionCode;
	public bool Development;
	public string KeystoreName;
	public string KeyAliasPass;
	public string KeyAliasName;
	public string KeystorePass;
	public bool UseAPKExpansionFiles;
}
