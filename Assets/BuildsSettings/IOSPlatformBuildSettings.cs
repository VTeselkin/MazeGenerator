﻿using UnityEngine;

public class IOSPlatformBuildSettings : ScriptableObject
{

	public string CompanyName;
	public string ProductName;
	public string ApplicationIdentifier;
	public string BundleVersion;
	public int BundleVersionCode;
	public bool Development;
	public string TargetOSVersionString;

}

