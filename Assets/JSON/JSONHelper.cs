﻿using System;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine;
using UnityEngine.Networking.NetworkSystem;

internal static class JSONHelper
{

    public static JSONObject ConvertToJSON(List<string> collection)
    {
        var result = new JSONObject(JSONObject.Type.ARRAY);
        foreach (var value in collection)
        {
            result.Add(value);
        }

        return result;
    }

    public static List<string> ConvertToStringList(JSONObject jsonData)
    {
        var result = new List<string>();

        foreach (var jsonObject in jsonData.list)
        {
            result.Add(jsonObject.str);
        }

        return result;
    }

    public static JSONObject ConvertToJSON(Dictionary<string, bool> collection)
    {
        var result = new JSONObject(JSONObject.Type.OBJECT);
        foreach (var kvp in collection)
        {
            result.AddField(kvp.Key, kvp.Value);
        }

        return result;
    }

    public static Dictionary<string, bool> ConvertToBoolDictionary(JSONObject jsonData)
    {
        var result = new Dictionary<string, bool>();

        if (!jsonData.IsNull)
        {
            foreach (var key in jsonData.keys)
            {
                result.Add(key, jsonData[key].b);
            }
        }

        return result;
    }

    public static JSONObject ConvertToJSON(Dictionary<string, int> collection)
    {
        var result = new JSONObject(JSONObject.Type.OBJECT);
        foreach (var kvp in collection)
        {
            result.AddField(kvp.Key, kvp.Value.ToString());
        }

        return result;
    }

    public static JSONObject ConvertToJSON(Dictionary<string, object> collection)
    {
        var result = new JSONObject(JSONObject.Type.OBJECT);
        foreach (var kvp in collection)
        {
            result.AddField(kvp.Key, kvp.Value.ToString());
        }

        return result;
    }


    public static JSONObject ConvertToJSON(Dictionary<int, int> collection)
    {
        var result = new JSONObject(JSONObject.Type.OBJECT);
        foreach (var kvp in collection)
        {
            result.AddField(kvp.Key.ToString(), kvp.Value.ToString());
        }

        return result;
    }

    public static Dictionary<int, int> ConvertToIntDictionary(JSONObject jsonData)
    {
        var result = new Dictionary<int, int>();

        if (!jsonData.IsNull)
        {
            foreach (var key in jsonData.keys)
            {
                result.Add(int.Parse(key), int.Parse(jsonData[key].str));
            }
        }

        return result;
    }


    public static Dictionary<string, string> ConvertToStrDictionary(JSONObject jsonData)
    {
        var result = new Dictionary<string, string>();

        if (!jsonData.IsNull)
        {
            foreach (var key in jsonData.keys)
            {
                result.Add(key, jsonData[key].str);
            }
        }

        return result;
    }
    public static JSONObject ConvertToJSON(Dictionary<string, DateTime> collection)
    {
        var result = new JSONObject(JSONObject.Type.OBJECT);
        foreach (var kvp in collection)
        {
            result.AddField(kvp.Key, kvp.Value.ToString(NumberFormatInfo.InvariantInfo));
        }

        return result;
    }

    public static JSONObject ConvertToJSON(Dictionary<int, DateTime> collection)
    {
        Dictionary<string, string> convertedDictionary = new Dictionary<string, string>();
        foreach (KeyValuePair<int, DateTime> pair in collection)
            convertedDictionary.Add(pair.Key.ToString(), pair.Value.ToString(NumberFormatInfo.InvariantInfo));

        return new JSONObject(convertedDictionary);
    }

    public static Dictionary<string, DateTime> ConvertToStringDateTimeDictionary(JSONObject jsonData)
    {
        var result = new Dictionary<string, DateTime>();

        if (!jsonData.IsNull)
        {
            foreach (var key in jsonData.keys)
            {
                result.Add(key, DateTime.Parse(jsonData[key].str));
            }
        }

        return result;
    }

    public static Dictionary<int, DateTime> ConvertToIntDateTimeDictionary(JSONObject jsonData)
    {
        var result = new Dictionary<int, DateTime>();
        foreach (var key in jsonData.keys)
        {
            result.Add(int.Parse(key), DateTime.Parse(jsonData[key].str));
        }

        return result;
    }

    public static JSONObject ConvertToJSON(List<int> collection)
    {
        var result = new JSONObject(JSONObject.Type.ARRAY);
        foreach (var value in collection)
        {
            result.Add(value);
        }

        return result;
    }

    public static List<int> ConvertToIntList(JSONObject jsonData)
    {
        var result = new List<int>();

        foreach (var jsonObject in jsonData.list)
        {
            result.Add((int)jsonObject.f);
        }

        return result;
    }
    public static JSONObject ConvertToJSON(List<byte> collection)
    {
        var result = new JSONObject(JSONObject.Type.ARRAY);
        foreach (var value in collection)
        {
            result.Add(value);
        }

        return result;
    }

    public static List<byte> ConvertToByteList(JSONObject jsonData)
    {
        var result = new List<byte>();

        foreach (var jsonObject in jsonData.list)
        {
            result.Add((byte)jsonObject.f);
        }

        return result;
    }
    public static JSONObject ConvertToJSON(Vector3 vector)
    {
        var result = new JSONObject();
        result.AddField("x", vector.x);
        result.AddField("y", vector.y);
        result.AddField("z", vector.z);
        return result;
    }
    public static Vector3 ConvertToVector3(JSONObject jsonData)
    {
        Vector3 result = Vector3.zero;
        result.x = jsonData["x"].f;
        result.y = jsonData["y"].f;
        result.z = jsonData["z"].f;
        return result;
    }

    public static JSONObject ConvertToJSON(Quaternion quaternion)
    {
        var result = new JSONObject();
        result.AddField("x", quaternion.x);
        result.AddField("y", quaternion.y);
        result.AddField("z", quaternion.z);
        result.AddField("w", quaternion.w);
        return result;
    }
    public static Quaternion ConvertToQuaternion(JSONObject jsonData)
    {
        Quaternion result = new Quaternion();
        result.x = jsonData["x"].f;
        result.y = jsonData["y"].f;
        result.z = jsonData["z"].f;
        result.w = jsonData["w"].f;
        return result;
    }
}
