using System;
using Assets.Scripts.Helpers;
using Assets.Scripts.Settings;
using UnityEngine;
using Assets.Scripts.Server;
using Assets.Scripts.Shared;
using System.IO;
using Facebook.Unity;
using Assets.Scripts.GUI.Managers;

public class UserStorage : DataStorageProvider
{
    #region Properties

    public static readonly int MaxIndex = 5;



    public int EnergyBoost { get; private set; }
    public int SpeedBoost { get; private set; }
    public int DistanceBoost { get; private set; }
    public int MaxOpenedLevel { get; private set; }
    public int Coins { get; private set; }


    public int CurrentSelectLevel { get; private set; }

    public int Stars
    {
        get
        {
            int stars = 0;
            foreach (var lvlProgress in LevelProgress.Instance.LevelsProgress)
            {
                stars += lvlProgress.Star;
            }
            return stars;
        }
    }

    #endregion
    public override void Initialize()
    {
        PurchaseManager.Instance.Purchase += PurchaseComplete;

        InternetReachabilityController.Instance.SubscribeToInternetStatusChange((obj) =>
        {
            if (!AppSettings.Instance.IsNeedSynchronize && InternetReachabilityController.Instance.IsInternetAvalible)
            {
                SendUserDataToServer();
                Save();
            }
        });
        AppSettings.Instance.NoADSChanged += () =>
        {
            GameSparkManager.Instance.PostUserProgress(GameSparkManager.UserProgressType.NoADS, AppSettings.Instance.NoADS ? 1 : 0, null, null);
        };

        AppSettings.Instance.NoBannerChanged += () =>
        {
            GameSparkManager.Instance.PostUserProgress(GameSparkManager.UserProgressType.NoBaner, AppSettings.Instance.NoBanner ? 1 : 0, null, null);
        };

        base.Initialize();

    }
    public void FacebookConnectRequest(bool isHandleLogin)
    {
        if (isHandleLogin)
        {
            if (FaceBookManager.Instance.IsLogin())
            {
                UIManager.Instance.CreateNewUIElement<PopupSync>(Assets.Scripts.GUI.UIElementType.PopupSync, UIManager.Instance.GetTopPopup() != null);
            }
        }
        else
        {
            SendUserDataToServer();
            Save();
        }
    }



    private void PurchaseComplete(bool res, PurchaseManager.PurchaseType type)
    {
        if (res)
        {
            switch (type)
            {
                case PurchaseManager.PurchaseType.CoinsMini:
                    IncCoins(GameStorage.Instance.Data.CountCoinsMini);
                    break;
                case PurchaseManager.PurchaseType.CoinsMedium:
                    IncCoins(GameStorage.Instance.Data.CountCoinsMedium);
                    break;
                case PurchaseManager.PurchaseType.CoinsHard:
                    IncCoins(GameStorage.Instance.Data.CountCoinsHard);
                    break;
                case PurchaseManager.PurchaseType.NoAds:
                    AppSettings.Instance.NoBanner = true;
                    break;
            }
        }
    }
    //TODO !!!
    public void SendUserDataToServer()
    {
        AppSettings.Instance.IsNeedSynchronize = true;
        GameSparkManager.Instance.PostUserProgress(GameSparkManager.UserProgressType.EnergyBoost, EnergyBoost, (res0) =>
        {
            GameSparkManager.Instance.PostUserProgress(GameSparkManager.UserProgressType.SpeedBoost, SpeedBoost, (res1) =>
            {
                GameSparkManager.Instance.PostUserProgress(GameSparkManager.UserProgressType.DistanceBoost, DistanceBoost, (res2) =>
                {
                    GameSparkManager.Instance.PostUserProgress(GameSparkManager.UserProgressType.Coins, Coins, (res3) =>
                    {
                        GameSparkManager.Instance.PostUserProgress(GameSparkManager.UserProgressType.NoBaner, AppSettings.Instance.NoBanner ? 1 : 0, (res4) =>
                        {
                            GameSparkManager.Instance.PostUserProgress(GameSparkManager.UserProgressType.NoADS, AppSettings.Instance.NoADS ? 1 : 0, null, null);
                        }, null);
                    }, null);

                }, null);
            }, null);

        }, null);
    }

    #region Change UserData

    public bool IncEnergyBoost()
    {
        if (Coins >= (EnergyBoost + 1) * GameStorage.Instance.Data.CostIndexByEnergy && EnergyBoost < MaxIndex)
        {
            Coins = Coins - (EnergyBoost + 1) * GameStorage.Instance.Data.CostIndexByEnergy;
            EnergyBoost++;
            if (EnergyBoost > MaxIndex) EnergyBoost = MaxIndex;
            OnChangeUserData(TypeBoostStore.EnergyBoost);
            GameSparkManager.Instance.PostUserProgress(GameSparkManager.UserProgressType.EnergyBoost, EnergyBoost, DebugLogger.Log, DebugLogger.LogError);
            GameSparkManager.Instance.PostUserProgress(GameSparkManager.UserProgressType.Coins, Coins, DebugLogger.Log, DebugLogger.LogError);
            AudioManager.Instance.PlaySoundUi(SoundConstants.UIAudioClipTypes.Pop_up_Small_OK);
            Save();
            return true;
        }
        AudioManager.Instance.PlaySoundUi(SoundConstants.UIAudioClipTypes.Pop_up_Small_Error);
        return false;
    }

    public bool IncSpeedBoost()
    {
        if (Coins >= (SpeedBoost + 1) * GameStorage.Instance.Data.CostIndexBySpeed && SpeedBoost < MaxIndex)
        {
            Coins = Coins - (SpeedBoost + 1) * GameStorage.Instance.Data.CostIndexBySpeed;
            SpeedBoost++;
            if (SpeedBoost > MaxIndex) SpeedBoost = MaxIndex;
            OnChangeUserData(TypeBoostStore.SpeedBoost);
            GameSparkManager.Instance.PostUserProgress(GameSparkManager.UserProgressType.SpeedBoost, SpeedBoost, DebugLogger.Log, DebugLogger.LogError);
            GameSparkManager.Instance.PostUserProgress(GameSparkManager.UserProgressType.Coins, Coins, DebugLogger.Log, DebugLogger.LogError);
            AudioManager.Instance.PlaySoundUi(SoundConstants.UIAudioClipTypes.Pop_up_Small_OK);
            Save();
            return true;
        }
        AudioManager.Instance.PlaySoundUi(SoundConstants.UIAudioClipTypes.Pop_up_Small_Error);
        return false;
    }

    public bool IncDistanceBoost()
    {
        if (Coins >= (DistanceBoost + 1) * GameStorage.Instance.Data.CostIndexByDistance && DistanceBoost < MaxIndex)
        {
            Coins = Coins - (DistanceBoost + 1) * GameStorage.Instance.Data.CostIndexByDistance;
            DistanceBoost++;
            if (DistanceBoost > MaxIndex) DistanceBoost = MaxIndex;
            OnChangeUserData(TypeBoostStore.DistanceBoost);
            GameSparkManager.Instance.PostUserProgress(GameSparkManager.UserProgressType.DistanceBoost, DistanceBoost, DebugLogger.Log, DebugLogger.LogError);
            GameSparkManager.Instance.PostUserProgress(GameSparkManager.UserProgressType.Coins, Coins, DebugLogger.Log, DebugLogger.LogError);
            AudioManager.Instance.PlaySoundUi(SoundConstants.UIAudioClipTypes.Pop_up_Small_OK);
            Save();
            return true;
        }
        AudioManager.Instance.PlaySoundUi(SoundConstants.UIAudioClipTypes.Pop_up_Small_Error);
        return false;
    }

    public void IncCoins(int value)
    {
        DebugLogger.Log("IncCoins = " + value);
        Coins += value;
        OnChangeUserData(TypeBoostStore.Coins);
        GameSparkManager.Instance.PostUserProgress(GameSparkManager.UserProgressType.Coins, Coins, DebugLogger.Log, DebugLogger.LogError);
        GameSparkManager.Instance.PostUserScore(Coins, DebugLogger.Log, DebugLogger.LogError);
        Save();
    }

    public bool BuyByCoins(int coins)
    {
        if (Coins >= coins)
        {
            Coins -= coins;
            Save();
            GameSparkManager.Instance.PostUserProgress(GameSparkManager.UserProgressType.Coins, Coins, DebugLogger.Log, DebugLogger.LogError);
            AudioManager.Instance.PlaySoundUi(SoundConstants.UIAudioClipTypes.Pop_up_Small_OK);
            return true;
        }
        AudioManager.Instance.PlaySoundUi(SoundConstants.UIAudioClipTypes.Pop_up_Small_Error);
        return false;
    }

    public void SetCurrentLevel(int currentLevel)
    {

        if (GameBuildConfig.IsDevelopBuild || (LevelStorage.Instance.LevelCount > currentLevel && currentLevel <= MaxOpenedLevel && currentLevel != CurrentSelectLevel))
        {
            CurrentSelectLevel = currentLevel;
            OnChangeCurrentLevel();
        }
    }

    public void SetMaxOpenedLevel(int currentLevel)
    {
        if (LevelStorage.Instance.LevelCount > currentLevel)
        {
            MaxOpenedLevel = currentLevel;
            OnChangeUserProgress();
            Save();
        }
    }

    public void ChangeCurrentLevelInc()
    {
        if (CurrentSelectLevel < MaxOpenedLevel)
        {
            CurrentSelectLevel++;
            OnChangeCurrentLevel();
        }
    }

    public void ChangeCurrentLevelDec()
    {
        if (CurrentSelectLevel > 0)
        {
            CurrentSelectLevel--;
            OnChangeCurrentLevel();
        }
    }

    #endregion

    #region Singleton members

    private static readonly object _lockObject = new object();

    private static UserStorage _instance;

    public static UserStorage Instance
    {
        get
        {
            if (_instance == null)
            {
                lock (_lockObject)
                {
                    if (_instance == null)
                    {
                        var gameObject = new GameObject("Singleton: " + (typeof(UserStorage)));
                        _instance = gameObject.AddComponent<UserStorage>();
                    }
                }
            }

            return _instance;
        }
    }

    private void Awake()
    {
        if (_instance)
        {
            DestroyImmediate(gameObject);
        }
        else
        {
            _instance = this.GetComponent<UserStorage>();
            DontDestroyOnLoad(gameObject);
        }
        LateAwake();
    }

    private void LateAwake()
    {

    }


    #endregion

    #region Loading and Saving

    protected override string StorageFilePath
    {
        get
        {
            return Helper.StorageFilePath("UserData.dat");
        }
    }

    protected override JSONObject GetState()
    {
        var data = new JSONObject(JSONObject.Type.OBJECT);
        data.AddField(JSONUserUpdaterKey.JSONUserCoins, Coins);
        data.AddField(JSONUserUpdaterKey.JSONUserEnergyBoost, EnergyBoost);
        data.AddField(JSONUserUpdaterKey.JSONUserSpeedBoost, SpeedBoost);
        data.AddField(JSONUserUpdaterKey.JSONUserDistanceBoost, DistanceBoost);
        data.AddField(JSONUserUpdaterKey.JSONUserNoADS, AppSettings.Instance.NoADS);
        data.AddField(JSONUserUpdaterKey.JSONUserNoBanner, AppSettings.Instance.NoBanner);
        return data;
    }

    protected override void RestoreState(JSONObject data)
    {
        if (data != null)
        {
            if (data[JSONUserUpdaterKey.JSONUserCoins] != null)
            {
                Coins = (int)data[JSONUserUpdaterKey.JSONUserCoins].i;
            }
            if (data[JSONUserUpdaterKey.JSONUserEnergyBoost] != null)
            {
                EnergyBoost = (int)data[JSONUserUpdaterKey.JSONUserEnergyBoost].i;
            }
            if (data[JSONUserUpdaterKey.JSONUserSpeedBoost] != null)
            {
                SpeedBoost = (int)data[JSONUserUpdaterKey.JSONUserSpeedBoost].i;
            }
            if (data[JSONUserUpdaterKey.JSONUserDistanceBoost] != null)
            {
                DistanceBoost = (int)data[JSONUserUpdaterKey.JSONUserDistanceBoost].i;
            }

        }

    }

    protected override void RestoreState()
    {
        if (AppSettings.Instance.IsFirstLaunch || !File.Exists(StorageFilePath))
        {
            Save();
        }
        if (File.Exists(StorageFilePath))
        {
            var decriptedData = File.ReadAllText(StorageFilePath);
            decriptedData = CryptoScript.Crypt(File.ReadAllText(StorageFilePath));
            var data = new JSONObject(decriptedData);
            try
            {
                RestoreState(data);
            }
            catch (Exception e)
            {
                DebugLogger.Log(e);
            }
        }
        else
        {
            DebugLogger.LogError("DataStorageProvider file doesn't exit = " + StorageFilePath);
        }
        if (InternetReachabilityController.Instance.IsInternetAvalible)
        {
            if (!AppSettings.Instance.IsNeedSynchronize)
            {
                AppSettings.Instance.IsNeedSynchronize = true;
                SendUserDataToServer();
                OnCompleteLoad(true);
            }
            else
            {
                GameSparkManager.Instance.GetUserProgress(RestoreFromServer, ErrorLoad);
                return;
            }
        }
        else
        {
            OnCompleteLoad(false);
            return;
        }
    }

    public void RestoreFromServer(string jSONString)
    {
        DebugLogger.Log("UserStorage Load = " + jSONString);
        JSONObject json = new JSONObject(jSONString);
        if (json["scriptData"] != null)
        {
            JSONObject scriptData = json["scriptData"];
            if (scriptData["data"] != null)
            {
                JSONObject data = scriptData["data"];
                int tempVal = 0;
                if (data != null)
                {
                    if (data[JSONUserUpdaterKey.JSONUserEnergyBoost] != null)
                    {
                        tempVal = (int)data[JSONUserUpdaterKey.JSONUserEnergyBoost].i;
                        EnergyBoost = tempVal;
                    }

                    if (data[JSONUserUpdaterKey.JSONUserSpeedBoost] != null)
                    {
                        tempVal = (int)data[JSONUserUpdaterKey.JSONUserSpeedBoost].i;
                        SpeedBoost = tempVal;
                    }

                    if (data[JSONUserUpdaterKey.JSONUserDistanceBoost] != null)
                    {
                        tempVal = (int)data[JSONUserUpdaterKey.JSONUserDistanceBoost].i;
                        DistanceBoost = tempVal;
                    }

                    if (data[JSONUserUpdaterKey.JSONUserCoins] != null)
                    {
                        tempVal = (int)data[JSONUserUpdaterKey.JSONUserCoins].i;
                        Coins = tempVal;
                    }

                    if (data[JSONUserUpdaterKey.JSONUserNoADS] != null)
                    {
                        bool noAds = (int)data[JSONUserUpdaterKey.JSONUserNoADS].i == 1;
                        DebugLogger.Log("UserStorage noAds = " + noAds);
                        if (noAds)
                        {
                            AppSettings.Instance.NoADS = noAds;
                        }
                    }

                    if (data[JSONUserUpdaterKey.JSONUserNoBanner] != null)
                    {
                        bool noBanner = (int)data[JSONUserUpdaterKey.JSONUserNoBanner].i == 1;
                        DebugLogger.Log("UserStorage noBanner = " + noBanner);
                        if (noBanner)
                        {
                            AppSettings.Instance.NoBanner = noBanner;
                        }
                    }
                    Save();

                }
            }
        }
        OnCompleteLoad(json != null);
    }

    private void ErrorLoad(string error)
    {
        OnCompleteLoad(false);
        DebugLogger.LogError(error);
    }
    #endregion

    #region Events

    public event Action<TypeBoostStore> ChangeUserData;

    private void OnChangeUserData(TypeBoostStore type)
    {
        var handler = ChangeUserData;
        if (handler != null) handler(type);
    }
    public event Action ChangeUserProgress;

    private void OnChangeUserProgress()
    {
        var handler = ChangeUserProgress;
        if (handler != null) handler();
    }

    public event Action ChangeCurrentLevel;

    private void OnChangeCurrentLevel()
    {
        var handler = ChangeCurrentLevel;
        if (handler != null) handler();
    }
    #endregion



}
