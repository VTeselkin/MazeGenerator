public static class JSONUserUpdaterKey
{
	public const string JSONUserCoins = "UserCoins";
	public const string JSONUserEnergyBoost = "UserEnergyBoost";
	public const string JSONUserSpeedBoost = "UserSpeedBoost";
	public const string JSONUserDistanceBoost = "UserDistanceBoost";
	public const string JSONUserScore = "Score";
	public const string JSONUserMaxOpenedLevel = "MaxOpenedLevel";
	public const string JSONUserNoADS = "UserNoADS";
	public const string JSONUserNoBanner = "UserNoBanner";
}
