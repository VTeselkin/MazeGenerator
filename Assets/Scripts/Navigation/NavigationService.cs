﻿using System;
using System.Collections;
using AssemblyCSharp.Scripts.MainManager;
using Assets.Scripts.Navigation.Scenes;
using UnityEngine;
using UnityEngine.SceneManagement;
using Assets.Scripts.Shared;

namespace Assets.Scripts.Navigation
{
	internal static class NavigationService
	{
		private const float AsyncLoadingAccuracy = 0.9f;
		// private static PopupLoadingBar _popupLoading;
		private static BaseScene _currentScene;
		private static NavigationArgs _navigationArgs;
		private static AsyncOperation _asyncOperation;

		/// <summary>
		///     The asynchronous loading complete
		/// </summary>
		public static Action AsyncLoadingComplete;

		/// <summary>
		///     Gets the type of the current scene.
		/// </summary>
		/// <value>The type of the current scene.</value>
		public static SceneType CurrentSceneType
		{
			get
			{
				if (_currentScene == null)
				{
					return SceneType.Undefined;
				}
				return _currentScene.SceneType;
			}
		}

		/// <summary>
		///     Navigates the specified navigator.
		/// </summary>
		/// <param name="navigator">The navigator.</param>
		/// <param name="allowSceneActivation">if set to <c>true</c> [allow scene activation].</param>
		public static void Navigate(NavigationArgs navigator, bool allowSceneActivation = true)
		{
			if (_currentScene != null)
			{
				//  UIManager.Instance.CreateNewUIElement<PopupBase>(UIElementType.LoadingBar);
				_currentScene.OnNavigatedFrom();
				//TODO Change Audio manager
				//  AudioManager.Instance.Unload();
			}

			_navigationArgs = navigator;

			if (_currentScene != null)
			{
				LoadLevelAsynchronously(navigator.SceneName, allowSceneActivation);
			}
			else
			{
				SceneManager.LoadScene(navigator.SceneName);
				//not used for native UI
				//var popupLoading = Object.FindObjectOfType<PopupLoading>();
				//if (popupLoading != null)
				//{
				//    popupLoading.StartCoroutine(LoadLevelAsync(navigator.SceneName, allowSceneActivation));
				//}
				//else
				//{
				//    Application.LoadLevel(navigator.SceneName);
				//}
			}
		}

		/// <summary>
		///     Loads the level asynchronously.
		/// </summary>
		/// <param name="name">The name.</param>
		/// <param name="allowSceneActivation">if set to <c>true</c> [allow scene activation].</param>
		public static void LoadLevelAsynchronously(string name, bool allowSceneActivation)
		{
			MonoHelper.Instance.StartCoroutine(LoadLevelAsync(name, allowSceneActivation));
			//_popupLoading = (PopupLoadingBar) UIManager.Instance.GetLoadingPopup();
			//if (_popupLoading)
			//{
			//    _popupLoading.StartCoroutine(LoadLevelAsync(name, allowSceneActivation));
			//    _popupLoading.SetProgressVisible(true);
			//}
		}

		private static void OnAsyncLoadingComplete()
		{
			if (AsyncLoadingComplete != null)
			{
				_currentScene.OnNavigatedTo(_navigationArgs);
				AsyncLoadingComplete();
			}
		}

		private static IEnumerator LoadLevelAsync(string name, bool allowSceneActivation)
		{
			_asyncOperation = SceneManager.LoadSceneAsync(name);
			_asyncOperation.allowSceneActivation = allowSceneActivation;
			while (!_asyncOperation.isDone)
			{
				//if (_popupLoading != null)
				//{
				//    var currentProgress = Mathf.FloorToInt(_asyncOperation.progress*100.0f);
				//    _popupLoading.SetProgress(currentProgress);
				//}
				if (_asyncOperation.progress >= AsyncLoadingAccuracy)
				{
					OnAsyncLoadingComplete();
				}
				yield return null;
			}
			yield return _asyncOperation;
		}

		/// <summary>
		///     Sets the current scene.
		/// </summary>
		/// <param name="scene">The scene.</param>
		public static void SetCurrentScene(BaseScene scene)
		{
			// UIManager.Instance.ClearUIElementCollection();
			DebugLogger.Log("Load = " + scene.SceneType.ToString() + " scene");
			_currentScene = scene;
			_currentScene.OnNavigatedTo(_navigationArgs);
		}
	}
}