﻿using Assets.Scripts.Navigation.Scenes;

public class GameNavigationArgs : NavigationArgs
{
    public int LevelNumber { get; private set; }
    public bool ShowAds { get; private set; }
    private const string MapSceneName = "GameScene";

    /// <summary>
    ///     Initializes a new instance of the <see cref="GameNavigationArgs" /> class.
    /// </summary>
    public GameNavigationArgs(int levelNumber) : base(SceneType.Game, MapSceneName)
    {
        LevelNumber = levelNumber;
        ShowAds = false;
    }
    /// <summary>
    /// Initializes a new instance of the <see cref="T:GameNavigationArgs"/> class.
    /// </summary>
    /// <param name="levelNumber">Level number.</param>
    /// <param name="showAds">If set to <c>true</c> show ads.</param>
    public GameNavigationArgs(int levelNumber, bool showAds) : base(SceneType.Game, MapSceneName)
    {
        LevelNumber = levelNumber;
        ShowAds = showAds;
    }

}
