﻿using Assets.Scripts.Navigation.Scenes;
using Assets.Scripts.Navigation;
using Assets.Scripts.GUI.Managers;
using Assets.Scripts.GUI;
using Assets.Scripts.Shared;
using Managers;

internal class GameScene : BaseScene
{
    /// <summary>
    /// Gets the type of the scene.
    /// </summary>
    /// <value>The type of the scene.</value>
    public override SceneType SceneType
    {
        get
        {
            return SceneType.Game;
        }
    }

    /// <summary>
    /// Ons the navigated to.
    /// </summary>
    /// <param name="args">Arguments.</param>
    public override void OnNavigatedTo(NavigationArgs args)
    {
        AudioManager.Instance.Unload();
        BackButtonManager.Instance.BackButtonAction += BackButtonActions;
        var gameNavigationArgs = args as GameNavigationArgs;
        AnalyticsWrapper.Instance.StartScene(AnalyticsHelper.SceneKey.StartGameScene);
        GameManager<BaseGameObject>.InitInstance(gameNavigationArgs.LevelNumber);
        var levelData = GameManager<BaseGameObject>.Instance.LevelData;
        if (!levelData.IsNeedShowTutorial || UserStorage.Instance.MaxOpenedLevel >= levelData.LevelNumber)
        {
            GameManager<BaseGameObject>.Instance.StartToPlay();
        }
        else
        {
            var popup = UIManager.Instance.CreateNewUIElement<PopupTutorial>(UIElementType.Tutorial);
            popup.Init(levelData.TutorialIndex, () => { GameManager<BaseGameObject>.Instance.StartToPlay(); });
        }
        SetAdsConfigurator();
    }

    /// <summary>
    /// Ons the navigated from.
    /// </summary>
    public override void OnNavigatedFrom()
    {
        GameManager<BaseGameObject>.Instance.OnDestroy();
        BackButtonManager.Instance.BackButtonAction -= BackButtonActions;
    }

    /// <summary>
    /// Backs the button actions.
    /// </summary>
    public override void BackButtonActions()
    {
        if (!UIManager.Instance.IsOpenedPopups)
        {
            AnalyticsWrapper.Instance.EventPressBtn(AnalyticsHelper.EventKey.PressBtnPauseMenu);
            UIManager.Instance.CreateNewUIElement<PopupPause>(UIElementType.Pause);
        }
    }

    private void SetAdsConfigurator()
    {
        AdsManager.Instance.ShowBanner();
    }
}
