﻿namespace Assets.Scripts.Navigation.Scenes
{
    /// <summary>
    ///     Class NavigationArgs.
    /// </summary>
    public abstract class NavigationArgs
    {
        /// <summary>
        ///     Stores the scene name.
        /// </summary>
        public readonly string SceneName;

        /// <summary>
        ///     Stores the scene type.
        /// </summary>
        public readonly SceneType SceneType;

        protected NavigationArgs(SceneType sceneType, string sceneName)
        {
            SceneType = sceneType;
            SceneName = sceneName;
        }
       
    }
}