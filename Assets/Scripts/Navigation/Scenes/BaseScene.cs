﻿using UnityEngine;

namespace Assets.Scripts.Navigation.Scenes
{
	internal abstract class BaseScene : MonoBehaviour
	{
		/// <summary>
		///     Gets the type of the scene.
		/// </summary>
		/// <value>The type of the scene.</value>
		public abstract SceneType SceneType { get; }

		/// <summary>
		///     Backs the button actions.
		/// </summary>
		public abstract void BackButtonActions();

		/// <summary>
		///     Starts this instance.
		/// </summary>
		public virtual void Start()
		{
			AudioManager.Instance.PauseBackgroundMusic(false);
			AudioManager.Instance.ChangeScene();
			NavigationService.SetCurrentScene(this);
			BackButtonManager.Instance.SetPause(false);
			BackButtonManager.Instance.BackButtonAction += BackButtonActions;
		}

		/// <summary>
		///     Called when [navigated to].
		/// </summary>
		/// <param name="args">The arguments.</param>
		public abstract void OnNavigatedTo(NavigationArgs args);

		/// <summary>
		///     Called when [navigated from].
		/// </summary>
		public abstract void OnNavigatedFrom();
	}
}