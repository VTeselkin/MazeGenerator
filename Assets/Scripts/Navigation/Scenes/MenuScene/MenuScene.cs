﻿using System;
using System.Collections;
using Assets.Scripts.GUI;
using Assets.Scripts.GUI.Managers;
using Assets.Scripts.Shared;
using Managers;
using UnityEngine;

namespace Assets.Scripts.Navigation.Scenes.Map
{
    internal class MenuScene : BaseScene
    {
        /// <summary>
        ///     Gets the type of the scene.
        /// </summary>
        /// <value>The type of the scene.</value>
        public override SceneType SceneType
        {
            get { return SceneType.Menu; }
        }

        /// <summary>
        /// Called when [navigated to].
        /// </summary>
        /// <param name="args">The arguments.</param>
        public override void OnNavigatedTo(NavigationArgs args)
        {
            BackButtonManager.Instance.BackButtonAction += BackButtonActions;
            var menuNavigationArgs = args as MenuNavigationArgs;
            AnalyticsWrapper.Instance.StartScene(AnalyticsHelper.SceneKey.StartMenuScene);
            SetAdsConfigurator();
            float delay = 0.5f;
            if (menuNavigationArgs.StartNextLevel)
            {
                StartCoroutine(OpenPopupLoadOut(delay));
            }
            else
            {
                RateItController.Instance.ShowPopupIfNeeded();
            }
        }

        private IEnumerator OpenPopupLoadOut(float delay)
        {
            yield return new WaitForSeconds(delay);
            if (!UIManager.Instance.IsOpenedPopups)
            {
                AnalyticsWrapper.Instance.EventPressBtn(AnalyticsHelper.EventKey.StartNextLevel);
                UIManager.Instance.CreateNewUIElement<PopupLoadOut>(UIElementType.LoadOut);
            }
        }


        /// <summary>
        /// Called when [navigated from].
        /// </summary>
        public override void OnNavigatedFrom()
        {
            AdsManager.Instance.HideBanner();
            BackButtonManager.Instance.BackButtonAction -= BackButtonActions;
        }

        /// <summary>
        /// Backs the button actions.
        /// </summary>
        public override void BackButtonActions()
        {
            Application.Quit();
        }

        private void SetAdsConfigurator()
        {
            AppSettings.Instance.NoADSChanged += () =>
             {
                 if (AppSettings.Instance.NoADS)
                 {
                     AdsManager.Instance.HideBanner();
                 }
             };
            AppSettings.Instance.NoBannerChanged += () =>
            {
                if (AppSettings.Instance.NoBanner)
                {
                    AdsManager.Instance.HideBanner();
                }
            };
            AdsManager.Instance.ShowBanner();
        }



    }
}