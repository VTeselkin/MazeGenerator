﻿namespace Assets.Scripts.Navigation.Scenes.Map
{
    internal class MenuNavigationArgs : NavigationArgs
    {
        private const string MapSceneName = "MenuScene";
        public bool StartNextLevel { get; set; }

        /// <summary>
        ///     Initializes a new instance of the <see cref="MapNavigationArgs" /> class.
        /// </summary>
        public MenuNavigationArgs() : base(SceneType.Menu, MapSceneName)
        {
            StartNextLevel = false;
        }
        /// <summary>
        /// Initializes a new instance of the <see cref="T:Assets.Scripts.Navigation.Scenes.Map.MenuNavigationArgs"/> class.
        /// </summary>
        /// <param name="startNextLevel">If set to <c>true</c> start next level.</param>
        public MenuNavigationArgs(bool startNextLevel) : base(SceneType.Menu, MapSceneName)
        {
            StartNextLevel = startNextLevel;
        }

    }
}