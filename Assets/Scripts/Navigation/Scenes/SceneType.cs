﻿namespace Assets.Scripts.Navigation.Scenes
{
	/// <summary>
	///     Enum SceneType
	/// </summary>
	public enum SceneType
	{
		/// <summary>
		///     The undefined
		/// </summary>
		Undefined,

		/// <summary>
		///     The map
		/// </summary>
		Menu,

		/// <summary>
		/// The game.
		/// </summary>
		Game
	}
}