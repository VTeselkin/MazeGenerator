﻿using System;
using Assets.Scripts.Shared;
using UnityEngine;
using Assets.Scripts.GUI.Managers;

namespace Assets.Scripts.Navigation
{
	/// <summary>
	/// Class BackButtonManager.
	/// </summary>
	/// <seealso cref="Assets.Scripts.Shared.UnitySingleton{BackButtonManager}" />
	internal class BackButtonManager : UnitySingleton<BackButtonManager>
	{
		#region Fields

		private bool _pause;

		#endregion

		#region Events

		/// <summary>
		///     The back button action
		/// </summary>
		public Action BackButtonAction;

		public event Action PopupBackButtonAction;

		#endregion

		#region Methods

		/// <summary>
		///     Sets the pause.
		/// </summary>
		/// <param name="value">if set to <c>true</c> [value].</param>
		public void SetPause(bool value)
		{
			_pause = value;
		}

		private BackButtonManager()
		{
		}

		private void OnPopupBackButtonAction()
		{
			var handler = PopupBackButtonAction;
			if (handler != null) handler();
		}

#if UNITY_EDITOR || PLATFORM_ANDROID || PLATFORM_AMAZON
		private void Update()
		{
			if (_pause)
				return;

			if (Input.GetKeyDown(KeyCode.Escape))
			{

				if (UIManager.Instance.IsOpenedPopups)
				{
					var popup = UIManager.Instance.GetTopPopup();

					if (PopupBackButtonAction != null)
						PopupBackButtonAction();
					popup.Close();
				}
				else
				{
					BackButtonAction.SafeInvoke();
				}
			}

		}
#endif
	}



	#endregion

}

