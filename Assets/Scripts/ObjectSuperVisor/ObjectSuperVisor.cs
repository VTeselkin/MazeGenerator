using UnityEngine;
using Assets.Scripts.Helpers;
using Assets.Scripts.Strategy.Interfaces;
using System;
using Object = System.Object;
using Assets.Scripts.Game;

using Assets.Scripts.Shared;
using Assets.Scripts.GUI.Managers;
using Assets.Scripts.GUI;

internal class ObjectSuperVisor : MonoBehaviour, IObjectSuperVisor
{
    private static GridDimension _gridDimension;
    private static ObjectSuperVisor _instance;
    private IObjectStorage<BaseGameObject> _objectStorage;
    private static CamPos _campos;
    private bool _flagIsClosedUpdate = true;
    private static IObjectTracker<BaseGameObject> _objectTracker;
    private LevelCheckState _levelCheckState;

    #region Constructor
    public static IObjectSuperVisor InitInstance(IObjectStorage<BaseGameObject> objectStorage, LevelData levelData)
    {
        if (_instance == null)
        {
            _instance = new GameObject("CellSuperVisor").AddComponent<ObjectSuperVisor>();
            _campos = GameObject.Find("Main Camera").GetComponent<CamPos>();
            _campos.Init(levelData);
            _instance._objectStorage = objectStorage;
            _instance._objectStorage.Changed += _instance.OnChangedCallBack;
            _gridDimension = levelData.PlayDimension;
            _objectTracker = ObjectTrackerLine.Instance;
            _objectTracker.Initialize(_gridDimension);
            _objectTracker.Changed += StartGamePlayActMouseUpCallBack;
            _instance._levelCheckState = new LevelCheckState(levelData);

        }
        return _instance;
    }

    #endregion

    #region Properties

    public static IObjectSuperVisor Instance
    {
        get { return _instance; }
    }

    public bool IsClosedUpdate
    {
        get { return _flagIsClosedUpdate; }
        set
        {
            _flagIsClosedUpdate = value;
        }
    }
    public GridDimension GridDimension
    {
        get { return _objectStorage.Dimension(); }
    }

    public IObjectStorage<BaseGameObject> ObjectStorage
    {
        get { return _objectStorage; }
    }

    #endregion

    #region Public Method

    public void PreUpdateManage()
    {
        CameraSubscibe();
        _flagIsClosedUpdate = false;

    }

    public void BlockMouse()
    {
        InputHandler.Instance.IsFreezeInput = true;
    }

    public void UnBlockMouse()
    {
        InputHandler.Instance.IsFreezeInput = false;
    }

    public void DestroyInstance()
    {
        CameraUnSubscibe();
        _objectTracker.Changed -= StartGamePlayActMouseUpCallBack;
        _instance._objectStorage.Changed -= _instance.OnChangedCallBack;
        DestroyObject(_instance);
        _objectStorage = null;
        _instance = null;
        _flagIsClosedUpdate = true;
    }

    public void OnChangedCallBack(object sender, EventArgsGeneric<Object> eventArgsGeneric)
    {
        BaseGameObject gemGameObject = eventArgsGeneric.SomeObject as BaseGameObject;
        if (gemGameObject != null)
        {
            StartEventCallBackForBaseGameObject(gemGameObject, eventArgsGeneric.Info);
            return;
        }
    }
    #endregion

    #region Private Method

    private void CameraSubscibe()
    {
        var players = _objectStorage.GetListOfObjectsByType(ObjectTypeEnum.Player);
        foreach (var player in players)
        {
            player.ChangedObject += _campos.PlayerChange;
        }
    }

    private void CameraUnSubscibe()
    {
        var players = _objectStorage.GetListOfObjectsByType(ObjectTypeEnum.Player);
        foreach (var player in players)
        {
            player.ChangedObject -= _campos.PlayerChange;
        }
    }

    private static void StartGamePlayActMouseUpCallBack(object sender, BaseGameObjectEventArgs e)
    {
        if (e.Info == EventMessages.EventTrackStartWork)
        {
            InputHandler.Instance.IsFreezeInput = true;
        }
    }

    private void Update()
    {
        if (_flagIsClosedUpdate == false)
        {
            UpdateObjects();
            CamUpdate();
            OnUpdateChanged();
        }
    }

    private void UpdateObjects()
    {
        foreach (var o in _objectStorage.GetListOfMovableObjects())
        {
            o.CustomUpdate();
        }

    }

    private void CamUpdate()
    {
        _campos.CustomUpdate();
    }

    private void StartEventCallBackForBaseGameObject(BaseGameObject gemGameObject, String eventInfo)
    {
        if (eventInfo == EventMessages.EventAddGameObject)
        {
            gemGameObject.Changed += GameObjectObserverWorker;
            gemGameObject.ChangedObject += GameObjectChangedObject;
            OnChanged(EventMessages.EventAddGameObject, gemGameObject.Coordinates, gemGameObject);
        }

        if (eventInfo == EventMessages.EventRemoveGameObject)
        {
            if (gemGameObject != null)
            {
                gemGameObject.Changed -= GameObjectObserverWorker;
                gemGameObject.ChangedObject -= GameObjectChangedObject;
                OnChanged(EventMessages.EventRemoveGameObject, gemGameObject.Coordinates, gemGameObject);
            }
        }
    }

    private void GameObjectChangedObject(object sender, BaseGameObjectEventArgs e)
    {
        BaseGameObject objectOnEvent = (BaseGameObject)sender;
        if (e.Info == EventMessages.EventCollisionPlayerByFood)
        {
            objectOnEvent.Energy += e.GemObjectSender.Energy;
            AudioManager.Instance.PlaySoundDamage(ObjectTypeEnum.Food);
            objectOnEvent.Speed += e.GemObjectSender.Speed;
            objectOnEvent.ChangeUIHandlers();
            e.GemObjectSender.DestroyObject();
            if (_levelCheckState.CheckWinLevel(ObjectTypeEnum.Food))
            {
                objectOnEvent.OnChanged(EventMessages.EventLevelComplete);
            }

        }
        if (e.Info == EventMessages.EventCollisionPlayerByFinish)
        {
            e.GemObjectSender.DestroyObject();
            AudioManager.Instance.PlaySoundDamage(ObjectTypeEnum.Finish);
            if (_levelCheckState.CheckWinLevel(ObjectTypeEnum.Finish))
            {
                objectOnEvent.OnChanged(EventMessages.EventLevelComplete);
            }

        }
        if (e.Info == EventMessages.EventCollisionEnemyByPlayer)
        {
            BaseGameObject baseObject = e.GemObjectSender.GetComponent<BaseGameObject>();
            e.GemObjectSender.DamageObject(objectOnEvent.Energy);

            objectOnEvent.ChangeUIHandlers();
            AudioManager.Instance.PlaySoundDamage(objectOnEvent.ObjectType);
            switch (objectOnEvent.ObjectType)
            {
                case ObjectTypeEnum.EnemySimple:
                case ObjectTypeEnum.EnemySee:
                    objectOnEvent.Configuration.Movable.PauseToMove(true, 0.3f);
                    break;
                case ObjectTypeEnum.EnemyZombie:
                case ObjectTypeEnum.EnemySeeZombie:
                    objectOnEvent.Configuration.Movable.PauseToMove(true, 2.0f);
                    break;
                case ObjectTypeEnum.EnemySeeBat:
                    e.GemObjectSender.SetBleedingForObject();
                    break;
                case ObjectTypeEnum.EnemySnailGreen:
                    e.GemObjectSender.SetInfectionForObject();
                    objectOnEvent.SetAnimationDestroy();
                    break;
                case ObjectTypeEnum.ScoreText:
                case ObjectTypeEnum.Undefined:
                case ObjectTypeEnum.Wall:
                case ObjectTypeEnum.Free:
                case ObjectTypeEnum.Player:
                case ObjectTypeEnum.Finish:
                case ObjectTypeEnum.Food:
                    break;
            }
            e.GemObjectSender.SetAnimationDamage();
            objectOnEvent.SetAnimationDamage();

            _objectStorage.AddScoreObject(ScoreTypeEnum.Damage, e.GemObjectSender.Coordinates, out baseObject);
            (baseObject as ObjectiveText).SetText(objectOnEvent.Energy.ToString());
        }
        if (e.Info == EventMessages.EventCollisionEnemyByEnemy)
        {
            //e.GemObjectSender.Configuration.Movable.PauseToMove(true, 0.2f);
            //objectOnEvent.Configuration.Movable.PauseToMove(true, 0.2f);

        }

    }

    private void GameObjectObserverWorker(object sender, EventArgsSimple e)
    {
        BaseGameObject objectOnEvent = (BaseGameObject)sender;

        if (e.Info == EventMessages.EventGameObjectDestroyed)
        {
            _objectStorage.RemoveGameObject((BaseGameObject)sender);
        }

        if (e.Info == EventMessages.EventEnemySeePlayer)
        {
            objectOnEvent.SetVisibleEyes(true);
        }
        if (e.Info == EventMessages.EventEnemyDontSeePlayer)
        {
            objectOnEvent.SetVisibleEyes(false);
        }

    }
    private void Start()
    {
        UIManager.Instance.PopupOpened += OnPopupOpened;
        UIManager.Instance.PopupClosed += OnPopupClosed;
    }

    private void OnPopupOpened(UIElementType obj)
    {
        BlockMouse();
        _flagIsClosedUpdate = true;

    }

    private void OnPopupClosed(UIElementType obj)
    {
        if (!UIManager.Instance.IsOpenedPopups)
        {
            UnBlockMouse();
            _flagIsClosedUpdate = false;
        }
    }

    private void OnDestroy()
    {
        UIManager.Instance.PopupOpened -= OnPopupOpened;
        UIManager.Instance.PopupClosed -= OnPopupClosed;
    }
    #endregion

    #region Custom Event

    public event EventHandler UpdateChanged;

    public void OnUpdateChanged()
    {
        EventHandler handler = UpdateChanged;
        if (handler != null)
        {
            var args = new EventArgs();

            handler(this, args);
        }
    }

    public event EventHandler PreUpdateChanged;

    public void OnPreUpdateChanged()
    {
        EventHandler handler = PreUpdateChanged;
        if (handler != null)
        {
            var args = new EventArgs();

            handler(this, args);
        }
    }
    public event EventHandler<BaseGameObjectEventArgs> Changed;

    // BaseGameObject management Event ======
    public void OnChanged(string eventInfo, Point position, BaseGameObject gemObjectSender)
    {
        //Debug.Log(eventInfo);
        EventHandler<BaseGameObjectEventArgs> handler = Changed;
        if (handler != null)
        {
            var args = new BaseGameObjectEventArgs
            {
                Info = eventInfo,
                GemObjectSender = gemObjectSender
            };
            // Raise custom game object event with specific info
            handler(this, args);
        }
    }

    public void InitContainer()
    {
        throw new NotImplementedException();
    }

    #endregion
}
