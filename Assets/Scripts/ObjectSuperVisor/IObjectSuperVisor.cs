using System;
using Assets.Scripts.Helpers;
using Assets.Scripts.Strategy.Interfaces;
using Object = System.Object;

interface IObjectSuperVisor
{
    void InitContainer();

    void DestroyInstance();

    void OnChangedCallBack(object sender, EventArgsGeneric<Object> eventArgsGeneric);

    void OnChanged(string eventInfo, Point position, BaseGameObject gemObjectSender);

    GridDimension GridDimension { get; }

    void PreUpdateManage();

    event EventHandler<BaseGameObjectEventArgs> Changed;

    event EventHandler UpdateChanged;

    event EventHandler PreUpdateChanged;

    IObjectStorage<BaseGameObject> ObjectStorage { get; }

    void BlockMouse();

    void UnBlockMouse();

}
