﻿using UnityEngine;
using Assets.Scripts.Helpers;
using System.Collections.Generic;
using System;
using Object = System.Object;

namespace Assets.Scripts.Strategy.Interfaces
{
    public interface IObjectStorage<T> where T : BaseGameObject
    {
        void CreateGameObject(ResourceIdentifier objectId, object initInfo, Point point, out T baseObject);

        void AddGameObject(T gemGameObject);

        void AddGameObject(GameObject gameObject);

        void AddGameObject(ObjectTypeEnum objectType);

        void AddGameObject(ObjectTypeEnum objectTypeEnum, FloatPoint point, out T baseGameObject);

        void AddGameObjectDecor(ObjectTypeEnum objectTypeEnum, Point point, out T baseGameObject);

        void AddGameObject(ObjectTypeEnum objectType, Point point, out T baseGameObject);

        void AddScoreObject(ScoreTypeEnum scoreType, Point point, out T baseGameObject);

        T GetGameObjectByPosition(Point point);

        List<T> GetAllGameObjectByPosition(Point point);

        //T GetGameObjectMovableByPosition(Point point);

        T GetGameFreeObjectByPosition(Point point);

        List<T> GetListOfMovableObjects();

        List<T> GetListFreeGameObjectsByPositionWithMoreDistance(T targetObject, float minDistance);

        List<T> GetListFreeGameObjectsByPositionWithLessDistance(T targetObject, float maxDistance);

        int GetTotalCount();

        List<T> ObjectsStorageList { get; }

        event EventHandler<EventArgsGeneric<Object>> Changed;

        void DestroyInstance();

        GridDimension Dimension();

        List<T> GetListOfObjectsByTypeInArea(ObjectTypeEnum requiredType, AreaDimension dimension);

        List<T> GetListOfObjectsByType(ObjectTypeEnum requiredType);

        List<T> GetListOfObjectsByGroupe(ObjectGroupEnum requiredType);

        List<T> GetNeighborsObjectByType(T requiredObject);

        List<Point> GetListOfPositionsByType(ObjectTypeEnum requiredType);

        List<Point> GetListOfPositionsByGroup(ObjectGroupEnum requiredType);

        void RemoveGameObject(T gemGameObject);
    }
}