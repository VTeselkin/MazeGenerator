﻿using Assets.Scripts.Helpers;

public interface IScorable
{
    ObjectTypeEnum ObjectType { get; set; }
}