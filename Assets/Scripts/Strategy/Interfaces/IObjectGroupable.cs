using Assets.Scripts.Helpers;

public interface IObjectGroupable
{
    ObjectGroupEnum ObjectGroup { get; set; }

    ObjectTypeEnum ObjectType { get; set; }
}
