﻿using Assets.Scripts.Helpers;

public interface IConfigurator<T> where T : BaseGameObject
{
    void SetConfiguration(T baseObject, ResourceIdentifier objectType, object initInfo);
}