﻿using UnityEngine;

namespace Assets.Scripts.Strategy.Classes.Textable
{
    internal class TextableLabel : ITextable
    {
        private readonly UILabel _label;

        public TextableLabel(UILabel label)
        {
            _label = label;

            _label.alpha = 1.0f;
        }

        public string Text
        {
            get { return _label.text; }
            set
            {
                _label.text = value;
                TextBounds = new Bounds(_label.localCenter, _label.localSize);
            }
        }

        public Color Color
        {
            get { return _label.color; }
            set { _label.color = value; }
        }

        public Color ColorOutLine { get; set; }

        public Bounds TextBounds { get; private set; }

        public int Size
        {
            get { return _label.fontSize; }
            set { _label.fontSize = value; }
        }

        public int Height
        {
            get { return _label.height; }
            set { _label.height = value; }
        }

        public int Weidth
        {
            get { return _label.width; }
            set { _label.width = value; }
        }
    }
}