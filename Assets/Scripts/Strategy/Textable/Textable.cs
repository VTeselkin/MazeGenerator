﻿using UnityEngine;

namespace Assets.Scripts.Strategy.Classes.Textable {
    public class Textable : ITextable {
        private TextMesh _textMesh;
        private string _text;
        private Bounds _textBounds;

        public string Text {
            get { return _text; }
            set {
                _text = value;

                _textMesh.text = _text;
                _textBounds = _textMesh.GetComponent<Renderer>().bounds;
            }
        }

        public Color Color {
            get { return _textMesh.color; }
            set { _textMesh.color = value; }
        }

        public Color ColorOutLine { get; set; }

        public Bounds TextBounds {
            get { return _textBounds; }
        }

        public int Size { get; set; }
        public int Height { get; set; }
        public int Weidth { get; set; }

        public Textable(TextMesh textMesh) {
            _textMesh = textMesh;
        }
    }
}