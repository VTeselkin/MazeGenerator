﻿using System;
using UnityEngine;
using System.Collections;

public class NoTextable : ITextable {
    
    public string Text { get; set; }
    public Color Color { get; set; }
    public Color ColorOutLine { get; set; }
    public Bounds TextBounds { get; set; }
    public int Size { get; set; }
    public int Height { get; set; }
    public int Weidth { get; set; }

    public NoTextable(){
        Text = String.Empty;
    }
}
