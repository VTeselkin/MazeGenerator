﻿using UnityEngine;
using UnityEngine.UI;

public class TextableUI : ITextable
{
    private Text _text;
    public string Text {
        get { return _text.text; }
        set {
            _text.text = value;
            TextBounds = new Bounds(new Vector3(_text.minHeight/2, _text.minWidth/2), new Vector2(_text.minHeight,_text.minWidth));
        }
    }

    public Color Color {
        get { return _text.color; }
        set { _text.color = value; }
    }

    public Color ColorOutLine { get; set; }

    public Bounds TextBounds { get; private set; }
    public int Size { get; set; }
    public int Height { get; set; }
    public int Weidth { get; set; }

    public TextableUI(Text text) {
        _text = text;
    }
}
