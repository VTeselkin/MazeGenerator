﻿using System;
using UnityEngine;

public interface ITextable
{
    String Text { get; set; }

    Color Color { get; set; }
    Color ColorOutLine { get; set; }
    Bounds TextBounds { get; }

    int Size { get; set; }
    int Height { get; set; }
    int Weidth { get; set; }
}
