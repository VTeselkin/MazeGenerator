using Assets.Scripts.Helpers;

public class NoObjectGroupable : IObjectGroupable
{

    public ObjectGroupEnum ObjectGroup
    {
        get
        {
            return ObjectGroupEnum.Undefined;
        }

        set { throw new System.NotImplementedException(); }
    }

    public ObjectTypeEnum ObjectType { get; set; }

}