using Assets.Scripts.Helpers;

public class ObjectGroupable : IObjectGroupable
{

    #region Fields

    #endregion

    #region Properties

    public ObjectGroupEnum ObjectGroup { get; set; }

    public ObjectTypeEnum ObjectType { get; set; }

    #endregion
}