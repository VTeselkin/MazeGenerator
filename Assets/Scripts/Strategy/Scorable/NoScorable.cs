using Assets.Scripts.Helpers;

public class NoScorable : IScorable
{
    public int Score { get; set; }

    public ObjectTypeEnum ObjectType { get; set; }
}