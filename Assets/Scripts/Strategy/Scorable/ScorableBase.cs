using Assets.Scripts.Helpers;

namespace Assets.Scripts.Strategy.Classes.Scorable
{
    internal class ScorableBase : IScorable
    {
        public int Score
        {
            get { return ScoresLibrary.GetScore(ObjectType); }
        }

        public ObjectTypeEnum ObjectType { get; set; }
    }
}
