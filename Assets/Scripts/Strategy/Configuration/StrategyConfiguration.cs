
using Assets.Scripts.Helpers;

internal class StrategyConfiguration<T> : IStrategyConfiguration<T> where T : BaseGameObject
{
    public IMovable Movable { get; set; }
    public IScorable Scorable { get; set; }
    public ObjectTypeEnum ObjectType { get; set; }
    public IObjectGroupable Groupable { get; set; }
    public IColorable<T> Colorable { get; set; }
    public IInteractionable<T> Interactionable { get; set; }
    public ITextable Textable { get; set; }

    #region Constructor 

    public StrategyConfiguration(
        IScorable scorable,
        IMovable movable,
        IObjectGroupable groupable,
        IColorable<T> colorable,
        IInteractionable<T> interactionable,
        ITextable textable)
    {
        Movable = movable;
        Scorable = scorable;
        Groupable = groupable;
        Colorable = colorable;
        ObjectType = ObjectTypeEnum.Undefined;
        Interactionable = interactionable;
        Textable = textable;
    }

    #endregion
    #region Methods

    public IStrategyConfiguration<T> Clone()
    {
        return (StrategyConfiguration<T>)this.MemberwiseClone();
    }

    public void Destroy()
    {
        //throw new System.NotImplementedException();
    }


    #endregion
}
