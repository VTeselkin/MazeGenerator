using Assets.Scripts.Helpers;
using System.Collections.Generic;

public static class Strings
{
    internal static readonly IDictionary<ResourceIdentifier, string> UriResourceDictionaryMainObjects;
    internal static readonly IDictionary<ResourceIdentifierUI, string> UriResourceDictionaryUIObjects;

    static Strings()
    {
        UriResourceDictionaryMainObjects = new Dictionary<ResourceIdentifier, string>();
        UriResourceDictionaryUIObjects = new Dictionary<ResourceIdentifierUI, string>();
        InitMainObjectsDictionary();
    }
    public static string GetUriResource(ResourceIdentifier resource)
    {
        if (UriResourceDictionaryMainObjects.ContainsKey(resource))
        {
            return UriResourceDictionaryMainObjects[resource];
        }

        return null;
    }
    public static string GetUriResource(ResourceIdentifierUI resource)
    {
        if (UriResourceDictionaryUIObjects.ContainsKey(resource))
        {
            return UriResourceDictionaryUIObjects[resource];
        }

        return null;
    }
    private static void InitMainObjectsDictionary()
    {
        #region Enemys

        UriResourceDictionaryMainObjects.Add(new ResourceIdentifier(ObjectTypeEnum.EnemySimple),
            "Prefabs/GamePlayObjects/Enemy");

        UriResourceDictionaryMainObjects.Add(new ResourceIdentifier(ObjectTypeEnum.EnemyZombie),
            "Prefabs/GamePlayObjects/EnemyZombie");

        UriResourceDictionaryMainObjects.Add(new ResourceIdentifier(ObjectTypeEnum.EnemySee),
            "Prefabs/GamePlayObjects/EnemySee");

        UriResourceDictionaryMainObjects.Add(new ResourceIdentifier(ObjectTypeEnum.EnemySeeZombie),
            "Prefabs/GamePlayObjects/EnemySeeZombie");

        UriResourceDictionaryMainObjects.Add(new ResourceIdentifier(ObjectTypeEnum.EnemySeeBat),
           "Prefabs/GamePlayObjects/EnemySeeBat");

        UriResourceDictionaryMainObjects.Add(new ResourceIdentifier(ObjectTypeEnum.EnemySnailGreen),
                                             "Prefabs/GamePlayObjects/EnemySnailGreen");

        #endregion

        #region Target

        UriResourceDictionaryMainObjects.Add(new ResourceIdentifier(ObjectTypeEnum.Finish),
            "Prefabs/GamePlayObjects/Finish");

        #endregion

        #region Player

        UriResourceDictionaryMainObjects.Add(new ResourceIdentifier(ObjectTypeEnum.Player),
            "Prefabs/GamePlayObjects/Player");

        #endregion

        #region Other

        UriResourceDictionaryMainObjects.Add(new ResourceIdentifier(ObjectTypeEnum.Wall),
            "Prefabs/GamePlayObjects/Wall");

        UriResourceDictionaryMainObjects.Add(new ResourceIdentifier(ObjectTypeEnum.Free),
           "Prefabs/GamePlayObjects/Free");

        UriResourceDictionaryMainObjects.Add(new ResourceIdentifier(ObjectTypeEnum.Food),
            "Prefabs/GamePlayObjects/Food");

        UriResourceDictionaryMainObjects.Add(new ResourceIdentifier(ObjectTypeEnum.ScoreText),
                                             "Prefabs/GamePlayObjects/ObjectiveText");
        #endregion

        #region UI

        UriResourceDictionaryUIObjects.Add(new ResourceIdentifierUI(ObjectUITypeEnum.LevelButton),
            "Prefabs/UIGameObjects/Level");
        #endregion
    }
}
