using Assets.Scripts.Helpers;

internal interface IStrategyConfiguration<T> where T : BaseGameObject
{
    IMovable Movable { get; set; }
    IScorable Scorable { get; set; }
    ObjectTypeEnum ObjectType { get; set; }
    IColorable<T> Colorable { get; set; }
    ITextable Textable { get; set; }
    IObjectGroupable Groupable { get; set; }
    IInteractionable<T> Interactionable { get; set; }
    IStrategyConfiguration<T> Clone();
    void Destroy();
}

