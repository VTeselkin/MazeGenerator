﻿using System;
using Assets.Scripts.Helpers;

namespace Assets.Scripts.Strategy.Classes.Scorable
{
    internal class ScoresLibrary
    {
        public static int GetScore(ObjectTypeEnum objectType)
        {
            switch (objectType)
            {
                case ObjectTypeEnum.Undefined:
                case ObjectTypeEnum.Player:
                case ObjectTypeEnum.Finish:
                case ObjectTypeEnum.EnemySimple:
                    return 200;
            }
            throw new NotImplementedException("No scores for specified object type");
        }
    }
}