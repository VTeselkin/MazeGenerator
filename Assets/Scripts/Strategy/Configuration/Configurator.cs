using Assets.Scripts.Helpers;
using Assets.Scripts.Strategy.Classes.Scorable;
using Assets.Scripts.Strategy.Interfaces;
using Assets.Scripts.Strategy.Classes.Colorable;
using Assets.Scripts.Strategy.Classes.Textable;
using Assets.Scripts.Strategy.Classes.Movable;

internal class Configurator<T> : IConfigurator<T> where T : BaseGameObject
{
    private readonly IObjectStorage<T> _objectStorage;
    private IStrategyConfiguration<T> _defaultConfiguration;

    #region Constructor

    public Configurator(IObjectStorage<T> objectStorage)
    {
        _objectStorage = objectStorage;
    }

    #endregion

    /// <summary>
    /// Returns "Null" or default configuration for Strategy pattern.
    /// BaseGameObject Tuning
    /// </summary>
    /// <param name="baseObject"></param>
    /// <returns></returns>
    private IStrategyConfiguration<T> GetDefaultConfiguration(T baseObject)
    {
        if (_defaultConfiguration == null)
        {
            _defaultConfiguration = new StrategyConfiguration<T>(
                new NoScorable(), new NoMovable(baseObject), new NoObjectGroupable(), new NoColorable<T>(), new NoInteractionable<T>(baseObject), new NoTextable());
        }
        var configuration = _defaultConfiguration.Clone();
        configuration.Movable = new NoMovable(baseObject);
        return configuration;
    }

    /// <summary>
    /// Sets the configuration.
    /// </summary>
    /// <param name="baseObject">Base object.</param>
    /// <param name="resourceIdentifier">Resource identifier.</param>
    /// <param name="initInfo">Init info.</param>
    public void SetConfiguration(T baseObject, ResourceIdentifier resourceIdentifier, object initInfo)
    {
        IStrategyConfiguration<T> configuration;
        var objectType = resourceIdentifier.GameObjectType;

        switch (objectType)
        {
            case ObjectTypeEnum.Undefined:
                configuration = GetDefaultConfiguration(baseObject);
                SetConfigurationAndAddedParameters(baseObject, configuration, objectType);
                return;
            case ObjectTypeEnum.Wall:
                configuration = GetConfigurationWall(baseObject);
                SetConfigurationAndAddedParameters(baseObject, configuration, objectType);
                return;
            case ObjectTypeEnum.Free:
                configuration = GetDefaultConfiguration(baseObject);
                SetConfigurationAndAddedParameters(baseObject, configuration, objectType);
                return;
            case ObjectTypeEnum.Player:
                configuration = GetConfigurationPlayer(baseObject);
                SetConfigurationAndAddedParameters(baseObject, configuration, objectType);
                return;
            case ObjectTypeEnum.Finish:
                configuration = GetConfigurationTarget(baseObject);
                SetConfigurationAndAddedParameters(baseObject, configuration, objectType);
                return;
            case ObjectTypeEnum.EnemySimple:
                configuration = GetConfigurationEnemySimple(baseObject);
                SetConfigurationAndAddedParameters(baseObject, configuration, objectType);
                return;
            case ObjectTypeEnum.EnemyZombie:
                configuration = GetConfigurationEnemyZombie(baseObject);
                SetConfigurationAndAddedParameters(baseObject, configuration, objectType);
                return;
            case ObjectTypeEnum.Food:
                configuration = GetConfigurationFood(baseObject);
                SetConfigurationAndAddedParameters(baseObject, configuration, objectType);
                return;
            case ObjectTypeEnum.EnemySee:
                configuration = GetConfigurationEnemySee(baseObject);
                SetConfigurationAndAddedParameters(baseObject, configuration, objectType);
                break;
            case ObjectTypeEnum.EnemySeeZombie:
                configuration = GetConfigurationEnemySeeZombie(baseObject);
                SetConfigurationAndAddedParameters(baseObject, configuration, objectType);
                break;
            case ObjectTypeEnum.EnemySeeBat:
                configuration = GetConfigurationEnemyBat(baseObject);
                SetConfigurationAndAddedParameters(baseObject, configuration, objectType);
                break;
            case ObjectTypeEnum.EnemySnailGreen:
                configuration = GetConfigurationEnemySnailGreen(baseObject);
                SetConfigurationAndAddedParameters(baseObject, configuration, objectType);
                break;

            case ObjectTypeEnum.ScoreText:
                configuration = GetConfigurationScoreText(baseObject);
                SetConfigurationAndAddedParameters(baseObject, configuration, objectType);
                break;
        }

    }
    private IStrategyConfiguration<T> GetConfigurationWall(T baseObject)
    {
        var configuration = GetDefaultConfiguration(baseObject);
        configuration.Groupable = new ObjectGroupable { ObjectGroup = ObjectGroupEnum.Walls };
        return configuration;
    }

    private IStrategyConfiguration<T> GetConfigurationFree(T baseObject)
    {
        var configuration = GetDefaultConfiguration(baseObject);
        configuration.Groupable = new ObjectGroupable { ObjectGroup = ObjectGroupEnum.Undefined };
        return configuration;
    }
    private IStrategyConfiguration<T> GetConfigurationPlayer(T baseObject)
    {
        var configuration = GetDefaultConfiguration(baseObject);
        configuration.Groupable = new ObjectGroupable { ObjectGroup = ObjectGroupEnum.Players };
        configuration.Colorable = new ColorableBase<T>(baseObject);
        configuration.Movable = new PlayerMovable(baseObject);
        configuration.Interactionable = new PlayerInteractionable<T>(baseObject);
        return configuration;
    }

    private IStrategyConfiguration<T> GetConfigurationTarget(T baseObject)
    {
        var configuration = GetDefaultConfiguration(baseObject);
        configuration.Groupable = new ObjectGroupable { ObjectGroup = ObjectGroupEnum.Targets };
        configuration.Colorable = new ColorableBase<T>(baseObject);
        configuration.Scorable = new ScorableBase();
        configuration.Interactionable = new NoInteractionable<T>(baseObject);
        return configuration;
    }

    private IStrategyConfiguration<T> GetConfigurationEnemySimple(T baseObject)
    {
        var configuration = GetDefaultConfiguration(baseObject);
        configuration.Groupable = new ObjectGroupable { ObjectGroup = ObjectGroupEnum.Enemies };
        configuration.Colorable = new ColorableBase<T>(baseObject);
        configuration.Scorable = new ScorableBase();
        configuration.Movable = new EnemySimpleMovable(baseObject);
        configuration.Interactionable = new EnemyInteractionable<T>(baseObject);
        return configuration;
    }
    private IStrategyConfiguration<T> GetConfigurationEnemyZombie(T baseObject)
    {
        var configuration = GetDefaultConfiguration(baseObject);
        configuration.Groupable = new ObjectGroupable { ObjectGroup = ObjectGroupEnum.Enemies };
        configuration.Colorable = new ColorableBase<T>(baseObject);
        configuration.Scorable = new ScorableBase();
        configuration.Movable = new EnemySimpleMovable(baseObject);
        configuration.Interactionable = new EnemyInteractionable<T>(baseObject);
        return configuration;
    }
    private IStrategyConfiguration<T> GetConfigurationFood(T baseObject)
    {
        var configuration = GetDefaultConfiguration(baseObject);
        configuration.Groupable = new ObjectGroupable { ObjectGroup = ObjectGroupEnum.Used };
        configuration.Colorable = new ColorableBase<T>(baseObject);
        configuration.Scorable = new ScorableBase();
        return configuration;
    }

    private IStrategyConfiguration<T> GetConfigurationEnemySee(T baseObject)
    {
        var configuration = GetDefaultConfiguration(baseObject);
        configuration.Groupable = new ObjectGroupable { ObjectGroup = ObjectGroupEnum.Enemies };
        configuration.Colorable = new ColorableBase<T>(baseObject);
        configuration.Scorable = new ScorableBase();
        configuration.Movable = new EnemySeeMovable(baseObject);
        configuration.Interactionable = new EnemyInteractionable<T>(baseObject);
        return configuration;
    }

    private IStrategyConfiguration<T> GetConfigurationEnemySeeZombie(T baseObject)
    {
        var configuration = GetDefaultConfiguration(baseObject);
        configuration.Groupable = new ObjectGroupable { ObjectGroup = ObjectGroupEnum.Enemies };
        configuration.Colorable = new ColorableBase<T>(baseObject);
        configuration.Scorable = new ScorableBase();
        configuration.Movable = new EnemySeeMovable(baseObject);
        configuration.Interactionable = new EnemyInteractionable<T>(baseObject);
        return configuration;
    }

    private IStrategyConfiguration<T> GetConfigurationScoreText(T baseObject)
    {
        var configuration = GetDefaultConfiguration(baseObject);
        var bonusLabel = baseObject.gameObject.GetComponent<UILabel>();
        configuration.Textable = new TextableLabel(bonusLabel);
        configuration.Movable = new ObjectiveBonusMovable(baseObject);
        return configuration;
    }
    private IStrategyConfiguration<T> GetConfigurationEnemyBat(T baseObject)
    {
        var configuration = GetDefaultConfiguration(baseObject);
        configuration.Groupable = new ObjectGroupable { ObjectGroup = ObjectGroupEnum.Enemies };
        configuration.Colorable = new ColorableBase<T>(baseObject);
        configuration.Scorable = new ScorableBase();
        configuration.Movable = new EnemySeeBatMovable(baseObject);
        configuration.Interactionable = new EnemyInteractionable<T>(baseObject);
        return configuration;
    }

    private IStrategyConfiguration<T> GetConfigurationEnemySnailGreen(T baseObject)
    {
        var configuration = GetDefaultConfiguration(baseObject);
        configuration.Groupable = new ObjectGroupable { ObjectGroup = ObjectGroupEnum.Enemies };
        configuration.Colorable = new ColorableBase<T>(baseObject);
        configuration.Scorable = new ScorableBase();
        configuration.Movable = new EnemySimpleMovable(baseObject);
        configuration.Interactionable = new EnemyInteractionable<T>(baseObject);
        return configuration;
    }
    private void SetConfigurationAndAddedParameters(T baseObject, IStrategyConfiguration<T> configuration,
                                                        ObjectTypeEnum objectType)
    {
        configuration.ObjectType = objectType;
        configuration.Scorable.ObjectType = objectType;
        baseObject.Configuration = (IStrategyConfiguration<BaseGameObject>)configuration;
    }
}

