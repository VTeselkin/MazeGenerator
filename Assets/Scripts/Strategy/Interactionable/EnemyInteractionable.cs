using System;
using Assets.Scripts.Shared;
using Assets.Scripts.Strategy.Interfaces;

internal class EnemyInteractionable<T> : IInteractionable<T> where T : BaseGameObject
{
    private T _baseGameObject;
    private IObjectStorage<BaseGameObject> _objectStorage;
    private IObjectTracker<BaseGameObject> _objectTrackLine;

    public EnemyInteractionable(T baseGameObject)
    {
        _baseGameObject = baseGameObject;
        _objectTrackLine = ObjectTrackerLine.Instance;
        _objectStorage = ObjectSuperVisor.Instance.ObjectStorage;
    }

    public bool IsCanCollision { get { return true; } }

    public T BaseObject
    {
        get { return _baseGameObject; }
        set { _baseGameObject = value; }
    }


    public void OnTriggerEnter2D(T other)
    {
        if (other.ObjectType == Assets.Scripts.Helpers.ObjectTypeEnum.Player)
        {
            _baseGameObject.OnChangedObject(EventMessages.EventCollisionEnemyByPlayer, other);
        }
        if (other.ObjectType == Assets.Scripts.Helpers.ObjectTypeEnum.Finish)
        {
            _baseGameObject.OnChangedObject(EventMessages.EventCollisionEnemyByFinish, other);
        }
        if (other.ObjectGroup == Assets.Scripts.Helpers.ObjectGroupEnum.Enemies)
        {
            _baseGameObject.OnChangedObject(EventMessages.EventCollisionEnemyByEnemy, other);
        }
    }

    public void SetObserve(Func<bool> validationFunction, Action validAction)
    {
        throw new NotImplementedException();
    }
}
