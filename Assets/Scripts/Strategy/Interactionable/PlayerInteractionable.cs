using System;
using Assets.Scripts.Strategy.Interfaces;
using Assets.Scripts.Shared;

public class PlayerInteractionable<T> : IInteractionable<T> where T : BaseGameObject
{
    private T _baseGameObject;
    private IObjectStorage<BaseGameObject> _objectStorage;
    private IObjectTracker<BaseGameObject> _objectTrackLine;

    public PlayerInteractionable(T baseGameObject)
    {
        _baseGameObject = baseGameObject;
        _objectStorage = ObjectSuperVisor.Instance.ObjectStorage;
        _objectTrackLine = ObjectTrackerLine.Instance;
    }

    public bool IsCanCollision { get { return true; } }

    public T BaseObject
    {
        get { return _baseGameObject; }
        set { _baseGameObject = value; }
    }

    public void OnTriggerEnter2D(T other)
    {
        if (other.ObjectType == Assets.Scripts.Helpers.ObjectTypeEnum.Finish)
        {
            _baseGameObject.OnChangedObject(EventMessages.EventCollisionPlayerByFinish, other);
        }
        if (other.ObjectType == Assets.Scripts.Helpers.ObjectTypeEnum.Food)
        {
            _baseGameObject.OnChangedObject(EventMessages.EventCollisionPlayerByFood, other);
        }
    }

    public void SetObserve(Func<bool> validationFunction, Action validAction)
    {
        throw new NotImplementedException();
    }
}
