using System;

public interface IInteractionable<T> where T : BaseGameObject
{
    Boolean IsCanCollision { get; }
    T BaseObject { get; set; }
    void OnTriggerEnter2D(T other);
    void SetObserve(Func<bool> validationFunction, Action validAction);
}
