using System;

public class NoInteractionable<T> : IInteractionable<T> where T : BaseGameObject
{
    private T _baseGameObject;

    public NoInteractionable(T baseGameObject)
    {
        _baseGameObject = baseGameObject;
    }
    public bool IsCanCollision { get { return false; } }

    public T BaseObject
    {
        get { return _baseGameObject; }
        set { _baseGameObject = value; }
    }

    public void OnTriggerEnter2D(T other)
    {

    }

    public void SetObserve(Func<bool> validationFunction, Action validAction)
    {
        throw new NotImplementedException();
    }
}
