using Assets.Scripts.Helpers;

public interface IColorable<T> where T : BaseGameObject
{
    ColorEnum GetColor();

    void SetColor(ColorEnum color);
}