using Assets.Scripts.Helpers;

namespace Assets.Scripts.Strategy.Classes.Colorable
{
    public class NoColorable<T> : IColorable<T> where T : BaseGameObject
    {
        public ColorEnum GetColor()
        {
            return ColorEnum.Undefined;
        }

        public void SetColor(ColorEnum color)
        {
            // Empty implementation
        }
    }
}
