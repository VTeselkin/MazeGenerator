using Assets.Scripts.Helpers;

internal class ColorableBase<T> : IColorable<T> where T : BaseGameObject
{
    #region Fields

    private readonly T _baseObject;
    private ColorEnum _color = ColorEnum.Undefined;

    #endregion

    #region Constructor 

    public ColorableBase(T baseObject)
    {
        _baseObject = baseObject;

        // ???
        if (baseObject.Configuration != null)
        {
            _color = baseObject.Color;
        }
    }

    #endregion

    #region Methods

    public ColorEnum GetColor()
    {
        return _color;
    }

    public void SetColor(ColorEnum color)
    {
        _color = color;
    }

    #endregion
}
