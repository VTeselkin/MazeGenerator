﻿using System.Collections.Generic;
using Assets.Scripts.Helpers;
using Assets.Scripts.Shared;
using UnityEngine;
using System.Linq;
using System.Collections;
using AssemblyCSharp.Scripts.MainManager;

public class EnemySeeBatMovable : BaseMovable
{
    private float journeyLength;
    private int _endPositionX;
    private int _endPositionY;
    private SideDirectional lastDirectional;
    private float period = 0.0f;
    private int maxDiscanceShow = 10;
    private bool _isCanSeePlayer = false;
    /// <summary>
    /// Initializes a new instance of the <see cref="T:EnemySeeMovable"/> class.
    /// </summary>
    /// <param name="baseGameObject">Base game object.</param>
    public EnemySeeBatMovable(BaseGameObject baseGameObject) : base(baseGameObject)
    {
        _moveToPositionFlag = false;
    }

    /// <summary>
    /// Customs the update.
    /// </summary>
    public override void CustomUpdate()
    {
        base.CustomUpdate();

        if (period > _baseGameObject.DelayBeforeUpdate && !PauseMove)
        {
            PointToMove = MoveGameObject();
            if (_isCanSeePlayer)
            {
                if (!_moveToPositionFlag)
                {

                    _moveToPositionFlag = true;
                    _endPositionX = PointToMove.X;
                    _endPositionY = PointToMove.Y;
                    MonoHelper.Instance.StartCoroutine(DelayBerofeChangeDirectional(Side));
                }
                if (_baseGameObject == null) return;
                if (_baseGameObject.Coordinates == PointToMove) return;
                _baseGameObject.OnChanged(EventMessages.EventStartMoveGameObject);
                MoveEnemy();
            }

        }
        else
        {
            period += Time.deltaTime;
        }
    }

    /// <summary>
    /// Pauses to move.
    /// </summary>
    /// <param name="isPause">If set to <c>true</c> is pause.</param>
    /// <param name="deltaTime">Delta time.</param>
    public override void PauseToMove(bool isPause, float deltaTime)
    {
        base.PauseToMove(isPause, deltaTime);
        if (isPause && deltaTime > 0)
        {
            MonoHelper.Instance.StartCoroutine(StartMove(deltaTime));
        }
    }

    private IEnumerator StartMove(float deltaTime)
    {
        yield return new WaitForSeconds(deltaTime);
        PointToMove = MoveGameObject();
        lastDirectional = Side;
        _endPositionX = PointToMove.X;
        _endPositionY = PointToMove.Y;
        MonoHelper.Instance.StartCoroutine(DelayBerofeChangeDirectional(Side));
        _moveToPositionFlag = true;
        PauseMove = false;
    }

    private IEnumerator DelayBerofeChangeDirectional(SideDirectional side)
    {
        if (side != lastDirectional)
        {
            _baseGameObject.SetAnimationMove(side);
            yield return new WaitForSeconds(_baseGameObject.DelayBeforAnimation);
            lastDirectional = side;
        }
        else
        {
            _baseGameObject.SetAnimationMove(side);
        }
    }

    private void MoveEnemy()
    {
        if (_moveToPositionFlag && lastDirectional == Side && Side != SideDirectional.Undefined && _isCanSeePlayer)
        {
            var target = new Vector3(_endPositionX * BaseGameObject.ElementsDistance, _endPositionY * BaseGameObject.ElementsDistance, _baseGameObject.FixedZPosition);
            journeyLength = Vector3.Distance(_baseGameObject.transform.position, target);
            float distCovered = Time.deltaTime * _baseGameObject.Speed;
            float fracJourney = distCovered / journeyLength;
            _baseGameObject.transform.position = Vector3.Lerp(_baseGameObject.transform.position, target, fracJourney);
            CheckObserve();
            if (CheckPositioning(_endPositionX, _endPositionY))
            {
                _baseGameObject.Coordinates = new Point(_endPositionX, _endPositionY);
                StopMove();
            }
        }
    }

    private void StopMove()
    {
        period = 0;
        _moveToPositionFlag = false;
        _baseGameObject.OnChanged(EventMessages.EventMoveToPositionEnd);
    }

    private Point MoveGameObject()
    {
        List<SideDirectional> sides = new List<SideDirectional> { SideDirectional.Right, SideDirectional.Left, SideDirectional.Up, SideDirectional.Down };
        var dirTarget = MoveToObject(ObjectTypeEnum.Player);
        if (dirTarget >= 0 && !PauseMove)
        {
            var p = Helper.GetNeighborsObject(_baseGameObject, sides[dirTarget]);
            if (CheckAvalibleDirection(p))
            {
                Side = sides[dirTarget];
                MonoHelper.Instance.StartCoroutine(DelayBerofeChangeDirectional(Side));
                _isCanSeePlayer = true;
                _baseGameObject.OnChanged(EventMessages.EventEnemySeePlayer);
                return p;
            }

        }
        _baseGameObject.OnChanged(EventMessages.EventEnemyDontSeePlayer);
        var target = Helper.GetNeighborsObject(_baseGameObject, Side);
        if (CheckAvalibleDirection(target))
        {
            return target;
        }
        else
        {
            _isCanSeePlayer = false;
            return _baseGameObject.Coordinates;
        }

    }

    private int MoveToObject(ObjectTypeEnum objectType)
    {

        List<Vector2> directions = new List<Vector2> { Vector2.right, Vector2.left, Vector2.up, Vector2.down };
        int index = -1;
        foreach (var dir in directions)
        {
            List<RaycastHit> hits;
            hits = Physics.RaycastAll(_baseGameObject.transform.position, dir * maxDiscanceShow).ToList();
            if (hits.Count() > 0)
            {
                index++;
                foreach (var h in hits)
                {
                    if (h.collider != null)
                    {
                        var baseGameObject = h.collider.GetComponent<BaseGameObject>();
                        if (baseGameObject != null)
                        {
                            if (baseGameObject.ObjectType == ObjectTypeEnum.Wall)
                            {
                                break;
                            }
                            if (baseGameObject.ObjectType == objectType)
                            {
                                return index;
                            }
                        }
                    }
                }
            }
        }
        return -1;
    }
}
