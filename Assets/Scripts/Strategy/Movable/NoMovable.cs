using System;
using Assets.Scripts.Helpers;

public class NoMovable : IMovable
{
    private BaseGameObject _baseObject;

    #region Constructors

    public NoMovable(BaseGameObject baseGameObject)
    {
        _baseObject = baseGameObject;
    }
    #endregion

    public bool IsAtMoving
    {
        get { return false; }
    }

    public BaseGameObject BaseObject
    {
        get { return _baseObject; }
        set { _baseObject = value; }
    }

    public void CustomUpdate()
    {
        // No IMovable implementation
    }

    public void MoveToPosition(Point point)
    {
        throw new NotImplementedException();
    }

    public void PauseToMove(bool isPause, float deltaTime)
    {
        // not used
    }

    public void SetObserve(Func<bool> validationFunction, Action validAction)
    {
        throw new NotImplementedException();
    }


}
