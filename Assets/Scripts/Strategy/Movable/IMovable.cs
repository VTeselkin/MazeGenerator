﻿using System;
using Assets.Scripts.Helpers;

interface IMovable
{
    Boolean IsAtMoving { get; }
    BaseGameObject BaseObject { get; set; }
    void MoveToPosition(Point point);
    void PauseToMove(bool isPause, float deltaTime);
    void CustomUpdate();
    void SetObserve(Func<bool> validationFunction, Action validAction);
}