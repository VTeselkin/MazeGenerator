﻿using System.Collections;
using AssemblyCSharp.Scripts.MainManager;
using Assets.Scripts.Helpers;
using Assets.Scripts.Shared;
using UnityEngine;

public sealed class EnemySimpleMovable : BaseMovable
{
    private float journeyLength;
    private int _endPositionX;
    private int _endPositionY;
    private SideDirectional lastDirectional;
    private float period = 0.0f;

    /// <summary>
    /// Initializes a new instance of the <see cref="T:EnemySimpleMovable"/> class.
    /// </summary>
    /// <param name="baseGameObject">Base game object.</param>
    public EnemySimpleMovable(BaseGameObject baseGameObject) : base(baseGameObject)
    {
        _moveToPositionFlag = false;
    }

    /// <summary>
    /// Customs the update.
    /// </summary>
    public override void CustomUpdate()
    {
        base.CustomUpdate();
        if (period > _baseGameObject.DelayBeforeUpdate && !PauseMove && !_baseGameObject.IsDead)
        {
            if (!_moveToPositionFlag)
            {
                _moveToPositionFlag = true;
                PointToMove = MoveGameObject();
                _endPositionX = PointToMove.X;
                _endPositionY = PointToMove.Y;
                MonoHelper.Instance.StartCoroutine(DelayBerofeChangeDirectional(Side));
            }

            if (_baseGameObject == null) return;
            if (_baseGameObject.Coordinates == PointToMove) return;
            _baseGameObject.OnChanged(EventMessages.EventStartMoveGameObject);
            MoveEnemy();

        }
        else
        {
            period += Time.deltaTime;
        }
    }

    /// <summary>
    /// Pauses to move.
    /// </summary>
    /// <param name="isPause">If set to <c>true</c> is pause.</param>
    /// <param name="deltaTime">Delta time.</param>
    public override void PauseToMove(bool isPause, float deltaTime)
    {
        base.PauseToMove(isPause, deltaTime);
        if (isPause && deltaTime > 0)
        {
            MonoHelper.Instance.StartCoroutine(StartMove(deltaTime));
        }
    }

    private IEnumerator StartMove(float deltaTime)
    {
        yield return new WaitForSeconds(deltaTime);
        PointToMove = MoveGameObject();
        lastDirectional = Side;
        _endPositionX = PointToMove.X;
        _endPositionY = PointToMove.Y;
        MonoHelper.Instance.StartCoroutine(DelayBerofeChangeDirectional(Side));
        _moveToPositionFlag = true;
        PauseMove = false;
    }

    private IEnumerator DelayBerofeChangeDirectional(SideDirectional side)
    {
        if (side != lastDirectional)
        {
            _baseGameObject.SetAnimationMove(side);
            yield return new WaitForSeconds(_baseGameObject.DelayBeforAnimation);
            lastDirectional = side;
        }
        else
        {
            _baseGameObject.SetAnimationMove(side);
        }
    }

    private void MoveEnemy()
    {
        if (_moveToPositionFlag && lastDirectional == Side && Side != SideDirectional.Undefined && !_baseGameObject.IsDead)
        {
            var target = new Vector3(_endPositionX * BaseGameObject.ElementsDistance, _endPositionY * BaseGameObject.ElementsDistance, _baseGameObject.FixedZPosition);
            journeyLength = Vector3.Distance(_baseGameObject.transform.position, target);
            float distCovered = Time.deltaTime * _baseGameObject.Speed;
            float fracJourney = distCovered / journeyLength;
            _baseGameObject.transform.position = Vector3.Lerp(_baseGameObject.transform.position, target, fracJourney);
            CheckObserve();
            if (CheckPositioning(_endPositionX, _endPositionY))
            {
                _baseGameObject.Coordinates = new Point(_endPositionX, _endPositionY);
                StopMove();

            }
        }
    }

    private void StopMove()
    {
        period = 0;
        _moveToPositionFlag = false;
        _baseGameObject.OnChanged(EventMessages.EventMoveToPositionEnd);
    }

    private Point MoveGameObject()
    {
        //if (PauseMove)
        //{
        //    Side = ReverseDirection();
        //    return PointToMove;
        //}
        return GetDirection();
    }

}