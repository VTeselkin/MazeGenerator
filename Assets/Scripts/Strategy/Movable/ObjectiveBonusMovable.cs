﻿using Assets.Scripts.Shared;
using UnityEngine;
using Assets.Scripts.Helpers;

namespace Assets.Scripts.Strategy.Classes.Movable
{
    internal class ObjectiveBonusMovable : BaseMovable
    {
        private FloatPoint _destination;

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="T:Assets.Scripts.Strategy.Classes.Movable.ObjectiveBonusMovable"/> class.
        /// </summary>
        /// <param name="baseObject">Base object.</param>
        public ObjectiveBonusMovable(BaseGameObject baseObject) : base(baseObject)
        {
            BaseObject = baseObject;
        }
        /// <summary>
        /// Moves to position.
        /// </summary>
        /// <param name="point">Point.</param>
        public override void MoveToPosition(Point point)
        {
            base.MoveToPosition(point);
            _destination = new FloatPoint(point.X, point.Y);
            _moveToPositionFlag = true;
            BaseObject.OnChanged(EventMessages.EventStartMoveGameObject);
        }

        /// <summary>
        /// Customs the update.
        /// </summary>
        public override void CustomUpdate()
        {
            base.CustomUpdate();
            if (_moveToPositionFlag)
            {
                var step = 0.02f;
                var target = new Vector3(_destination.X * BaseGameObject.ElementsDistance,
     _destination.Y * BaseGameObject.ElementsDistance, BaseObject.FixedZPosition);
                BaseObject.transform.position = Vector3.MoveTowards(BaseObject.transform.position, target, step);
                if (CheckPositioning(_destination))
                {
                    BaseObject.CoordinatesFloat = _destination;
                    StopMove();
                }
            }
        }

        private void StopMove()
        {
            _moveToPositionFlag = false;
            BaseObject.OnChanged(EventMessages.EventMoveToPositionEnd);
        }

        private bool CheckPositioning(FloatPoint destination)
        {
            if (BaseObject.transform.position ==
                new Vector3(destination.X * BaseGameObject.ElementsDistance, destination.Y * BaseGameObject.ElementsDistance,
                    BaseObject.FixedZPosition))
            {
                return true;
            }
            return false;
        }
    }
}