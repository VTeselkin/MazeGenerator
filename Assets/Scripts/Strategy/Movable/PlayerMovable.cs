﻿using System;
using Assets.Scripts.Helpers;
using Assets.Scripts.Shared;
using Assets.Scripts.Strategy.Interfaces;
using UnityEngine;
using System.Collections.Generic;
using AssemblyCSharp.Scripts.MainManager;
using System.Collections;

internal class PlayerMovable : BaseMovable
{
    private float journeyLength;
    private int _endPositionX;
    private int _endPositionY;
    private bool _isMoved = false;
    private IObjectTracker<BaseGameObject> _objectTrackLine;

    private List<BaseGameObject> _pathToMove;

    public PlayerMovable(BaseGameObject baseGameObject) : base(baseGameObject)
    {
        _objectTrackLine = ObjectTrackerLine.Instance;
        _pathToMove = new List<BaseGameObject>();
        _objectTrackLine.ObjectsTrackReleased += (sender, e) =>
        {
            _baseGameObject.OnChanged(EventMessages.EventMoveToPositionStart);
            _baseGameObject.OnChangedObject(EventMessages.EventMoveToPositionStart, _baseGameObject);
            //if (!_moveToPositionFlag && !_isMoved)
            {
                _pathToMove = _objectTrackLine.TrackListOfGameObjects;
                _moveToPositionFlag = true;
            }

        };
        baseGameObject.Changed += (sender, e) =>
        {
            if (e.Info == EventMessages.EventLevelComplete)
            {
                _pathToMove = new List<BaseGameObject>();
            }
        };

    }

    public override void CustomUpdate()
    {
        base.CustomUpdate();
        if (_moveToPositionFlag)
        {
            CheckMoveToPath();
            MoveToPosition();
        }
    }

    private void CheckMoveToPath()
    {
        if (_pathToMove.Count > 0 && !_isMoved)
        {
            _endPositionX = _pathToMove[0].Coordinates.X;
            _endPositionY = _pathToMove[0].Coordinates.Y;

        }
    }

    private void MoveToPosition()
    {
        if (CheckPositioning(_endPositionX, _endPositionY))
        {
            _baseGameObject.Coordinates = new Point(_endPositionX, _endPositionY);
            StopMove();
        }
        else if (_moveToPositionFlag)
        {
            _isMoved = true;
            var target = new Vector3(_endPositionX * BaseGameObject.ElementsDistance, _endPositionY * BaseGameObject.ElementsDistance, _baseGameObject.FixedZPosition);
            journeyLength = Vector3.Distance(_baseGameObject.transform.position, target);
            float distCovered = Time.deltaTime * _baseGameObject.Speed;
            float fracJourney = distCovered / journeyLength;
            _baseGameObject.transform.position = Vector3.Lerp(_baseGameObject.transform.position, target, fracJourney);
        }


    }

    private SideDirectional lastDirectional;

    private void StopMove()
    {
        if (_pathToMove.Count > 0)
        {
            _objectTrackLine.ClearStepFirst();
            _pathToMove.Remove(_pathToMove[0]);
            if (_pathToMove.Count > 0)
            {
                _isMoved = false;
                _baseGameObject.SetAnimationMove(GetDirectionToMove());
                if (GameManager<BaseGameObject>.Instance.LevelData.LevelNumber <= GameStorage.Instance.Data.MaxLevelForInfinityEnergy && _baseGameObject.Energy > 2)
                {
                    _baseGameObject.DamageObject(_baseGameObject.StepByEnergy);
                }
                else if (GameManager<BaseGameObject>.Instance.LevelData.LevelNumber > GameStorage.Instance.Data.MaxLevelForInfinityEnergy)
                {
                    _baseGameObject.DamageObject(_baseGameObject.StepByEnergy);
                }
                AudioManager.Instance.PlayGameAudioClip(SoundConstants.GameAudioClipType.PlayerStep);
            }
        }
        else
        {
            _baseGameObject.SetAnimationMove(SideDirectional.Undefined);
            _moveToPositionFlag = false;
            _isMoved = false;
            _objectTrackLine.Clear();
        }
        _baseGameObject.SetName();
        _baseGameObject.OnChanged(EventMessages.EventMoveToPositionEnd);
        _baseGameObject.OnChangedObject(EventMessages.EventMoveToPositionEnd, _baseGameObject);
    }



    private SideDirectional GetDirectionToMove()
    {
        Vector3 currentPosition = _baseGameObject.transform.position;
        Vector3 directPosition = new Vector3(_pathToMove[0].Coordinates.X * BaseGameObject.ElementsDistance, _pathToMove[0].Coordinates.Y * BaseGameObject.ElementsDistance, _baseGameObject.FixedZPosition);
        if (Math.Abs(currentPosition.x - directPosition.x) < 0.01f && Math.Abs(currentPosition.y - directPosition.y) > 0.01f)
        {
            if (currentPosition.y - directPosition.y > 0.01f)
            {
                return SideDirectional.Down;
            }
            else
            {
                return SideDirectional.Up;
            }
        }
        else
        {
            if (currentPosition.x - directPosition.x > 0.01f)
            {
                return SideDirectional.Left;
            }
            else
            {
                return SideDirectional.Right;
            }
        }

    }
}