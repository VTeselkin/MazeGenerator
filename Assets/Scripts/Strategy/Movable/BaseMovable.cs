﻿using System;
using System.Collections.Generic;
using Assets.Scripts.Helpers;
using Assets.Scripts.Strategy.Interfaces;
using UnityEngine;

public class BaseMovable : IMovable
{
    protected BaseGameObject _baseGameObject;
    protected bool PauseMove;
    protected Func<bool> _validationPositioningFunction;
    protected Action _validPositioningAction;
    protected SideDirectional Side = SideDirectional.Left;
    protected IObjectStorage<BaseGameObject> _objectStorage;
    protected Point PointToMove;
    protected bool _moveToPositionFlag;


    /// <summary>
    /// Gets a value indicating whether this <see cref="T:BaseMovable"/> is at moving.
    /// </summary>
    /// <value><c>true</c> if is at moving; otherwise, <c>false</c>.</value>
    public bool IsAtMoving { get { return true; } }

    /// <summary>
    /// Gets or sets the base object.
    /// </summary>
    /// <value>The base object.</value>
    public BaseGameObject BaseObject
    {
        get { return _baseGameObject; }
        set { _baseGameObject = value; }
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="T:BaseMovable"/> class.
    /// </summary>
    /// <param name="baseGameObject">Base game object.</param>
    public BaseMovable(BaseGameObject baseGameObject)
    {
        _baseGameObject = baseGameObject;
        _objectStorage = ObjectSuperVisor.Instance.ObjectStorage;
    }

    public virtual void CustomUpdate()
    {

    }

    public virtual void MoveToPosition(Point point)
    {
        PauseMove = false;
    }

    public virtual void PauseToMove(bool isPause, float deltaTime)
    {
        PauseMove = isPause;
    }

    public void SetObserve(Func<bool> validationFunction, Action validAction)
    {
        _validationPositioningFunction = validationFunction;
        _validPositioningAction = validAction;
    }

    protected virtual void CheckObserve()
    {
        if (_validationPositioningFunction != null && _validationPositioningFunction())
        {
            _validPositioningAction();
        }
    }

    protected SideDirectional ReverseDirection()
    {
        switch (Side)
        {
            case SideDirectional.Up:
                return SideDirectional.Down;

            case SideDirectional.Down:
                return SideDirectional.Up;

            case SideDirectional.Left:
                return SideDirectional.Right;

            case SideDirectional.Right:
                return SideDirectional.Left;

            case SideDirectional.Undefined:
                return SideDirectional.Undefined;
        }
        return SideDirectional.Undefined;
    }



    protected bool CheckAvalibleDirection(Point p)
    {
        if (!Helper.CheckPointInsideGrid(p, ObjectSuperVisor.Instance.GridDimension))
        {
            return false;
        }
        if (PauseMove && Helper.CheckPositionByPlayer(p))
        {
            return false;
        }
        return _objectStorage.GetGameFreeObjectByPosition(p) != null;
    }

    protected Point GetDirection()
    {
        List<SideDirectional> sides = new List<SideDirectional>();

        if (CheckAvalibleDirection(_baseGameObject.CoordinatesLeft))
        {
            sides.Add(SideDirectional.Left);
        }
        if (CheckAvalibleDirection(_baseGameObject.CoordinatesRight))
        {
            sides.Add(SideDirectional.Right);
        }
        if (CheckAvalibleDirection(_baseGameObject.CoordinatesUp))
        {
            sides.Add(SideDirectional.Up);
        }
        if (CheckAvalibleDirection(_baseGameObject.CoordinatesDown))
        {
            sides.Add(SideDirectional.Down);
        }
        if (sides.Count > 2)
        {
            sides.Remove(Side);
            if (Side == SideDirectional.Left)
            {
                sides.Remove(SideDirectional.Right);
            }
            else if (Side == SideDirectional.Right)
            {
                sides.Remove(SideDirectional.Left);
            }
            else if (Side == SideDirectional.Up)
            {
                sides.Remove(SideDirectional.Down);
            }
            else if (Side == SideDirectional.Down)
            {
                sides.Remove(SideDirectional.Up);
            }

            System.Random rand = new System.Random();
            var index = rand.Next(0, sides.Count);
            Side = sides[index];

            return Helper.GetNeighborsObject(_baseGameObject, Side);
        }

        if (sides.Count == 2)
        {
            if (Side == SideDirectional.Left || Side == SideDirectional.Right)
            {
                if (sides.Contains(SideDirectional.Up))
                {
                    Side = SideDirectional.Up;
                    return Helper.GetNeighborsObject(_baseGameObject, Side);
                }
                if (sides.Contains(SideDirectional.Down))
                {
                    Side = SideDirectional.Down;
                    return Helper.GetNeighborsObject(_baseGameObject, Side);
                }
            }
            if (Side == SideDirectional.Up || Side == SideDirectional.Down)
            {
                if (sides.Contains(SideDirectional.Left))
                {
                    Side = SideDirectional.Left;
                    return Helper.GetNeighborsObject(_baseGameObject, Side);
                }
                if (sides.Contains(SideDirectional.Right))
                {
                    Side = SideDirectional.Right;
                    return Helper.GetNeighborsObject(_baseGameObject, Side);
                }
            }
        }
        if (sides.Count == 1)
        {
            Side = sides[0];
        }
        if (sides.Count == 0)
        {
            return PointToMove;
        }

        return Helper.GetNeighborsObject(_baseGameObject, Side);
    }

    protected bool CheckPositioning(int endX, int endY)
    {
        if (_baseGameObject.transform.position == new Vector3(endX * BaseGameObject.ElementsDistance, endY * BaseGameObject.ElementsDistance, _baseGameObject.FixedZPosition))
        {
            return true;
        }
        return false;
    }
}
