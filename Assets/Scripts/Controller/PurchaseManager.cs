﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Purchasing;
using UnityEngine.Purchasing.Security;
using Assets.Scripts.Shared;

public class PurchaseManager : MonoBehaviour, IStoreListener
{

    #region Singleton

    private static PurchaseManager _instance;

    public static PurchaseManager Instance
    {
        get
        {
            if (_instance == null)
            {
                GameObject go = new GameObject("AdsWrapper");
                DontDestroyOnLoad(go);
                _instance = go.AddComponent<PurchaseManager>();

            }
            return _instance;
        }
    }



    private void Awake()
    {
        if (_instance == this)
        {
            DestroyImmediate(this.gameObject);
        }
        else
        {
            _instance = this;
        }

    }
    #endregion

    #region IAP


    public Action<bool, PurchaseType> Purchase;

    public enum PurchaseType
    {
        CoinsMini,
        CoinsMedium,
        CoinsHard,
        NoAds
    }

    private static IStoreController m_StoreController;          // The Unity Purchasing system.
    private static IExtensionProvider m_StoreExtensionProvider; // The store-specific Purchasing subsystems.
    public const string NoAdsGooglePlay = "gp_no_ads";
    public const string NoAdsIOS = "ios_no_ads";

    public const string CoinsMiniGooglePlay = "gp_coins_mini";
    public const string CoinsMediumGooglePlay = "gp_coins_medium";
    public const string CoinsHardGooglePlay = "gp_coins_hard";
    public const string CoinsMiniIOS = "ios_coins_mini";
    public const string CoinsMediumIOS = "ios_coins_medium";
    public const string CoinsHardIOS = "ios_coins_hard";

    public void Init()
    {
        var builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());

        builder.AddProduct(PurchaseType.CoinsMini.ToString(), ProductType.Consumable, new IDs{
            { CoinsMiniGooglePlay, GooglePlay.Name },
            { CoinsMiniIOS, AppleAppStore.Name }
        });

        builder.AddProduct(PurchaseType.CoinsMedium.ToString(), ProductType.Consumable, new IDs{
            { CoinsMediumGooglePlay, GooglePlay.Name },
            { CoinsMediumIOS, AppleAppStore.Name }
        });

        builder.AddProduct(PurchaseType.CoinsHard.ToString(), ProductType.Consumable, new IDs{
            { CoinsHardGooglePlay, GooglePlay.Name },
            { CoinsHardIOS, AppleAppStore.Name }
        });

        builder.AddProduct(PurchaseType.NoAds.ToString(), ProductType.NonConsumable, new IDs{
            { NoAdsGooglePlay, GooglePlay.Name },
            { NoAdsIOS, AppleAppStore.Name }
        });

        UnityPurchasing.Initialize(this, builder);
        if (!InternetReachabilityController.Instance.IsInternetAvalible)
        {
            InternetReachabilityController.Instance.SubscribeToInternetStatusChange(InternetStatusChange);
        }
    }

    private void InternetStatusChange(NetworkReachability state)
    {
        if (state != NetworkReachability.NotReachable)
        {
            if (!IsInitialized())
            {
                InternetReachabilityController.Instance.UnsubscribeFromInternetStatusChange(InternetStatusChange);
                Init();
            }
        }
    }

    private bool IsInitialized()
    {
        return m_StoreController != null && m_StoreExtensionProvider != null;
    }

    public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
    {
        m_StoreController = controller;
        m_StoreExtensionProvider = extensions;
    }

    public void OnInitializeFailed(InitializationFailureReason error)
    {
        DebugLogger.Log("OnPurchase : OnInitializeFailed InitializationFailureReason:" + error);
    }

    public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs args)
    {
        bool validPurchase = true; // Presume valid for platforms with no R.V.

        // Unity IAP's validation logic is only included on these platforms.
#if UNITY_ANDROID || UNITY_IOS || UNITY_STANDALONE_OSX
        // Prepare the validator with the secrets we prepared in the Editor
        // obfuscation window.
        var validator = new CrossPlatformValidator(GooglePlayTangle.Data(),
                                                   AppleTangle.Data(), Application.identifier);

        try
        {
            // On Google Play, result has a single product ID.
            // On Apple stores, receipts contain multiple products.
            var result = validator.Validate(args.purchasedProduct.receipt);

            PurchaseType purchaseType = (PurchaseType)Enum.Parse(typeof(PurchaseType), args.purchasedProduct.definition.id);
            Purchase.SafeInvoke(true, purchaseType);

            // For informational purposes, we list the receipt(s)
            DebugLogger.Log("OnPurchase : Receipt is valid. Contents:");
            foreach (IPurchaseReceipt productReceipt in result)
            {
                DebugLogger.Log(productReceipt.productID);
                DebugLogger.Log(productReceipt.purchaseDate);
                DebugLogger.Log(productReceipt.transactionID);
            }
        }
        catch (IAPSecurityException ex)
        {
            AnalyticsWrapper.Instance.EventPurchaseFail(ex.Message);
            DebugLogger.Log("OnPurchase : Invalid receipt, not unlocking content");
            validPurchase = false;
        }
#endif

        if (validPurchase)
        {
            AnalyticsWrapper.Instance.EventPurchaseSuccess(args.purchasedProduct);
            if (args.purchasedProduct.definition.id == PurchaseType.NoAds.ToString())
            {
                AppSettings.Instance.NoBanner = true;
            }
            if (args.purchasedProduct.definition.id == PurchaseType.CoinsMedium.ToString() || args.purchasedProduct.definition.id == PurchaseType.CoinsHard.ToString())
            {
                AppSettings.Instance.NoADS = true;
                AppSettings.Instance.NoBanner = true;
            }
        }

        return PurchaseProcessingResult.Complete;

    }

    public void OnPurchaseFailed(Product product, PurchaseFailureReason failureReason)
    {
        var reason = string.Format("OnPurchase: FAIL. Product: '{0}', PurchaseFailureReason: {1}", product.definition.storeSpecificId, failureReason);
        AnalyticsWrapper.Instance.EventPurchaseCancel(reason);
        DebugLogger.Log(reason);
    }

    public void BuyProduct(PurchaseType type)
    {
        try
        {
            if (IsInitialized())
            {
                Product product = m_StoreController.products.WithID(type.ToString());
                if (product != null && product.availableToPurchase)
                {
                    DebugLogger.Log(string.Format("OnPurchase : Purchasing product asychronously: '{0}'", product.definition.id));
                    m_StoreController.InitiatePurchase(product);
                }
                else
                {
                    DebugLogger.Log("OnPurchase : BuyProductID: FAIL. Not purchasing product, either is not found or is not available for purchase");
                }
            }
            else
            {
                DebugLogger.Log("OnPurchase : BuyProductID FAIL. Not initialized.");
            }
        }
        catch (Exception e)
        {
            var reason = "OnPurchase : BuyProductID: FAIL. Exception during purchase. " + e;
            AnalyticsWrapper.Instance.EventPurchaseCancel(reason);
            DebugLogger.Log(reason);
        }
    }
    public void RestorePurchases(Action<bool> handler)
    {
        if (!IsInitialized())
        {
            handler.SafeInvoke(false);
            DebugLogger.Log("OnPurchase : RestorePurchases FAIL. Not initialized.");
            return;
        }
        if (Application.platform == RuntimePlatform.IPhonePlayer || Application.platform == RuntimePlatform.OSXPlayer)
        {
            DebugLogger.Log("OnPurchase : RestorePurchases started ...");
            var apple = m_StoreExtensionProvider.GetExtension<IAppleExtensions>();
            apple.RestoreTransactions((result) =>
            {
                handler.SafeInvoke(result);
                DebugLogger.Log("OnPurchase : RestorePurchases continuing: " + result + ". If no further messages, no purchases available to restore.");
            });
        }
        else
        {
            handler.SafeInvoke(false);
            DebugLogger.Log("OnPurchase : RestorePurchases FAIL. Not supported on this platform. Current = " + Application.platform);
        }
    }


    #endregion
}
