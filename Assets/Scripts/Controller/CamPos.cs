﻿using Assets.Scripts.Shared;
using UnityEngine;
using System;
using Assets.Scripts.Helpers;

public class CamPos : MonoBehaviour
{
    [SerializeField] private UICamColor camColor;
    private LevelData _levelData;
    private Camera cam;
    private float speed = .2f;


    float horBound;
    float vertBound;

    float leftBound, rightBound, upBound, downBound;
    private float minX;
    private float maxX;
    private float minY;
    private float maxY;
    private Vector3 _toPosision;
    private Transform cameraTransform;
    private GameObject player;
    private bool isNeedMove = false;

    public void Init(LevelData levelData)
    {
        camColor.Init(levelData);
        _levelData = levelData;
        cam = GetComponent<Camera>();
        cam.orthographicSize += (UserStorage.Instance.DistanceBoost * GameStorage.Instance.Data.DistanceByIndex);
        var min = Mathf.Min(levelData.Height, levelData.Width);
        var size = (int)cam.orthographicSize;
        if (size * 2 > min)
        {
            cam.orthographicSize = min / 2;
        }
        cameraTransform = cam.transform;
        vertBound = cam.orthographicSize;
        horBound = cam.orthographicSize / ScreenHelper.Height * ScreenHelper.Width;

        leftBound = cameraTransform.position.x - horBound;
        upBound = cameraTransform.position.y + vertBound;

        rightBound = cameraTransform.position.x + horBound;
        downBound = cameraTransform.position.y - vertBound;

        minX = levelData.CoordinatesMin.X + Math.Abs(leftBound) - 0.5f;
        maxX = levelData.CoordinatesMax.X - Math.Abs(rightBound) + 0.5f;
        maxY = levelData.CoordinatesMax.Y - Math.Abs(upBound) + cam.orthographicSize / 5f;
        minY = levelData.CoordinatesMin.Y + Math.Abs(downBound) - cam.orthographicSize / 5f;
        DebugLogger.Log("CamPos minX = " + minX);
        DebugLogger.Log("CamPos maxX = " + maxX);
        DebugLogger.Log("CamPos maxY = " + maxY);
        DebugLogger.Log("CamPos minX = " + minY);
        if (!AppSettings.Instance.NoBanner && GameStorage.Instance.Data.IsBannerEnable)
        {
            minY = minY - 1.5f;
        }
        else
        {
            minY = minY - 0.5f;
        }
        cameraTransform.position = new Vector2(minX, minY);

    }

    private void Start()
    {
        AppSettings.Instance.NoADSChanged += ReInit;
    }
    private void ReInit()
    {
        minY = _levelData.CoordinatesMin.Y + Math.Abs(downBound);
        if (AppSettings.Instance.NoBanner || GameStorage.Instance.Data.IsBannerEnable)
        {
            minY = minY - 0.5f;
        }
    }

    private void OnDestroy()
    {
        AppSettings.Instance.NoADSChanged -= ReInit;
    }

    public void CustomUpdate()
    {
        var gameManager = GameManager<BaseGameObject>.Instance;
        if (gameManager != null)
        {
            player = gameManager.Player.gameObject;
        }
        if (gameManager != null && player != null)
        {
            _toPosision = player.transform.position;
            cameraTransform.position = _toPosision;
            //journeyLength = Vector3.Distance(cameraTransform.position, _toPosision);
            //float distCovered = (Time.time - startTime) * speed;
            //journeyLength = Vector3.Distance(cameraTransform.position, _toPosision);
            //float fracJourney = distCovered / journeyLength;
            //var pos = Vector3.Lerp(cameraTransform.position, _toPosision, fracJourney);
            //if (!float.IsNaN(pos.x) && !float.IsNaN(pos.y) && !float.IsNaN(pos.z))
            //{
            //	cameraTransform.position = pos;
            //}
            if (cameraTransform.position.x < minX)
            {
                cameraTransform.position = new Vector3(minX, cameraTransform.position.y, cameraTransform.position.z);
            }
            if (cameraTransform.position.x > maxX)
            {
                cameraTransform.position = new Vector3(maxX, cameraTransform.position.y, cameraTransform.position.z);
            }
            if (cameraTransform.position.y < minY)
            {
                cameraTransform.position = new Vector3(cameraTransform.position.x, minY, cameraTransform.position.z);
            }
            if (cameraTransform.position.y > maxY)
            {
                cameraTransform.position = new Vector3(cameraTransform.position.x, maxY, cameraTransform.position.z);
            }

            if (Vector3.Distance(cameraTransform.position, _toPosision) < 0.01f)
            {
                isNeedMove = false;
            }
        }
    }

    internal void PlayerChange(object sender, BaseGameObjectEventArgs e)
    {
        if (e.Info == EventMessages.EventMoveToPositionEnd)
        {
            //startTime = Time.time;
            //	_toPosision = new Vector2(e.GemObjectSender.CoordinatesFloat.X, e.GemObjectSender.CoordinatesFloat.Y);
            //journeyLength = Vector3.Distance(cameraTransform.position, _toPosision);
            isNeedMove = true;
        }

    }
}
