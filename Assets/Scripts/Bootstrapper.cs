﻿using UnityEngine;
using Assets.Scripts.Navigation;
using Assets.Scripts.Navigation.Scenes.Map;
using Assets.Scripts.Server;
using Assets.Scripts;
using System;
using System.Collections;
using Assets.Scripts.Shared;

public class Bootstrapper : MonoBehaviour
{
    [SerializeField] private GameObject _gdprPopup;
    [SerializeField] private UIButton _agreeButton;
    [SerializeField] private UISplashSpawner _spawner;
    // Use this for initialization
    void Start()
    {

        Screen.fullScreen = true;
        AppSettings.Instance.Initialize();
#if !UNITY_EDITOR
		if (AppSettings.Instance.GDPR)
		{
			StartInit();
		}
		else
		{
			_gdprPopup.SetActive(true);
			UIEventListener.Get(_agreeButton.gameObject).onClick += (go) =>
			{
                DebugLogger.Log("Bootstrapper : GDPR button click");
		        _gdprPopup.SetActive(false);
				AppSettings.Instance.GDPR = true;
				StartInit();
			};
		}
#else
        StartInit();
#endif
    }

    private void StartInit()
    {
        _spawner.StartSpawn();
        Subscribe();
        ExceptionHandler.Instance.Initialize();
        FirebaseWrapper.Instance.Init();
        AnalyticsWrapper.Instance.Init();
        LocalNotificationWrapper.Instance.Init();
        AudioManager.Instance.Initialize();
        PurchaseManager.Instance.Init();
        GameSparkManager.Instance.Init();
    }

    private void Subscribe()
    {
        GameSparkManager.Instance.InitComplete += GameSparkManager_InitComplete;
        GameStorage.Instance.CompleteLoad += GameStorage_CompleteLoad;
        LevelStorage.Instance.CompleteLoad += LevelStorage_CompleteLoad;
        UserStorage.Instance.CompleteLoad += UserStorage_CompleteLoad;
        LevelProgress.Instance.CompleteLoad += LevelProgress_CompleteLoad;

    }

    private void GameSparkManager_InitComplete(bool obj)
    {
        DebugLogger.Log("Bootstrapper : GameSparkManager_InitComplete");
        GameSparkManager.Instance.InitComplete -= GameSparkManager_InitComplete;
        GameStorage.Instance.Initialize();
    }

    private void GameStorage_CompleteLoad(bool obj)
    {
        DebugLogger.Log("Bootstrapper : GameStorage_CompleteLoad");
        GameStorage.Instance.CompleteLoad -= GameStorage_CompleteLoad;
        LevelStorage.Instance.Initialize();
    }

    private void LevelStorage_CompleteLoad(bool obj)
    {
        DebugLogger.Log("Bootstrapper : LevelStorage_CompleteLoad");
        LevelStorage.Instance.CompleteLoad -= LevelStorage_CompleteLoad;
        UserStorage.Instance.Initialize();
    }

    private void UserStorage_CompleteLoad(bool obj)
    {
        DebugLogger.Log("Bootstrapper : UserStorage_CompleteLoad");
        UserStorage.Instance.CompleteLoad -= UserStorage_CompleteLoad;
        FaceBookManager.Instance.Init();
        LevelProgress.Instance.Initialize();
    }

    private void LevelProgress_CompleteLoad(bool obj)
    {
        DebugLogger.Log("Bootstrapper : LevelProgress_CompleteLoad");
        LevelProgress.Instance.CompleteLoad -= LevelProgress_CompleteLoad;
        StartCoroutine(Delayed());
    }

    private IEnumerator Delayed()
    {
#if !UNITY_EDITOR
        yield return new WaitForSeconds(4f);
#endif
        DebugLogger.Log("Bootstrapper : NavigationService MenuNavigationArgs");
        NavigationService.Navigate(new MenuNavigationArgs());
        yield return null;
    }

}
