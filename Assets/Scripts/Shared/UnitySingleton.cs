﻿using UnityEngine;

namespace Assets.Scripts.Shared
{
    public class UnitySingleton<T> : MonoBehaviour where T : MonoBehaviour
    {
        protected static T instance = null;

        private static object lockObject = new object();

        public static T Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (lockObject)
                    {
                        if (instance == null)
                        {
                            GameObject gameObject = new GameObject("Singleton: " + (typeof(T).ToString()));
                            instance = gameObject.AddComponent<T>();
                        }
                    }
                }

                return instance;
            }
        }

        private void Awake()
        {
            //Check if there is an existing instance of this object
            if (instance)
            {
                // DebugLogger.Log("Singleton object destroyed: " + this.name);

                DestroyImmediate(gameObject); //Delete duplicate
            }
            else
            {
                instance = this.GetComponent<T>(); //Make this object the only instance
                                                   //   DebugLogger.Log("Singleton object created: " + this.name);
                LateAwake();
            }
        }

        private void OnDestroy()
        {
            //  DebugLogger.Log("Singleton object destroyed: " + this.name);
        }

        protected virtual void LateAwake()
        {
        }

        public virtual void Init(bool isDontDestroyOnLoad = true)
        {
            if (isDontDestroyOnLoad)
            {
                DontDestroyOnLoad(gameObject); //Set as do not destroy
            }
        }
    }
}