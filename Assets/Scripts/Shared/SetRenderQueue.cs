﻿using UnityEngine;
using System.Collections;
using Assets.Scripts.Shared;

[AddComponentMenu("Effects/SetRenderQueue")]
[RequireComponent(typeof(Renderer))]
public class SetRenderQueue : MonoBehaviour {

    [SerializeField]
    public int QueueValue = 0;

    private void Awake() {        
        RenderQueue = QueueValue;
    }

    public int RenderQueue {
        get {
            return GetComponent<Renderer>().sharedMaterial.renderQueue;
        }
        set {
            GetComponent<Renderer>().sharedMaterial.renderQueue = value;
        }
    }
}