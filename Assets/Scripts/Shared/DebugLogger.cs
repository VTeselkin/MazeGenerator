﻿using System;
using UnityEngine;


namespace Assets.Scripts.Shared
{
	public class DebugLogger
	{
		#region Fields

		static DebugLogger()
		{
#if UNITY_ANDROID
			GPPlatformBuildSettings currentSettings = Resources.Load<GPPlatformBuildSettings>("GPPlatformBuildSettings");
			IsDevelopmentBuild = currentSettings.Development;
#else
         IOSPlatformBuildSettings currentSettings = Resources.Load<IOSPlatformBuildSettings>("IOSPlatformBuildSettings");
         IsDevelopmentBuild = currentSettings.Development;
#endif
		}
		public static bool IsDevelopmentBuild = true;
		private static string TAG = "[ Maze ] ";
		#endregion

		#region Methods
		public static void Log(object message)
		{
			if (IsDevelopmentBuild)
				Debug.Log(TAG + message);
			//Crashlytics.Log(message.ToString());
		}

		public static void Log(object message, UnityEngine.Object obj)
		{
			if (IsDevelopmentBuild)
				Debug.Log(TAG + message, obj);
			//Crashlytics.Log(message.ToString());
		}

		public static void LogWarning(object message)
		{
			if (IsDevelopmentBuild)
				Debug.LogWarning(TAG + message);
			//Crashlytics.Log(message.ToString());
		}

		public static void LogWarning(object message, UnityEngine.Object obj)
		{
			if (IsDevelopmentBuild)
				Debug.LogWarning(TAG + message, obj);
			//Crashlytics.Log(message.ToString());
		}

		public static void LogError(object message)
		{
			if (IsDevelopmentBuild)
				Debug.LogError(TAG + message);
			//Crashlytics.Log(message.ToString());
		}

		public static void LogError(object message, UnityEngine.Object obj)
		{
			if (IsDevelopmentBuild)
				Debug.LogError(TAG + message, obj);
			//Crashlytics.Log(message.ToString());
		}

		public static void LogException(Exception exception)
		{
			if (IsDevelopmentBuild)
				Debug.LogException(exception);
			//Crashlytics.Log(exception.ToString());
		}

		public static void LogException(Exception exception, UnityEngine.Object obj)
		{
			if (IsDevelopmentBuild)
				Debug.LogException(exception, obj);
			//Crashlytics.Log(exception.ToString());
		}

		#endregion
	}
}