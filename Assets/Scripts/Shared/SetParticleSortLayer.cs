﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Shared {

    [AddComponentMenu("Effects/SetRenderQueue")]
    [RequireComponent(typeof(Renderer))]

    public class SetParticleSortLayer : MonoBehaviour {
        [SerializeField]
        public int QueueValue = 0;

        [SerializeField]
        public int LayerID = 1;

        private void Awake() {
            GetComponent<Renderer>().sortingLayerID = LayerID;
            RenderQueue = QueueValue;
        }

        public int RenderQueue {
            get {
                return GetComponent<Renderer>().sharedMaterial.renderQueue;
            }
            set {
                GetComponent<Renderer>().sharedMaterial.renderQueue = value;
            }
        }
    }
}
