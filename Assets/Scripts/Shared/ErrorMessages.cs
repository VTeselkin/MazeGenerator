using UnityEngine;
using System.Collections;

public static class ErrorMessages
{
    public const string ErrorInitObjectsStorage =
        "At first it requires to init ObjectStorage by InitInstance static method.";
    public const string ErrorTwoObjectsInTheSamePlace = "Two objects are in the same Position in Grid";
    public const string ErrorGameObjectNull = "Game Object is NULL";
    public const string ErrorInitGameManager =
            "At first it requires to init GameManager by InitInstance static method.";
}
