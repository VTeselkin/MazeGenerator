﻿using System.Globalization;
using System.Text;

public static class CryptoScript
{

	#region Methods

	/// <summary>
	/// Crypts string using UTF8 encoding.
	/// </summary>
	public static string Crypt(string text)
	{
		int len = text.Length;

		string x1 = "very";
		string x2 = "very_";
		string x3 = "hard";
		string x4 = "password";

		string additionalKey = ((10 * 12 / 2)).ToString(CultureInfo.InvariantCulture);

		string password = x1 + x2 + x2 + x1 + additionalKey + x3 + x4;

		return Algorithm(Encoding.UTF8.GetBytes(text),
			Encoding.UTF8.GetBytes(password), password.Length, Encoding.UTF8);
	}

	/// <summary>
	/// Crypts data using UTF8 encoding.
	/// </summary>
	public static string Crypt(byte[] bytes)
	{
		int len = bytes.Length;

		string x1 = "very";
		string x2 = "very_";
		string x3 = "hard";
		string x4 = "password";

		string additionalKey = ((10 * 12 / 2)).ToString(CultureInfo.InvariantCulture);

		string password = x1 + x2 + x2 + x1 + additionalKey + x3 + x4;

		return Algorithm(bytes, Encoding.UTF8.GetBytes(password), password.Length, Encoding.UTF8);
	}

	private static string Algorithm(byte[] text, byte[] password, int passworLength, Encoding encoding)
	{
		//var uuu = 1;

		byte[] result = new byte[text.Length];

		for (int i = 0; i < text.Length; i++)
		{
			result[i] = (byte)(text[i] ^ password[i % passworLength]);
		}

		return encoding.GetString(result, 0, result.Length);
	}

	/// <summary>
	/// Crypts data using specified encoding.
	/// </summary>
	public static string Crypt(string text, Encoding encoding)
	{
		int len = text.Length;

		string x1 = "very";
		string x2 = "very_";
		string x3 = "hard";
		string x4 = "password";

		string additionalKey = ((10 * 12 / 2)).ToString(CultureInfo.InvariantCulture);

		string password = x1 + x2 + x2 + x1 + additionalKey + x3 + x4;

		return Algorithm(encoding.GetBytes(text), encoding.GetBytes(password), password.Length, encoding);
	}

	/// <summary>
	/// Crypts data using specified encoding.
	/// </summary>
	public static string Crypt(byte[] bytes, Encoding encoding)
	{
		int len = bytes.Length;

		string x1 = "very";
		string x2 = "very_";
		string x3 = "hard";
		string x4 = "password";

		string additionalKey = ((10 * 12 / 2)).ToString(CultureInfo.InvariantCulture);

		string password = x1 + x2 + x2 + x1 + additionalKey + x3 + x4;

		return Algorithm(bytes, encoding.GetBytes(password), password.Length, encoding);
	}

	#endregion
}

