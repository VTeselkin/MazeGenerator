namespace Assets.Scripts.Shared
{
    /// <summary>
    /// Something like String resources for Event management
    /// </summary>
    public static class EventMessages
    {
        public const string EventAddGameObject = "Add Game Object Event";
        public const string EventRemoveGameObject = "Remove Game Object Event";
        public const string EventGameObjectDestroyed = "Game Object Destroyed Event";
        public const string EventMoveToPositionStart = "Move To Position Start";
        public const string EventMoveToPositionEnd = "Move To Position End";
        public const string EventJumpBeforeFalling = "Jump Before Falling";
        public const string EventGameObjectDamageStarted = "Game Object Damage processing started";
        public const string EventGameObjectDamageCompleted = "Game Object Damage processing completed";
        public const string EventGameObjectCollected = "Game Object collected Event";
        public const string EventStartMoveGameObject = "Start Move Game Object";
        public const string EventTrackStopWork = "Track Stop Work Event";
        public const string EventObjectRemoveFromTrack = "Object Removed from Track";
        //public const string EventObjectAddedToTrack = "Object Added To Track";
        public const string EventTrackStartWork = "Track Start Work Event";

        public const string EventCollisionPlayerByFinish = "Collision Player by Finish";
        public const string EventCollisionEnemyByPlayer = "Collision Enemy by Player";
        public const string EventCollisionEnemyByFinish = "Collision Enemy by Finish";
        public const string EventCollisionPlayerByFood = "Collision Player by Food";
        public const string EventCollisionEnemyByEnemy = "Collision Enemy by Enemy";

        public const string EventGameObjectIncrementStarted = "Game Object Increment processing started";
        public const string EventGameObjectIncrementFinished = "Game Object Increment processing finished";

        public const string EventGameManagerCreatLevel = "Game Manager Creat Level";
        public const string EventGameManagerCreatePlayer = "Game Manager Creat Player";
        public const string EventGameManagerCreatEnemy = "Game Manager Creat Enemy";
        public const string EventGameManagerInitComplete = "Game Manager Init Complete";


        public const string EventEnemySeePlayer = "Event Enemy See Player";
        public const string EventEnemyDontSeePlayer = "Event Enemy Don't See Player";

        public const string EventGameObjectChangeEnergyByStep = "Event GameObject Change Energy By Step";
        public const string EventGameObjectChangeEnergy = "Event GameObject Change Energy";
        public const string EventGameObjectChangeSpeed = "Event GameObject Change Speed";

        public const string EventLevelComplete = "Event Level Complete";
    }
}