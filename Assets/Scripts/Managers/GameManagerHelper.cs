﻿// NewMonoBehaviour.cs
// /Users/doc/mazegenerator/Assets/Scripts/Managers/NewMonoBehaviour.cs
// doc
// 
// 572018

using System;
using System.Collections.Generic;
using Assets.Scripts.Helpers;

public class GameManagerHelper<T> where T : BaseGameObject
{
    /// <summary>
    /// Gets the coordinate for target.
    /// </summary>
    /// <returns>The coordinate for target.</returns>
    /// <param name="baseGameObject">Base game object.</param>
    /// <param name="_currentLevelData">Current level data.</param>
    public static T GetCoordForTarget(T baseGameObject, LevelData _currentLevelData)
    {
        var scale = BaseGameObject.ElementsDistance;
        var distance = (_currentLevelData.PlayDimension.DimX * scale + _currentLevelData.PlayDimension.DimY * scale) / 3 - 1;
        return GetCoordForTarget(baseGameObject, distance);
    }

    public static T GetCoordForTargetByArea(T baseGameObject, LevelData _currentLevelData)
    {
        var scale = BaseGameObject.ElementsDistance;
        var distance = (_currentLevelData.PlayDimension.DimX * scale + _currentLevelData.PlayDimension.DimY * scale) / 3 - 1;
        return GetCoordForTarget(baseGameObject, distance);
    }

    public static T GetCoordForFarTarget(T baseGameObject, ObjectTypeEnum typeEnum, float distance)
    {
        var _storageInstance = ObjectsStorage<T>.Instance;
        return GetCoordForTarget(baseGameObject, distance);
    }

    public static T GetCoordForFarTarget(T baseGameObject, ObjectTypeEnum typeEnum, int countObjectDistane)
    {
        var _storageInstance = ObjectsStorage<T>.Instance;
        var distance = BaseGameObject.ElementsDistance * countObjectDistane;
        return GetCoordForTarget(baseGameObject, distance);
    }

    public static T GetCoordForNearTarget(T baseGameObject, ObjectTypeEnum typeEnum, float distance)
    {
        var _storageInstance = ObjectsStorage<T>.Instance;
        return GetCoordNearTarget(baseGameObject, distance);
    }

    public static T GetCoordForNearTarget(T baseGameObject, ObjectTypeEnum typeEnum, int countObjectDistane)
    {
        var _storageInstance = ObjectsStorage<T>.Instance;
        var distance = BaseGameObject.ElementsDistance * countObjectDistane;
        return GetCoordNearTarget(baseGameObject, distance);
    }

    private static T GetCoordForTarget(T baseGameObject, float minDistance)
    {
        var _storageInstance = ObjectsStorage<T>.Instance;
        var avaliblePosition = _storageInstance.GetListFreeGameObjectsByPositionWithMoreDistance(baseGameObject, minDistance);

        while (minDistance > 0 && avaliblePosition.Count == 0)
        {
            minDistance--;
            avaliblePosition = _storageInstance.GetListFreeGameObjectsByPositionWithMoreDistance(baseGameObject, minDistance);
        }
        return GetCoord(avaliblePosition);
    }

    private static T GetCoordNearTarget(T baseGameObject, float minDistance)
    {
        var _storageInstance = ObjectsStorage<T>.Instance;
        var avaliblePosition = _storageInstance.GetListFreeGameObjectsByPositionWithLessDistance(baseGameObject, minDistance);

        while (minDistance > 0 && avaliblePosition.Count == 0)
        {
            minDistance--;
            avaliblePosition = _storageInstance.GetListFreeGameObjectsByPositionWithLessDistance(baseGameObject, minDistance);
        }
        return GetCoord(avaliblePosition);
    }

    private static T GetCoord(List<T> avaliblePosition)
    {
        var objectStorage = ObjectsStorage<T>.Instance;
        List<T> actualPosition = new List<T>();
        foreach (var obj in avaliblePosition)
        {
            List<T> positions = objectStorage.GetNeighborsObjectByType(obj);
            if (positions.Count < 2)
            {
                actualPosition.Add(obj);
            }
        }
        if (actualPosition.Count > 0)
        {
            if (actualPosition.Count > 1)
            {
                System.Random rand = new System.Random(((int)DateTime.Now.Ticks));
                int index = rand.Next(0, actualPosition.Count - 1);
                return actualPosition[index];
            }
            else
            {
                return actualPosition[0];
            }
        }
        else
        {
            if (avaliblePosition.Count > 0)
            {
                if (avaliblePosition.Count > 1)
                {
                    System.Random rand = new System.Random(((int)DateTime.Now.Ticks));
                    int index = rand.Next(0, avaliblePosition.Count - 1);
                    return avaliblePosition[index];
                }
                else
                {
                    return avaliblePosition[0];
                }
            }
            else
            {
                return null;
            }
        }
    }

}
