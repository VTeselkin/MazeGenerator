﻿
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.Navigation;
using Assets.Scripts.Navigation.Scenes;
using System;
using Random = UnityEngine.Random;
using Assets.Scripts.Shared;
using Assets.Scripts.Helpers;

public class AudioManager : MonoBehaviour
{
    #region Singleton

    private static AudioManager _instance;

    public static AudioManager Instance
    {
        get
        {
            if (_instance == null)
            {
                GameObject go = new GameObject("AudioManager");
                DontDestroyOnLoad(go);
                _instance = go.AddComponent<AudioManager>();

            }
            return _instance;
        }
    }


    private void Awake()
    {
        if (_instance == this)
        {
            DestroyImmediate(this.gameObject);
        }
        else
        {
            _instance = this;
        }


    }
    #endregion

    #region Fields

    private AudioSource _soundAudioSource;
    private AudioSource _musicAudioSource;

    private bool _isMusicOn;
    private bool _isSoundOn;

    #region Sounds

    private AudioClip SoundMenu;

    private Dictionary<string, AudioClip> _soundsStorage = new Dictionary<string, AudioClip>();
    private bool _bgMusicPaused;

    #endregion

    #endregion

    #region Properties

    public float SoundClipLength
    {
        get
        {
            if (_soundAudioSource.clip != null)
            {
                return _soundAudioSource.clip.length;
            }
            return 0;
        }
    }

    public float MusicClipLength
    {
        get
        {
            if (_musicAudioSource.clip != null)
            {
                return _musicAudioSource.clip.length;
            }
            return 0;
        }
    }

    public bool IsMusicOn
    {
        get { return _isMusicOn; }
        set
        {
            if (_isMusicOn == value)
            {
                return;
            }

            _isMusicOn = value;

            if (_isMusicOn)
            {
                ChooseBackClip();
            }
            else
            {
                _musicAudioSource.Stop();
            }
        }
    }

    public bool IsSoundOn
    {
        get { return _isSoundOn; }
        set
        {
            if (_isSoundOn == value)
                return;

            _isSoundOn = value;

            if (_isSoundOn)
                NGUITools.soundVolume = 1.0f;
            else
                NGUITools.soundVolume = 0.0f;
        }
    }

    #endregion

    #region Init

    public void Initialize()
    {
        _soundAudioSource = gameObject.AddComponent<AudioSource>();
        _musicAudioSource = gameObject.AddComponent<AudioSource>();
        _musicAudioSource.volume = 0.5f;

        InitAudioClips();

        IsSoundOn = AppSettings.Instance.IsSoundsOn;
        IsMusicOn = AppSettings.Instance.IsMusicOn;

        _soundsStorage = new Dictionary<string, AudioClip>();

        InitEventsSubscriptions();
    }

    private void InitEventsSubscriptions()
    {
        AppSettings.Instance.SoundsChanged += SoundOnChange;
        AppSettings.Instance.MusicChanged += MusicOnChange;
    }

    #endregion


    #region Public methods

    public void Unload()
    {
        foreach (var audioClip in _soundsStorage)
        {
            Resources.UnloadAsset(audioClip.Value);
        }
        _soundsStorage.Clear();
        _bgMusicPaused = false;
    }
    public void ChangeScene()
    {
        _musicAudioSource.Stop();
    }
    public void PauseBackgroundMusic(bool pause)
    {
        if (!IsMusicOn)
        {
            return;
        }

        _bgMusicPaused = pause;

        if (pause)
        {
            _musicAudioSource.Stop();
        }
        else
        {
            _musicAudioSource.Play();
        }
    }

    public void StopPlayingSound()
    {
        _soundAudioSource.Stop();
    }

    public void PlaySoundDamage(ObjectTypeEnum type, object specialParam = null)
    {
        switch (type)
        {
            case ObjectTypeEnum.Finish:
            case ObjectTypeEnum.Food:
                PlaySoundGemDestroy(type);
                break;
            case ObjectTypeEnum.Player:
            case ObjectTypeEnum.Undefined:
            case ObjectTypeEnum.Wall:
            case ObjectTypeEnum.Free:
            case ObjectTypeEnum.EnemySimple:
            case ObjectTypeEnum.EnemyZombie:
            case ObjectTypeEnum.EnemySee:
            case ObjectTypeEnum.EnemySeeZombie:
            case ObjectTypeEnum.EnemySeeBat:
            case ObjectTypeEnum.EnemySnailGreen:
                PlaySoundDamageObjects(type);
                break;
        }
    }

    public void PlaySoundDamage(BaseGameObject gameObject, object specialParam = null)
    {
        var type = gameObject.ObjectType;
        switch (type)
        {
            case ObjectTypeEnum.Finish:
            case ObjectTypeEnum.Food:
                PlaySoundGemDestroy(type);
                break;
            case ObjectTypeEnum.Player:
            case ObjectTypeEnum.Undefined:
            case ObjectTypeEnum.Wall:
            case ObjectTypeEnum.Free:
            case ObjectTypeEnum.EnemySimple:
            case ObjectTypeEnum.EnemyZombie:
            case ObjectTypeEnum.EnemySee:
            case ObjectTypeEnum.EnemySeeZombie:
            case ObjectTypeEnum.EnemySeeBat:
            case ObjectTypeEnum.EnemySnailGreen:
                PlaySoundDamageObjects(type);
                break;
        }
    }

    public AudioClip PlayGameAudioClip(SoundConstants.GameAudioClipType type)
    {
        return PlaySound(SoundConstants.ListSoundGame[type]);
    }

    public AudioClip PlaySoundUi(SoundConstants.UIAudioClipTypes type, object specialParam = null)
    {
        switch (type)
        {
            case SoundConstants.UIAudioClipTypes.Star1:
            case SoundConstants.UIAudioClipTypes.Star2:
            case SoundConstants.UIAudioClipTypes.Star3:
                return PlayUiAudioClip(GetStarTypeByNumber((int)specialParam));
            default:
                return PlayUiAudioClip(type);
        }
    }


    public static SoundConstants.UIAudioClipTypes GetStarTypeByNumber(int number)
    {
        switch (number)
        {
            case 0:
                return SoundConstants.UIAudioClipTypes.Star1;
            case 1:
                return SoundConstants.UIAudioClipTypes.Star2;
            case 2:
                return SoundConstants.UIAudioClipTypes.Star3;
            default:
                return SoundConstants.UIAudioClipTypes.Star1;
        }
    }

    public void PlaySoundAddToTrack(BaseGameObject gameObject, int objectsInTrack)
    {
        if (objectsInTrack >= SoundConstants.ListSoundPathTrackSelect.Count)
        {
            objectsInTrack = SoundConstants.ListSoundPathTrackSelect.Count - 1;
        }
        PlaySound(SoundConstants.ListSoundPathTrackSelect[objectsInTrack]);
    }

    public void PlaySoundRemoveFromTrack(int objectsInTrack)
    {
        if (objectsInTrack >= SoundConstants.ListSoundPathTrackSelect.Count)
        {
            objectsInTrack = SoundConstants.ListSoundPathTrackSelect.Count - 1;
        }
        PlaySound(SoundConstants.ListSoundPathTrackSelect[objectsInTrack - 1]);
    }


    #endregion

    #region Private methods

    private void ChooseBackClip()
    {
        AudioClip clip = null;
        switch (NavigationService.CurrentSceneType)
        {
            case SceneType.Menu:
                if (SoundMenu == null)
                {
                    InitAudioClips();
                }
                clip = SoundMenu;
                break;
            case SceneType.Game:
                clip = GetLocationBackClip();
                break;
            default:
                clip = null;
                break;
        }

        PlayBackClip(clip);
    }

    private void InitAudioClips()
    {
        //SoundMenu =
        //AssetBundleLoader.Instance.LoadFromBundle<AudioClip>(SoundConstants.MusicPath +
        //SoundConstants.SoundPathMenu);
        if (SoundMenu == null)
            SoundMenu = Resources.Load<AudioClip>(SoundConstants.MusicLocalPath + SoundConstants.SoundPathMenu);
    }

    private AudioClip GetLocationBackClip()
    {
        AudioClip clip = null;
        try
        {
            var maxClips = SoundConstants.LocationMusic.Count;
            var currentLevel = UserStorage.Instance.CurrentSelectLevel;
            var index = (currentLevel - (int)(currentLevel / maxClips) * maxClips);
            var locationClips = SoundConstants.LocationMusic[index];
            clip = GetSound(locationClips);
        }
        catch (Exception e)
        {
            DebugLogger.LogError(e.Message);
        }

        return clip;
    }

    private void PlayBackClip(AudioClip clip)
    {
        if (IsMusicOn && clip != null && !_musicAudioSource.isPlaying && !_bgMusicPaused)
        {
            _musicAudioSource.clip = clip;
            _musicAudioSource.Play();
        }
    }

    private AudioClip GetSound(string soundPath)
    {
        if (!_soundsStorage.ContainsKey(soundPath))
        {
            //_soundsStorage[soundPath] =
            //AssetBundleLoader.Instance.LoadFromBundle<AudioClip>(SoundConstants.MusicPath + soundPath);
            //if (_soundsStorage[soundPath] == null)
            _soundsStorage[soundPath] = Resources.Load<AudioClip>(SoundConstants.MusicLocalPath + soundPath);
        }

        return _soundsStorage[soundPath];
    }



    private void PlaySoundDamageObjects(ObjectTypeEnum type)
    {
        PlaySound(SoundConstants.ListSoundPathObjectsDamage[type]);
    }
    private void PlaySoundGemDestroy(ObjectTypeEnum type)
    {
        PlaySound(SoundConstants.ListSoundPathObjectsDestroy[type]);
    }
    private AudioClip PlaySound(string soundPath)
    {
        if (!_soundsStorage.ContainsKey(soundPath))
        {
            //_soundsStorage[soundPath] =
            //AssetBundleLoader.Instance.LoadFromBundle<AudioClip>(SoundConstants.MusicPath + soundPath);
            //if (_soundsStorage[soundPath] == null)
            _soundsStorage[soundPath] = Resources.Load<AudioClip>(SoundConstants.MusicLocalPath + soundPath);
        }

        var sound = _soundsStorage[soundPath];
        if (IsSoundOn && sound)
        {
            _soundAudioSource.PlayOneShot(sound);
        }

        return sound;
    }
    private AudioClip PlayUiAudioClip(SoundConstants.UIAudioClipTypes type)
    {
        return PlaySound(SoundConstants.ListSoundUi[type]);
    }

    private void SoundOnChange()
    {
        IsSoundOn = AppSettings.Instance.IsSoundsOn;
    }

    private void MusicOnChange()
    {
        IsMusicOn = AppSettings.Instance.IsMusicOn;
    }
    private void Update()
    {
        if (IsMusicOn && !_musicAudioSource.isPlaying)
        {
            ChooseBackClip();
        }
    }
    #endregion
}
