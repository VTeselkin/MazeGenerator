﻿using System;
using System.Linq;
using Assets.Scripts.Helpers;
using Assets.Scripts.Game;
using Assets.Scripts.Strategy.Interfaces;
using Assets.Scripts.Shared;
using Assets.Scripts.Navigation;
using Assets.Scripts.Navigation.Scenes.Map;
using System.Collections.Generic;

public class GameManager<T> where T : BaseGameObject
{

    private LevelData _currentLevelData;
    private PoolManager _poolManager;
    private InputHandler _inputHandler;
    private IObjectStorage<T> _storageInstance;
    private static GameManager<T> _instance;
    private IObjectSuperVisor _objectSuperVisor;

    private int _levelNumber;
    private Player _player;


    public static GameManager<T> Instance
    {
        get
        {
            if (_instance == null)
                return null;
            return _instance;
        }
    }
    public LevelData LevelData { get { return _currentLevelData; } }

    public Player Player { get { return _player; } }



    public static GameManager<T> InitInstance(int levelNumber)
    {
        if (_instance == null)
        {
            _instance = new GameManager<T>(levelNumber);
        }
        return _instance;
    }

    public void StartToPlay()
    {
        HintManager.Instance.StartHintManager();
        OnChanged(EventMessages.EventGameManagerInitComplete);
        InputHandler.Instance.IsFreezeInput = false;
        InputHandler.BlockInput = false;
        AnalyticsWrapper.Instance.EventLevelStart(_currentLevelData);
    }

    public void RestartGame()
    {
        NavigationService.Navigate(new GameNavigationArgs(_levelNumber, true));
    }

    public void FinishGame()
    {
        NavigationService.Navigate(new MenuNavigationArgs(false));
    }

    public void StartNextLevel()
    {
        NavigationService.Navigate(new MenuNavigationArgs(true));
    }

    private GameManager(int levelNumber)
    {
        _levelNumber = levelNumber;
        _currentLevelData = LevelLoader.LoadLevel(levelNumber);
        _poolManager = PoolManager.Instance;
        _inputHandler = InputHandler.Instance;
        var _dimension = new GridDimension(_currentLevelData.PlayDimension.DimX, _currentLevelData.PlayDimension.DimY);
        ObjectsStorage<T>.InitInstance(_dimension);
        _storageInstance = ObjectsStorage<T>.Instance;
        _objectSuperVisor =
               ObjectSuperVisor.InitInstance((IObjectStorage<BaseGameObject>)_storageInstance, _currentLevelData);

        FillGridByObjects(_currentLevelData);
        _objectSuperVisor.PreUpdateManage();

    }

    private void FillGridByObjects(LevelData levelData)
    {
        ClearGrid();
        T player;
        FillGridByObjectsFromGenerator(levelData);
        OnChanged(EventMessages.EventGameManagerCreatLevel);
        FillGridByPlayerObject(levelData, out player);
        OnChanged(EventMessages.EventGameManagerCreatePlayer);
        FillGridByObjectsTarget(player, levelData);
        FilGridObjectFood(levelData);
        FillGridByMovedObject(levelData);
        FillGridByObjectsDecor(levelData);
        OnChanged(EventMessages.EventGameManagerCreatEnemy);

    }

    private void ClearGrid()
    {
        var _gridDimension = _storageInstance.Dimension();
        if (_storageInstance.GetTotalCount() != 0)
        {
            for (var i = 0; i < _gridDimension.DimX; i++)
            {
                for (var j = 0; j < _gridDimension.DimY; j++)
                {
                    var currentPosition = new Point { X = i, Y = j };
                    var existingObject = _storageInstance.GetGameObjectByPosition(currentPosition);
                    _storageInstance.RemoveGameObject(existingObject);
                    existingObject.DestroyObject();
                }
            }
        }
    }

    private void FillGridByObjectsFromGenerator(LevelData levelData)
    {
        for (var i = 0; i < levelData.PlayDimension.DimX; i++)
        {
            for (var j = 0; j < levelData.PlayDimension.DimY; j++)
            {
                var currentPosition = new Point { X = i, Y = j };
                var existingObject = _storageInstance.GetGameObjectByPosition(currentPosition);
                if (existingObject != null)
                {
                    _storageInstance.RemoveGameObject(existingObject);
                    existingObject.DestroyObject();
                }

                var cellInfo = levelData.Map[i, j];
                var position = new Point { X = i, Y = j };
                T baseGameObject;
                _storageInstance.AddGameObject((ObjectTypeEnum)cellInfo, position, out baseGameObject);
                DataInfo data = new DataInfo(levelData.ViewIndexFree, levelData.ViewIndexWall);
                baseGameObject.Initialize(data);
            }
        }
    }

    private void FillGridByObjectsDecor(LevelData levelData)
    {

        for (var i = 0; i < levelData.PlayDimension.DimX; i++)
        {
            for (var j = -2; j < 0; j++)
            {
                var cellInfo = levelData.Map[0, 0];
                var position = new Point { X = i, Y = j };
                T baseGameObject;
                _storageInstance.AddGameObject((ObjectTypeEnum)cellInfo, position, out baseGameObject);
                DataInfo data = new DataInfo(levelData.ViewIndexFree, levelData.ViewIndexWall);
                baseGameObject.Initialize(data);
            }
        }
        for (var i = 0; i < levelData.PlayDimension.DimX; i++)
        {
            for (var j = levelData.PlayDimension.DimY; j < levelData.PlayDimension.DimY + 2; j++)
            {
                var cellInfo = levelData.Map[0, 0];
                var position = new Point { X = i, Y = j };
                T baseGameObject;
                _storageInstance.AddGameObject((ObjectTypeEnum)cellInfo, position, out baseGameObject);
                DataInfo data = new DataInfo(levelData.ViewIndexFree, levelData.ViewIndexWall);
                baseGameObject.Initialize(data);
            }
        }
    }

    private void FillGridByPlayerObject(LevelData levelData, out T baseGameObject)
    {
        baseGameObject = null;
        for (var i = 0; i < levelData.PlayDimension.DimX; i++)
        {
            for (var j = 0; j < levelData.PlayDimension.DimY; j++)
            {
                var position = new Point { X = i, Y = j };
                var freeObject = _storageInstance.GetGameObjectByPosition(position);
                if (freeObject != null && freeObject.ObjectType == ObjectTypeEnum.Free)
                {
                    _storageInstance.AddGameObject(ObjectTypeEnum.Player, position, out baseGameObject);
                    baseGameObject.Initialize(null);
                    _player = baseGameObject as Player;
                    return;
                }
            }
        }
    }

    private void FillGridByObjectsTarget(T player, LevelData levelData)
    {
        if (_currentLevelData.LevelTarget != LevelTargetEnum.Collect)
        {
            var target = GameManagerHelper<T>.GetCoordForTarget(player, levelData);
            var position = new Point { X = target.CoordinateX, Y = target.CoordinateY };
            T baseGameObject;
            _storageInstance.AddGameObject(ObjectTypeEnum.Finish, position, out baseGameObject);
            var finishInfo = new FinishInfo(levelData);
            baseGameObject.Initialize(finishInfo);
        }
    }

    private void FilGridObjectFood(LevelData levelData)
    {
        foreach (var food in levelData.Foods)
        {
            var freeObjects = _storageInstance.GetListOfObjectsByTypeInArea(ObjectTypeEnum.Free, new AreaDimension(1, 1, levelData.CoordinatesMax.X - 1, levelData.CoordinatesMax.Y - 1));
            System.Random rand = new System.Random(((int)DateTime.Now.Ticks));
            int index = rand.Next(0, freeObjects.Count() - 1);
            var position = freeObjects[index].Coordinates;
            T baseGameObject;
            _storageInstance.AddGameObject(ObjectTypeEnum.Food, position, out baseGameObject);
            var foodInfo = new FoodInfo(food.Type, food.Energy);

            foodInfo.MaxEnergyForBoost = levelData.PlayerEnergy / 3;
            baseGameObject.Initialize(foodInfo);
        }
    }

    private void FillGridByMovedObject(LevelData levelData)
    {
        int nearDistance = 2;
        var foods = _objectSuperVisor.ObjectStorage.GetListOfObjectsByType(ObjectTypeEnum.Food);
        var finish = _objectSuperVisor.ObjectStorage.GetListOfObjectsByType(ObjectTypeEnum.Finish);
        var player = _objectSuperVisor.ObjectStorage.GetListOfObjectsByType(ObjectTypeEnum.Player);
        bool isBatSetFinish = false;
        int BatSetFoodCount = 0;
        for (int i = 0; i < levelData.Enemies.Count; i++)
        {
            var freeObjects = _storageInstance.GetListOfObjectsByType(ObjectTypeEnum.Free);
            System.Random rand = new System.Random(((int)DateTime.Now.Ticks));
            int index = rand.Next(0, freeObjects.Count() - 1);
            var position = freeObjects[index].Coordinates;
            T baseGameObject;
            T target = null;
            switch (levelData.Enemies[i].Type)
            {
                case ObjectTypeEnum.EnemySimple:
                case ObjectTypeEnum.EnemyZombie:
                case ObjectTypeEnum.EnemySee:
                case ObjectTypeEnum.EnemySeeZombie:
                case ObjectTypeEnum.EnemySnailGreen:
                    target = GameManagerHelper<T>.GetCoordForTarget((T)player[0], levelData);
                    break;
                case ObjectTypeEnum.EnemySeeBat:
                    if (!isBatSetFinish && finish != null && finish.Count > 0)
                    {
                        isBatSetFinish = true;
                        target = GameManagerHelper<T>.GetCoordForNearTarget((T)finish[0], ObjectTypeEnum.Finish, nearDistance);
                    }
                    else
                    {
                        if (foods != null && foods.Count > 0 && BatSetFoodCount < foods.Count)
                        {
                            target = GameManagerHelper<T>.GetCoordForNearTarget((T)foods[BatSetFoodCount], ObjectTypeEnum.Food, nearDistance);
                            BatSetFoodCount++;
                        }
                        else
                        {
                            target = null;
                        }
                    }
                    break;

            }
            if (target != null)
            {
                position = target.Coordinates;
            }
            _storageInstance.AddGameObject(levelData.Enemies[i].Type, position, out baseGameObject);
            var enemyInfo = new EnemyInfo(levelData.Enemies[i].Energy);
            baseGameObject.Initialize(enemyInfo);
        }



    }

    public void OnDestroy()
    {
        _instance = null;
        _storageInstance.DestroyInstance();
        _currentLevelData = null;

    }

    public event EventHandler<EventArgsSimple> Changed;

    public void OnChanged(string eventInfo)
    {
        EventHandler<EventArgsSimple> handler = Changed;
        if (handler != null)
        {
            var args = new EventArgsSimple { Info = eventInfo };
            // Raise custom game object event with specific info
            handler(this, args);
        }
    }
}

