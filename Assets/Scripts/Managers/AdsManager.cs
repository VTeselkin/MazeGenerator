﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Advertisements;
using UnityEngine.Monetization;
using ShowResult = UnityEngine.Advertisements.ShowResult;

namespace Managers
{
    public class AdsManager : MonoBehaviour
    {
        public static AdsManager Instance;

        private Action<UnityEngine.Monetization.ShowResult> _callback;

        private void Awake()
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
#if UNITY_EDITOR
        private string gameId = "";
#elif UNITY_IOS
        private string gameId = "3265466";
#elif UNITY_ANDROID
    private string gameId = "3265467";
#endif
        private void Start()
        {
            Monetization.Initialize(gameId, Debug.isDebugBuild);
        }

        public void ShowVideo()
        {
            StartCoroutine(ShowAdWhenReady("video", false));
        }

        public void ShowRewarded(Action<UnityEngine.Monetization.ShowResult> callback)
        {
            _callback = callback;
            StartCoroutine(ShowAdWhenReady("rewardedVideo", true));
        }

        private IEnumerator ShowAdWhenReady(string placementId, bool rewarded)
        {
            while (!Monetization.IsReady(placementId))
            {
                yield return new WaitForSeconds(0.25f);
            }

            var ad = Monetization.GetPlacementContent(placementId) as ShowAdPlacementContent;
            if (!rewarded)
            {
                ad?.Show();
            }
            else
            {
                ad?.Show(AdFinished);
            }
        }

        public void ShowBanner()
        {
            StartCoroutine(ShowBannerWhenReady("banner"));
        }

        public void HideBanner()
        {
            Advertisement.Banner.Hide();
        }

        public bool ShowVideoReady()
        {
            return isReady("video");
        }

        public bool RewardedReady()
        {
            return isReady("rewardedVideo");
        }

        private bool isReady(string placement)
        {
            return !Monetization.IsReady(placement);
        }

        void AdFinished(UnityEngine.Monetization.ShowResult finishState)
        {
            _callback(finishState);
        }

        IEnumerator ShowBannerWhenReady(string placementId)
        {
            while (!Advertisement.IsReady(placementId))
            {
                yield return new WaitForSeconds(0.5f);
            }

            Advertisement.Banner.Show(placementId);
        }

        public enum AdType
        {
            video,
            rewardedVideo
        }
    }
}