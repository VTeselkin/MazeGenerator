using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.Helpers;
using Assets.Scripts.Shared;

public class PoolManager : MonoBehaviour
{
    private Dictionary<ResourceIdentifier, GameObject> _storageOfInstances;
    //private Dictionary<ObjectTypeEnum, List<BaseGameObject>> _poolObjectsStorage;
    private static PoolManager _instance;

    #region Game objects
    private GameObject _wall;
    private GameObject _target;
    private GameObject _player;
    private GameObject _enemy;
    private GameObject _enemyZombie;
    private GameObject _free;
    private GameObject _food;
    private GameObject _enemySee;
    private GameObject _enemySeeZombie;
    private GameObject _enemySeeBat;
    private GameObject _enemySnailGreen;
    private GameObject _scorerText;
    private static GameObject go;
    #endregion

    private void InitStorageDictionary()
    {
        _storageOfInstances = new Dictionary<ResourceIdentifier, GameObject>();

        ResourceIdentifier powerUpResourceId = new ResourceIdentifier(ObjectTypeEnum.Wall);
        string powerUpResourceUri = Strings.GetUriResource(powerUpResourceId);
        _wall = ResourceHelper.Load(powerUpResourceUri) as GameObject;
        _storageOfInstances[powerUpResourceId] = _wall;

        powerUpResourceId = new ResourceIdentifier(ObjectTypeEnum.Finish);
        powerUpResourceUri = Strings.GetUriResource(powerUpResourceId);
        _target = ResourceHelper.Load(powerUpResourceUri) as GameObject;
        _storageOfInstances[powerUpResourceId] = _target;

        powerUpResourceId = new ResourceIdentifier(ObjectTypeEnum.Player);
        powerUpResourceUri = Strings.GetUriResource(powerUpResourceId);
        _player = ResourceHelper.Load(powerUpResourceUri) as GameObject;
        _storageOfInstances[powerUpResourceId] = _player;

        powerUpResourceId = new ResourceIdentifier(ObjectTypeEnum.EnemySimple);
        powerUpResourceUri = Strings.GetUriResource(powerUpResourceId);
        _enemy = ResourceHelper.Load(powerUpResourceUri) as GameObject;
        _storageOfInstances[powerUpResourceId] = _enemy;

        powerUpResourceId = new ResourceIdentifier(ObjectTypeEnum.EnemyZombie);
        powerUpResourceUri = Strings.GetUriResource(powerUpResourceId);
        _enemyZombie = ResourceHelper.Load(powerUpResourceUri) as GameObject;
        _storageOfInstances[powerUpResourceId] = _enemyZombie;

        powerUpResourceId = new ResourceIdentifier(ObjectTypeEnum.Free);
        powerUpResourceUri = Strings.GetUriResource(powerUpResourceId);
        _free = ResourceHelper.Load(powerUpResourceUri) as GameObject;
        _storageOfInstances[powerUpResourceId] = _free;

        powerUpResourceId = new ResourceIdentifier(ObjectTypeEnum.Food);
        powerUpResourceUri = Strings.GetUriResource(powerUpResourceId);
        _food = ResourceHelper.Load(powerUpResourceUri) as GameObject;
        _storageOfInstances[powerUpResourceId] = _food;

        powerUpResourceId = new ResourceIdentifier(ObjectTypeEnum.EnemySee);
        powerUpResourceUri = Strings.GetUriResource(powerUpResourceId);
        _enemySee = ResourceHelper.Load(powerUpResourceUri) as GameObject;
        _storageOfInstances[powerUpResourceId] = _enemySee;

        powerUpResourceId = new ResourceIdentifier(ObjectTypeEnum.EnemySeeZombie);
        powerUpResourceUri = Strings.GetUriResource(powerUpResourceId);
        _enemySeeZombie = ResourceHelper.Load(powerUpResourceUri) as GameObject;
        _storageOfInstances[powerUpResourceId] = _enemySeeZombie;

        powerUpResourceId = new ResourceIdentifier(ObjectTypeEnum.EnemySeeBat);
        powerUpResourceUri = Strings.GetUriResource(powerUpResourceId);
        _enemySeeBat = ResourceHelper.Load(powerUpResourceUri) as GameObject;
        _storageOfInstances[powerUpResourceId] = _enemySeeBat;

        powerUpResourceId = new ResourceIdentifier(ObjectTypeEnum.EnemySnailGreen);
        powerUpResourceUri = Strings.GetUriResource(powerUpResourceId);
        _enemySnailGreen = ResourceHelper.Load(powerUpResourceUri) as GameObject;
        _storageOfInstances[powerUpResourceId] = _enemySnailGreen;

        powerUpResourceId = new ResourceIdentifier(ObjectTypeEnum.ScoreText);
        powerUpResourceUri = Strings.GetUriResource(powerUpResourceId);
        _scorerText = ResourceHelper.Load(powerUpResourceUri) as GameObject;
        _storageOfInstances[powerUpResourceId] = _scorerText;

    }

    public static PoolManager Instance
    {
        get
        {
            if (_instance == null)
            {
                go = new GameObject("PoolManager");
                _instance = go.AddComponent<PoolManager>();
                _instance.transform.parent = NGUITools.GetRoot(Camera.main.gameObject).transform;
            }
            return _instance;
        }
    }

    private void Awake()
    {
        if (_instance == this)
        {
            DestroyImmediate(this.gameObject);
        }
        else
        {
            _instance = this;
        }

        InitStorageDictionary();
    }

    public void DestroyInstance()
    {
        _storageOfInstances = null;
        _instance = null;
    }




    /// <summary>
    /// Returns GameObject by ObjectTypeEnum. Uses Lazy load initialization 
    /// </summary>
    /// <param name="objectTypeEnum"></param>
    /// <returns></returns>
    public GameObject GetInstanceByType(ObjectTypeEnum objectTypeEnum)
    {
        //if (_poolObjectsStorage.ContainsKey(objectTypeEnum))
        //{

        //    if (_poolObjectsStorage[objectTypeEnum].Count > 0)
        //    {
        //        GameObject go = _poolObjectsStorage[objectTypeEnum][0].gameObject;
        //        _poolObjectsStorage[objectTypeEnum].RemoveAt(0);
        //        return go;
        //    }
        //    else
        //    {
        //        GameObject goInstance = null;
        //        var identifier = new ResourceIdentifier(objectTypeEnum);
        //        if (!_storageOfInstances.ContainsKey(identifier))
        //        {
        //            goInstance = ResourceHelper.Load(Strings.GetUriResource(identifier)) as GameObject;
        //            _storageOfInstances[identifier] = goInstance;
        //        }
        //        else
        //        {
        //            goInstance = _storageOfInstances[identifier];
        //            return MonoBehaviour.Instantiate(goInstance) as GameObject;
        //        }
        //    }
        //}

        var resourceIdentifier = new ResourceIdentifier(objectTypeEnum);
        if (!_storageOfInstances.ContainsKey(resourceIdentifier))
        {
            GameObject gemInstance = ResourceHelper.Load(Strings.GetUriResource(resourceIdentifier)) as GameObject;
            _storageOfInstances[resourceIdentifier] = gemInstance;
        }
        return _storageOfInstances[resourceIdentifier] as GameObject;

    }

    /// <summary>
    /// Returns GameObject by ResourceIdentifier. Uses Lazy load initialization 
    /// </summary>
    /// <param name="resource">Resource identifier for loading</param>
    /// <returns></returns>
    public Object GetInstanceByType(ResourceIdentifier resource)
    {

        if (!_storageOfInstances.ContainsKey(resource))
        {
            if (Strings.UriResourceDictionaryMainObjects.ContainsKey(resource))
            {
                GameObject gemInstance = ResourceHelper.Load(Strings.UriResourceDictionaryMainObjects[resource]) as GameObject;
                return gemInstance;
            }
        }
        else
        {
            return _storageOfInstances[resource];
        }


        return GetInstanceByType(resource.GameObjectType);
    }

    /// <summary>
    /// The goal is Unity Resource loading 
    /// </summary>
    /// <param name="objectTypeEnum"></param>
    public void InitInstanceByType(ObjectTypeEnum objectTypeEnum)
    {
        InitInstanceByType(new ResourceIdentifier(objectTypeEnum));
    }

    /// <summary>
    /// The goal is Unity Resource loading 
    /// </summary>
    /// <param name="objectTypeEnum"></param>
    public void InitInstanceByType(ResourceIdentifier objectTypeEnum)
    {
        if (_storageOfInstances.ContainsKey(objectTypeEnum) == false)
        {
            GameObject gemInstance = ResourceHelper.Load(Strings.UriResourceDictionaryMainObjects[objectTypeEnum]) as GameObject;
            _storageOfInstances[objectTypeEnum] = gemInstance;
        }
    }

    //public void AddToPool(BaseGameObject baseGO)
    //{
    //    baseGO.gameObject.SetActive(false);
    //    //_poolObjectsStorage[baseGO.ObjectType].Add(baseGO);
    //    // baseGO.transform.SetParent(_poolObjectsParent);
    //}
}
