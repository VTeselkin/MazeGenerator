﻿using System.Collections;
using UnityEngine;
using System;
using System.Collections.Generic;
using System.Linq;

public class MazeGenerator : MonoBehaviour
{

    [SerializeField] private int width;
    [SerializeField] private int height;
    [SerializeField] public float sampleSize = 1;

    private Dictionary<Vector2Int, Vector3> coordFreeSpace;
    private Dictionary<Vector2Int, Vector3> coordWallSpace;

    private int[,] map;

    /// <summary>
    /// Clear this instance.
    /// </summary>
    public void Clear()
    {
        UITexture[] ren = GetComponentsInChildren<UITexture>();
        for (int i = 0; i < ren.Length; i++)
        {
            Destroy(ren[i].gameObject);
        }
    }
    /// <summary>
    /// Gets the map.
    /// </summary>
    /// <returns>The map.</returns>
    public int[,] GetMap()
    {
        return map;
    }
    /// <summary>
    /// Gets the width.
    /// </summary>
    /// <returns>The width.</returns>
    public int GetWidth()
    {
        return width;
    }
    /// <summary>
    /// Gets the height.
    /// </summary>
    /// <returns>The height.</returns>
    public int GetHeight()
    {
        return height;
    }

    public Vector3 BoundTopLeft() { return coordWallSpace.First().Value; }
    public Vector3 BoundBottomRight() { return coordWallSpace.Last().Value; }

    public void CreateMap()
    {
        Clear();
        coordFreeSpace = new Dictionary<Vector2Int, Vector3>();
        coordWallSpace = new Dictionary<Vector2Int, Vector3>();
        map = Maze.Generate(width, height); // генерируем лабиринт

        if (map == null)
            return;

        width = Maze.Round(width); // проверка на четные/нечетные
        height = Maze.Round(height);

        float posX = -sampleSize * width / 2f - sampleSize / 2f;
        float posY = sampleSize * height / 2f - sampleSize / 2f;
        float Xreset = posX;
        int z = 0;
        for (int y = 0; y < height; y++)
        {
            for (int x = 0; x < width; x++)
            {
                posX += sampleSize;
                if (map[x, y] != 1)
                {
                    GameObject go = Instantiate(Resources.Load("Prefabs/GamePlayObjects/Wall"), new Vector3(posX, posY, 0), Quaternion.identity, transform) as GameObject;
                    go.transform.name = "Wall " + "[" + x + "],[" + y + "]";
                    go.GetComponent<UITexture>().color = (map[x, y] == 1) ? Color.white : Color.gray;
                    coordWallSpace.Add(new Vector2Int(x, y), go.transform.position);
                }
                else
                {
                    coordFreeSpace.Add(new Vector2Int(x, y), new Vector3(posX, posY, 0));
                }
                z++;
            }
            posY -= sampleSize;
            posX = Xreset;
        }

    }

}