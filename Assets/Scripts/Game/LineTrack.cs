using UnityEngine;
using System.Collections.Generic;

internal class LineTrack
{

    private List<BaseGameObject> _selectedObjects;

    public LineTrack()
    {
        _selectedObjects = new List<BaseGameObject>();
    }

    public void Draw(BaseGameObject lastGemObjectForDrawing)
    {
        if (lastGemObjectForDrawing != null)
        {
            _selectedObjects.Add(lastGemObjectForDrawing);
            lastGemObjectForDrawing.SetVisibleSelect(true);
        }

    }

    public void RemoveLastPoint()
    {
        if (_selectedObjects.Count > 0)
        {
            _selectedObjects[_selectedObjects.Count - 1].SetVisibleSelect(false);
            _selectedObjects.RemoveAt(_selectedObjects.Count - 1);
        }
    }

    public void Clear()
    {
        foreach (var o in _selectedObjects)
        {
            o.SetVisibleSelect(true);
        }
        _selectedObjects.Clear();

    }

    public void RemoveFirstPoint()
    {
        if (_selectedObjects.Count > 0)
        {
            _selectedObjects[0].SetVisibleSelect(false);
            _selectedObjects.RemoveAt(0);
        }
    }
}
