using Assets.Scripts.Helpers;
using Assets.Scripts.Navigation;
using Assets.Scripts.Shared;
using UnityEngine;

namespace Assets.Scripts.Game
{
    public class InputHandler : MonoBehaviour, IInput
    {
        #region Fields

        private static MouseStateEnum _mouseStateEnum = MouseStateEnum.Normal;
        private static InputHandler _instance;
        private bool _isFreezeInput = false;
        private static bool _blockInput = true;

        private const float _forceTouchPressure = 6.5f;
        private bool _isForceTouchSupported = false;

        #endregion

        #region Properties
        // Used for local ObjectTracker working
        public bool IsFreezeInput
        {
            get
            {
                return _isFreezeInput;
            }
            set
            {
                _isFreezeInput = value;
            }
        }
        // Used for global blocking of any Input
        public static bool BlockInput
        {
            get
            {
                return _blockInput;
            }
            set
            {
                if (_blockInput == value) return;
                //Debug.Log("BlockInput " + value);

                _blockInput = value;

                BackButtonManager.Instance.SetPause(_blockInput);
                //if (value== false){
                //   Instance.IsFreezeInput = false;
                //}
            }
        }

        //
        // iPhone 6 / 6s 3D Touch support
        //
        public bool IsForceTouchSupported
        {
            get
            {
                return _isForceTouchSupported;
            }
        }

        public float ForceTouchPressure
        {
            get
            {
                return _forceTouchPressure;
            }
        }

        // Emulation of something like mouse state
        // (aka in XNA)
        public static MouseStateEnum MouseState
        {
            get
            {
                return _mouseStateEnum;
            }
        }

        public static InputHandler Instance
        {
            get
            {
                if (_instance == null)
                {
                    // DebugLogger.Log("Instance created");
                    _instance = new GameObject("InputHandler").AddComponent<InputHandler>();
                }
                return _instance;
            }
        }

        public static void Reset()
        {
            _mouseStateEnum = MouseStateEnum.Normal;
        }

        #endregion

        #region Methods

        // CellUpdate is called once per frame
        void Update()
        {
            if (_blockInput == false)
            {
                if (Input.GetMouseButtonUp(0))
                {
                    _mouseStateEnum = MouseStateEnum.Normal;
                    return;
                }

                if (Input.GetMouseButtonDown(0))
                {
                    if (_isFreezeInput == false)
                    {
                        _mouseStateEnum = MouseStateEnum.MouseLeftButtonPressed;
                    }
                    else
                    {
                        _mouseStateEnum = MouseStateEnum.Normal;
                    }
                }
            }
        }

        private void Awake()
        {
            if (_instance == this)
            {
                DestroyImmediate(this.gameObject);
            }
            else
            {
                _instance = this;
            }

            _isForceTouchSupported = Input.touchPressureSupported;
        }


        public void DestroyInstance()
        {
            _blockInput = true;
            _instance = null;
        }
        #endregion
    }
}
