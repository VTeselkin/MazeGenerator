using System;
using UnityEngine;
using Assets.Scripts.Settings;
using System.IO;
using System.Collections;
using Assets.Scripts.Shared;
using Assets.Scripts.Server;
using Assets.Scripts.Helpers;


public class GameStorage : DataStorageProvider
{
    public JSONObject _updateConfigData;
    public GameConfig Data;
    private int vertion;
    public bool IsHasUpdate { get; private set; }

    #region Singleton

    private static GameStorage _instance;

    public static GameStorage Instance
    {
        get
        {
            if (_instance == null)
            {
                GameObject go = new GameObject("GameStorage");
                DontDestroyOnLoad(go);
                _instance = go.AddComponent<GameStorage>();

            }
            return _instance;
        }
    }

    protected override string StorageFilePath
    {
        get
        {
            return Helper.StorageFilePath("GameData.dat");
        }
    }

    private string StorageLocalFilePath()
    {
        return Helper.StorageLocalFilePath("GameData.dat");
    }

    private void Awake()
    {
        IsHasUpdate = false;
        Data = new GameConfig();
        if (_instance == this)
        {
            DestroyImmediate(this.gameObject);
        }
        else
        {
            _instance = this;
        }

    }


    #endregion

    private IEnumerator LoadData()
    {
        if (AppSettings.Instance.IsFirstLaunch || !File.Exists(StorageFilePath))
        {
            DebugLogger.Log("Load StreamAsset Config ");
            if (StorageLocalFilePath().Contains("://"))
            {
                DebugLogger.Log("LoadStreamAssetConfig = " + StorageLocalFilePath());
                WWW www = new WWW(StorageLocalFilePath());
                yield return www;
                if (string.IsNullOrEmpty(www.error))
                {
                    var data = www.text;
                    DebugLogger.Log("LoadStreamAssetConfig = " + data);
                    JSONObject jSONObject = new JSONObject(data);
                    RestoreState(jSONObject);
                }
                else
                {
                    DebugLogger.LogError("LoadStreamAssetConfig = " + www.error + " : " + StorageLocalFilePath());
                }
            }
            else
            {
                DebugLogger.Log("LoadStreamAssetConfig = " + StorageLocalFilePath());
                var data = File.ReadAllText(StorageLocalFilePath());
                JSONObject jSONObject = new JSONObject(data);
                RestoreState(jSONObject);

            }
        }

        if (File.Exists(StorageFilePath))
        {
            DebugLogger.Log("Load Cache Config");
            var decriptedData = File.ReadAllText(StorageFilePath);
            decriptedData = CryptoScript.Crypt(File.ReadAllText(StorageFilePath));
            var data = new JSONObject(decriptedData);
            try
            {
                RestoreState(data);
            }
            catch (Exception e)
            {
                DebugLogger.Log(e);
            }
        }
        else
        {
            DebugLogger.LogError("DataStorageProvider file doesn't exit = " + StorageFilePath);
        }
        if (InternetReachabilityController.Instance.IsInternetAvalible)
        {
            DebugLogger.Log("Load Server Config");
            RestoreFromServer();
        }
        else
        {
            OnCompleteLoad(false);
        }

        yield return null;
    }

    protected override JSONObject GetState()
    {
        return _updateConfigData;
    }

    protected override void RestoreState()
    {
        StartCoroutine(LoadData());
    }

    private void ErrorLoad(string error)
    {
        OnCompleteLoad(false);
        DebugLogger.LogError(error);
    }

    protected override void RestoreState(JSONObject data)
    {
        _updateConfigData = data;
        if (data != null)
        {
            if (data[JSONLevelUpdaterKey.JSONConfigPlayerVertion] != null)
            {
                Data.Vertion = (int)(data[JSONLevelUpdaterKey.JSONConfigPlayerVertion].i);
            }
            vertion = Data.Vertion;
            if (data[JSONLevelUpdaterKey.JSONConfigPlayerEnergy] != null)
            {
                Data.PlayerEnergy = (int)(data[JSONLevelUpdaterKey.JSONConfigPlayerEnergy].i);
            }
            if (data[JSONLevelUpdaterKey.JSONConfigPlayerLevelMaxX] != null)
            {
                Data.LevelMaxX = (int)(data[JSONLevelUpdaterKey.JSONConfigPlayerLevelMaxX].i);
            }
            if (data[JSONLevelUpdaterKey.JSONConfigPlayerLevelMaxY] != null)
            {
                Data.LevelMaxY = (int)(data[JSONLevelUpdaterKey.JSONConfigPlayerLevelMaxY].i);
            }
            if (data[JSONLevelUpdaterKey.JSONConfigPlayerIsBannerEnable] != null)
            {
                Data.IsBannerEnable = data[JSONLevelUpdaterKey.JSONConfigPlayerIsBannerEnable].i == 1;
            }
            if (data[JSONLevelUpdaterKey.JSONConfigPlayerIsInterEnable] != null)
            {
                Data.IsInterEnable = data[JSONLevelUpdaterKey.JSONConfigPlayerIsInterEnable].i == 1;
            }
            if (data[JSONLevelUpdaterKey.JSONConfigPlayerIsRewardEnable] != null)
            {
                Data.IsRewardEnable = data[JSONLevelUpdaterKey.JSONConfigPlayerIsRewardEnable].i == 1;
            }
            if (data[JSONLevelUpdaterKey.JSONConfigMaxGameTexture] != null)
            {
                Data.MaxGameTexture = (int)(data[JSONLevelUpdaterKey.JSONConfigMaxGameTexture].i);
            }
            if (data[JSONLevelUpdaterKey.JSONConfigPercentByIndex] != null)
            {
                Data.PercentByIndex = (int)(data[JSONLevelUpdaterKey.JSONConfigPercentByIndex].i);
            }

            if (data[JSONLevelUpdaterKey.JSONConfigCostIndexByEnergy] != null)
            {
                Data.CostIndexByEnergy = (int)(data[JSONLevelUpdaterKey.JSONConfigCostIndexByEnergy].i);
            }
            if (data[JSONLevelUpdaterKey.JSONConfigCostIndexBySpeed] != null)
            {
                Data.CostIndexBySpeed = (int)(data[JSONLevelUpdaterKey.JSONConfigCostIndexBySpeed].i);
            }
            if (data[JSONLevelUpdaterKey.JSONConfigCostIndexByDistance] != null)
            {
                Data.CostIndexByDistance = (int)(data[JSONLevelUpdaterKey.JSONConfigCostIndexByDistance].i);
            }
            if (data[JSONLevelUpdaterKey.JSONConfigCountCoinsMini] != null)
            {
                Data.CountCoinsMini = (int)(data[JSONLevelUpdaterKey.JSONConfigCountCoinsMini].i);
            }
            if (data[JSONLevelUpdaterKey.JSONConfigCountCoinsMedium] != null)
            {
                Data.CountCoinsMedium = (int)(data[JSONLevelUpdaterKey.JSONConfigCountCoinsMedium].i);
            }
            if (data[JSONLevelUpdaterKey.JSONConfigDistanceByIndex] != null)
            {
                Data.DistanceByIndex = data[JSONLevelUpdaterKey.JSONConfigDistanceByIndex].f;
            }
            if (data[JSONLevelUpdaterKey.JSONConfigShowInterAfterLevel] != null)
            {
                Data.ShowInterAfterLevel = (int)data[JSONLevelUpdaterKey.JSONConfigShowInterAfterLevel].i;
            }
            if (data[JSONLevelUpdaterKey.JSONConfigShowInterEachAttempt] != null)
            {
                Data.ShowInterEachAttempt = (int)data[JSONLevelUpdaterKey.JSONConfigShowInterEachAttempt].i;
            }
            if (data[JSONLevelUpdaterKey.JSONConfigCostNoBanner] != null)
            {
                Data.CostNoBanner = (int)data[JSONLevelUpdaterKey.JSONConfigCostNoBanner].i;
            }
            Save();
        }
    }

    private void RestoreFromServer()
    {
        GameSparkManager.Instance.GetGameConfig((result) =>
        {
            JSONObject jSONObject = new JSONObject(result);
            if (jSONObject != null)
            {
                if (jSONObject[JSONLevelUpdaterKey.JSONConfigPlayerVertion] != null)
                {
                    if ((int)(jSONObject[JSONLevelUpdaterKey.JSONConfigPlayerVertion].i) > vertion)
                    {
                        IsHasUpdate = true;
                    }
                }
            }
            RestoreState(jSONObject);
            OnCompleteLoad(IsHasUpdate);
        }, ErrorLoad);

    }

    public class GameConfig
    {
        private float defaultSpeed = 1.2f;
        private int playerEnergy = 100;
        private int levelMaxX = 100;
        private int levelMaxY = 100;
        private int vertion = -1;
        private bool isBannerEnable = true;
        private bool isInterEnable = true;
        private bool isRewardEnable = true;

        private int _maxGameTexture = 15;

        private int _percentByIndex = 20;
        private float _distanceByIndex = 0.5f;

        private int _costIndexByEnergy = 250;
        private int _costIndexBySpeed = 200;
        private int _costIndexByDistance = 300;

        private int _countCoinsMini = 500;
        private int _countCoinsMedium = 2500;
        private int _countCoinsHard = 5000;

        private int _showInterAfterLevel = 5;
        private int _showInterEachAttempt = 3;

        private int _costNoBanner = 3000;
        private int _maxLevelForInfinityEnergy = 1;
        public int PlayerEnergy { get { return playerEnergy; } set { playerEnergy = value; } }
        public int LevelMaxX { get { return levelMaxX; } set { levelMaxX = value; } }
        public int LevelMaxY { get { return levelMaxY; } set { levelMaxY = value; } }
        public int Vertion { get { return vertion; } set { vertion = value; } }
        public bool IsBannerEnable { get { return isBannerEnable; } set { isBannerEnable = value; } }
        public bool IsInterEnable { get { return isInterEnable; } set { isInterEnable = value; } }
        public bool IsRewardEnable { get { return isRewardEnable; } set { isRewardEnable = value; } }
        public int MaxGameTexture { get { return _maxGameTexture; } set { _maxGameTexture = value; } }
        public int PercentByIndex { get { return _percentByIndex; } set { _percentByIndex = value; } }
        public float DistanceByIndex { get { return _distanceByIndex; } set { _distanceByIndex = value; } }

        public int CostIndexByEnergy { get { return _costIndexByEnergy; } set { _costIndexByEnergy = value; } }
        public int CostIndexBySpeed { get { return _costIndexBySpeed; } set { _costIndexBySpeed = value; } }
        public int CostIndexByDistance { get { return _costIndexByDistance; } set { _costIndexByDistance = value; } }
        public int CountCoinsMini { get { return _countCoinsMini; } set { _countCoinsMini = value; } }
        public int CountCoinsMedium { get { return _countCoinsMedium; } set { _countCoinsMedium = value; } }
        public int CountCoinsHard { get { return _countCoinsHard; } set { _countCoinsHard = value; } }

        public int ShowInterAfterLevel { get { return _showInterAfterLevel; } set { _showInterAfterLevel = value; } }
        public int ShowInterEachAttempt { get { return _showInterEachAttempt; } set { _showInterEachAttempt = value; } }

        public int CostNoBanner { get { return _costNoBanner; } set { _costNoBanner = value; } }
        public int MaxLevelForInfinityEnergy { get { return _maxLevelForInfinityEnergy; } set { _maxLevelForInfinityEnergy = value; } }
        public float DefaultSpeed { get { return defaultSpeed; } set { defaultSpeed = value; } }
    }
}
