﻿using UnityEngine;
using Assets.Scripts.Server;
using System;
using System.Collections.Generic;
using Assets.Scripts.Shared;
using Assets.Scripts.Settings;
using System.Collections;
using System.IO;
using Assets.Scripts.Helpers;

public class LevelStorage : DataStorageProvider
{
    public List<LevelData> Data;
    public int Vertion { get; private set; }
    public int LevelCount { get; private set; }
    public JSONObject _updateLevelData;

    #region Singleton

    private static LevelStorage _instance;

    public static LevelStorage Instance
    {
        get
        {
            if (_instance == null)
            {
                GameObject go = new GameObject("LevelsData");
                DontDestroyOnLoad(go);
                _instance = go.AddComponent<LevelStorage>();

            }
            return _instance;
        }
    }

    protected override string StorageFilePath
    {
        get
        {
            return Helper.StorageFilePath("LevelsData.dat");
        }
    }

    private string StorageLocalFilePath()
    {
        return Helper.StorageLocalFilePath("LevelsData.dat");
    }



    private void Awake()
    {
        if (_instance == this)
        {
            DestroyImmediate(this.gameObject);
        }
        else
        {
            _instance = this;
        }


    }
    #endregion

    private void RestoreFromServer()
    {
        GameSparkManager.Instance.GetLevelData(CompleteLevelData, ErrorLoad);
    }

    #region Load and Save



    private IEnumerator LoadData()
    {
        if (AppSettings.Instance.IsFirstLaunch || !File.Exists(StorageFilePath))
        {
            if (StorageLocalFilePath().Contains("://"))
            {

                WWW www = new WWW(StorageLocalFilePath());
                yield return www;
                if (string.IsNullOrEmpty(www.error))
                {
                    var decriptedData = www.text;
                    decriptedData = CryptoScript.Crypt(decriptedData);
                    DebugLogger.Log("LoadStreamAssetLevel = " + decriptedData);
                    CompleteLevelData(decriptedData);
                }
                else
                {
                    DebugLogger.LogError("LoadStreamAssetLevel = " + www.error + " : " + StorageLocalFilePath());
                }
            }
            else
            {
                //var decriptedData = File.ReadAllText(StorageLocalFilePath());
                var decriptedData = CryptoScript.Crypt(File.ReadAllText(StorageLocalFilePath()));
                CompleteLevelData(decriptedData);
                DebugLogger.Log("System LoadStreamAssetLevel = " + decriptedData);
            }
        }
        if (File.Exists(StorageFilePath))
        {
            //var decriptedData = File.ReadAllText(StorageFilePath);
            var decriptedData = CryptoScript.Crypt(File.ReadAllText(StorageFilePath));
            var data = new JSONObject(decriptedData);
            try
            {
                RestoreState(data);
            }
            catch (Exception e)
            {
                DebugLogger.Log(e);
            }
        }
        else
        {
            DebugLogger.LogError("DataStorageProvider file doesn't exit = " + StorageFilePath);
        }

        if (InternetReachabilityController.Instance.IsInternetAvalible)
        {
            RestoreFromServer();
        }
        else
        {
            OnCompleteLoad(false);
        }

        yield return null;
    }

    private void CompleteLevelData(string obj)
    {
        DebugLogger.Log("LevelStorage Load = " + obj);
        JSONObject jSONObject = new JSONObject(obj);
        LoadLevelsData(jSONObject, true);
    }

    private void LoadLevelsData(JSONObject jSONObject, bool forceSave)
    {
        try
        {
            if (jSONObject != null)
            {

                if (jSONObject[JSONLevelUpdaterKey.JSONLevelCount] != null)
                {
                    LevelCount = (int)jSONObject[JSONLevelUpdaterKey.JSONLevelCount].i;
                }
                if (jSONObject[JSONLevelUpdaterKey.JSONLevelData] != null)
                {
                    Data = new List<LevelData>();
                    foreach (var jsonObject in jSONObject[JSONLevelUpdaterKey.JSONLevelData].list)
                    {
                        var levelData = new LevelData(jsonObject);
                        Data.Add(levelData);
                    }
                }


            }
            if (forceSave)
            {
                _updateLevelData = jSONObject;
                Save();
            }
            OnCompleteLoad(true);
        }
        catch (Exception e)
        {
            DebugLogger.LogError("CompleteLevelData = " + e.Message);
            OnCompleteLoad(false);
        }

    }

    /// <summary>
    /// Save data
    /// </summary>
    /// <returns>The state.</returns>
    protected override JSONObject GetState()
    {
        return _updateLevelData;
    }

    /// <summary>
    /// Load data
    /// </summary>
    /// <param name="data">Data.</param>
    protected override void RestoreState(JSONObject data)
    {
        LoadLevelsData(data, false);
    }


    protected override void RestoreState()
    {
        StartCoroutine(LoadData());
    }

    private void ErrorLoad(string error)
    {
        OnCompleteLoad(false);
        DebugLogger.LogError(error);
    }
    #endregion
}
