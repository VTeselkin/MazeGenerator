public static class JSONLevelUpdaterKey
{
    public const string JSONLevelVertion = "vertion";
    public const string JSONLevelCount = "levelCount";
    public const string JSONLevelData = "levelData";

    public const string JSONLevelNumber = "level";
    public const string JSONLevelTutorial = "tutorial";
    public const string JSONViewIndexWall = "viewIndexWall";
    public const string JSONViewIndexFree = "viewIndexFree";

    public const string JSONLevelPlayerEnergy = "playerEnergy";
    public const string JSONLevelPlayerTarget = "target";
    public const string JSONLevelWidth = "width";
    public const string JSONLevelHeight = "height";

    public const string JSONLevelEnemies = "enemies";
    public const string JSONLevelEnemiesType = "type";
    public const string JSONLevelEnemiesEnergy = "energy";

    public const string JSONLevelFoods = "foods";
    public const string JSONLevelFoodsType = "type";
    public const string JSONLevelFoodsEnergy = "energy";

    public const string JSONConfigPlayerVertion = "Vertion";
    public const string JSONConfigPlayerEnergy = "Energy";
    public const string JSONConfigPlayerLevelMaxX = "LevelMaxX";
    public const string JSONConfigPlayerLevelMaxY = "LevelMaxY";
    public const string JSONConfigPlayerIsBannerEnable = "IsBannerEnable";
    public const string JSONConfigPlayerIsInterEnable = "IsInterEnable";
    public const string JSONConfigPlayerIsRewardEnable = "IsRewardEnable";
    public const string JSONConfigMaxGameTexture = "MaxGameTexture";
    public const string JSONConfigPercentByIndex = "PercentByIndex";

    public const string JSONConfigCostIndexByEnergy = "CostIndexByEnergy";
    public const string JSONConfigCostIndexBySpeed = "CostIndexBySpeed";
    public const string JSONConfigCostIndexByDistance = "CostIndexByDistance";
    public const string JSONConfigCountCoinsMini = "CountCoinsMini";
    public const string JSONConfigCountCoinsMedium = "CountCoinsMedium";
    public const string JSONConfigCountCoinsHard = "CountCoinsHard";
    public const string JSONConfigDistanceByIndex = "DistanceByIndex";

    public const string JSONConfigShowInterAfterLevel = "ShowInterAfterLevel";
    public const string JSONConfigShowInterEachAttempt = "ShowInterEachAttempt";
    public const string JSONConfigCostNoBanner = "CostNoBanner";

}
