﻿using System;
using System.IO;
using Assets.Scripts.Helpers;
using Assets.Scripts.Server;
using Assets.Scripts.Settings;
using Assets.Scripts.Shared;
using UnityEngine;

internal class LevelProgress : DataStorageProvider
{
    private const int LevelStarsCount = 3;
    private const int LevelMaxCount = 300;
    public Progress[] LevelsProgress { get; private set; }

    #region Singleton

    private static LevelProgress _instance;

    public static LevelProgress Instance
    {
        get
        {
            if (_instance == null)
            {
                GameObject go = new GameObject("LevelProgress");
                DontDestroyOnLoad(go);
                _instance = go.AddComponent<LevelProgress>();

            }
            return _instance;
        }
    }

    protected override string StorageFilePath
    {
        get
        {
            return Helper.StorageFilePath("LevelProgress.dat");
        }
    }

    private void Awake()
    {
        if (_instance == this)
        {
            DestroyImmediate(this.gameObject);
        }
        else
        {
            _instance = this;
        }
        InitLevelData();
    }

    #endregion
    private void InitLevelData()
    {
        LevelsProgress = new Progress[LevelMaxCount];

        for (var i = 0; i < LevelMaxCount; i++)
        {
            LevelsProgress[i] = new Progress();
        }
    }

    protected override JSONObject GetState()
    {
        var data = new JSONObject(JSONObject.Type.OBJECT);
        for (int i = 0; i < LevelStorage.Instance.LevelCount; i++)
        {
            var level = new JSONObject(JSONObject.Type.OBJECT);
            level.AddField("score", LevelsProgress[i].Coins);
            level.AddField("stars", LevelsProgress[i].Star);
            data.AddField("Level_" + i, level);
        }
        return data;
    }

    protected override void RestoreState(JSONObject data)
    {
        LoadProgressData(data);
    }

    protected override void RestoreState()
    {
        if (File.Exists(StorageFilePath))
        {
            var decriptedData = File.ReadAllText(StorageFilePath);
            decriptedData = CryptoScript.Crypt(File.ReadAllText(StorageFilePath));
            var data = new JSONObject(decriptedData);
            try
            {
                RestoreState(data);
            }
            catch (Exception e)
            {
                DebugLogger.Log(e);
            }
        }
        if (InternetReachabilityController.Instance.IsInternetAvalible)
        {
            GameSparkManager.Instance.GetLevelProgress(CompleteProgressData, ErrorLoad);
        }
        else
        {
            OnCompleteLoad(false);
        }
    }

    private void CompleteProgressData(string obj)
    {
        DebugLogger.Log("LevelStorage Load = " + obj);
        JSONObject jSONObject = new JSONObject(obj);
        SunchronizedProgress(jSONObject);
        OnCompleteLoad(true);
    }

    private void ErrorLoad(string error)
    {
        OnCompleteLoad(false);
        DebugLogger.LogError(error);
    }

    private void LoadProgressData(JSONObject data)
    {
        for (int i = 0; i < LevelStorage.Instance.LevelCount; i++)
        {
            if (data["Level_" + i] != null)
            {
                JSONObject level = data["Level_" + i];
                if (level != null)
                {
                    if (level["score"] != null)
                    {
                        LevelsProgress[i].Coins = (int)level["score"].i;
                    }
                    if (level["stars"] != null)
                    {
                        LevelsProgress[i].Star = (int)level["stars"].i;
                    }
                }
            }
        }
        SetMaxOpenLevel();
    }

    private void SunchronizedProgress(JSONObject jSONObject)
    {
        bool isNeedSync = false;
        if (jSONObject["scriptData"] != null)
        {
            JSONObject scriptData = jSONObject["scriptData"];
            if (scriptData["data"] != null)
            {
                JSONObject data = scriptData["data"];

                for (int i = 0; i < LevelStorage.Instance.LevelCount; i++)
                {
                    if (data["Level_" + i] != null)
                    {
                        JSONObject level = data["Level_" + i];
                        if (level != null)
                        {
                            int coins = 0;
                            int star = 0;
                            if (level["score"] != null)
                            {
                                coins = (int)level["score"].i;
                            }
                            if (level["stars"] != null)
                            {
                                star = (int)level["stars"].i;
                            }
                            if (LevelsProgress[i].Coins != coins || LevelsProgress[i].Star != star)
                            {
                                LevelsProgress[i].Coins = Math.Max(LevelsProgress[i].Coins, coins);
                                LevelsProgress[i].Star = Math.Max(LevelsProgress[i].Star, star);
                                LevelsProgress[i].IsNeedSync = true;
                                isNeedSync = true;
                            }
                        }
                    }
                }
            }
            Save();
            SetMaxOpenLevel();
            if (isNeedSync)
            {
                SendChangedProgressToGS(0);
            }
        }
    }

    private void SendChangedProgressToGS(int levelNumber)
    {
        if (levelNumber < LevelStorage.Instance.LevelCount)
        {
            if (LevelsProgress[levelNumber].IsNeedSync)
            {
                LevelsProgress[levelNumber].IsNeedSync = false;
                GameSparkManager.Instance.PostLevelProgress(levelNumber, LevelsProgress[levelNumber].Coins,
                                                            LevelsProgress[levelNumber].Star, (string obj) =>
            {
                if (levelNumber < LevelStorage.Instance.LevelCount)
                {
                    levelNumber++;
                    SendChangedProgressToGS(levelNumber);
                }

            }, (string obj) =>
            {
                DebugLogger.LogError("SendChangedProgressToGS Error update level= " + levelNumber);
            });
            }
            else
            {
                levelNumber++;
                SendChangedProgressToGS(levelNumber);
            }
        }

    }

    private void SetMaxOpenLevel()
    {
        int maxOpenLevel = 0;
        for (int i = 0; i < LevelStorage.Instance.LevelCount; i++)
        {
            if (LevelsProgress[i].IsFinished)
            {
                maxOpenLevel = i + 1;
            }
        }
        UserStorage.Instance.SetMaxOpenedLevel(maxOpenLevel);
        UserStorage.Instance.SetCurrentLevel(UserStorage.Instance.MaxOpenedLevel);
    }
    #region LevelProgress


    public int TotalStars
    {
        get { return LevelStorage.Instance.LevelCount * LevelStarsCount; }
    }

    public bool UpdateLevelProgress(int levelNumber, Progress levelProgress)
    {
        if (!levelProgress.IsFinished)
        {
            DebugLogger.LogError("UpdateLevelProgress level is not finished!");
            return false;
        }

        var isFirstWin = !LevelsProgress[levelNumber].IsFinished;
        if (LevelsProgress[levelNumber].IsFinished)
        {
            int difReward = levelProgress.Coins - LevelsProgress[levelNumber].Coins;
            if (difReward > 0)
            {
                UserStorage.Instance.IncCoins(difReward);
            }
        }
        else
        {
            UserStorage.Instance.IncCoins(levelProgress.Coins);
        }
        LevelsProgress[levelNumber].Coins = Math.Max(LevelsProgress[levelNumber].Coins, levelProgress.Coins);
        LevelsProgress[levelNumber].Star = Math.Max(LevelsProgress[levelNumber].Star, levelProgress.Star);

        Save();

        GameSparkManager.Instance.PostLevelProgress(levelNumber, levelProgress.Coins, levelProgress.Star, (string obj) =>
        {
            DebugLogger.Log("PostLevelProgress = " + obj);
        }, (string obj) =>
        {
            DebugLogger.LogError("PostLevelProgress = " + obj);
        });

        GameSparkManager.Instance.PostUserScoreByLevel(LevelsProgress[levelNumber].Coins, levelNumber, (string obj) =>
        {
            DebugLogger.Log("UpdateLevelProgress By Level = " + obj);
        }, (string obj) =>
        {
            DebugLogger.LogError("UpdateLevelProgress By Level = " + obj);
        });

        GameSparkManager.Instance.PostUserStars(UserStorage.Instance.Stars, DebugLogger.Log, DebugLogger.LogError);

        var isNewLevelOpened = false;
        if (isFirstWin)
        {
            var lastMaxOpenedLevel = UserStorage.Instance.MaxOpenedLevel;
            isNewLevelOpened = UserStorage.Instance.MaxOpenedLevel <= levelNumber;
            bool isNextLevelisExsist = LevelStorage.Instance.LevelCount > levelNumber + 1;
            if (isNewLevelOpened && isNextLevelisExsist)
            {
                UserStorage.Instance.SetMaxOpenedLevel(levelNumber + 1);
                UserStorage.Instance.SetCurrentLevel(levelNumber + 1);
            }
        }
        return isNewLevelOpened;
    }


    #endregion
}

public class Progress
{
    public int Coins { get; set; }
    public int Star { get; set; }
    public bool IsNeedSync = false;
    public bool IsFinished
    {
        get { return Coins > 0; }
    }

    public Progress()
    {
    }

    public Progress(int coins, int star)
    {
        Coins = coins;
        Star = star;
    }
}
