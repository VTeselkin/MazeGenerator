using Assets.Scripts.Helpers;
using Assets.Scripts.Shared;
using UnityEngine;

public class LevelLoader
{


    // Use this for initialization
    public static LevelData LoadLevel(int levelNumber)
    {
        var data = CreateMazeData(levelNumber);
        return data;

    }
    private static LevelData CreateMazeData(int levelNumber)
    {
        DebugLogger.Log("LevelData Count = " + LevelStorage.Instance.Data.Count);
        var levelOriginal = LevelStorage.Instance.Data[levelNumber];
        LevelData levelData = (LevelData)levelOriginal.Clone();
        //TODO
        levelData.Map = Maze.Generate(levelData.Width, levelData.Height);
        levelData.Height = Mathf.Max(10, levelData.Height);
        levelData.Width = Mathf.Max(10, levelData.Width);
        levelData.PlayDimension = new GridDimension(levelData.Width + 1, levelData.Height + 1);
        levelData.CoordinatesMin = new Point(0, 0);
        levelData.CoordinatesMax = new Point((int)(levelData.Width * BaseGameObject.ElementsDistance), (int)(levelData.Height * BaseGameObject.ElementsDistance));
        return levelData;

    }

}
