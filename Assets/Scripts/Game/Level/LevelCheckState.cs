﻿using Assets.Scripts.GUI;
using Assets.Scripts.GUI.Managers;
using Assets.Scripts.Helpers;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using System.Collections;
using AssemblyCSharp.Scripts.MainManager;
using Assets.Scripts.Game;

public class LevelCheckState
{
    private LevelData _levelData;

    public LevelCheckState(LevelData levelData)
    {
        _levelData = levelData;
    }

    public bool CheckWinLevel(ObjectTypeEnum typeObject)
    {
        switch (_levelData.LevelTarget)
        {
            case LevelTargetEnum.Finish:
                if (typeObject == ObjectTypeEnum.Finish)
                {
                    MonoHelper.Instance.StartCoroutine(ShowPopupWinWithPause());
                    InputHandler.BlockInput = true;
                    return true;
                }
                break;
            case LevelTargetEnum.Collect:
                if (typeObject == ObjectTypeEnum.Food)
                {
                    var foods = ObjectSuperVisor.Instance.ObjectStorage.GetListOfObjectsByType(ObjectTypeEnum.Food);
                    if (foods.Count == 0)
                    {
                        MonoHelper.Instance.StartCoroutine(ShowPopupWinWithPause());
                        InputHandler.BlockInput = true;
                        return true;
                    }
                }
                break;
            case LevelTargetEnum.FinishAndCollect:
                if (typeObject == ObjectTypeEnum.Food)
                {
                    var foods = ObjectSuperVisor.Instance.ObjectStorage.GetListOfObjectsByType(ObjectTypeEnum.Food);
                    if (foods.Count == 0)
                    {
                        var finish = ObjectSuperVisor.Instance.ObjectStorage.GetListOfObjectsByType(ObjectTypeEnum.Finish);
                        if (finish.Count > 0)
                        {
                            finish[0].SetVisibleSelect(true);
                        }
                    }
                }
                if (typeObject == ObjectTypeEnum.Finish)
                {
                    var foods = ObjectSuperVisor.Instance.ObjectStorage.GetListOfObjectsByType(ObjectTypeEnum.Food);
                    if (foods.Count == 0)
                    {
                        MonoHelper.Instance.StartCoroutine(ShowPopupWinWithPause());
                        InputHandler.BlockInput = true;
                        return true;
                    }
                }
                break;
        }
        return false;
    }

    private IEnumerator ShowPopupWinWithPause()
    {

        yield return new WaitForSeconds(1.3f);
        UIManager.Instance.CreateNewUIElement<PopupWin>(UIElementType.PopupWin);
        InputHandler.BlockInput = false;
    }
}
