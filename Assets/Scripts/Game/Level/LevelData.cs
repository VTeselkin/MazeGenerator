using Assets.Scripts.Helpers;
using System.Collections.Generic;
using System;

public class LevelData : ICloneable
{
    public int LevelNumber { get; set; }
    public int[,] Map;
    public int PlayerEnergy { get; set; }
    public int Height { get; set; }
    public int Width { get; set; }
    public int ViewIndexWall { get; set; }
    public int ViewIndexFree { get; set; }
    public LevelTargetEnum LevelTarget;
    public Point CoordinatesMin { get; set; }
    public Point CoordinatesMax { get; set; }
    public GridDimension PlayDimension { get; set; }
    public List<Enemy> Enemies = new List<Enemy>();
    public List<Food> Foods = new List<Food>();
    public bool IsNeedShowTutorial;
    public int TutorialIndex = 0;

    public LevelData(JSONObject data)
    {
        if (data != null)
        {
            if (data[JSONLevelUpdaterKey.JSONLevelNumber] != null)
            {
                LevelNumber = (int)data[JSONLevelUpdaterKey.JSONLevelNumber].i;
                ViewIndexWall = SetViewIndex();
                ViewIndexFree = SetViewIndex();
            }
            if (data[JSONLevelUpdaterKey.JSONLevelTutorial] != null)
            {
                IsNeedShowTutorial = true;
                TutorialIndex = (int)data[JSONLevelUpdaterKey.JSONLevelTutorial].i - 1;
            }
            if (data[JSONLevelUpdaterKey.JSONViewIndexWall] != null)
            {
                ViewIndexWall = (int)data[JSONLevelUpdaterKey.JSONViewIndexWall].i;
            }
            if (data[JSONLevelUpdaterKey.JSONViewIndexFree] != null)
            {
                ViewIndexFree = (int)data[JSONLevelUpdaterKey.JSONViewIndexFree].i;
            }
            if (data[JSONLevelUpdaterKey.JSONLevelPlayerEnergy] != null)
            {
                PlayerEnergy = (int)data[JSONLevelUpdaterKey.JSONLevelPlayerEnergy].i;
            }
            if (data[JSONLevelUpdaterKey.JSONLevelPlayerTarget] != null)
            {
                try
                {
                    LevelTarget = (LevelTargetEnum)Enum.Parse(typeof(LevelTargetEnum), data[JSONLevelUpdaterKey.JSONLevelPlayerTarget].str, true);
                }
                catch (Exception)
                {
                    LevelTarget = LevelTargetEnum.Finish;
                }

            }
            if (data[JSONLevelUpdaterKey.JSONLevelWidth] != null)
            {
                Width = (int)data[JSONLevelUpdaterKey.JSONLevelWidth].i;

            }

            if (data[JSONLevelUpdaterKey.JSONLevelHeight] != null)
            {
                Height = (int)data[JSONLevelUpdaterKey.JSONLevelHeight].i;

            }

            if (data[JSONLevelUpdaterKey.JSONLevelEnemies] != null)
            {
                foreach (var jsonObject in data[JSONLevelUpdaterKey.JSONLevelEnemies].list)
                {
                    Enemies.Add(new Enemy(jsonObject));
                }
            }

            if (data[JSONLevelUpdaterKey.JSONLevelFoods] != null)
            {
                foreach (var jsonObject in data[JSONLevelUpdaterKey.JSONLevelFoods].list)
                {
                    Foods.Add(new Food(jsonObject));
                }
            }
        }
    }

    private int SetViewIndex()
    {
        var maxTexture = GameStorage.Instance.Data.MaxGameTexture;
        var index = (LevelNumber - (int)(LevelNumber / maxTexture) * maxTexture);
        if (index == 0) index = 1;
        return index;
    }

    public object Clone()
    {
        return this.MemberwiseClone();
    }

    public class Enemy
    {
        public ObjectTypeEnum Type;
        public int Energy;

        public Enemy(JSONObject jSONObject)
        {
            if (jSONObject[JSONLevelUpdaterKey.JSONLevelEnemiesType] != null)
            {
                try
                {
                    Type = (ObjectTypeEnum)Enum.Parse(typeof(ObjectTypeEnum), jSONObject[JSONLevelUpdaterKey.JSONLevelEnemiesType].str, true);
                }
                catch (Exception)
                {
                    Type = ObjectTypeEnum.EnemySimple;
                }
            }
            if (jSONObject[JSONLevelUpdaterKey.JSONLevelEnemiesEnergy] != null)
            {
                Energy = (int)jSONObject[JSONLevelUpdaterKey.JSONLevelEnemiesEnergy].i;
            }
        }


    }

    public class Food
    {
        public ObjectTypeFood Type;
        public float Energy;

        public Food(JSONObject jSONObject)
        {
            if (jSONObject[JSONLevelUpdaterKey.JSONLevelEnemiesType] != null)
            {
                try
                {
                    Type = (ObjectTypeFood)Enum.Parse(typeof(ObjectTypeFood), jSONObject[JSONLevelUpdaterKey.JSONLevelFoodsType].str, true);
                }
                catch (Exception)
                {
                    Type = ObjectTypeFood.Energy;
                }
            }
            if (jSONObject[JSONLevelUpdaterKey.JSONLevelFoodsEnergy] != null)
            {
                Energy = jSONObject[JSONLevelUpdaterKey.JSONLevelFoodsEnergy].f;
            }
        }

    }
}
