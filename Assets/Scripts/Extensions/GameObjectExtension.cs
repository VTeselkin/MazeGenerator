﻿using System;
using System.Linq;
using UnityEngine;

namespace Assets.Scripts.Extensions
{
    public static class GameObjectExtension
    {


        public static T[] GetComponentsByTypeAndNamePart<T>(this GameObject obj, String namepart) where T : Component
        {
            T[] components = obj.GetComponentsInChildren<T>().Where(s => s.name.Contains(namepart)).ToArray();

            return components;
        }

        public static void SetColliderStateInAllChildren(this GameObject obj, Boolean colliderState)
        {
            foreach (BoxCollider boxCollider in obj.GetComponentsInChildren<BoxCollider>())
            {
                boxCollider.enabled = colliderState;
            }
        }

        public static T GetComponentByTypeAndName<T>(this GameObject obj, String name) where T : Component
        {
            T component = obj.GetComponentsInChildren<T>().FirstOrDefault(f => f.name.Equals(name));
            return component;
        }
    }
}