using System;

public static class SystemExtends
{
    public static void SafeInvoke(this Action action)
    {
        if (action != null)
        {
            action();
        }
    }

    public static void SafeInvoke<T>(this Action<T> action, T par)
    {
        if (action != null)
        {
            action(par);
        }
    }

    public static void SafeInvoke<T1, T2>(this Action<T1, T2> action, T1 par1, T2 par2)
    {
        if (action != null)
        {
            action(par1, par2);
        }
    }

    public static void SafeInvoke<T1, T2, T3>(this Action<T1, T2, T3> action, T1 par1, T2 par2, T3 par3)
    {
        if (action != null)
        {
            action(par1, par2, par3);
        }
    }

    public static void SafeInvoke<T1, T2, T3, T4>(this Action<T1, T2, T3, T4> action, T1 par1, T2 par2, T3 par3, T4 par4)
    {
        if (action != null)
        {
            action(par1, par2, par3, par4);
        }
    }

    private static System.Random rng = new System.Random();

    public static void Shuffle<T>(this System.Collections.Generic.IList<T> list)
    {
        int n = list.Count;
        while (n > 1)
        {
            n--;
            int k = rng.Next(n + 1);
            T value = list[k];
            list[k] = list[n];
            list[n] = value;
        }
    }
}
