﻿using System;
using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.Shared;
using Fabric.Answers;
using Facebook.Unity;
using GameSparks.Api.Requests;
using UnityEngine;

namespace Assets.Scripts.Server
{
    public class FaceBookManager : UnitySingleton<FaceBookManager>
    {

        public string UserID { get; private set; }

        public void Init()
        {
            if (!FB.IsInitialized)
            {
                DebugLogger.Log("FB : Initialize the Facebook SDK");
                FB.Init(InitCallback, OnHideUnity);
            }
            else
            {
                DebugLogger.Log("FB : Already initialized, signal an app activation App Event");
                FB.ActivateApp();
                OnCompleteAutorization();
            }
        }

        public void Login()
        {
            DebugLogger.Log("FB : Try to Login");
            var perms = new List<string> { "email", "user_friends", "public_profile" };
            FB.LogInWithReadPermissions(perms, AuthCallback);
        }

        public void Logout()
        {
            if (FB.IsLoggedIn)
            {
                FB.LogOut();
                StartCoroutine(CheckForSuccussfulLogout());
                DebugLogger.Log("FB : Try to Logout");
            }
            else
            {
                DebugLogger.Log("FB : You not connected");
            }
        }

        private IEnumerator CheckForSuccussfulLogout()
        {
            if (FB.IsLoggedIn)
            {
                yield return new WaitForSeconds(0.1f);
                StartCoroutine(CheckForSuccussfulLogout());
            }
            else
            {
                var aToken = AccessToken.CurrentAccessToken;
                if (aToken != null)
                {
                    UserID = aToken.UserId;
                    GameSparkManager.instance.FacebookConnectRequest(aToken, (string obj) => { UserStorage.Instance.FacebookConnectRequest(true); }, DebugLogger.LogError);
                }
                OnCompleteAutorization();
            }
        }

        public bool IsLogin()
        {
            return FB.IsLoggedIn;
        }

        public void ShareScoreWihtFriend(int Score, int level)
        {
            if (IsLogin())
            {
                FB.FeedShare(linkCaption: "I`m win World of Maze game",
                    picture: new Uri("https://www.dropbox.com/s/7ksrreaovjwj0te/512.png?dl=0"),
                    linkName: "My Score is " + Score + "in " + level + "level !",
                             link: new Uri("https://play.google.com/store/apps/details?id=com.teamsoft.maze"));
            }
            else
            {
                Login();
            }
        }
        public void InviteFriends(FacebookDelegate<IAppRequestResult> callback)
        {
            if (IsLogin())
            {
                FB.AppRequest(
                   "You are invited to take part in a wondrous game World of Maze! Download it for FREE today and join your friends!",
                    title: "Invite your friend to join you", callback: callback);
            }
            else
            {
                Login();
            }
        }


        #region Event



        public event Action<bool> CompleteAutorization;

        private void OnCompleteAutorization()
        {
            var handler = CompleteAutorization;
            if (handler != null) { handler(FB.IsLoggedIn); }
        }


        #endregion

        #region CallBack

        private void InitCallback()
        {
            if (FB.IsInitialized)
            {
                FB.ActivateApp();
                DebugLogger.Log("FB : IsInitialized Complete ");
                DebugLogger.Log("FB : IsLogin = " + FB.IsLoggedIn);
                var aToken = AccessToken.CurrentAccessToken;
                if (aToken != null)
                {
                    UserID = aToken.UserId;
                    GameSparkManager.instance.FacebookConnectRequest(aToken, (string obj) => { UserStorage.Instance.FacebookConnectRequest(false); }, DebugLogger.LogError);
                }
                OnCompleteAutorization();
            }
            else
            {
                Login();
            }

        }

        private void OnHideUnity(bool isGameShown)
        {
            if (!isGameShown)
            {
                // Pause the game - we will need to hide
                Time.timeScale = 0;
            }
            else
            {
                // Resume the game - we're getting focus again
                Time.timeScale = 1;
            }
        }

        private void AuthCallback(ILoginResult result)
        {
            if (FB.IsLoggedIn)
            {
                DebugLogger.Log("FB : Login Complete");
                var aToken = AccessToken.CurrentAccessToken;
                if (aToken != null)
                {
                    AnalyticsWrapper.Instance.EventFBLogin(true);
                    UserID = aToken.UserId;
                    GameSparkManager.instance.FacebookConnectRequest(aToken, (string obj) => { UserStorage.Instance.FacebookConnectRequest(true); }, DebugLogger.LogError);
                }
                else
                {
                    AnalyticsWrapper.Instance.EventFBLogin(false);
                    DebugLogger.Log("User cancelled login");
                }
                OnCompleteAutorization();
            }

            #endregion
        }
    }
}