﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class FacebookImageCacheManager : MonoBehaviour
{
	#region Singleton members

	private static readonly object _lockObject = new object();

	private static FacebookImageCacheManager _instance;

	public static FacebookImageCacheManager Instance
	{
		get
		{
			if (_instance == null)
			{
				lock (_lockObject)
				{
					if (_instance == null)
					{
						var gameObject = new GameObject("Singleton: " + (typeof(FacebookImageCacheManager)));
						_instance = gameObject.AddComponent<FacebookImageCacheManager>();
					}
				}
			}

			return _instance;
		}
	}

	private void Awake()
	{
		if (_instance)
		{
			DestroyImmediate(gameObject);
		}
		else
		{
			_instance = this.GetComponent<FacebookImageCacheManager>();
			DontDestroyOnLoad(gameObject);
		}
	}
	#endregion

	#region Example

	private void LoadingExample()
	{
		Texture avatar = GetOrAddTextureForKey("100008251142079", true, updateAvatar);
	}

	private void updateAvatar(Texture texture)
	{
		Texture avatarTexture = FacebookImageCacheManager.Instance.GetTextureForKey("100008251142079");

		GameObject avatar = GameObject.FindGameObjectWithTag("ScoreImage");
		UITexture text = NGUITools.AddWidget<UITexture>(avatar);
		text.material = new Material(Shader.Find("Unlit/Transparent Colored"));
		text.material.mainTexture = texture;
		text.MakePixelPerfect();
	}

	#endregion

	#region Fields

	private Dictionary<string, SavedTexture> _images = new Dictionary<string, SavedTexture>();
	private string[] _storageFiles = null;

	#endregion

	#region Events

	public event Action<SavedTexture> TextureLoaded;
	protected virtual void OnTextureLoaded(SavedTexture obj)
	{
		Action<SavedTexture> handler = TextureLoaded;
		if (handler != null) handler(obj);
	}

	#endregion

	#region Methods
	protected FacebookImageCacheManager()
	{
	}



	public void Initialize()
	{
		//LoadImagesFromLocalStorage();

		string directory = Path.GetDirectoryName(FacebookImageCacheConstants.ImageStoragePath);

		if (Directory.Exists(directory))
			_storageFiles = Directory.GetFiles(directory, "*" + FacebookImageCacheConstants.ImageExtension);
	}

	//private void LoadImagesFromLocalStorage() {                        
	//    string directory = Path.GetDirectoryName(FacebookImageCacheConstants.ImageStoragePath);
	//    if (Directory.Exists(directory)) {                
	//        _storageFiles = Directory.GetFiles(directory, "*" + FacebookImageCacheConstants.ImageExtension);
	//        StartCoroutine("LoadAllImagesFromLocalStorage");
	//    }
	//}

	private IEnumerator LoadAllImagesFromLocalStorage()
	{

		foreach (string filePath in _storageFiles)
		{
			WWW www = new WWW("file://" + filePath);
			yield return www;

			if (string.IsNullOrEmpty(www.error))
			{
				AddImageToCache(Path.GetFileNameWithoutExtension(filePath), "file://" + filePath, false);
			}
		}
	}

	private IEnumerator LoadImageFromLocalStorage(string id, Action<Texture> textureLoadingCallback)
	{

		foreach (string filePath in _storageFiles)
		{
			string fileNameWithoutExtension = Path.GetFileNameWithoutExtension(filePath);
			if (fileNameWithoutExtension == id)
			{
				WWW www = new WWW("file://" + filePath);
				yield return www;

				if (string.IsNullOrEmpty(www.error))
					AddImageToCache(Path.GetFileNameWithoutExtension(filePath), "file://" + filePath, false, textureLoadingCallback);
			}
		}
	}


	private bool TextureIsLoadingFromLocalStorage(string id)
	{
		if (_storageFiles != null)
		{
			foreach (string fileName in _storageFiles)
			{
				string fileNameWithoutExtension = Path.GetFileNameWithoutExtension(fileName);

				if (fileNameWithoutExtension == id)
					return true;
			}
		}

		return false;
	}

	public void AddImageToCache(string key, string url, bool needSaveInStorage, Action<Texture> textureLoadingCallback = null)
	{

#if PLATFORM_WEBPLAYER
            needSaveInStorage = false;
#endif
		if (!_images.ContainsKey(key))
		{
			var go = new GameObject(key);
			var comp = go.AddComponent<SavedTexture>();
			comp.NeedSaveInStorage = needSaveInStorage;
			comp.FacebookId = key;
			comp.loadingCompleteCallback = textureLoadingCallback;

			comp.Loaded += SavedTexture_Loaded;
			go.transform.parent = transform;

			comp.URL = url;
		}
	}

	private void SavedTexture_Loaded(object sender, EventArgs e)
	{
		var savedTexture = sender as SavedTexture;
		_images[savedTexture.FacebookId] = savedTexture;

		OnTextureLoaded(savedTexture);
	}

	private string GetUrlFromID(string fbId)
	{
		return string.Format(FacebookImageCacheConstants.ImageUrl, fbId, FacebookImageCacheConstants.ImageSize);
	}

	public Texture GetTextureForKey(string key)
	{
		if (!TextureExist(key))
			return null;

		return _images[key].GetTexture();
	}

	public bool TextureExist(string key)
	{
		return _images.ContainsKey(key);
	}

	public Texture GetOrAddTextureForKey(string key, bool needSaveInStorage, Action<Texture> textureLoadingCallback = null)
	{

		if (!TextureExist(key))
		{
			if (!TextureIsLoadingFromLocalStorage(key))
			{

				string url = GetUrlFromID(key);

				AddImageToCache(key, url, needSaveInStorage, textureLoadingCallback);

				return null;
			}
			else
			{
				StartCoroutine(LoadImageFromLocalStorage(key, textureLoadingCallback));
				return null;
			}
		}
		else
		{
			return _images[key].GetTexture();
		}
	}

	public Texture GetOrAddTextureForURL(string key, string url, bool needSaveInStorage, Action<Texture> textureLoadingCallback = null)
	{
		if (!TextureExist(key))
		{
			if (!TextureIsLoadingFromLocalStorage(key))
			{
				AddImageToCache(key, url, needSaveInStorage, textureLoadingCallback);

				return null;
			}
			else
			{
				StartCoroutine(LoadImageFromLocalStorage(key, textureLoadingCallback));

				return null;
			}
		}
		else
		{
			return _images[key].GetTexture();
		}
	}
	#endregion
}
