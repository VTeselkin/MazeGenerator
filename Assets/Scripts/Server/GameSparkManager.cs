﻿using System;
using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.Shared;
using GameSparks.Api.Requests;
using UnityEngine;

namespace Assets.Scripts.Server
{
    public class GameSparkManager : UnitySingleton<GameSparkManager>
    {

        public bool isInit { get; private set; }
        public bool IsDevelopmentBuild { get; private set; }
        public enum UserProgressType
        {
            Coins,
            EnergyBoost,
            SpeedBoost,
            DistanceBoost,
            NoADS,
            NoBaner
        }

        public void Init()
        {
            base.Init();
            StartCoroutine(WaitForTaskComplete(InitComplete, 5f));
#if UNITY_ANDROID
            GPPlatformBuildSettings currentSettings = Resources.Load<GPPlatformBuildSettings>("GPPlatformBuildSettings");
            IsDevelopmentBuild = currentSettings.Development;

#else
         IOSPlatformBuildSettings currentSettings = Resources.Load<IOSPlatformBuildSettings>("IOSPlatformBuildSettings");
         IsDevelopmentBuild = currentSettings.Development;
#endif
            GameSparksSettings.DebugBuild = IsDevelopmentBuild;
            GameSparksSettings.PreviewBuild = IsDevelopmentBuild;
            if (GameSparks.Core.GS.Instance.Available)
            {
                DeviceAuthenticationRequest();
            }
            else
            {
                GameSparks.Core.GS.GameSparksAvailable += GsServiceHandler;
            }
            if (!InternetReachabilityController.Instance.IsInternetAvalible)
            {
                StopCoroutine(WaitForTaskComplete(InitComplete, 5));
                OnInitComplete(isInit);
            }

        }

        private void GsServiceHandler(bool obj)
        {
            GameSparks.Core.GS.GameSparksAvailable -= GsServiceHandler;
            DeviceAuthenticationRequest();
            StopCoroutine(WaitForTaskComplete(InitComplete, 5));
        }

        private void DeviceAuthenticationRequest()
        {
            new DeviceAuthenticationRequest().Send((response) =>
            {
                DebugLogger.Log("GameSparkManager 3 = " + isInit);
                isInit = !response.HasErrors;
                //initilized user records on the server
                if (isInit && !AppSettings.Instance.IsInitDataGameSparks)
                {
                    PostUserProgress(UserProgressType.DistanceBoost, 0, (string successMess) =>
                    {
                        DebugLogger.Log("GameSparkManager first init complete = " + successMess);
                    }, (string errorMess) =>
                    {
                        DebugLogger.LogError("GameSparkManager first init error = " + errorMess);
                    });
                    AppSettings.Instance.IsInitDataGameSparks = true;
                }
                OnInitComplete(isInit);
                DebugLogger.Log("Game Spark Serive Initilized = " + isInit + " !");
            });
        }
        #region User Data

        public void GetScoreLeaderBoard(Action<List<GameSparkUser>> callBackSucces, Action<string> callBackError)
        {
            var leaderBoard = new List<GameSparkUser>();
            new LeaderboardDataRequest().SetLeaderboardShortCode("highScoreLeaderboard")
                .SetEntryCount(20)
                .Send(response =>
                {
                    if (!response.HasErrors)
                    {
                        int index = 1;
                        foreach (var data in response.Data)
                        {
                            var user = new GameSparkUser
                            {
                                Id = data.ExternalIds.GetString("FB"),
                                UserName = data.UserName,
                                Country = data.Country,
                                City = data.City
                            };

                            var numberValue = data.GetNumberValue("score");
                            if (numberValue != null) user.UserScore = (int)numberValue;
                            if (!IsDevelopmentBuild && user.Id == "10208766383996463")
                            {
                                continue;
                            }
                            user.UserRank = index;
                            index++;
                            leaderBoard.Add(user);

                        }
                        if (callBackSucces != null)
                        {
                            callBackSucces(leaderBoard);
                        }
                    }
                    else
                    {
                        if (callBackError != null)
                        {
                            callBackError(response.JSONString);
                        }
                    }
                });
        }

        public void GetStarsLeaderBoard(Action<List<GameSparkUser>> callBackSucces, Action<string> callBackError)
        {
            var leaderBoard = new List<GameSparkUser>();
            new LeaderboardDataRequest().SetLeaderboardShortCode("highStarsLeaderboard")
                .SetEntryCount(20)
                .Send(response =>
                {
                    if (!response.HasErrors)
                    {
                        foreach (var data in response.Data)
                        {
                            var user = new GameSparkUser
                            {
                                Id = data.ExternalIds.GetString("FB"),
                                UserName = data.UserName,
                                Country = data.Country,
                                City = data.City
                            };
                            JSONObject json = new JSONObject(data.JSONString);
                            if (json["stars"] != null)
                            {
                                user.UserStars = (int)json["stars"].i;
                            }
                            if (data.Rank != null) user.UserRank = (int)data.Rank;
                            var numberValue = data.GetNumberValue("score");
                            if (numberValue != null) user.UserScore = (int)numberValue;
                            if (!IsDevelopmentBuild && user.Id == "10208766383996463")
                            {
                                continue;
                            }

                            leaderBoard.Add(user);

                        }
                        if (callBackSucces != null)
                        {
                            callBackSucces(leaderBoard);
                        }
                    }
                    else
                    {
                        if (callBackError != null)
                        {
                            callBackError(response.JSONString);
                        }
                    }
                });
        }


        public void GetLeaderBoardByLevel(int level, Action<List<GameSparkUser>> callBackSucces, Action<string> callBackError)
        {
            var leaderBoard = new List<GameSparkUser>();
            new LeaderboardDataRequest().SetLeaderboardShortCode("highScoreLeaderboardLevel.level." + level)
                .SetEntryCount(20)
                .Send(response =>
                {
                    DebugLogger.Log(response.JSONString);
                    if (!response.HasErrors)
                    {
                        foreach (var data in response.Data)
                        {
                            var user = new GameSparkUser
                            {
                                Id = data.ExternalIds.GetString("FB"),
                                UserName = data.UserName,
                                Country = data.Country,
                                City = data.City
                            };
                            if (data.Rank != null) user.UserRank = (int)data.Rank;
                            var numberValue = data.GetNumberValue("score");
                            if (numberValue != null) user.UserScore = (int)numberValue;
                            if (!IsDevelopmentBuild && user.Id == "10208766383996463")
                            {
                                continue;
                            }
                            leaderBoard.Add(user);
                        }
                        if (callBackSucces != null)
                        {
                            callBackSucces(leaderBoard);
                        }
                    }
                    else
                    {
                        if (callBackError != null)
                        {
                            callBackError(response.JSONString);
                        }
                    }
                });
        }

        public void GetUserData(Action<string> callBackSucces, Action<string> callBackError)
        {
            if (!isInit || !FaceBookManager.instance.IsLogin()) return;
            new AccountDetailsRequest().Send((response) =>
            {
                if (!response.HasErrors)
                {
                    if (callBackSucces != null)
                    {
                        callBackSucces(response.JSONString);
                    }
                }
                else
                {
                    if (callBackError != null)
                    {
                        callBackError(response.JSONString);
                    }
                }
            });
        }

        public void PostUserScore(int score, Action<string> callBackSucces, Action<string> callBackError)
        {
            if (!isInit || !FaceBookManager.instance.IsLogin()) return;
            if (!InternetReachabilityController.Instance.IsInternetAvalible)
            {
                AppSettings.Instance.IsNeedSynchronize = false;
                return;
            }
            new LogEventRequest().SetEventKey("postScore").SetEventAttribute("score", score).Send((response) =>
            {
                if (!response.HasErrors)
                {
                    if (callBackSucces != null)
                    {
                        callBackSucces(response.JSONString);
                    }
                }
                else
                {
                    if (callBackError != null)
                    {
                        AppSettings.Instance.IsNeedSynchronize = false;
                        callBackError(response.JSONString);
                    }
                }
            });
        }

        public void PostUserScoreByLevel(int score, int level, Action<string> callBackSucces, Action<string> callBackError)
        {
            if (!isInit || !FaceBookManager.instance.IsLogin()) return;
            if (!InternetReachabilityController.Instance.IsInternetAvalible)
            {
                AppSettings.Instance.IsNeedSynchronize = false;
                return;
            }
            new LogEventRequest().SetEventKey("postScoreByLevel")
                                 .SetEventAttribute("score", score)
                                 .SetEventAttribute("level", level)
                                 .Send((response) =>
            {
                if (!response.HasErrors)
                {
                    if (callBackSucces != null)
                    {
                        callBackSucces(response.JSONString);
                    }
                }
                else
                {
                    if (callBackError != null)
                    {
                        AppSettings.Instance.IsNeedSynchronize = false;
                        callBackError(response.JSONString);
                    }
                }
            });
        }

        public void PostUserStars(int stars, Action<string> callBackSucces, Action<string> callBackError)
        {
            if (!isInit || !FaceBookManager.instance.IsLogin()) return;
            if (!InternetReachabilityController.Instance.IsInternetAvalible)
            {
                AppSettings.Instance.IsNeedSynchronize = false;
                return;
            }
            new LogEventRequest().SetEventKey("postStars").SetEventAttribute("stars", stars).Send((response) =>
            {
                if (!response.HasErrors)
                {
                    if (callBackSucces != null)
                    {
                        callBackSucces(response.JSONString);
                    }
                }
                else
                {
                    if (callBackError != null)
                    {
                        AppSettings.Instance.IsNeedSynchronize = false;
                        callBackError(response.JSONString);
                    }
                }
            });
        }

        public void PostUserStarsByLevel(int score, int level, Action<string> callBackSucces, Action<string> callBackError)
        {
            if (!isInit || !FaceBookManager.instance.IsLogin()) return;
            if (!InternetReachabilityController.Instance.IsInternetAvalible)
            {
                AppSettings.Instance.IsNeedSynchronize = false;
                return;
            }
            new LogEventRequest().SetEventKey("postStarsByLevel")
                                 .SetEventAttribute("stars", score)
                                 .SetEventAttribute("level", level)
                                 .Send((response) =>
                                 {
                                     if (!response.HasErrors)
                                     {
                                         if (callBackSucces != null)
                                         {
                                             callBackSucces(response.JSONString);
                                         }
                                     }
                                     else
                                     {
                                         if (callBackError != null)
                                         {
                                             AppSettings.Instance.IsNeedSynchronize = false;
                                             callBackError(response.JSONString);
                                         }
                                     }
                                 });
        }

        public void PostUserProgress(UserProgressType path, int value, Action<string> callBackSucces, Action<string> callBackError)
        {
            if (!isInit) return;
            if (!InternetReachabilityController.Instance.IsInternetAvalible)
            {
                AppSettings.Instance.IsNeedSynchronize = false;
                return;
            }
            new LogEventRequest().SetEventKey("progressUpdate").
                                 SetEventAttribute("PATH", "User" + path.ToString()).
                                 SetEventAttribute("VAL", value)
                                 .Send((response) =>

                                 {
                                     if (!response.HasErrors)
                                     {
                                         DebugLogger.Log(response.JSONString);
                                         if (callBackSucces != null)
                                         {
                                             callBackSucces(response.JSONString);
                                         }
                                     }
                                     else
                                     {
                                         DebugLogger.Log(response.JSONString);
                                         if (callBackError != null)
                                         {
                                             AppSettings.Instance.IsNeedSynchronize = false;
                                             callBackError(response.JSONString);
                                         }
                                     }
                                 });
        }

        public void GetUserProgress(Action<string> callBackSucces, Action<string> callBackError)
        {
            if (!isInit) return;
            new LogEventRequest().SetEventKey("progressQuery").SetEventAttribute("PATH", "").Send((response) =>
                                 {
                                     if (!response.HasErrors)
                                     {
                                         if (callBackSucces != null)
                                         {
                                             callBackSucces(response.JSONString);
                                         }
                                     }
                                     else
                                     {

                                         if (callBackError != null)
                                         {
                                             callBackError(response.JSONString);
                                         }
                                     }
                                 });
        }

        #endregion

        #region Level Progress

        public void PostLevelProgress(int level, int score, int stars, Action<string> callBackSucces, Action<string> callBackError)
        {
            if (!isInit)
            {
                return;
            }
            if (!InternetReachabilityController.Instance.IsInternetAvalible)
            {
                AppSettings.Instance.IsNeedSynchronize = false;
                return;
            }
            new LogEventRequest().SetEventKey("progressUpdate").
                                 SetEventAttribute("PATH", "Level_" + level + ".stars").
                                 SetEventAttribute("VAL", stars)
                                 .Send((response) =>
            {
                if (!response.HasErrors)
                {

                    if (callBackSucces != null)
                    {
                        callBackSucces(response.JSONString);
                    }
                }
                else
                {
                    if (callBackError != null)
                    {
                        AppSettings.Instance.IsNeedSynchronize = false;
                        callBackError(response.JSONString);
                    }

                }
            });
            new LogEventRequest().SetEventKey("progressUpdate").
                                 SetEventAttribute("PATH", "Level_" + level + ".score").
                                SetEventAttribute("VAL", score)
                     .Send((response) =>
                     {
                         if (!response.HasErrors)
                         {

                             if (callBackSucces != null)
                             {
                                 callBackSucces(response.JSONString);
                             }
                         }
                         else
                         {
                             if (callBackError != null)
                             {
                                 callBackError(response.JSONString);
                             }

                         }
                     });

        }

        public void GetLevelProgress(Action<string> callBackSucces, Action<string> callBackError)
        {
            if (!isInit)
            {
                return;
            }
            new LogEventRequest().SetEventKey("progressQuery").
                                 SetEventAttribute("PATH", "")
                                 .Send((response) =>
                                 {
                                     if (!response.HasErrors)
                                     {
                                         if (callBackSucces != null)
                                         {
                                             callBackSucces(response.JSONString);
                                         }
                                     }
                                     else
                                     {
                                         if (callBackError != null)
                                         {
                                             callBackError(response.JSONString);
                                         }

                                     }
                                 });
        }

        #endregion

        #region GameConfig

        public void GetGameConfig(Action<string> callBackSucces, Action<string> callBackError)
        {
            if (!isInit) return;
            new GetPropertyRequest().SetPropertyShortCode("gameConfig").Send((response) =>
            {
                if (!response.HasErrors)
                {
                    if (callBackSucces != null)
                    {
                        callBackSucces(response.Property.JSON);
                    }

                }
                else
                {
                    if (callBackError != null)
                    {
                        callBackError(response.JSONString);
                    }
                }

            });
        }

        public void GetLevelData(Action<string> callBackSucces, Action<string> callBackError)
        {
            if (!isInit) return;
            new GetPropertyRequest().SetPropertyShortCode("levelDataConfig").Send((response) =>
            {
                if (!response.HasErrors)
                {
                    if (callBackSucces != null)
                    {
                        callBackSucces(response.Property.JSON);
                    }
                }
                else
                {
                    if (callBackError != null)
                    {
                        callBackError(response.JSONString);
                    }
                }

            });
        }

        #endregion

        #region Facebook Request

        public void FacebookConnectRequest(Facebook.Unity.AccessToken aToken, Action<string> callBackSucces, Action<string> callBackError)
        {
            new FacebookConnectRequest().SetAccessToken(aToken.TokenString).Send((responce) =>
           {
               if (responce.HasErrors)
               {
                   callBackError(responce.JSONString);
               }
               else
               {
                   callBackSucces(responce.JSONString);
               }
           }
                );
        }
        #endregion

        private IEnumerator WaitForTaskComplete(Action<bool> action, float delay)
        {
            yield return new WaitForSeconds(delay);
        }


        #region  Events

        public event Action<bool> InitComplete;

        private void OnInitComplete(bool complete)
        {
            var handler = InitComplete;
            if (handler != null) handler(complete);
        }



        #endregion


    }
}
