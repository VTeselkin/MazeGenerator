﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FacebookImageCacheConstants : MonoBehaviour
{

    public const string ImageUrl = "https://graph.facebook.com/{0}/picture?type=square&width={1}&height={1}";
    public const int ImageSize = 200;
    public const string ImageExtension = ".png";
    public static string ImageStoragePath
    {
        get
        {
            return AppSettings.StorageDataPath + "/Cache/Avatars/";
        }
    }
}

