﻿using System.Collections;
using System.IO;
using UnityEngine;
using System;



namespace Assets.Scripts.Server.FacebookImageCacheManager
{
    public class SavedTexture : MonoBehaviour
    {

        #region Fileds

        private const int _attemptsCount = 10;
        private const float _delayBetweenAttempts = 2.0f;

        private string _url = null;
        private Texture _texture = null;

        public string editorUrl = null;

        private bool _requestStarted = false;

        #endregion

        #region Properties
        public string URL
        {
            set
            {
                if (_url == null)
                {
                    _url = value;
                }
            }
        }
        public string FacebookId { get; set; }
        public bool NeedSaveInStorage { get; set; }

        #endregion

        #region Events

        public Action<Texture> loadingCompleteCallback = null;

        public event EventHandler Loaded;

        protected virtual void OnLoaded()
        {
            EventHandler handler = Loaded;
            if (handler != null) handler(this, EventArgs.Empty);
        }

        public event EventHandler Error;

        protected virtual void OnError()
        {
            EventHandler handler = Error;
            if (handler != null) handler(this, EventArgs.Empty);
        }

        #endregion

        #region Methods

        public SavedTexture()
        {
            NeedSaveInStorage = false;
        }

        private IEnumerator UpdateImageCoroutine()
        {
            int attemptNum = 0;

            while (attemptNum < _attemptsCount)
            {
                WWW www = new WWW(_url);
                yield return www;

                if (string.IsNullOrEmpty(www.error))
                {
                    MeshRenderer mr = gameObject.GetComponent<MeshRenderer>();
                    if (mr == null)
                        mr = gameObject.AddComponent<MeshRenderer>();
                    mr.material.mainTexture = www.texture;

                    _texture = www.texture;

                    if (NeedSaveInStorage)
                    {
                        string directory = Path.GetDirectoryName(FacebookImageCacheConstants.ImageStoragePath);
                        if (!Directory.Exists(directory))
                            Directory.CreateDirectory(directory);

                        string imageLocalStoragePath = FacebookImageCacheConstants.ImageStoragePath + FacebookId + FacebookImageCacheConstants.ImageExtension;
                        File.WriteAllBytes(imageLocalStoragePath, www.bytes);
                    }

                    if (loadingCompleteCallback != null)
                        loadingCompleteCallback(_texture);

                    OnLoaded();

                    break;
                }

                yield return new WaitForSeconds(_delayBetweenAttempts);

                attemptNum++;
            }

            if (attemptNum >= _attemptsCount)
            {
                OnError();
            }
        }

        private void Update()
        {
            if (_url != null && !_requestStarted)
            {
                editorUrl = _url;
                StartCoroutine("UpdateImageCoroutine");
                _requestStarted = true;
            }
        }

        public Texture GetTexture()
        {
            return _texture;
        }

        #endregion
    }
}