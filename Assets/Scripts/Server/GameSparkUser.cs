﻿using UnityEngine;

public class GameSparkUser
{

    public string UserName { get; set; }
    public int UserScore { get; set; }
    public int UserStars { get; set; }
    public int UserRank { get; set; }
    public string Country { get; set; }
    public string City { get; set; }
    public string Id { get; set; }
    public bool isBot = false;
}
