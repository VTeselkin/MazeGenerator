using UnityEngine;
using Assets.Scripts.GUI;
using Firebase.Analytics;
using UnityEngine.Purchasing;
using Fabric.Answers;
using System;

public class AnalyticsWrapper : MonoBehaviour
{
    #region Singleton

    private static AnalyticsWrapper _instance;

    public static AnalyticsWrapper Instance
    {
        get
        {
            if (_instance == null)
            {
                GameObject go = new GameObject("AnalyticsWrapper");
                DontDestroyOnLoad(go);
                _instance = go.AddComponent<AnalyticsWrapper>();

            }
            return _instance;
        }
    }



    private void Awake()
    {
        if (_instance == this)
        {
            DestroyImmediate(this.gameObject);
        }
        else
        {
            _instance = this;
        }

    }

    #endregion

    #region Event Method

    public void Init()
    {
        Parameter[] parameters = {
            new Parameter("CountLaunch", AppSettings.Instance.CountLaunch),
            new Parameter("NoADS", AppSettings.Instance.NoADS.ToString()),
            new Parameter("NoBanner", AppSettings.Instance.NoBanner.ToString()),
            new Parameter("IsNewVertion", AppSettings.Instance.IsNewVertion.ToString())
        };
        FirebaseAnalytics.LogEvent(AnalyticsHelper.EventKey.StartApplication.ToString());
    }

    public void EventPressBtn(AnalyticsHelper.EventKey key)
    {
        FirebaseAnalytics.LogEvent(key.ToString());
    }
    public void EventPopUp(UIElementType type, bool hidePrevios)
    {
        string value = hidePrevios ? "hide" : "show";
        Parameter[] parameters = { new Parameter("hidePrevios", value) };
        FirebaseAnalytics.LogEvent("PopupOpen_" + type.ToString(), parameters);
    }

    public void EventWinLevel(LevelData level, int score, int stars)
    {
        Parameter[] parameters = {
            new Parameter("level", level.LevelNumber),
            new Parameter("level_type", level.LevelTarget.ToString()),
            new Parameter("player_energy", level.PlayerEnergy),
            new Parameter("player_reward_score", score),
            new Parameter("player_reward_star", stars)
        };
        FirebaseAnalytics.LogEvent(AnalyticsHelper.EventKey.LevelWin.ToString(), parameters);
        Answers.LogLevelEnd(level.LevelNumber.ToString(), score, true, new System.Collections.Generic.Dictionary<string, object> { { "stars", stars } });
    }

    public void EventLoseLevel(LevelData level)
    {
        Parameter[] parameters = {
            new Parameter("level", level.LevelNumber),
            new Parameter("level_type", level.LevelTarget.ToString()),
            new Parameter("player_energy", level.PlayerEnergy)
        };
        FirebaseAnalytics.LogEvent(AnalyticsHelper.EventKey.LevelLose.ToString(), parameters);
        Answers.LogLevelEnd(level.LevelNumber.ToString(), null, false);
    }

    public void EventLoadOut(LevelData level)
    {
        Parameter[] parameters = {
            new Parameter("level", level.LevelNumber),
            new Parameter("level_type", level.LevelTarget.ToString()),
            new Parameter("player_energy", level.PlayerEnergy),
            new Parameter("player_dimention", level.PlayDimension.ToString())
        };
        FirebaseAnalytics.LogEvent(AnalyticsHelper.EventKey.LevelLoadOut.ToString(), parameters);

    }

    public void EventByEnergyInGame(int addEnergy, int cost)
    {
        Parameter[] parameters = {
            new Parameter("currentEnergy", addEnergy),
            new Parameter("cost", cost)
        };

        FirebaseAnalytics.LogEvent(AnalyticsHelper.EventKey.ByEnergyInGame.ToString(), parameters);
    }


    public void EventBySpeedInGame(float addSpeed, int cost)
    {
        Parameter[] parameters = {
            new Parameter("currentSpeed", addSpeed),
            new Parameter("cost", cost)
        };

        FirebaseAnalytics.LogEvent(AnalyticsHelper.EventKey.BySpeedInGame.ToString(), parameters);
    }

    public void StartScene(AnalyticsHelper.SceneKey key)
    {
        FirebaseAnalytics.LogEvent(key.ToString());
    }

    public void EventPurchaseSuccess(Product product)
    {
        Parameter[] parameters = {
            new Parameter("transactionID", product.transactionID),
            new Parameter("ID", product.definition.id),
            new Parameter("receipt", product.receipt),
        };
        FirebaseAnalytics.LogEvent(AnalyticsHelper.EventKey.PurchaseSuccess.ToString(), parameters);
        Answers.LogPurchase(product.metadata.localizedPrice, product.metadata.isoCurrencyCode, true, product.definition.type.ToString(), product.definition.storeSpecificId);
    }

    public void EventPurchaseFail(string message)
    {
        Parameter[] parameters = {
            new Parameter("reason", message)
        };
        FirebaseAnalytics.LogEvent(AnalyticsHelper.EventKey.PurchaseFail.ToString(), parameters);
    }

    public void EventPurchaseCancel(string message)
    {
        Parameter[] parameters = {
            new Parameter("reason", message)
        };
        FirebaseAnalytics.LogEvent(AnalyticsHelper.EventKey.PurchaseCances.ToString(), parameters);
    }

    public void EventAdsState(AnalyticsHelper.ADSKey key)
    {
        FirebaseAnalytics.LogEvent(key.ToString());
    }

    public void EventIniviteState(bool result)
    {
        Answers.LogInvite();
        Parameter[] parameters = {
            new Parameter("result", result.ToString())
        };
        FirebaseAnalytics.LogEvent(AnalyticsHelper.EventKey.SendInviteFrieds.ToString(), parameters);
    }

    public void EventFBLogin(bool result)
    {
        Answers.LogLogin(null, result);
        Parameter[] parameters = {
            new Parameter("result", result.ToString())
        };
        FirebaseAnalytics.LogEvent(AnalyticsHelper.EventKey.FBLogin.ToString(), parameters);
    }


    public void EventLevelStart(LevelData level)
    {
        Answers.LogLevelStart(level.LevelNumber.ToString());
        Parameter[] parameters = {
            new Parameter("levelNumber", level.LevelNumber)
        };
        FirebaseAnalytics.LogEvent(AnalyticsHelper.EventKey.LevelStart.ToString(), parameters);
    }


    public void TrackReceiveLocalNotificationByDay(int day, int mess0DayIndex)
    {
        Parameter[] parameters = {
            new Parameter("day", day),
            new Parameter("index", mess0DayIndex)
        };
        FirebaseAnalytics.LogEvent(AnalyticsHelper.EventKey.LocalNotification.ToString(), parameters);
    }

    #endregion

    private void OnDestroy()
    {
        FirebaseAnalytics.LogEvent(AnalyticsHelper.EventKey.CloseAplication.ToString());
    }


}
