using UnityEngine;
using System.Collections;

public static class AnalyticsHelper
{
    #region Event Key

    public enum EventKey
    {
        StartApplication,
        PressBtnMenuPlay,
        PressBtnMenuFb,
        PressBtnMenuStoreTop,
        PressBtnMenuSettings,
        PressBtnMenuStore,
        PressBtnCoinsLeaderBoard,
        PressBtnStarsLeaderBoard,
        PressBtnGameAddEnergy,
        PressBtnGameEnergy,
        PressBtnGameSpeed,
        PressBtnGamePause,
        PressBtnGameNoAds,
        PressBtnPauseMenu,
        PressBtnPauseRestart,
        PressBtnPauseReturn,
        PressBtnLostMenu,
        PressBtnLostRestart,
        PressBtnWinMenu,
        PressBtnWinRestart,
        PressBtnWinNext,

        PressSnailOnMenu,

        StartNextLevel,
        UserDisableBanner,
        CloseAplication,

        LevelWin,
        LevelLose,
        LevelStart,
        ByEnergyInGame,
        BySpeedInGame,

        PurchaseSuccess,
        PurchaseFail,
        PurchaseCances,

        SendInviteFrieds,
        FBLogin,

        LocalNotification,
        LevelLoadOut,


    }
    public enum ADSKey
    {
        onInterstitialShown,
        onInterstitialClosed,
        onInterstitialClicked,

        onRewardedVideoShown,
        onRewardedVideoClosed,

        onBannerClicked
    }

    public enum SceneKey
    {
        StartMenuScene,
        StartGameScene
    }
    #endregion
}
