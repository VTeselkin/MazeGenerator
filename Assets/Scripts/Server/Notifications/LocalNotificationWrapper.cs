using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using Assets.Scripts.Shared;


class LocalNotificationWrapper : MonoBehaviour
{
    private static LocalNotificationWrapper s_instance;
    public static bool IsDevelopmentBuild = false;
    public static string bundleId = "";
    public static LocalNotificationWrapper Instance
    {
        get
        {
            if (s_instance == null)
            {
                GameObject go = new GameObject("LocalNotificationWrapper");
                s_instance = go.AddComponent<LocalNotificationWrapper>();
                DontDestroyOnLoad(go);
            }
            return s_instance;
        }
    }

    bool notificationsCreated = false;
    List<int> m_currentNotificationsIds = new List<int>();


    public void Init()
    {
#if UNITY_ANDROID
        GPPlatformBuildSettings currentSettings = Resources.Load<GPPlatformBuildSettings>("GPPlatformBuildSettings");
        IsDevelopmentBuild = currentSettings.Development;
        bundleId = currentSettings.ApplicationIdentifier;
#else
         IOSPlatformBuildSettings currentSettings = Resources.Load<IOSPlatformBuildSettings>("IOSPlatformBuildSettings");
         IsDevelopmentBuild = currentSettings.Development;
        bundleId = currentSettings.ApplicationIdentifier;
#endif
        CancelAll();
        LoadIndexForCurrentTextNotification();
        SetIndexForNotificationMessage();




    }

    public void Create(int id, DateTime dt, string title, string message, bool repeat = false, IDictionary dict = null)
    {
        Create(id, dt, (dt - DateTime.Now).TotalSeconds, title, message, repeat, dict);
    }

    //with image
    public void Create(int id, DateTime dt, string title, string message, byte[] image, bool repeat = false, IDictionary dict = null)
    {
        Create(id, dt, (dt - DateTime.Now).TotalSeconds, title, message, repeat, dict, image);
    }

    public void Create(int id, DateTime dt, double seconds, string title, string message, bool repeat = false, IDictionary dict = null, byte[] image = null)
    {
        DebugLogger.Log("LocalNotificationWrapper.Create " + message + ": " + seconds.ToString() + "  " + dt.ToString() + " image = " + image);

        Notification en = new Notification();
        en.ID = id;
        using (var version = new AndroidJavaClass("android.os.Build$VERSION"))
        {
            if (version.GetStatic<int>("SDK_INT") >= 21 && version.GetStatic<int>("SDK_INT") <= 22)
            {
                DebugLogger.Log("LocalNotificationWrapper EmojiPattern = " + message);
                var EmojiPattern = @"(?:\uD83D(?:[\uDC76\uDC66\uDC67](?:\uD83C[\uDFFB-\uDFFF])?|\uDC68(?:(?:\uD83C(?:[\uDFFB-\uDFFF](?:\u200D(?:\u2695\uFE0F?|\uD83C[\uDF93\uDFEB\uDF3E\uDF73\uDFED\uDFA4\uDFA8]|\u2696\uFE0F?|\uD83D[\uDD27\uDCBC\uDD2C\uDCBB\uDE80\uDE92]|\u2708\uFE0F?|\uD83E[\uDDB0-\uDDB3]))?)|\u200D(?:\u2695\uFE0F?|\uD83C[\uDF93\uDFEB\uDF3E\uDF73\uDFED\uDFA4\uDFA8]|\u2696\uFE0F?|\uD83D(?:\uDC69\u200D\uD83D(?:\uDC66(?:\u200D\uD83D\uDC66)?|\uDC67(?:\u200D\uD83D[\uDC66\uDC67])?)|\uDC68\u200D\uD83D(?:\uDC66(?:\u200D\uD83D\uDC66)?|\uDC67(?:\u200D\uD83D[\uDC66\uDC67])?)|\uDC66(?:\u200D\uD83D\uDC66)?|\uDC67(?:\u200D\uD83D[\uDC66\uDC67])?|[\uDD27\uDCBC\uDD2C\uDCBB\uDE80\uDE92])|\u2708\uFE0F?|\uD83E[\uDDB0-\uDDB3]|\u2764(?:\uFE0F\u200D\uD83D(?:\uDC8B\u200D\uD83D\uDC68|\uDC68)|\u200D\uD83D(?:\uDC8B\u200D\uD83D\uDC68|\uDC68)))))?|\uDC69(?:(?:\uD83C(?:[\uDFFB-\uDFFF](?:\u200D(?:\u2695\uFE0F?|\uD83C[\uDF93\uDFEB\uDF3E\uDF73\uDFED\uDFA4\uDFA8]|\u2696\uFE0F?|\uD83D[\uDD27\uDCBC\uDD2C\uDCBB\uDE80\uDE92]|\u2708\uFE0F?|\uD83E[\uDDB0-\uDDB3]))?)|\u200D(?:\u2695\uFE0F?|\uD83C[\uDF93\uDFEB\uDF3E\uDF73\uDFED\uDFA4\uDFA8]|\u2696\uFE0F?|\uD83D(?:\uDC69\u200D\uD83D(?:\uDC66(?:\u200D\uD83D\uDC66)?|\uDC67(?:\u200D\uD83D[\uDC66\uDC67])?)|\uDC66(?:\u200D\uD83D\uDC66)?|\uDC67(?:\u200D\uD83D[\uDC66\uDC67])?|[\uDD27\uDCBC\uDD2C\uDCBB\uDE80\uDE92])|\u2708\uFE0F?|\uD83E[\uDDB0-\uDDB3]|\u2764(?:\uFE0F\u200D\uD83D(?:\uDC8B\u200D\uD83D[\uDC68\uDC69]|[\uDC68\uDC69])|\u200D\uD83D(?:\uDC8B\u200D\uD83D[\uDC68\uDC69]|[\uDC68\uDC69])))))?|[\uDC74\uDC75](?:\uD83C[\uDFFB-\uDFFF])?|\uDC6E(?:(?:\uD83C(?:[\uDFFB-\uDFFF](?:\u200D(?:[\u2642\u2640]\uFE0F?))?)|\u200D(?:[\u2642\u2640]\uFE0F?)))?|\uDD75(?:(?:\uFE0F(?:\u200D(?:[\u2642\u2640]\uFE0F?))?|\uD83C(?:[\uDFFB-\uDFFF](?:\u200D(?:[\u2642\u2640]\uFE0F?))?)|\u200D(?:[\u2642\u2640]\uFE0F?)))?|[\uDC82\uDC77](?:(?:\uD83C(?:[\uDFFB-\uDFFF](?:\u200D(?:[\u2642\u2640]\uFE0F?))?)|\u200D(?:[\u2642\u2640]\uFE0F?)))?|\uDC78(?:\uD83C[\uDFFB-\uDFFF])?|\uDC73(?:(?:\uD83C(?:[\uDFFB-\uDFFF](?:\u200D(?:[\u2642\u2640]\uFE0F?))?)|\u200D(?:[\u2642\u2640]\uFE0F?)))?|\uDC72(?:\uD83C[\uDFFB-\uDFFF])?|\uDC71(?:(?:\uD83C(?:[\uDFFB-\uDFFF](?:\u200D(?:[\u2642\u2640]\uFE0F?))?)|\u200D(?:[\u2642\u2640]\uFE0F?)))?|[\uDC70\uDC7C](?:\uD83C[\uDFFB-\uDFFF])?|[\uDE4D\uDE4E\uDE45\uDE46\uDC81\uDE4B\uDE47\uDC86\uDC87\uDEB6](?:(?:\uD83C(?:[\uDFFB-\uDFFF](?:\u200D(?:[\u2642\u2640]\uFE0F?))?)|\u200D(?:[\u2642\u2640]\uFE0F?)))?|[\uDC83\uDD7A](?:\uD83C[\uDFFB-\uDFFF])?|\uDC6F(?:\u200D(?:[\u2642\u2640]\uFE0F?))?|[\uDEC0\uDECC](?:\uD83C[\uDFFB-\uDFFF])?|\uDD74(?:(?:\uD83C[\uDFFB-\uDFFF]|\uFE0F))?|\uDDE3\uFE0F?|[\uDEA3\uDEB4\uDEB5](?:(?:\uD83C(?:[\uDFFB-\uDFFF](?:\u200D(?:[\u2642\u2640]\uFE0F?))?)|\u200D(?:[\u2642\u2640]\uFE0F?)))?|[\uDCAA\uDC48\uDC49\uDC46\uDD95\uDC47\uDD96](?:\uD83C[\uDFFB-\uDFFF])?|\uDD90(?:(?:\uD83C[\uDFFB-\uDFFF]|\uFE0F))?|[\uDC4C-\uDC4E\uDC4A\uDC4B\uDC4F\uDC50\uDE4C\uDE4F\uDC85\uDC42\uDC43](?:\uD83C[\uDFFB-\uDFFF])?|\uDC41(?:(?:\uFE0F(?:\u200D\uD83D\uDDE8\uFE0F?)?|\u200D\uD83D\uDDE8\uFE0F?))?|[\uDDE8\uDDEF\uDD73\uDD76\uDECD\uDC3F\uDD4A\uDD77\uDD78\uDDFA\uDEE3\uDEE4\uDEE2\uDEF3\uDEE5\uDEE9\uDEF0\uDECE\uDD70\uDD79\uDDBC\uDDA5\uDDA8\uDDB1\uDDB2\uDCFD\uDD6F\uDDDE\uDDF3\uDD8B\uDD8A\uDD8C\uDD8D\uDDC2\uDDD2\uDDD3\uDD87\uDDC3\uDDC4\uDDD1\uDDDD\uDEE0\uDDE1\uDEE1\uDDDC\uDECF\uDECB\uDD49]\uFE0F?|[\uDE00-\uDE06\uDE09-\uDE0B\uDE0E\uDE0D\uDE18\uDE17\uDE19\uDE1A\uDE42\uDE10\uDE11\uDE36\uDE44\uDE0F\uDE23\uDE25\uDE2E\uDE2F\uDE2A\uDE2B\uDE34\uDE0C\uDE1B-\uDE1D\uDE12-\uDE15\uDE43\uDE32\uDE41\uDE16\uDE1E\uDE1F\uDE24\uDE22\uDE2D\uDE26-\uDE29\uDE2C\uDE30\uDE31\uDE33\uDE35\uDE21\uDE20\uDE37\uDE07\uDE08\uDC7F\uDC79\uDC7A\uDC80\uDC7B\uDC7D\uDC7E\uDCA9\uDE3A\uDE38\uDE39\uDE3B-\uDE3D\uDE40\uDE3F\uDE3E\uDE48-\uDE4A\uDC64\uDC65\uDC6B-\uDC6D\uDC8F\uDC91\uDC6A\uDC63\uDC40\uDC45\uDC44\uDC8B\uDC98\uDC93-\uDC97\uDC99-\uDC9C\uDDA4\uDC9D-\uDC9F\uDC8C\uDCA4\uDCA2\uDCA3\uDCA5\uDCA6\uDCA8\uDCAB-\uDCAD\uDC53-\uDC62\uDC51\uDC52\uDCFF\uDC84\uDC8D\uDC8E\uDC35\uDC12\uDC36\uDC15\uDC29\uDC3A\uDC31\uDC08\uDC2F\uDC05\uDC06\uDC34\uDC0E\uDC2E\uDC02-\uDC04\uDC37\uDC16\uDC17\uDC3D\uDC0F\uDC11\uDC10\uDC2A\uDC2B\uDC18\uDC2D\uDC01\uDC00\uDC39\uDC30\uDC07\uDC3B\uDC28\uDC3C\uDC3E\uDC14\uDC13\uDC23-\uDC27\uDC38\uDC0A\uDC22\uDC0D\uDC32\uDC09\uDC33\uDC0B\uDC2C\uDC1F-\uDC21\uDC19\uDC1A\uDC0C\uDC1B-\uDC1E\uDC90\uDCAE\uDD2A\uDDFE\uDDFB\uDC92\uDDFC\uDDFD\uDD4C\uDD4D\uDD4B\uDC88\uDE82-\uDE8A\uDE9D\uDE9E\uDE8B-\uDE8E\uDE90-\uDE9C\uDEB2\uDEF4\uDEF9\uDEF5\uDE8F\uDEA8\uDEA5\uDEA6\uDED1\uDEA7\uDEF6\uDEA4\uDEA2\uDEEB\uDEEC\uDCBA\uDE81\uDE9F-\uDEA1\uDE80\uDEF8\uDD5B\uDD67\uDD50\uDD5C\uDD51\uDD5D\uDD52\uDD5E\uDD53\uDD5F\uDD54\uDD60\uDD55\uDD61\uDD56\uDD62\uDD57\uDD63\uDD58\uDD64\uDD59\uDD65\uDD5A\uDD66\uDD25\uDCA7\uDEF7\uDD2E\uDD07-\uDD0A\uDCE2\uDCE3\uDCEF\uDD14\uDD15\uDCFB\uDCF1\uDCF2\uDCDE-\uDCE0\uDD0B\uDD0C\uDCBB\uDCBD-\uDCC0\uDCFA\uDCF7-\uDCF9\uDCFC\uDD0D\uDD0E\uDCA1\uDD26\uDCD4-\uDCDA\uDCD3\uDCD2\uDCC3\uDCDC\uDCC4\uDCF0\uDCD1\uDD16\uDCB0\uDCB4-\uDCB8\uDCB3\uDCB9\uDCB1\uDCB2\uDCE7-\uDCE9\uDCE4-\uDCE6\uDCEB\uDCEA\uDCEC-\uDCEE\uDCDD\uDCBC\uDCC1\uDCC2\uDCC5-\uDCD0\uDD12\uDD13\uDD0F-\uDD11\uDD28\uDD2B\uDD27\uDD29\uDD17\uDD2C\uDD2D\uDCE1\uDC89\uDC8A\uDEAA\uDEBD\uDEBF\uDEC1\uDED2\uDEAC\uDDFF\uDEAE\uDEB0\uDEB9-\uDEBC\uDEBE\uDEC2-\uDEC5\uDEB8\uDEAB\uDEB3\uDEAD\uDEAF\uDEB1\uDEB7\uDCF5\uDD1E\uDD03\uDD04\uDD19-\uDD1D\uDED0\uDD4E\uDD2F\uDD00-\uDD02\uDD3C\uDD3D\uDD05\uDD06\uDCF6\uDCF3\uDCF4\uDD31\uDCDB\uDD30\uDD1F\uDCAF\uDD20-\uDD24\uDD36-\uDD3B\uDCA0\uDD18\uDD32-\uDD35\uDEA9])|\uD83E(?:[\uDDD2\uDDD1\uDDD3](?:\uD83C[\uDFFB-\uDFFF])?|[\uDDB8\uDDB9](?:\u200D(?:[\u2640\u2642]\uFE0F?))?|[\uDD34\uDDD5\uDDD4\uDD35\uDD30\uDD31\uDD36](?:\uD83C[\uDFFB-\uDFFF])?|[\uDDD9-\uDDDD](?:(?:\uD83C(?:[\uDFFB-\uDFFF](?:\u200D(?:[\u2640\u2642]\uFE0F?))?)|\u200D(?:[\u2640\u2642]\uFE0F?)))?|[\uDDDE\uDDDF](?:\u200D(?:[\u2640\u2642]\uFE0F?))?|[\uDD26\uDD37](?:(?:\uD83C(?:[\uDFFB-\uDFFF](?:\u200D(?:[\u2642\u2640]\uFE0F?))?)|\u200D(?:[\u2642\u2640]\uFE0F?)))?|[\uDDD6-\uDDD8](?:(?:\uD83C(?:[\uDFFB-\uDFFF](?:\u200D(?:[\u2640\u2642]\uFE0F?))?)|\u200D(?:[\u2640\u2642]\uFE0F?)))?|\uDD38(?:(?:\uD83C(?:[\uDFFB-\uDFFF](?:\u200D(?:[\u2642\u2640]\uFE0F?))?)|\u200D(?:[\u2642\u2640]\uFE0F?)))?|\uDD3C(?:\u200D(?:[\u2642\u2640]\uFE0F?))?|[\uDD3D\uDD3E\uDD39](?:(?:\uD83C(?:[\uDFFB-\uDFFF](?:\u200D(?:[\u2642\u2640]\uFE0F?))?)|\u200D(?:[\u2642\u2640]\uFE0F?)))?|[\uDD33\uDDB5\uDDB6\uDD1E\uDD18\uDD19\uDD1B\uDD1C\uDD1A\uDD1F\uDD32](?:\uD83C[\uDFFB-\uDFFF])?|[\uDD23\uDD70\uDD17\uDD29\uDD14\uDD28\uDD10\uDD24\uDD11\uDD2F\uDD75\uDD76\uDD2A\uDD2C\uDD12\uDD15\uDD22\uDD2E\uDD27\uDD20\uDD21\uDD73\uDD74\uDD7A\uDD25\uDD2B\uDD2D\uDDD0\uDD13\uDD16\uDD3A\uDD1D\uDDB0-\uDDB3\uDDE0\uDDB4\uDDB7\uDDE1\uDD7D\uDD7C\uDDE3-\uDDE6\uDD7E\uDD7F\uDDE2\uDD8D\uDD8A\uDD9D\uDD81\uDD84\uDD93\uDD8C\uDD99\uDD92\uDD8F\uDD9B\uDD94\uDD87\uDD98\uDDA1\uDD83\uDD85\uDD86\uDDA2\uDD89\uDD9A\uDD9C\uDD8E\uDD95\uDD96\uDD88\uDD80\uDD9E\uDD90\uDD91\uDD8B\uDD97\uDD82\uDD9F\uDDA0\uDD40\uDD6D\uDD5D\uDD65\uDD51\uDD54\uDD55\uDD52\uDD6C\uDD66\uDD5C\uDD50\uDD56\uDD68\uDD6F\uDD5E\uDDC0\uDD69\uDD53\uDD6A\uDD59\uDD5A\uDD58\uDD63\uDD57\uDDC2\uDD6B\uDD6E\uDD5F-\uDD61\uDDC1\uDD67\uDD5B\uDD42\uDD43\uDD64\uDD62\uDD44\uDDED\uDDF1\uDDF3\uDDE8\uDDE7\uDD47-\uDD49\uDD4E\uDD4F\uDD4D\uDD4A\uDD4B\uDD45\uDD4C\uDDFF\uDDE9\uDDF8\uDD41\uDDEE\uDDFE\uDDF0\uDDF2\uDDEA-\uDDEC\uDDEF\uDDF4-\uDDF7\uDDF9-\uDDFD])|[\u263A\u2639\u2620]\uFE0F?|\uD83C(?:\uDF85(?:\uD83C[\uDFFB-\uDFFF])?|\uDFC3(?:(?:\uD83C(?:[\uDFFB-\uDFFF](?:\u200D(?:[\u2642\u2640]\uFE0F?))?)|\u200D(?:[\u2642\u2640]\uFE0F?)))?|[\uDFC7\uDFC2](?:\uD83C[\uDFFB-\uDFFF])?|\uDFCC(?:(?:\uFE0F(?:\u200D(?:[\u2642\u2640]\uFE0F?))?|\uD83C(?:[\uDFFB-\uDFFF](?:\u200D(?:[\u2642\u2640]\uFE0F?))?)|\u200D(?:[\u2642\u2640]\uFE0F?)))?|[\uDFC4\uDFCA](?:(?:\uD83C(?:[\uDFFB-\uDFFF](?:\u200D(?:[\u2642\u2640]\uFE0F?))?)|\u200D(?:[\u2642\u2640]\uFE0F?)))?|\uDFCB(?:(?:\uFE0F(?:\u200D(?:[\u2642\u2640]\uFE0F?))?|\uD83C(?:[\uDFFB-\uDFFF](?:\u200D(?:[\u2642\u2640]\uFE0F?))?)|\u200D(?:[\u2642\u2640]\uFE0F?)))?|[\uDFCE\uDFCD\uDFF5\uDF36\uDF7D\uDFD4-\uDFD6\uDFDC-\uDFDF\uDFDB\uDFD7\uDFD8\uDFDA\uDFD9\uDF21\uDF24-\uDF2C\uDF97\uDF9F\uDF96\uDF99-\uDF9B\uDF9E\uDFF7\uDD70\uDD71\uDD7E\uDD7F\uDE02\uDE37]\uFE0F?|\uDFF4(?:(?:\u200D\u2620\uFE0F?|\uDB40\uDC67\uDB40\uDC62\uDB40(?:\uDC65\uDB40\uDC6E\uDB40\uDC67\uDB40\uDC7F|\uDC73\uDB40\uDC63\uDB40\uDC74\uDB40\uDC7F|\uDC77\uDB40\uDC6C\uDB40\uDC73\uDB40\uDC7F)))?|\uDFF3(?:(?:\uFE0F(?:\u200D\uD83C\uDF08)?|\u200D\uD83C\uDF08))?|\uDDE6\uD83C[\uDDE8-\uDDEC\uDDEE\uDDF1\uDDF2\uDDF4\uDDF6-\uDDFA\uDDFC\uDDFD\uDDFF]|\uDDE7\uD83C[\uDDE6\uDDE7\uDDE9-\uDDEF\uDDF1-\uDDF4\uDDF6-\uDDF9\uDDFB\uDDFC\uDDFE\uDDFF]|\uDDE8\uD83C[\uDDE6\uDDE8\uDDE9\uDDEB-\uDDEE\uDDF0-\uDDF5\uDDF7\uDDFA-\uDDFF]|\uDDE9\uD83C[\uDDEA\uDDEC\uDDEF\uDDF0\uDDF2\uDDF4\uDDFF]|\uDDEA\uD83C[\uDDE6\uDDE8\uDDEA\uDDEC\uDDED\uDDF7-\uDDFA]|\uDDEB\uD83C[\uDDEE-\uDDF0\uDDF2\uDDF4\uDDF7]|\uDDEC\uD83C[\uDDE6\uDDE7\uDDE9-\uDDEE\uDDF1-\uDDF3\uDDF5-\uDDFA\uDDFC\uDDFE]|\uDDED\uD83C[\uDDF0\uDDF2\uDDF3\uDDF7\uDDF9\uDDFA]|\uDDEE\uD83C[\uDDE8-\uDDEA\uDDF1-\uDDF4\uDDF6-\uDDF9]|\uDDEF\uD83C[\uDDEA\uDDF2\uDDF4\uDDF5]|\uDDF0\uD83C[\uDDEA\uDDEC-\uDDEE\uDDF2\uDDF3\uDDF5\uDDF7\uDDFC\uDDFE\uDDFF]|\uDDF1\uD83C[\uDDE6-\uDDE8\uDDEE\uDDF0\uDDF7-\uDDFB\uDDFE]|\uDDF2\uD83C[\uDDE6\uDDE8-\uDDED\uDDF0-\uDDFF]|\uDDF3\uD83C[\uDDE6\uDDE8\uDDEA-\uDDEC\uDDEE\uDDF1\uDDF4\uDDF5\uDDF7\uDDFA\uDDFF]|\uDDF4\uD83C\uDDF2|\uDDF5\uD83C[\uDDE6\uDDEA-\uDDED\uDDF0-\uDDF3\uDDF7-\uDDF9\uDDFC\uDDFE]|\uDDF6\uD83C\uDDE6|\uDDF7\uD83C[\uDDEA\uDDF4\uDDF8\uDDFA\uDDFC]|\uDDF8\uD83C[\uDDE6-\uDDEA\uDDEC-\uDDF4\uDDF7-\uDDF9\uDDFB\uDDFD-\uDDFF]|\uDDF9\uD83C[\uDDE6\uDDE8\uDDE9\uDDEB-\uDDED\uDDEF-\uDDF4\uDDF7\uDDF9\uDDFB\uDDFC\uDDFF]|\uDDFA\uD83C[\uDDE6\uDDEC\uDDF2\uDDF3\uDDF8\uDDFE\uDDFF]|\uDDFB\uD83C[\uDDE6\uDDE8\uDDEA\uDDEC\uDDEE\uDDF3\uDDFA]|\uDDFC\uD83C[\uDDEB\uDDF8]|\uDDFD\uD83C\uDDF0|\uDDFE\uD83C[\uDDEA\uDDF9]|\uDDFF\uD83C[\uDDE6\uDDF2\uDDFC]|[\uDFFB-\uDFFF\uDF92\uDFA9\uDF93\uDF38-\uDF3C\uDF37\uDF31-\uDF35\uDF3E-\uDF43\uDF47-\uDF53\uDF45\uDF46\uDF3D\uDF44\uDF30\uDF5E\uDF56\uDF57\uDF54\uDF5F\uDF55\uDF2D-\uDF2F\uDF73\uDF72\uDF7F\uDF71\uDF58-\uDF5D\uDF60\uDF62-\uDF65\uDF61\uDF66-\uDF6A\uDF82\uDF70\uDF6B-\uDF6F\uDF7C\uDF75\uDF76\uDF7E\uDF77-\uDF7B\uDF74\uDFFA\uDF0D-\uDF10\uDF0B\uDFE0-\uDFE6\uDFE8-\uDFED\uDFEF\uDFF0\uDF01\uDF03-\uDF07\uDF09\uDF0C\uDFA0-\uDFA2\uDFAA\uDF11-\uDF20\uDF00\uDF08\uDF02\uDF0A\uDF83\uDF84\uDF86-\uDF8B\uDF8D-\uDF91\uDF80\uDF81\uDFAB\uDFC6\uDFC5\uDFC0\uDFD0\uDFC8\uDFC9\uDFBE\uDFB3\uDFCF\uDFD1-\uDFD3\uDFF8\uDFA3\uDFBD\uDFBF\uDFAF\uDFB1\uDFAE\uDFB0\uDFB2\uDCCF\uDC04\uDFB4\uDFAD\uDFA8\uDFBC\uDFB5\uDFB6\uDFA4\uDFA7\uDFB7-\uDFBB\uDFA5\uDFAC\uDFEE\uDFF9\uDFE7\uDFA6\uDD8E\uDD91-\uDD9A\uDE01\uDE36\uDE2F\uDE50\uDE39\uDE1A\uDE32\uDE51\uDE38\uDE34\uDE33\uDE3A\uDE35\uDFC1\uDF8C])|\u26F7\uFE0F?|\u26F9(?:(?:\uFE0F(?:\u200D(?:[\u2642\u2640]\uFE0F?))?|\uD83C(?:[\uDFFB-\uDFFF](?:\u200D(?:[\u2642\u2640]\uFE0F?))?)|\u200D(?:[\u2642\u2640]\uFE0F?)))?|[\u261D\u270C](?:(?:\uD83C[\uDFFB-\uDFFF]|\uFE0F))?|[\u270B\u270A](?:\uD83C[\uDFFB-\uDFFF])?|\u270D(?:(?:\uD83C[\uDFFB-\uDFFF]|\uFE0F))?|[\u2764\u2763\u26D1\u2618\u26F0\u26E9\u2668\u26F4\u2708\u23F1\u23F2\u2600\u2601\u26C8\u2602\u26F1\u2744\u2603\u2604\u26F8\u2660\u2665\u2666\u2663\u260E\u2328\u2709\u270F\u2712\u2702\u26CF\u2692\u2694\u2699\u2696\u26D3\u2697\u26B0\u26B1\u26A0\u2622\u2623\u2B06\u2197\u27A1\u2198\u2B07\u2199\u2B05\u2196\u2195\u2194\u21A9\u21AA\u2934\u2935\u269B\u267E\u2721\u2638\u262F\u271D\u2626\u262A\u262E\u25B6\u23ED\u23EF\u25C0\u23EE\u23F8-\u23FA\u23CF\u2640\u2642\u2695\u267B\u269C\u2611\u2714\u2716\u303D\u2733\u2734\u2747\u203C\u2049\u3030\u00A9\u00AE\u2122]\uFE0F?|[\u0023\u002A\u0030-\u0039](?:\uFE0F\u20E3|\u20E3)|[\u2139\u24C2\u3297\u3299\u25AA\u25AB\u25FB\u25FC]\uFE0F?|[\u2615\u26EA\u26F2\u26FA\u26FD\u2693\u26F5\u231B\u23F3\u231A\u23F0\u2B50\u26C5\u2614\u26A1\u26C4\u2728\u26BD\u26BE\u26F3\u267F\u26D4\u2648-\u2653\u26CE\u23E9-\u23EC\u2B55\u2705\u274C\u274E\u2795-\u2797\u27B0\u27BF\u2753-\u2755\u2757\u25FD\u25FE\u2B1B\u2B1C\u26AA\u26AB])";
                message = Regex.Replace(message, EmojiPattern, "");
                title = Regex.Replace(title, EmojiPattern, "");
                DebugLogger.Log("LocalNotificationWrapper EmojiPattern = " + message);
            }
        }
        en.title = title;
        en.message = message;
        en.delayTypeTime = EnumTimeType.Seconds;
        en.delay = (int)seconds;
        en.advancedNotification = true;
        en.useSound = true;
        en.useVibration = true;
        en.sendOnStart = false;
        var json = Fabric.Internal.ThirdParty.MiniJSON.Json.Serialize(dict);
        en.localType = json;
        en.fullClassName = "com.teamsoft.MazeMainActivity";
        en.image = image;
        en.send();

    }

    public void CancelAll()
    {
        if (m_currentNotificationsIds.Count == 0)
        {
            var lns = AppSettings.Instance.LocalNotifications;
            if (lns != null)
            {
                m_currentNotificationsIds = lns;
            }
            AppSettings.Instance.LocalNotifications = null;
        }
        foreach (var id in m_currentNotificationsIds)
        {
            NotificationManager.CancelLocalNotification(id);
        }
        m_currentNotificationsIds.Clear();
    }

    public void OnApplicationPause(bool val)
    {
        if (!val)
        {
            CancelAll();
            notificationsCreated = false;
            LoadIndexForCurrentTextNotification();
            SetIndexForNotificationMessage();
        }
        else
        {
            ApplicationQuitHandler();
            SaveIndexForCurrentTextNotification();
            notificationsCreated = true;
        }
    }

    void ApplicationQuitHandler()
    {
        if (notificationsCreated) return;
        try
        {

            if (!AppSettings.Instance.NoADS)
            {
                SendDayNotification();
            }
            else
            {
                DebugLogger.Log("LocalNotificationWrapper : NoADS = " + AppSettings.Instance.NoADS);
            }
            AppSettings.Instance.LocalNotifications = m_currentNotificationsIds;
        }
        catch (Exception e)
        {
            DebugLogger.Log(e.Message);
        }
    }
    #region New Logic Notification

    private List<string> mess0Day = new List<string> { "👍Hey! Your hero himself can not get out of the maze ", "👍😜It's time to go through several mazes! ", "My friend I'm stuck. Need your help! 🚑", "🎲 This maze is too complex. Help me! " };
    private List<string> mess1Day = new List<string> { "I've been waiting for you all day! Will we play? ⚡⚡⚡", "🍒 Where have you been all day? Let's play?", "Can we pass this labyrinth today?⏰⏰⏰", "😥 I again stymied! Let's walk together! " };
    private List<string> mess3Day = new List<string> { "😃😃😃Where have you been all these days?", "😃😃😃Three days we did not play! Let `s play!", "Look what's new with your hero! 🤩🤩🤩" };
    private List<string> mess5Day = new List<string> { "Your hero is absolutely bored! 😢😢", "😒 We have not been through the labyrinths so long! What's the matter? ", "Oh, your hero is in danger! 😬👣👣" };
    private List<string> mess8Day = new List<string> { "Check how your hero is! ⭐⭐⭐" };
    private List<string> mess12Day = new List<string> { "🎰New levels are already ready! Test yourself!" };

    private bool mess0DaySend = false;
    private int mess0DayIndex = 0;
    private int mess1DayIndex = 0;
    private int mess3DayIndex = 0;
    private int mess5DayIndex = 0;
    private int mess8DayIndex = 0;
    private int mess12DayIndex = 0;

    public void SendDayNotification()
    {
        if (mess0DayIndex < mess0Day.Count)
        {
            SendPush("mess0DayIndex_time", 0);
        }
        else
        {
            mess0DayIndex = 0;
            SaveIndexForCurrentTextNotification();
            SendPush("mess0DayIndex_time", 0);
        }

        if (mess1DayIndex < mess1Day.Count)
        {
            SendPush("mess1DayIndex_time", 1);
        }
        else
        {
            mess1DayIndex = 0;
            SaveIndexForCurrentTextNotification();
            SendPush("mess1DayIndex_time", 0);
        }

        if (mess3DayIndex < mess3Day.Count)
        {
            SendPush("mess3DayIndex_time", 3);
        }
        else
        {
            DebugLogger.Log("LocalNotificationWrapper error start push = mess3DayIndex");
        }

        if (mess5DayIndex < mess5Day.Count)
        {
            SendPush("mess5DayIndex_time", 5);
        }
        else
        {
            DebugLogger.Log("LocalNotificationWrapper error start push = mess5DayIndex");
        }

        if (mess8DayIndex == 0)
        {
            SendPush("mess8DayIndex_time", 8);
        }
        else
        {
            DebugLogger.Log("LocalNotificationWrapper error start push = mess8DayIndex");
        }

        if (mess12DayIndex == 0)
        {
            SendPush("mess12DayIndex_time", 12);
        }
        else
        {
            DebugLogger.Log("LocalNotificationWrapper error start push = mess12DayIndex");
        }
    }

    private void SendPush(string key, int day)
    {
        System.Random rand = new System.Random();
        var id = rand.Next(0, int.MaxValue);
        m_currentNotificationsIds.Add(id);

        int textIndex = 0;
        var message = GetTextForNotification(day, out textIndex);
        string title = GetTitleForNotification(day, out textIndex);
        DateTime targetTime = DateTime.Now;
        var result = GetTimeForNotification(key, day, out targetTime);
        if (day == 0)
        {
            mess0DaySend = result;
        }
        if (!result) return;

        {
            SaveNotificationTime(key, targetTime);
            GetImageForNotification((Texture2D obj) =>
            {
                DebugLogger.Log("LocalNotificationWrapper Texture2D is = " + obj);
                if (obj != null)
                {
                    var bytes = obj.EncodeToPNG();
#if !UNITY_EDITOR
                    Create(id, targetTime, title, message, bytes, dict:
                        new Dictionary<string, object>() { { "day", day }, { "text", textIndex }, { "type", "no_first" } });
#endif
                }
                else
                {
                    DebugLogger.LogError("LocalNotificationWrapper image is null");
                }
            }, day);

        }
    }

    private void GetImageForNotification(Action<Texture2D> result, int day)
    {
        switch (day)
        {
            case 0:
                result(Resources.Load<Texture2D>("Textures/Push/push_1"));
                break;
            case 1:
                result(Resources.Load<Texture2D>("Textures/Push/push_2"));
                break;
            case 3:
                result(Resources.Load<Texture2D>("Textures/Push/push_3"));
                break;
            case 5:
                result(Resources.Load<Texture2D>("Textures/Push/push_4"));
                break;
            case 8:
                result(Resources.Load<Texture2D>("Textures/Push/push_5"));
                break;
            case 12:
                result(Resources.Load<Texture2D>("Textures/Push/push_6"));
                break;

        }

    }

    private string GetTextForNotification(int day, out int textIndex)
    {
        string res = "";
        textIndex = 0;
        try
        {
            switch (day)
            {
                case 0:
                    res = mess0Day[mess0DayIndex];
                    textIndex = mess0DayIndex;
                    break;
                case 1:
                    res = mess1Day[mess1DayIndex];
                    textIndex = mess1DayIndex;
                    break;
                case 3:
                    res = mess3Day[mess3DayIndex];
                    textIndex = mess3DayIndex;
                    break;
                case 5:
                    res = mess5Day[mess5DayIndex];
                    textIndex = mess5DayIndex;
                    break;
                case 8:
                    res = mess8Day[mess8DayIndex];
                    textIndex = mess8DayIndex;
                    break;
                case 12:
                    res = mess12Day[mess12DayIndex];
                    textIndex = mess12DayIndex;
                    break;
            }
        }
        catch (Exception e)
        {
            DebugLogger.LogError("LocalNotificationWrapper = " + e.Message);
        }
        return res;
    }

    private string GetTitleForNotification(int day, out int textIndex)
    {
        string res = "";
        textIndex = 0;
        try
        {
            switch (day)
            {
                case 0:
                    res = mess0Day[mess0DayIndex];
                    textIndex = mess0DayIndex;
                    break;
                case 1:
                    res = mess1Day[mess1DayIndex];
                    textIndex = mess1DayIndex;
                    break;
                case 3:
                    res = mess3Day[mess3DayIndex];
                    textIndex = mess3DayIndex;
                    break;
                case 5:
                    res = mess5Day[mess5DayIndex];
                    textIndex = mess5DayIndex;
                    break;
                case 8:
                    res = mess8Day[mess8DayIndex];
                    textIndex = mess8DayIndex;
                    break;
                case 12:
                    res = mess12Day[mess12DayIndex];
                    textIndex = mess12DayIndex;
                    break;
            }
        }
        catch (Exception e)
        {
            DebugLogger.LogError("LocalNotificationWrapper = " + e.Message);
        }
        return "World of Maze!!!";
    }

    private bool GetTimeForNotification(string key, int day, out DateTime targetTime)
    {
        if (IsDevelopmentBuild)
        {
            targetTime = DateTime.Now;
            targetTime += new TimeSpan(0, 0, 10 * day + 1);
            Debug.Log("LocalNotificationWrapper targetTime =" + key + " : " + targetTime);
            return true;
        }
        targetTime = DateTime.Now;
        if (day > 0)
        {
            targetTime = targetTime.AddDays(day);
        }
        else
        {
            targetTime += new TimeSpan(3, 0, 0);
        }
        int maxHour = 19;
        if (day == 0)
        {
            maxHour = 20;
        }
        if (targetTime.Hour >= 12 && targetTime.Hour <= maxHour)
        {
            DebugLogger.Log("LocalNotificationWrapper targetTime =" + targetTime);
            return true;
        }
        else
        {
            if (day == 0)
            {
                return false;
            }
            var tomorrow = targetTime.AddDays(1);
            targetTime = new DateTime(tomorrow.Year, tomorrow.Month, tomorrow.Day, 19, tomorrow.Minute, 0);
            DebugLogger.Log("LocalNotificationWrapper targetTime =" + targetTime);
            return true;
        }

    }

    private void SaveNotificationTime(string key, DateTime value)
    {
        PlayerPrefs.SetString(key, value.ToBinary().ToString());
        PlayerPrefs.Save();
    }

    private bool IsLocalNotifEnabled()
    {
        return true;
    }

    private void SetIndexForNotificationMessage()
    {
        if (CheckNotificationShow("mess0DayIndex_time"))
        {
            if (IsLocalNotifEnabled() && mess0DayIndex < mess0Day.Count && mess0DaySend)
                AnalyticsWrapper.Instance.TrackReceiveLocalNotificationByDay(0, mess0DayIndex);
            mess0DayIndex++;
        }
        if (CheckNotificationShow("mess1DayIndex_time"))
        {
            if (IsLocalNotifEnabled() && mess1DayIndex < mess1Day.Count)
                AnalyticsWrapper.Instance.TrackReceiveLocalNotificationByDay(1, mess1DayIndex);
            mess1DayIndex++;
        }
        if (CheckNotificationShow("mess3DayIndex_time"))
        {
            if (IsLocalNotifEnabled() && mess3DayIndex < mess3Day.Count)
                AnalyticsWrapper.Instance.TrackReceiveLocalNotificationByDay(3, mess3DayIndex);
            mess3DayIndex++;
        }
        if (CheckNotificationShow("mess5DayIndex_time"))
        {
            if (IsLocalNotifEnabled() && mess5DayIndex < mess5Day.Count)
                AnalyticsWrapper.Instance.TrackReceiveLocalNotificationByDay(5, mess5DayIndex);
            mess5DayIndex++;
        }
        if (CheckNotificationShow("mess8DayIndex_time"))
        {
            if (IsLocalNotifEnabled() && mess8DayIndex < mess8Day.Count)
                AnalyticsWrapper.Instance.TrackReceiveLocalNotificationByDay(8, mess8DayIndex);
            //mess8DayIndex++;
        }
        if (CheckNotificationShow("mess12DayIndex_time"))
        {
            if (IsLocalNotifEnabled() && mess12DayIndex < mess12Day.Count)
                AnalyticsWrapper.Instance.TrackReceiveLocalNotificationByDay(12, mess12DayIndex);
            //mess12DayIndex++;
        }
        SaveIndexForCurrentTextNotification();
    }

    private bool CheckNotificationShow(string key)
    {
        if (PlayerPrefs.HasKey(key))
        {
            try
            {
                var val = PlayerPrefs.GetString(key);
                long temp = Convert.ToInt64(val);
                DateTime oldDate = DateTime.FromBinary(temp);
                var currentDate = DateTime.Now;
                DebugLogger.Log("NOTIFICATION = " + key);
                DebugLogger.Log("currentDate = " + currentDate);
                DebugLogger.Log("oldDate = " + oldDate);
                if (currentDate >= oldDate)
                {
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
        return false;
    }

    private void SaveIndexForCurrentTextNotification()
    {

        PlayerPrefs.SetInt("mess0DaySend", mess0DaySend ? 1 : 0);
        PlayerPrefs.SetInt("mess0DayIndex", mess0DayIndex);
        PlayerPrefs.SetInt("mess1DayIndex", mess1DayIndex);
        PlayerPrefs.SetInt("mess3DayIndex", mess3DayIndex);
        PlayerPrefs.SetInt("mess5DayIndex", mess5DayIndex);
        PlayerPrefs.SetInt("mess8DayIndex", mess8DayIndex);
        PlayerPrefs.SetInt("mess12DayIndex", mess12DayIndex);
        PlayerPrefs.Save();
    }

    private void LoadIndexForCurrentTextNotification()
    {
        mess0DaySend = PlayerPrefs.GetInt("mess0DaySend") == 1;
        mess0DayIndex = PlayerPrefs.GetInt("mess0DayIndex", 0);
        mess1DayIndex = PlayerPrefs.GetInt("mess1DayIndex", 0);
        mess3DayIndex = PlayerPrefs.GetInt("mess3DayIndex", 0);
        mess5DayIndex = PlayerPrefs.GetInt("mess5DayIndex", 0);
        mess8DayIndex = PlayerPrefs.GetInt("mess8DayIndex", 0);
        mess12DayIndex = PlayerPrefs.GetInt("mess12DayIndex", 0);
    }

    #region NotificationHelper

    private Texture2D ConvertToGrayscale(Texture2D graph)
    {
        Color32[] pixels = graph.GetPixels32();
        for (int x = 0; x < graph.width; x++)
        {
            for (int y = 0; y < graph.height; y++)
            {
                Color32 pixel = pixels[x + y * graph.width];
                int p = ((256 * 256 + pixel.r) * 256 + pixel.b) * 256 + pixel.g;
                int b = p % 256;
                p = Mathf.FloorToInt(p / 256);
                int g = p % 256;
                p = Mathf.FloorToInt(p / 256);
                int r = p % 256;
                float l = (0.2126f * r / 255f) + 0.7152f * (g / 255f) + 0.0722f * (b / 255f);
                Color c = new Color(l, l, l, 1);
                graph.SetPixel(x, y, c);
            }
        }
        graph.Apply(false);
        return graph;
    }
    private Texture2D CombineTExture(Texture2D original, Texture2D progress)
    {

        int startX = 0;
        int startY = original.height - progress.height;

        for (int x = startX; x < original.width; x++)
        {

            for (int y = startY; y < original.height; y++)
            {
                Color bgColor = original.GetPixel(x, y);
                Color wmColor = progress.GetPixel(x - startX, y - startY);

                Color final_color = Color.Lerp(bgColor, wmColor, wmColor.a / 1.0f);

                original.SetPixel(x, y, final_color);
            }
        }

        original.Apply();
        return original;
    }

    private Texture2D Resize(Texture2D source, int newWidth, int newHeight)
    {
        source.filterMode = FilterMode.Point;
        RenderTexture rt = RenderTexture.GetTemporary(newWidth, newHeight);
        rt.filterMode = FilterMode.Point;
        RenderTexture.active = rt;
        Graphics.Blit(source, rt);
        var nTex = new Texture2D(newWidth, newHeight);
        nTex.ReadPixels(new Rect(0, 0, newWidth, newWidth), 0, 0);
        nTex.Apply();
        RenderTexture.active = null;
        return nTex;

    }

    #endregion

    #endregion

    #region CallBackJava
    public void OnOpenWithUrl(string s)
    {
        Debug.Log("Start App Local Notification");
    }
    #endregion
}

