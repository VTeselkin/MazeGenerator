using UnityEngine;
using Assets.Scripts.Shared;

public class FirebaseWrapper : MonoBehaviour
{
	#region Singleton

	private static FirebaseWrapper _instance;

	public static FirebaseWrapper Instance
	{
		get
		{
			if (_instance == null)
			{
				GameObject go = new GameObject("FirebaseWrapper");
				DontDestroyOnLoad(go);
				_instance = go.AddComponent<FirebaseWrapper>();

			}
			return _instance;
		}
	}



	private void Awake()
	{
		if (_instance == this)
		{
			DestroyImmediate(this.gameObject);
		}
		else
		{
			_instance = this;
		}

	}

	#endregion

	public void Init()
	{
		Firebase.Messaging.FirebaseMessaging.TokenReceived += OnTokenReceived;
		Firebase.Messaging.FirebaseMessaging.MessageReceived += OnMessageReceived;
	}

	public void OnTokenReceived(object sender, Firebase.Messaging.TokenReceivedEventArgs token)
	{
		DebugLogger.Log("Received Registration Token: " + token.Token);
	}

	public void OnMessageReceived(object sender, Firebase.Messaging.MessageReceivedEventArgs e)
	{
		DebugLogger.Log("Received a new message from: " + e.Message.From);
	}
}
