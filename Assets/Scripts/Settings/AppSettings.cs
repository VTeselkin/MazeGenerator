﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

internal class AppSettings
{

    #region Fields

    private bool _isMusicOn;
    private bool _isSoundsOn;
    private bool _firstLaunch;
    private bool _initDataGameSparks;
    private bool _isNeedSynchronize = true;
    private bool _noBanner;
    private bool _noADS;
    private bool _gdpr;
    private int _countLaunch;
    private int _rateCounter;
    private bool _isRated;
    private int _lastRateItShowLevel;
    private string _lastAppVerstion;
    private static AppSettings _instance;

    #endregion

    private AppSettings()
    {
        _isMusicOn = true;
        _isSoundsOn = true;
    }

    public static AppSettings Instance
    {
        get { return _instance ?? (_instance = new AppSettings()); }
    }

    public void Initialize()
    {
        Load();
        CheckAppVersion();
        CheckAppLaunch();
    }


    private void Load()
    {
        if (PlayerPrefs.HasKey(PlayerPrefsKeys.Music))
        {
            _isMusicOn = PlayerPrefs.GetInt(PlayerPrefsKeys.Music) == 1;
        }

        if (PlayerPrefs.HasKey(PlayerPrefsKeys.Sounds))
        {
            _isSoundsOn = PlayerPrefs.GetInt(PlayerPrefsKeys.Sounds) == 1;
        }

        if (PlayerPrefs.HasKey(PlayerPrefsKeys.NewVertion))
        {
            IsNewVertion = PlayerPrefs.GetInt(PlayerPrefsKeys.NewVertion) == 1;
        }

        if (PlayerPrefs.HasKey(PlayerPrefsKeys.LastRateItShowLevel))
        {
            _lastRateItShowLevel = PlayerPrefs.GetInt(PlayerPrefsKeys.LastRateItShowLevel);
        }

        if (PlayerPrefs.HasKey(PlayerPrefsKeys.LastAppVerstion))
        {
            _lastAppVerstion = PlayerPrefs.GetString(PlayerPrefsKeys.LastAppVerstion);
        }

        if (PlayerPrefs.HasKey(PlayerPrefsKeys.CountLaunch))
        {
            _countLaunch = PlayerPrefs.GetInt(PlayerPrefsKeys.CountLaunch, -1);
        }

        if (PlayerPrefs.HasKey(PlayerPrefsKeys.Rate))
        {
            _isRated = PlayerPrefs.GetInt(PlayerPrefsKeys.Rate) == 1;
        }

        if (PlayerPrefs.HasKey(PlayerPrefsKeys.NoBanner))
        {
            _noBanner = PlayerPrefs.GetInt(PlayerPrefsKeys.NoBanner) == 1;
        }

        if (PlayerPrefs.HasKey(PlayerPrefsKeys.NoADS))
        {
            _noADS = PlayerPrefs.GetInt(PlayerPrefsKeys.NoADS) == 1;
        }

        if (PlayerPrefs.HasKey(PlayerPrefsKeys.GDPR))
        {
            _gdpr = PlayerPrefs.GetInt(PlayerPrefsKeys.GDPR) == 1;
        }

        if (PlayerPrefs.HasKey(PlayerPrefsKeys.InitDataGameSparks))
        {
            _initDataGameSparks = PlayerPrefs.GetInt(PlayerPrefsKeys.InitDataGameSparks) == 1;
        }

        if (PlayerPrefs.HasKey(PlayerPrefsKeys.IsNeedSynchronize))
        {
            _isNeedSynchronize = PlayerPrefs.GetInt(PlayerPrefsKeys.IsNeedSynchronize) == 1;
        }
        if (PlayerPrefs.HasKey(PlayerPrefsKeys.RateCounter))
        {
            _rateCounter = PlayerPrefs.GetInt(PlayerPrefsKeys.RateCounter);
        }
    }

    private void CheckAppVersion()
    {
        if (AppVersion != _lastAppVerstion)
        {
            IsNewVertion = true;
            _lastAppVerstion = AppVersion;
            PlayerPrefs.SetInt(PlayerPrefsKeys.NewVertion, IsNewVertion ? 1 : 0);
            PlayerPrefs.SetString(PlayerPrefsKeys.LastAppVerstion, _lastAppVerstion);
            return;
        }
        IsNewVertion = false;
    }

    private void CheckAppLaunch()
    {
        if (_countLaunch > 0)
        {
            IsFirstLaunch = false;
        }
        else
        {
            IsFirstLaunch = true;
        }
        _countLaunch += 1;
        PlayerPrefs.SetInt(PlayerPrefsKeys.CountLaunch, _countLaunch);
    }


    #region Constants

    public static readonly string AppVersion = Application.version;

    public static object StorageDataPath
    {
        get
        {
            var dataPath = string.Empty;
#if UNITY_EDITOR || UNITY_STANDALONE
            dataPath = Application.dataPath;
#elif UNITY_IPHONE || UNITY_ANDROID
			dataPath = UnityEngine.Application.persistentDataPath + "/Cashes/";
#endif
            return dataPath;
        }
    }

    #endregion

    #region Properties

    /// <summary>
    /// Gets or sets a value indicating whether this <see cref="T:AppSettings"/> is first launch.
    /// </summary>
    /// <value><c>true</c> if is first launch; otherwise, <c>false</c>.</value>
    public bool IsFirstLaunch
    {
        get { return _firstLaunch; }
        private set
        {
            _firstLaunch = value;
        }
    }

    public bool IsInitDataGameSparks
    {
        get { return _initDataGameSparks; }
        set
        {
            _initDataGameSparks = value;
            PlayerPrefs.SetInt(PlayerPrefsKeys.InitDataGameSparks, _initDataGameSparks ? 1 : 0);

        }
    }

    /// <summary>
    /// Gets or sets a value indicating whether this <see cref="T:AppSettings"/> is need synchronize.
    /// </summary>
    /// <value><c>true</c> if is need synchronize; otherwise, <c>false</c>.</value>
    public bool IsNeedSynchronize
    {
        get { return _isNeedSynchronize; }
        set
        {
            _isNeedSynchronize = value;
            PlayerPrefs.SetInt(PlayerPrefsKeys.IsNeedSynchronize, _isNeedSynchronize ? 1 : 0);

        }
    }

    /// <summary>
    /// Gets the count rate.
    /// </summary>
    /// <value>The count rate.</value>
    public int RateCounter
    {
        get { return _rateCounter; }
        set
        {
            _rateCounter = value;
            PlayerPrefs.SetInt(PlayerPrefsKeys.RateCounter, _rateCounter);
        }
    }
    /// <summary>
    /// True if user rated app.
    /// </summary>
    public bool IsRated
    {
        get { return _isRated; }
        set
        {
            _isRated = value;
            PlayerPrefs.SetInt(PlayerPrefsKeys.Rate, IsRated ? 1 : 0);
        }
    }
    /// <summary>
    /// Gets the count launch.
    /// </summary>
    /// <value>The count launch.</value>
    public int CountLaunch
    {
        get { return _countLaunch; }
        private set
        {
            _countLaunch = value;
        }
    }
    /// <summary>
    ///     Gets or sets the value indicates is music on or not.
    /// </summary>
    public bool IsMusicOn
    {
        get { return _isMusicOn; }
        set
        {
            _isMusicOn = value;
            PlayerPrefs.SetInt(PlayerPrefsKeys.Music, IsMusicOn ? 1 : 0);
            OnMusicChanged();
        }
    }

    /// <summary>
    ///     Gets or sets the value indicates is sound effects on or not.
    /// </summary>
    public bool IsSoundsOn
    {
        get { return _isSoundsOn; }
        set
        {
            _isSoundsOn = value;
            PlayerPrefs.SetInt(PlayerPrefsKeys.Sounds, IsSoundsOn ? 1 : 0);
            OnSoundsChanged();
        }
    }
    /// <summary>
    /// Gets or sets a value indicating whether this <see cref="T:AppSettings"/> no banner.
    /// </summary>
    /// <value><c>true</c> if no banner; otherwise, <c>false</c>.</value>
    public bool NoBanner
    {
        get { return _noBanner; }
        set
        {
            _noBanner = value;
            PlayerPrefs.SetInt(PlayerPrefsKeys.NoBanner, NoBanner ? 1 : 0);
            OnNoBannerChanged();
        }
    }
    /// <summary>
    /// Gets or sets a value indicating whether this <see cref="T:AppSettings"/> no ads.
    /// </summary>
    /// <value><c>true</c> if no ads; otherwise, <c>false</c>.</value>
    public bool NoADS
    {
        get { return _noADS; }
        set
        {
            _noADS = value;
            PlayerPrefs.SetInt(PlayerPrefsKeys.NoADS, NoADS ? 1 : 0);
            OnNoADSChanged();
        }
    }
    /// <summary>
    /// Gets or sets a value indicating whether this <see cref="T:AppSettings"/> is gdpr.
    /// </summary>
    /// <value><c>true</c> if gdpr; otherwise, <c>false</c>.</value>
    public bool GDPR
    {
        get { return true; }
        set
        {
            _gdpr = value;
            PlayerPrefs.SetInt(PlayerPrefsKeys.GDPR, GDPR ? 1 : 0);
        }
    }
    /// <summary>
    ///     True if user rated app.
    /// </summary>
    public bool IsNewVertion { get; set; }

    /// <summary>
    ///     Gets or sets the number of the last level, which shows the PopupRateIt
    /// </summary>
    public int LastRateItShowLevel
    {
        get { return _lastRateItShowLevel; }
        set { _lastRateItShowLevel = value; PlayerPrefs.SetInt(PlayerPrefsKeys.LastRateItShowLevel, LastRateItShowLevel); }
    }

    #endregion

    #region Events

    public event Action MusicChanged;

    protected virtual void OnMusicChanged()
    {
        var handler = MusicChanged;
        if (handler != null) handler();
    }

    public event Action SoundsChanged;

    protected virtual void OnSoundsChanged()
    {
        var handler = SoundsChanged;
        if (handler != null) handler();
    }

    public event Action NoADSChanged;

    protected virtual void OnNoADSChanged()
    {
        var handler = NoADSChanged;
        if (handler != null) handler();
    }

    public event Action NoBannerChanged;

    protected virtual void OnNoBannerChanged()
    {
        var handler = NoBannerChanged;
        if (handler != null) handler();
    }
    #endregion


    public List<int> LocalNotifications
    {
        get
        {
            if (PlayerPrefs.HasKey(PlayerPrefsKeys.LocalNotificationsKey))
            {
                var ss = GetStringList(PlayerPrefsKeys.LocalNotificationsKey);
                List<int> list = new List<int>();

                foreach (string s in ss)
                {
                    list.Add(int.Parse(s));
                }
                return list;
            }
            return null;

        }
        set
        {
            if (value != null)
            {
                List<string> list = new List<string>();
                foreach (int val in value)
                {
                    list.Add(val.ToString());
                }
                SetStringList(PlayerPrefsKeys.LocalNotificationsKey, list);
            }
        }
    }
    private string s_listSeparator = "\n";
    public List<string> GetStringList(string key)
    {
        string s = PlayerPrefs.GetString(key);
        if (string.IsNullOrEmpty(s))
        {
            return new List<string>();
        }

        return s.Split(new[] { s_listSeparator }, StringSplitOptions.RemoveEmptyEntries).ToList();
    }

    public void SetStringList(string key, List<string> list)
    {
        if (list != null)
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            foreach (string s in list)
            {
                sb.Append(s).Append(s_listSeparator);
            }
            PlayerPrefs.SetString(key, sb.ToString());
        }
    }
}