﻿using UnityEngine;

public class PlayerPrefsKeys : MonoBehaviour
{

    public const string LastCompletedLevel = "lnumber";
    public const string LastRateItShowLevel = "lastRateItShowLevel";

    public const string LevelStarsTemplate = "lt_stars_{0}";
    public const string LevelCoinsTemplate = "lt_coins_{0}";

    public const string LevelHashTemplate = "lt_hash_{0}";
    public const string LevelAttemptsTemplate = "lt_attempts_{0}";

    public const string UserEnergy = "energy";
    public const string UserEnergyTimer = "energy_time";

    public const string UserGender = "Gender";
    public const string UserName = "NickName";

    public const string Sounds = "SoundStatusKey";
    public const string Music = "MusicStatusKey";
    public const string Rate = "rate_it_key";
    public const string RateCounter = "rate_counter";

    public const string LastAppVerstion = "version";
    public const string FirstLaunch = "first_launch";
    public const string CountLaunch = "count_launch";
    public const string NewVertion = "new_vertion";

    public const string ServerTimeDifference = "server_time";
    public const string ServerTimeChanged = "is_server_time_changed";

    public const string UserScoreMax = "UserScoreMax";
    public const string DayOfClickAd = "DayOfClickAd";
    public const string GameRulesType = "GameRulesType";
    public const string GameCurrentSetLevel = "GameCurrentSetLevel";

    public const string NoADS = "NoADS";
    public const string NoBanner = "NoBanner";

    public const string GDPR = "GDPR";

    public const string InitDataGameSparks = "IsInitDataGameSparks";
    public const string IsNeedSynchronize = "IsNeedSynchronize";

    public const string LocalNotificationsKey = "local_notifs";
}
