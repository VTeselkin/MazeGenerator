﻿using Assets.Scripts.Shared;
using System;
using System.Collections;
using System.IO;
using UnityEngine;
using Directory = Assets.Scripts.Engine.UserFileSystem.Directory;
using File = Assets.Scripts.Engine.UserFileSystem.File;

namespace Assets.Scripts.Settings
{
    public abstract class DataStorageProvider : MonoBehaviour
    {
        private bool isComplete = true;

        /// <summary>
        /// Path to the file with stored data.
        /// </summary>
        protected abstract string StorageFilePath { get; }

        /// <summary>
        /// Gets instance state for saving in storage.
        /// </summary>
        /// <returns></returns>
        protected abstract JSONObject GetState();

        /// <summary>
        /// Loads instance state from saved data.
        /// </summary>
        protected abstract void RestoreState(JSONObject data);

        /// <summary>
        /// Loads instance state from saved data.
        /// </summary>
        protected abstract void RestoreState();

        /// <summary>
        /// Initializes class resoureces.
        /// This method should be called before any other methods.
        /// </summary>
        public virtual void Initialize()
        {
            isComplete = false;
            var directory = Path.GetDirectoryName(StorageFilePath);
            if (!Directory.Exists(directory))
            {
                Directory.CreateDirectory(directory);
            }

            Load();
        }

        /// <summary>
        /// Loads saved data from storage.
        /// </summary>

        public virtual void Load()
        {
            RestoreState();
        }

        /// <summary>
        /// Saves instance data to storage.
        /// </summary>
        public virtual void Save()
        {
            if (File.Exists(StorageFilePath))
            {
                File.Delete(StorageFilePath);
            }
            var dataForSaving = GetState();
            if (dataForSaving != null)
            {
                var encryptedData = dataForSaving.ToString();
                encryptedData = CryptoScript.Crypt(dataForSaving.ToString());
                File.WriteAllText(StorageFilePath, encryptedData);
            }

        }

        public event Action<bool> CompleteLoad;

        protected void OnCompleteLoad(bool result)
        {
                CompleteLoad?.Invoke(result);
        }
    }
}