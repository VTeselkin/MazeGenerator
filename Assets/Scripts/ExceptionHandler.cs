﻿using System;
using System.IO;
using System.Text;
using UnityEngine;
using Directory = Assets.Scripts.Engine.UserFileSystem.Directory;
using File = Assets.Scripts.Engine.UserFileSystem.File;

namespace Assets.Scripts
{
    internal class ExceptionHandler : MonoBehaviour
    {
        private StringBuilder _fullLog;
        private bool _writeLog = false;

        private bool _initialized;

        private string LogFilePath
        {
            get
            {
                string savepath;

                if (Application.platform == RuntimePlatform.IPhonePlayer)
                {
                    savepath = Application.persistentDataPath;
                }
                else
                {
                    savepath = Application.dataPath;
#if UNITY_ANDROID
                    savepath = Application.persistentDataPath;
#endif
                }
                return savepath + "/log" + Application.version + ".txt";
            }
        }

        #region Singleton members

        private static readonly object _lockObject = new object();

        private static ExceptionHandler _instance;

        public static ExceptionHandler Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (_lockObject)
                    {
                        if (_instance == null)
                        {
                            var gameObject = new GameObject("Singleton: " + (typeof(ExceptionHandler)));
                            _instance = gameObject.AddComponent<ExceptionHandler>();
                        }
                    }
                }

                return _instance;
            }
        }

        private void Awake()
        {
            //Check if there is an existing instance of this object
            if (_instance)
            {
                DestroyImmediate(gameObject); //Delete duplicate
            }
            else
            {
                _instance = this.GetComponent<ExceptionHandler>(); //Make this object the only instance
                DontDestroyOnLoad(gameObject); //Set as do not destroy

                LateAwake();
            }
        }

        #endregion

        private void LateAwake()
        {
        }

        public void Initialize()
        {

            if (_initialized)
            {
                return;
            }

            _initialized = true;

            _fullLog = new StringBuilder();
            _fullLog.Append(DateTime.Now);
            _fullLog.Append('\n');

            //            DeleteLogFile();

            Application.logMessageReceived += HandleLog;
        }

        private void HandleLog(string condition, string stacktrace, LogType type)
        {
            var log = string.Format("<{0}> {1} {2}", type, condition, '\n' + stacktrace);
            _fullLog.Append(log);
            _fullLog.Append('\n');

            if (type == LogType.Exception || type == LogType.Error)
            {
                _writeLog = true;
                SaveLogToFile(_fullLog.ToString());
            }
        }

        void OnApplicationPause(bool pauseStatus)
        {
            if (pauseStatus)
            {
                SaveLogToFile(_fullLog.ToString());
            }
        }

        private void DeleteLogFile()
        {
            if (File.Exists(LogFilePath))
            {
                File.Delete(LogFilePath);
            }
        }

        private void SaveLogToFile(string log)
        {
#if !UNITY_EDITOR
                Byte[] info = new UTF8Encoding(true).GetBytes(log);
                AppendAllBytes(LogFilePath, info);       
#endif
        }
        
        public void AppendAllBytes(string path, byte[] bytes)
        {
            using (var stream = new FileStream(path, FileMode.Append))
            {
                stream.Write(bytes, 0, bytes.Length);
            }
        }
    }
}
