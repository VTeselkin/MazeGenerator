﻿using Assets.Scripts.Helpers;
using Assets.Scripts.Strategy.Interfaces;
using UnityEngine;

public class Free : BaseGameObject
{
    [SerializeField] private UITexture _freeTexture;
    [SerializeField] private UITexture _selectTexture;
    [SerializeField] private UITexture _selectTextureInfection;

    private IObjectStorage<BaseGameObject> objectStorage;
    public override float FixedZPosition { get; set; }
    public int ViewIndexFree { get; private set; }

    public override void Initialize(object info)
    {
        FixedZPosition = DefaultZPosition;
        ViewIndexFree = ((DataInfo)info).ViewIndexFree;
        DelayBeforeUpdate = 0f;
        Energy = 0;
        DefaultSpeed = 0f;
        DefaultStepEnergy = 0;
        objectStorage = ObjectSuperVisor.Instance.ObjectStorage;
        base.Initialize(info);
    }

    public override void ChangeUIHandlers()
    {
        //var maxTexture = GameStorage.Instance.Data.MaxGameTexture;
        //var currentLevel = UserStorage.Instance.CurrentSelectLevel;
        //var index = (currentLevel - (int)(currentLevel / maxTexture) * maxTexture) + 1;
        //var index = GameManager<BaseGameObject>.Instance.LevelData.ViewIndex;
        Texture texture = Resources.Load("Textures/Game/Free/Free_" + ViewIndexFree) as Texture;
        _freeTexture.mainTexture = texture;
    }

    public override void SetVisibleSelect(bool isSelect)
    {
        if (isSelect)
        {
            if (GameManager<BaseGameObject>.Instance.Player.IsInfection)
            {
                _selectTextureInfection.gameObject.SetActive(true);
            }
            else
            {
                _selectTexture.gameObject.SetActive(true);
            }
        }
        else
        {
            _selectTextureInfection.gameObject.SetActive(false);
            _selectTexture.gameObject.SetActive(false);
        }

    }

    public override void SetVisibleEyes(bool isSelect)
    {
        //not used
    }

    public override void SetAnimationMove(SideDirectional directional)
    {
        //not used
    }

    public override void SetAnimationDamage()
    {
        //not used
    }

    public override void SetAnimationDestroy()
    {
        //not used
    }
}
