﻿using System.Collections;
using Assets.Scripts.Helpers;
using Assets.Scripts.Shared;
using UnityEngine;

public class ObjectiveText : BaseGameObject
{

    #region Fields
    [SerializeField] private UILabel _text;

    private float _fixedZPosition = -6f;

    private Color newColor;
    private Color newOutlineColor;
    private float fadeTime = 10f;
    private const float AnimationUpdateDelay = 0.1f;
    private bool _isInitialized;
    private float ScoreWaitToFade = 0.8f;
    private float ScoreFadeSpeed = 0.8f;
    #endregion

    #region Properties

    public override float FixedZPosition
    {
        get { return _fixedZPosition; }
        set { _fixedZPosition = value; }
    }

    public override Point Coordinates
    {
        get { return _Coordinates; }
        set
        {
            _Coordinates = value;
            transform.position = new Vector3(_Coordinates.X - Configuration.Textable.TextBounds.size.x / 2, _Coordinates.Y - 5 * Configuration.Textable.TextBounds.center.y / 2, FixedZPosition);

            _CoordinateX = _Coordinates.X;
            _CoordinateY = _Coordinates.Y;
        }
    }

    public override int CoordinateX
    {
        get { return _CoordinateX; }
        set
        {
            _CoordinateX = value;
            var currentPosition = transform.position;
            transform.position = new Vector3(_Coordinates.X - Configuration.Textable.TextBounds.size.x / 2, currentPosition.y - 5 * Configuration.Textable.TextBounds.center.y / 2, FixedZPosition);
            _Coordinates.X = _CoordinateX;
        }
    }

    public override int CoordinateY
    {
        get { return _CoordinateY; }
        set
        {
            _CoordinateY = value;
            var currentPosition = transform.position;
            transform.position = new Vector3(currentPosition.x - Configuration.Textable.TextBounds.size.x / 2, _CoordinateY - 5 * Configuration.Textable.TextBounds.center.y / 2, FixedZPosition);
            _Coordinates.Y = _CoordinateY;
        }
    }

    private bool IsWithDelayBeforeFade
    {
        get
        {
            return ScoreWaitToFade > 0;
        }
    }

    #endregion

    #region Constructor 

    private void Awake()
    {
        if (_isInitialized == false)
        {
            _isInitialized = true;
            Speed = 0.1f;
            Changed += ScoreTextOnChanged;
        }

    }

    #endregion
    public void SetText(string text)
    {
        _text.text = "-" + text;
    }
    public void Update()
    {
        CustomUpdate();
    }

    private void ScoreTextOnChanged(object sender, EventArgsSimple e)
    {
        if (e.Info == EventMessages.EventStartMoveGameObject)
        {
            if (_text != null)
            {
                SetColor();
                var targetScale = new Vector3(transform.localScale.x * 1.2f, transform.localScale.y * 1.2f,
                    transform.localScale.z * 1.2f);
                gameObject.transform.localScale = Vector3.Lerp(gameObject.transform.localScale, Vector3.zero, 0.1f);
                StartCoroutine(FadeOut());
            }
        }
    }

    private IEnumerator FadeOut()
    {
        if (IsWithDelayBeforeFade)
        {
            yield return new WaitForSeconds(ScoreWaitToFade);
        }

        while (transform.localScale != Vector3.zero)
        {
            gameObject.transform.localScale = Vector3.Lerp(gameObject.transform.localScale, Vector3.zero, ScoreFadeSpeed);
            yield return new WaitForSeconds(AnimationUpdateDelay);
        }
        DestroyObject();
    }

    public override void DestroyObject()
    {
        if (gameObject != null)
        {
            Destroy(gameObject);
            OnChanged(EventMessages.EventGameObjectDestroyed);
        }
    }

    private void SetColor()
    {
        _text.color = ColorText;
    }

    public override void ChangeUIHandlers()
    {
    }

    public override void SetVisibleSelect(bool isSelect)
    {
    }

    public override void SetVisibleEyes(bool isSelect)
    {
    }

    public override void SetAnimationMove(SideDirectional directional)
    {
    }

    public override void SetAnimationDamage()
    {

    }

    public override void SetAnimationDestroy()
    {
        //not used
    }
}
