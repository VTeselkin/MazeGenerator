using System;
using System.Collections.Generic;
using Assets.Scripts.Helpers;

public interface IObjectTracker<T> where T : BaseGameObject
{
    void Clear();

    void ClearStepFirst();

    List<T> TrackListOfGameObjects { get; }

    BaseGameObject LastTrackedObject { get; }

    event EventHandler<BaseGameObjectEventArgs> Changed;

    event EventHandler<BaseGameObjectEventArgs> FirstObjectSelected;

    event EventHandler<BaseGameObjectEventArgs> ObjectsTrackReleased;

    event EventHandler<BaseGameObjectEventArgs> ObjectAddedToTrack;

    event EventHandler<BaseGameObjectEventArgs> ObjectRemovedToTrack;

    List<Point> TrackListOfPositions { get; }

    void DoWhenGameCycleStopped(object sender, EventArgsSimple e);

    int ObjectsInLineNumber { get; }

    void DestroyInstance();

    void Initialize(GridDimension playDimension);

    bool CheckObjectsTypeForTracking(List<BaseGameObject> trackListOfGameObjects);
    bool TrackStartEnabled(int currentPageDataCount, int possibleMatchObjectsCount, int possibleColoredObjectsCount, int possibleMatchSize);

    void KernelContinue(List<BaseGameObject> ojectsModified, bool onSwipe);

}
