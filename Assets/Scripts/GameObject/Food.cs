﻿using Assets.Scripts.Helpers;
using UnityEngine;
using System.Collections;

public class Food : BaseGameObject
{
    [SerializeField] private UILabel _energyLabel;
    [SerializeField] private UITexture _foodTexture;
    [SerializeField] private GameObject energyIcon;
    [SerializeField] private GameObject speedIcon;

    public override float FixedZPosition { get; set; }
    public ObjectTypeFood ObjectTypeFood { get; set; }
    private string _label = "";
    private FoodInfo _data;
    private bool isNeedAnimation;
    private Vector3 currentScale;
    private Vector3 newScale;

    public override void Initialize(object info)
    {
        FoodInfo data = (FoodInfo)info;
        _data = data;
        if (data != null)
        {
            ObjectTypeFood = data.ObjectTypeFood;
            Energy = data.Energy;
            DefaultSpeed = data.Speed;
            _label = data.GetLabelByType();
            UpdateEnergyView();
        }
        base.Initialize(info);

    }

    public override void SetVisibleSelect(bool isSelect)
    {
        // DebugLogger.Log("SetVisibleSelect isn't implemented");
    }

    public override void ChangeUIHandlers()
    {
        var maxTexture = GameStorage.Instance.Data.MaxGameTexture;
        var currentLevel = UserStorage.Instance.CurrentSelectLevel;
        var index = (currentLevel - (int)(currentLevel / maxTexture) * maxTexture) + 1;
        Texture texture = Resources.Load("Textures/Game/Free/Free_" + index) as Texture;

        if (ObjectTypeFood == ObjectTypeFood.Energy)
        {
            energyIcon.SetActive(true);
            float scale = Energy / (float)_data.MaxEnergyForBoost;
            SetViewItem(scale);
        }
        if (ObjectTypeFood == ObjectTypeFood.Speed)
        {
            speedIcon.SetActive(true);
            float scale = DefaultSpeed / _data.MaxSpeedForBoost;
            SetViewItem(scale);
        }

    }

    private void UpdateEnergyView()
    {
        if (_energyLabel != null)
        {
            switch (ObjectTypeFood)
            {
                case ObjectTypeFood.Undefined:
                    break;
                case ObjectTypeFood.Speed:
                    _label += DefaultSpeed;
                    break;
                case ObjectTypeFood.Energy:
                    _label += Energy;
                    break;
            }
            _energyLabel.text = _label;
        }
    }

    public override void SetVisibleEyes(bool isSelect)
    {

    }

    private void SetViewItem(float scale)
    {
        if (scale < 1 && scale > 0)
        {
            if (scale < 0.5f)
            {
                scale = 0.5f;
            }
            energyIcon.transform.localScale *= scale;
            speedIcon.transform.localScale *= scale;
        }
        else if (scale > 1)
        {
            isNeedAnimation = true;
            StartCoroutine(AnimateFood());
        }
    }

    private float m_animTouchTimeStart = 0.2f;
    private float m_animTouchTimeFinish = 0.5f;

    private IEnumerator AnimateFood()
    {
        while (isNeedAnimation)
        {
            currentScale = transform.localScale;
            newScale = transform.localScale + transform.localScale * 0.15f;
            float timeEnd = Time.time + m_animTouchTimeStart;
            while (timeEnd > Time.time)
            {
                transform.localScale = Vector3.Lerp(currentScale, newScale, 1 - ((timeEnd - Time.time) / m_animTouchTimeStart));
                yield return null;
            }

            timeEnd = Time.time + m_animTouchTimeFinish;
            while (timeEnd > Time.time)
            {
                transform.localScale = Vector3.Lerp(newScale, currentScale, 1 - ((timeEnd - Time.time) / m_animTouchTimeFinish));
                yield return null;
            }

            transform.localScale = currentScale;
            yield return null;
        }
    }

    public override void SetAnimationMove(SideDirectional directional)
    {

    }

    public override void SetAnimationDamage()
    {

    }

    public override void SetAnimationDestroy()
    {
        //not used
    }
}