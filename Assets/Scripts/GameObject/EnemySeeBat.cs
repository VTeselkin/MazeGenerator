﻿using Assets.Scripts.Helpers;
using UnityEngine;

public class EnemySeeBat : BaseGameObject
{

    [SerializeField] private UILabel _energyLabel;
    [SerializeField] private GameObject visibleTagret;
    [SerializeField] private Animator _animator;
    private SideDirectional lastSideDirectional;

    public override float FixedZPosition { get; set; }

    public override void Initialize(object info)
    {
        DefaultZPosition = -4f;
        DelayBeforeUpdate = 0.0f;
        Energy = 10;
        DefaultSpeed = 0.9f;
        DefaultStepEnergy = 0;
        DelayBeforAnimation = 0.0f;
        base.Initialize(info);
    }

    public override void ChangeUIHandlers()
    {
        if (_energyLabel != null)
        {
            _energyLabel.text = Energy.ToString();
        }
    }

    public override void SetVisibleSelect(bool isSelect)
    {

    }

    public override void SetVisibleEyes(bool isSelect)
    {
        visibleTagret.SetActive(isSelect);
    }

    public override void SetAnimationMove(SideDirectional directional)
    {
        if (lastSideDirectional != directional)
        {
            lastSideDirectional = directional;
            switch (lastSideDirectional)
            {

                case SideDirectional.Up:
                    _animator.SetTrigger("EnemyUp");
                    break;
                case SideDirectional.Down:
                    _animator.SetTrigger("EnemyDown");
                    break;
                case SideDirectional.Left:
                    _animator.SetTrigger("EnemyLeft");
                    break;
                case SideDirectional.Right:
                    _animator.SetTrigger("EnemyRight");
                    break;
            }
        }
    }



    public override void SetAnimationDamage()
    {
        switch (lastSideDirectional)
        {
            case SideDirectional.Up:
                break;
            case SideDirectional.Down:
                break;
            case SideDirectional.Left:
                break;
            case SideDirectional.Right:
                break;
            case SideDirectional.Undefined:
                break;
        }
    }

    public override void SetAnimationDestroy()
    {
        //not used
    }
}
