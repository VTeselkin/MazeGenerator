﻿using Assets.Scripts.Helpers;
using Assets.Scripts.Shared;
using UnityEngine;

public class Wall : BaseGameObject
{
    [SerializeField] private UITexture _wallTexture;
    public override float FixedZPosition { get; set; }
    public int ViewIndexWall { get; private set; }
    public override void Initialize(object info)
    {
        FixedZPosition = DefaultZPosition;
        ViewIndexWall = ((DataInfo)info).ViewIndexWall;
        DelayBeforeUpdate = 0f;
        Energy = 0;
        DefaultSpeed = 0f;
        DefaultStepEnergy = 0;
        base.Initialize(info);
    }

    public override void ChangeUIHandlers()
    {
        //var maxTexture = GameStorage.Instance.Data.MaxGameTexture;
        //var currentLevel = UserStorage.Instance.CurrentSelectLevel;
        //var index = (currentLevel - (int)(currentLevel / maxTexture) * maxTexture) + 1;
        //var index = GameManager<BaseGameObject>.Instance.LevelData.ViewIndex;
        Texture texture = Resources.Load("Textures/Game/Wall/Wall_" + ViewIndexWall) as Texture;
        _wallTexture.mainTexture = texture;
    }

    public override void SetVisibleSelect(bool isSelect)
    {
        // DebugLogger.Log("SetVisibleSelect isn't implemented");
    }

    public override void SetVisibleEyes(bool isSelect)
    {

    }

    public override void SetAnimationMove(SideDirectional directional)
    {

    }

    public override void SetAnimationDamage()
    {

    }

    public override void SetAnimationDestroy()
    {
        //not used
    }
}
