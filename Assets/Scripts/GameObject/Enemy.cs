﻿using Assets.Scripts.Helpers;
using UnityEngine;

public class Enemy : BaseGameObject
{
    [SerializeField] private UILabel _energyLabel;
    [SerializeField] private UITexture _enemyView;
    public override float FixedZPosition { get; set; }

    public override void Initialize(object info)
    {
        DefaultZPosition = -2.5f;
        DelayBeforeUpdate = 0f;
        Energy = ((EnemyInfo)info).Energy;
        DefaultSpeed = 1.0f;
        DefaultStepEnergy = 0;
        DelayBeforAnimation = 0.01f;
        base.Initialize(info);

    }

    public override void ChangeUIHandlers()
    {
        if (_energyLabel != null)
        {
            _energyLabel.text = Energy.ToString();
        }
        if (Energy <= 10)
        {
            _enemyView.mainTexture = Resources.Load<Texture>("Textures/Game/Enemy/blade_1");
        }
        else if (Energy >= 10 && Energy <= 15)
        {
            _enemyView.mainTexture = Resources.Load<Texture>("Textures/Game/Enemy/blade_2");
        }
        else
        {
            _enemyView.mainTexture = Resources.Load<Texture>("Textures/Game/Enemy/blade_3");
        }
    }

    public override void SetVisibleSelect(bool isSelect)
    {

    }

    public override void SetVisibleEyes(bool isSelect)
    {
    }

    public override void SetAnimationMove(SideDirectional directional)
    {

    }

    public override void SetAnimationDamage()
    {

    }

    public override void SetAnimationDestroy()
    {
        //not used
    }
}
