﻿public class EnemyInfo : DataInfo
{
    public int Energy { get; set; }

    public EnemyInfo(int energy) : base(0, 0)
    {
        Energy = energy;
    }
}
