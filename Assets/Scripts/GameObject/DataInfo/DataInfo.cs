﻿public class DataInfo
{
    public int ViewIndexFree { get; private set; }
    public int ViewIndexWall { get; private set; }
    public DataInfo(int viewIndexFree, int viewIndexWall)
    {
        ViewIndexFree = viewIndexFree;
        ViewIndexWall = viewIndexWall;
    }
}
