﻿

public class FinishInfo : DataInfo
{
    public LevelData LevelData { get; private set; }
    public FinishInfo(LevelData levelData) : base(0, 0)
    {
        LevelData = levelData;
    }
}
