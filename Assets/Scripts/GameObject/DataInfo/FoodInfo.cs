﻿using Assets.Scripts.Helpers;

public class FoodInfo : DataInfo
{
    public ObjectTypeFood ObjectTypeFood { get; set; }
    public float Speed { get; set; }
    public int Energy { get; set; }
    public int MaxEnergyForBoost { get; set; }
    public float MaxSpeedForBoost { get; set; }
    public FoodInfo(ObjectTypeFood objectTypeFood, float data) : base(0, 0)
    {
        ObjectTypeFood = objectTypeFood;
        MaxSpeedForBoost = 0.5f;
        switch (ObjectTypeFood)
        {
            case ObjectTypeFood.Undefined:
                Speed = 0;
                Energy = 0;
                break;
            case ObjectTypeFood.Speed:
                Speed = data;
                Energy = 0;
                break;
            case ObjectTypeFood.Energy:
                Speed = 0;
                Energy = (int)data;
                break;
        }
    }

    public string GetLabelByType()
    {
        switch (ObjectTypeFood)
        {
            case ObjectTypeFood.Undefined:
                return "";
            case ObjectTypeFood.Speed:
                return "S";
            case ObjectTypeFood.Energy:
                return "E";
        }
        return "";
    }
}
