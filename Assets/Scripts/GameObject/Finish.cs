﻿using Assets.Scripts.Helpers;
using UnityEngine;

public class Finish : BaseGameObject
{
    [SerializeField] private UISprite _view;
    [SerializeField] private Collider _collider;

    public override float FixedZPosition { get; set; }

    private FinishInfo _info;

    public override void Initialize(object info)
    {
        _info = ((FinishInfo)info);
        FixedZPosition = DefaultZPosition;
        base.Initialize(info);
    }

    public override void ChangeUIHandlers()
    {
        if (_info != null && _info.LevelData.LevelTarget == LevelTargetEnum.FinishAndCollect)
        {
            _view.enabled = false;
            _collider.enabled = false;
            IsDead = true;
        }
    }

    public override void SetVisibleSelect(bool isSelect)
    {
        IsDead = !isSelect;
        _view.enabled = isSelect;
        _collider.enabled = isSelect;
    }

    public override void SetVisibleEyes(bool isSelect)
    {
        //not used
    }

    public override void SetAnimationMove(SideDirectional directional)
    {
        //not used
    }

    public override void SetAnimationDamage()
    {
        //not used
    }

    public override void SetAnimationDestroy()
    {
        //not used
    }
}
