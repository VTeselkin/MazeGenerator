﻿using System;
using Assets.Scripts.Helpers;
using UnityEngine;
using Assets.Scripts.Shared;

public class EnemySnailGreen : BaseGameObject
{

    [SerializeField] private UILabel _energyLabel;
    [SerializeField] private Animator _animator;
    [SerializeField] private UIButton _butonTouch;
    private SideDirectional lastSideDirectional;
    private bool isTousch = false;

    public override float FixedZPosition { get; set; }

    public override void Initialize(object info)
    {
        DefaultZPosition = -5f;
        DefaultSpeed = 0.5f;
        DefaultStepEnergy = 0;
        Energy = 1;
        DelayBeforeUpdate = 1.0f;
        DelayBeforAnimation = 0.01f;

        if (_butonTouch != null)
        {
            UIEventListener.Get(_butonTouch.gameObject).onClick += OnClickSnail;
        }
        base.Initialize(info);

    }

    private void OnClickSnail(GameObject go)
    {
        if (!isTousch)
        {
            AnalyticsWrapper.Instance.EventPressBtn(AnalyticsHelper.EventKey.PressSnailOnMenu);
            isTousch = true;
            IsDead = true;
            _butonTouch.enabled = false;
            UIEventListener.Get(_butonTouch.gameObject).onClick -= OnClickSnail;
            SetAnimationDestroy();
        }
    }

    public override void ChangeUIHandlers()
    {
        if (_energyLabel != null)
        {
            _energyLabel.text = Energy.ToString();
        }
    }

    public override void SetVisibleSelect(bool isSelect)
    {

    }

    public override void SetVisibleEyes(bool isSelect)
    {
    }

    public override void SetAnimationMove(SideDirectional directional)
    {
        if (IsDead) return;
        lastSideDirectional = directional;
        switch (directional)
        {
            case SideDirectional.Up:
                _animator.Play("EnemySnailTop");
                break;
            case SideDirectional.Down:
                _animator.Play("EnemySnailBottom");
                break;
            case SideDirectional.Left:
                _animator.Play("EnemySnailLeft");
                break;
            case SideDirectional.Right:
                _animator.Play("EnemySnailRight");
                break;
            case SideDirectional.Undefined:
                break;
        }

    }

    public override void SetAnimationDamage()
    {
        //not used
    }

    public override void SetAnimationDestroy()
    {
        IsDead = true;
        switch (lastSideDirectional)
        {
            case SideDirectional.Up:
                _animator.Play("EnemySnailTopDead");
                break;
            case SideDirectional.Down:
                _animator.Play("EnemySnailBottomDead");
                break;
            case SideDirectional.Left:
                _animator.Play("EnemySnailLeftDead");
                break;
            case SideDirectional.Right:
                _animator.Play("EnemySnailRightDead");
                break;
            case SideDirectional.Undefined:
                break;
        }
        Destroy(gameObject, 2f);
    }
}
