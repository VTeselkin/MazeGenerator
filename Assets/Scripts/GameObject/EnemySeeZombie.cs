﻿using Assets.Scripts.Helpers;
using UnityEngine;

public class EnemySeeZombie : BaseGameObject
{

    [SerializeField] private UILabel _energyLabel;
    [SerializeField] private GameObject visibleTagret;
    [SerializeField] private Animator _animator;
    private SideDirectional lastSideDirectional;

    public override float FixedZPosition { get; set; }

    public override void Initialize(object info)
    {
        DefaultZPosition = -3.5f;
        Energy = 10;
        DefaultSpeed = 0.8f;
        DefaultStepEnergy = 0;
        DelayBeforeUpdate = 0.3f;
        DelayBeforAnimation = 0.01f;
        base.Initialize(info);

    }

    public override void ChangeUIHandlers()
    {
        if (_energyLabel != null)
        {
            _energyLabel.text = Energy.ToString();
        }
    }

    public override void SetVisibleSelect(bool isSelect)
    {

    }

    public override void SetVisibleEyes(bool isSelect)
    {
        visibleTagret.SetActive(isSelect);
    }

    public override void SetAnimationMove(SideDirectional directional)
    {
        lastSideDirectional = directional;
        switch (directional)
        {
            case SideDirectional.Up:
                _animator.Play("EnemyZombieTop");
                break;
            case SideDirectional.Down:
                _animator.Play("EnemyZombieBottom");
                break;
            case SideDirectional.Left:
                _animator.Play("EnemyZombieLeft");
                break;
            case SideDirectional.Right:
                _animator.Play("EnemyZombieRight");
                break;
            case SideDirectional.Undefined:
                break;
        }
    }

    public override void SetAnimationDamage()
    {

        switch (lastSideDirectional)
        {
            case SideDirectional.Up:
                _animator.Play("EnemyZombieTopAttack");
                break;
            case SideDirectional.Down:
                _animator.Play("EnemyZombieBottomAttack");
                break;
            case SideDirectional.Left:
                _animator.Play("EnemyZombieLeftAttack");
                break;
            case SideDirectional.Right:
                _animator.Play("EnemyZombieRightAttack");
                break;
            case SideDirectional.Undefined:
                break;
        }

    }

    public override void SetAnimationDestroy()
    {
        //not used
    }
}
