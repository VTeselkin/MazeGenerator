using UnityEngine;
using System.Collections;
using Assets.Scripts.Helpers;
using System;
using System.Collections.Generic;
using Assets.Scripts.Strategy.Interfaces;
using System.Linq;
using Assets.Scripts.Shared;
using Assets.Scripts.Game;

/* Interactive Line drawing main class */

public class ObjectTrackerLine : MonoBehaviour, IObjectTracker<BaseGameObject>
{
	private static ObjectTrackerLine _instance;
	private BaseGameObject _previousHittedGemObject;

	// MonoBehaviour based classes can not work
	// with Generic parameters therefore we use
	// BaseGameObject here for any derived objects
	private List<BaseGameObject> _trackListOfGameObjects;

	private LineTrack _lineInstance;
	private static Camera _camera;
	private BaseGameObject _firstTrackedObject;
	private IObjectStorage<BaseGameObject> _objectsStorage;
	private GridDimension _gridDimension;

	#region Properties

	public static IObjectTracker<BaseGameObject> Instance
	{
		get
		{
			if (_instance == null)
			{
				_instance = new GameObject("ObjectTrackerLine").AddComponent<ObjectTrackerLine>();
				_instance._trackListOfGameObjects = new List<BaseGameObject>();
				_instance._lineInstance = new LineTrack();
				_camera = GameObject.Find("Main Camera").GetComponent<Camera>();
			}
			return _instance;
		}
	}

	public List<BaseGameObject> TrackListOfGameObjects
	{
		get
		{
			return _trackListOfGameObjects;
		}
	}

	public BaseGameObject LastTrackedObject
	{
		get { return _trackListOfGameObjects.Last(); }
	}

	public List<Point> TrackListOfPositions
	{
		get
		{
			List<Point> listOfTrackedPositions = new List<Point>();
			foreach (var currentGameObject in _trackListOfGameObjects)
			{
				listOfTrackedPositions.Add(currentGameObject.Coordinates);
			}
			return listOfTrackedPositions;
		}
	}

	public int ObjectsInLineNumber
	{
		get
		{
			if (_trackListOfGameObjects == null)
				return 0;

			return _trackListOfGameObjects.Count;
		}
	}
	#endregion


	public bool CheckObjectsTypeForTracking(List<BaseGameObject> trackListOfGameObjects)
	{
		throw new NotImplementedException();
	}

	public void ClearStepFirst()
	{
		_lineInstance.RemoveFirstPoint();
	}

	public void Clear()
	{
		_firstTrackedObject = null;
		_trackListOfGameObjects = new List<BaseGameObject>();
		_lineInstance.Clear();
	}

	public void DestroyInstance()
	{
		_instance = null;
	}

	public void DoWhenGameCycleStopped(object sender, EventArgsSimple e)
	{
		if (e.Info == EventMessages.EventTrackStopWork)
		{
			this._trackListOfGameObjects.Clear();
			InputHandler.BlockInput = false;
		}
	}

	public void Initialize(GridDimension playDimension)
	{
		_gridDimension = playDimension;
	}

	public void KernelContinue(List<BaseGameObject> ojectsModified, bool onSwipe)
	{
		throw new NotImplementedException();
	}

	public bool TrackStartEnabled(int currentPageDataCount, int possibleMatchObjectsCount, int possibleColoredObjectsCount, int possibleMatchSize)
	{
		throw new NotImplementedException();
	}
	// CellUpdate is called once per frame
	private void Update()
	{
		// Here we need to know something like mouse state - not event case
		if (InputHandler.MouseState == MouseStateEnum.MouseLeftButtonPressed)
		{
			BaseGameObject checkedGemObject = GetObjectUnderMouse();
			// There is sense only if under mouse something exists 
			if (checkedGemObject == null) return;

			if (!Helper.CheckPointInsideGrid(checkedGemObject.Coordinates, _gridDimension))
			{
				return;
			}
			int currentCount = _trackListOfGameObjects.Count;
			if (_firstTrackedObject != null)
			{
				// Only near the neighbour we can do something 
				if (IsValidGameObjectByCoordinatesAndType(checkedGemObject))
				{
					if (_trackListOfGameObjects.Contains(checkedGemObject))
					{
						RemoveFromTrackIfMovingBack(checkedGemObject, currentCount);
					}
					else
					{
						AudioManager.Instance.PlaySoundAddToTrack(checkedGemObject, _trackListOfGameObjects.Count);
						AddToTrackAndDraw(checkedGemObject, currentCount);
						// Sound Manager is Subscriber
						//OnChanged(EventMessages.EventObjectAddedToTrack, checkedGemObject);
					}
				}
			}
			else
			{
				if (IsValidFirstGameObjectByType(checkedGemObject))
				{
					AudioManager.Instance.PlaySoundAddToTrack(checkedGemObject, _trackListOfGameObjects.Count);
					_trackListOfGameObjects.Add(checkedGemObject);
					_lineInstance.Draw(checkedGemObject);
					//OnChanged(EventMessages.EventObjectAddedToTrack, checkedGemObject);
					_firstTrackedObject = checkedGemObject;
					_previousHittedGemObject = _firstTrackedObject;
					if (FirstObjectSelected != null)
					{
						OnFirstObjectSelected(checkedGemObject);
					}
				}

			}
			if (_trackListOfGameObjects.Count != 0)
			{
				_previousHittedGemObject = _trackListOfGameObjects.Last();
			}

		}
		if (InputHandler.BlockInput == false)
		{
			if (Input.GetMouseButtonUp(0))
			{
				if (_trackListOfGameObjects.Count != 0)
				{
					//Remove player from track object
					if (_trackListOfGameObjects[0].ObjectType == ObjectTypeEnum.Player)
					{
						ClearStepFirst();
						_trackListOfGameObjects.Remove(_trackListOfGameObjects[0]);
					}
					OnObjectsTrackReleased();
				}

			}
		}
	}

	private void AddToTrackAndDraw(BaseGameObject checkedGemObject, int currentCount)
	{
		if (currentCount > 1)
		{
			OnObjectAddedToTrack(checkedGemObject);
		}
		_trackListOfGameObjects.Add(checkedGemObject);
		_lineInstance.Draw(checkedGemObject);
	}


	private bool IsValidGameObjectByCoordinatesAndType(BaseGameObject pretenderForAddingToTrack)
	{

		// Distance validation: One step is available
		if ((pretenderForAddingToTrack.CoordinateX == _previousHittedGemObject.CoordinateX &&
			 Mathf.Abs(pretenderForAddingToTrack.CoordinateY - _previousHittedGemObject.CoordinateY) < 2) ||
			(pretenderForAddingToTrack.CoordinateY == _previousHittedGemObject.CoordinateY &&
			 Mathf.Abs(pretenderForAddingToTrack.CoordinateX - _previousHittedGemObject.CoordinateX) < 2))
		{

			if (pretenderForAddingToTrack.ObjectGroup != ObjectGroupEnum.Walls)
			{
				return true;
			}

		}
		return false;
	}

	private bool IsValidFirstGameObjectByType(BaseGameObject pretenderForAddingToTrack)
	{
		if (pretenderForAddingToTrack.ObjectType == ObjectTypeEnum.Player)
		{
			return true;
		}
		return false;
	}

	// If Mouse move back within track we need remove last object
	private void RemoveFromTrackIfMovingBack(BaseGameObject checkedObject, int amountOfGems)
	{
		int indexInList = _trackListOfGameObjects.IndexOf(checkedObject);
		if (indexInList == (amountOfGems - 2))
		{
			_trackListOfGameObjects.Remove(_previousHittedGemObject);
			// Sound Manager is Subscriber ==========================
			OnObjectRemovedToTrack(checkedObject);
			OnChanged(EventMessages.EventObjectRemoveFromTrack, _previousHittedGemObject);
			AudioManager.Instance.PlaySoundRemoveFromTrack(_trackListOfGameObjects.Count);
			// ======================================================

			_lineInstance.RemoveLastPoint();

		}

	}

	private void Awake()
	{
		_objectsStorage = ObjectsStorage<BaseGameObject>.Instance;

		if (_instance == this)
			DestroyImmediate(this.gameObject);
		else
		{
			_instance = this;
		}
	}

	private BaseGameObject GetObjectUnderMouse()
	{

		Ray ray = _camera.ScreenPointToRay(Input.mousePosition);
		List<RaycastHit> raycastHit = Physics.RaycastAll(ray, 1000).ToList();

		foreach (var rightHit in raycastHit)
		{
			var baseGameObject = rightHit.collider.GetComponent<BaseGameObject>();

			if (baseGameObject != null && _firstTrackedObject != null && baseGameObject.ObjectType == ObjectTypeEnum.Free)
			{
				return baseGameObject;
			}
			else if (baseGameObject != null && _firstTrackedObject == null &&
					 baseGameObject.ObjectType == ObjectTypeEnum.Player)
			{
				return baseGameObject;
			}
		}
		return null;
	}

	#region Custom Event

	public event EventHandler<BaseGameObjectEventArgs> Changed;

	private void OnChanged(string eventInfo, BaseGameObject baseGameObject)
	{
		EventHandler<BaseGameObjectEventArgs> handler = Changed;

		if (handler != null)
		{
			var args = new BaseGameObjectEventArgs()
			{
				Info = eventInfo,
				GemObjectSender = baseGameObject
			};
			handler(this, args);
		}
	}

	public event EventHandler<BaseGameObjectEventArgs> FirstObjectSelected;

	private void OnFirstObjectSelected(BaseGameObject baseGameObject)
	{
		EventHandler<BaseGameObjectEventArgs> handler = FirstObjectSelected;

		if (handler != null)
		{
			var args = new BaseGameObjectEventArgs()
			{
				GemObjectSender = baseGameObject
			};
			handler(this, args);
		}
	}

	public event EventHandler<BaseGameObjectEventArgs> ObjectsTrackReleased;

	private void OnObjectsTrackReleased()
	{
		EventHandler<BaseGameObjectEventArgs> handler = ObjectsTrackReleased;

		if (handler != null)
		{
			var args = new BaseGameObjectEventArgs() { };
			handler(this, args);
		}
	}

	public event EventHandler<BaseGameObjectEventArgs> ObjectAddedToTrack;

	private void OnObjectAddedToTrack(BaseGameObject baseGameObject)
	{
		EventHandler<BaseGameObjectEventArgs> handler = ObjectAddedToTrack;

		if (handler != null)
		{
			var args = new BaseGameObjectEventArgs()
			{
				GemObjectSender = baseGameObject
			};
			handler(this, args);
		}
	}

	public event EventHandler<BaseGameObjectEventArgs> ObjectRemovedToTrack;

	private void OnObjectRemovedToTrack(BaseGameObject baseGameObject)
	{
		EventHandler<BaseGameObjectEventArgs> handler = ObjectRemovedToTrack;

		if (handler != null)
		{
			var args = new BaseGameObjectEventArgs()
			{
				GemObjectSender = baseGameObject
			};
			handler(this, args);
		}
	}

	#endregion
}