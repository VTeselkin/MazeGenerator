﻿using System.Collections;
using Assets.Scripts.Helpers;
using UnityEngine;
using Assets.Scripts.Shared;
public class Player : BaseGameObject
{
    [SerializeField] private UILabel _energyLabel;
    [SerializeField] private Animator _animatorMove;
    [SerializeField] private Animator _animatorDamage;
    [SerializeField] private GameObject _damageView;
    public override float FixedZPosition { get; set; }
    public int UserBoostEnergy { get; private set; }
    public float UserBoostSpeed { get; private set; }
    public override void Initialize(object info)
    {
        var energy = LevelStorage.Instance.Data[UserStorage.Instance.CurrentSelectLevel].PlayerEnergy;
        float userBoostEnergy = UserStorage.Instance.EnergyBoost * GameStorage.Instance.Data.PercentByIndex / 100.0f;
        var userBoostSpeed = (UserStorage.Instance.SpeedBoost * GameStorage.Instance.Data.PercentByIndex) / 100.0f;

        DelayBeforeUpdate = 1f;
        UserBoostEnergy = (int)(energy * userBoostEnergy);
        UserBoostSpeed = userBoostSpeed;
        Energy = energy + UserBoostEnergy;
        DefaultSpeed = GameStorage.Instance.Data.DefaultSpeed + userBoostSpeed;
        DefaultStepEnergy = 1;
        DelayBeforAnimation = 0.3f;
        base.Initialize(info);

    }

    public override void SetVisibleSelect(bool isSelect)
    {
        //DebugLogger.Log("SetVisibleSelect isn't implemented");
    }

    public override void ChangeUIHandlers()
    {
        UpdateEnergyView();
    }

    private void UpdateEnergyView()
    {
        if (_energyLabel != null)
        {
            _energyLabel.text = Energy.ToString();
        }
    }

    public override void SetVisibleEyes(bool isSelect)
    {

    }

    public override void SetAnimationMove(SideDirectional directional)
    {
        if (_animatorMove != null)
        {
            _animatorMove.Rebind();
            switch (directional)
            {
                case SideDirectional.Up:
                    _animatorMove.Play("PlayerUp");
                    break;
                case SideDirectional.Down:
                    _animatorMove.Play("PlayerDown");
                    break;
                case SideDirectional.Left:
                    _animatorMove.Play("PlayerLeft");
                    break;
                case SideDirectional.Right:
                    _animatorMove.Play("PlayerRight");
                    break;
                case SideDirectional.Undefined:
                    _animatorMove.Rebind();
                    break;
            }
        }
    }

    public override void SetAnimationDamage()
    {
        _damageView.SetActive(true);
        StartCoroutine(Delayed(0.5f));
    }

    private IEnumerator Delayed(float delay)
    {
        yield return new WaitForSeconds(delay);
        _damageView.SetActive(false);
    }

    public override void SetAnimationDestroy()
    {
        //not used
    }
}
