using UnityEngine;
using Assets.Scripts.Strategy.Interfaces;
using Assets.Scripts.Helpers;
using System;
using System.Collections.Generic;
using Object = System.Object;
using Assets.Scripts.Shared;
using System.Linq;

public class ObjectsStorage<T> : IObjectStorage<T> where T : BaseGameObject
{
    #region Fields

    private static ObjectsStorage<T> _instance;
    private readonly List<T> _objectsStorageList;
    private readonly GridDimension _gridDimension;
    private IConfigurator<T> _configurator;
    private static GameObject parentObject;
    private readonly PoolManager _poolManager;


    #endregion

    #region Properties

    public static IObjectStorage<T> Instance
    {
        get
        {
            if (_instance == null)
            {
                DebugLogger.LogError(ErrorMessages.ErrorInitObjectsStorage);
            }
            return _instance;
        }
    }

    public List<T> ObjectsStorageList
    {
        get { return _objectsStorageList; }
    }

    public IConfigurator<T> Configurator
    {
        get
        {
            return _configurator;
        }
    }

    #endregion

    #region Constructors and Initialization

    public ObjectsStorage(GridDimension gridDimension)
    {
        _objectsStorageList = new List<T>();
        _gridDimension = gridDimension;
        _configurator = new Configurator<T>(this);
        _poolManager = PoolManager.Instance;

    }

    #endregion

    /// <summary>
    /// Inits the instance.
    /// </summary>
    /// <param name="gridDimension">Grid dimension.</param>
    public static void InitInstance(GridDimension gridDimension)
    {
        if (_instance == null)
        {
            _instance = new ObjectsStorage<T>(gridDimension);
            parentObject = GameObject.Find("LevelMaze");
        }
    }

    #region Create and Add GameObject

    /// <summary>
    /// Creates the game object.
    /// </summary>
    /// <param name="objectId">Object identifier.</param>
    /// <param name="initInfo">Init info.</param>
    /// <param name="point">Point.</param>
    /// <param name="baseObject">Base object.</param>
    public void CreateGameObject(ResourceIdentifier objectId, object initInfo, Point point, out T baseObject)
    {
        GameObject gameObjectInstance = null;
        gameObjectInstance = _poolManager.GetInstanceByType(objectId.GameObjectType);
        gameObjectInstance.transform.SetParent(null);
        gameObjectInstance.SetActive(true);
        baseObject = gameObjectInstance.GetComponent<T>();
        SetStrategyConfiguration(baseObject, objectId, initInfo);
        baseObject.Initialize(initInfo);
        baseObject.transform.parent = parentObject.transform;
        baseObject.Coordinates = point;
        baseObject.Color = objectId.Color;
    }
    /// <summary>
    /// Adds the game object.
    /// </summary>
    /// <param name="gemGameObject">Gem game object.</param>
    public void AddGameObject(T gemGameObject)
    {
        gemGameObject.transform.parent = parentObject.transform;
        gemGameObject.Coordinates = Point.Default;
        _objectsStorageList.Add(gemGameObject);
        OnChanged(EventMessages.EventAddGameObject, gemGameObject);
    }
    /// <summary>
    /// Adds the game object.
    /// </summary>
    /// <param name="gameObject">Game object.</param>
    public void AddGameObject(GameObject gameObject)
    {
        T baseObject = gameObject.GetComponent<T>();
        baseObject.transform.parent = parentObject.transform;
        baseObject.Coordinates = Point.Default;
        _objectsStorageList.Add(baseObject);
        OnChanged(EventMessages.EventAddGameObject, baseObject);
    }
    /// <summary>
    /// Adds the game object.
    /// </summary>
    /// <param name="objectTypeEnum">Object type enum.</param>
    public void AddGameObject(ObjectTypeEnum objectTypeEnum)
    {
        GameObject gameObjectInstance = MonoBehaviour.Instantiate(_poolManager.GetInstanceByType(objectTypeEnum)) as GameObject;
        T baseObject = gameObjectInstance.GetComponent<T>();
        SetStrategyConfiguration(baseObject, new ResourceIdentifier(objectTypeEnum), null);
        baseObject.transform.parent = parentObject.transform;
        baseObject.Coordinates = Point.Default;
        _objectsStorageList.Add(baseObject);
        OnChanged(EventMessages.EventAddGameObject, baseObject);
    }
    /// <summary>
    /// Adds the game object.
    /// </summary>
    /// <param name="objectTypeEnum">Object type enum.</param>
    /// <param name="point">Point.</param>
    /// <param name="baseGameObject">Base game object.</param>
    public void AddGameObject(ObjectTypeEnum objectTypeEnum, FloatPoint point, out T baseGameObject)
    {
        GameObject gameObjectInstance = MonoBehaviour.Instantiate(_poolManager.GetInstanceByType(objectTypeEnum)) as GameObject;
        T baseObject = gameObjectInstance.GetComponent<T>();
        SetStrategyConfiguration(baseObject, new ResourceIdentifier(objectTypeEnum), null);
        baseObject.transform.parent = parentObject.transform;
        baseObject.CoordinatesFloat = point;
        baseGameObject = baseObject;
        _objectsStorageList.Add(baseObject);
        OnChanged(EventMessages.EventAddGameObject, baseObject);
    }
    /// <summary>
    /// Adds the game object.
    /// </summary>
    /// <param name="objectTypeEnum">Object type enum.</param>
    /// <param name="point">Point.</param>
    /// <param name="baseGameObject">Base game object.</param>
    public void AddGameObject(ObjectTypeEnum objectTypeEnum, Point point, out T baseGameObject)
    {
        GameObject gameObjectInstance = MonoBehaviour.Instantiate(_poolManager.GetInstanceByType(objectTypeEnum)) as GameObject;
        T baseObject = gameObjectInstance.GetComponent<T>();
        SetStrategyConfiguration(baseObject, new ResourceIdentifier(objectTypeEnum), null);
        baseObject.transform.parent = parentObject.transform;
        baseObject.Coordinates = point;
        baseGameObject = baseObject;
        _objectsStorageList.Add(baseObject);
        OnChanged(EventMessages.EventAddGameObject, baseObject);
    }
    /// <summary>
    /// Adds the game object decor.
    /// </summary>
    /// <param name="objectTypeEnum">Object type enum.</param>
    /// <param name="point">Point.</param>
    /// <param name="baseGameObject">Base game object.</param>
    public void AddGameObjectDecor(ObjectTypeEnum objectTypeEnum, Point point, out T baseGameObject)
    {
        GameObject gameObjectInstance = MonoBehaviour.Instantiate(_poolManager.GetInstanceByType(objectTypeEnum)) as GameObject;
        T baseObject = gameObjectInstance.GetComponent<T>();
        baseObject.transform.parent = parentObject.transform;
        baseObject.Coordinates = point;
        baseGameObject = baseObject;
    }
    /// <summary>
    /// Adds the score object.
    /// </summary>
    /// <param name="scoreType">Score type.</param>
    /// <param name="point">Point.</param>
    /// <param name="baseGameObject">Base game object.</param>
    public void AddScoreObject(ScoreTypeEnum scoreType, Point point, out T baseGameObject)
    {
        GameObject gameObjectInstance = MonoBehaviour.Instantiate(_poolManager.GetInstanceByType(ObjectTypeEnum.ScoreText)) as GameObject;
        T baseObject = gameObjectInstance.GetComponent<T>();
        SetStrategyConfiguration(baseObject, new ResourceIdentifier(ObjectTypeEnum.ScoreText), null);
        baseObject.transform.parent = parentObject.transform;
        baseObject.Coordinates = point;
        baseObject.Configuration.Movable.MoveToPosition(Helper.GetScoreMovePosition(scoreType, baseObject));
        baseObject.ColorText = Helper.GetScoreColor(scoreType);
        baseGameObject = baseObject;
    }

    #endregion

    public T GetGameObjectByPosition(Point point)
    {
        T objectInPosition = null;
        foreach (T currentObject in ObjectsStorageList)
        {
            if (currentObject.Coordinates == point)
            {
                objectInPosition = currentObject;
                break;
            }
        }

        return objectInPosition;
    }

    public List<T> GetAllGameObjectByPosition(Point point)
    {
        List<T> objectInPosition = new List<T>();
        foreach (T currentObject in ObjectsStorageList)
        {
            if (currentObject.Coordinates == point)
            {
                objectInPosition.Add(currentObject);

            }
        }
        return objectInPosition;
    }

    public T GetGameFreeObjectByPosition(Point point)
    {
        T objectInPosition = null;

        foreach (T currentObject in ObjectsStorageList)
        {
            if (currentObject.Coordinates == point &&
                currentObject.ObjectType == ObjectTypeEnum.Free)
            {
                objectInPosition = currentObject;
                break;
            }
        }

        return objectInPosition;
    }

    public List<T> GetListFreeGameObjectsByPositionWithMoreDistance(T targetObject, float minDistance)
    {
        List<T> objectsInPosition = new List<T>();
        foreach (T currentObject in _objectsStorageList)
        {
            if (currentObject.ObjectType == ObjectTypeEnum.Free && GetAllGameObjectByPosition(currentObject.Coordinates).Count == 1)
            {
                var targetPos = new Vector2(targetObject.CoordinatesFloat.X, targetObject.CoordinatesFloat.Y);
                var currentPos = new Vector2(currentObject.CoordinatesFloat.X, currentObject.CoordinatesFloat.Y);
                if (Vector2.Distance(targetPos, currentPos) > minDistance)
                {
                    objectsInPosition.Add(currentObject);
                }
            }
        }

        return objectsInPosition;
    }

    public List<T> GetListFreeGameObjectsByPositionWithLessDistance(T targetObject, float maxDistance)
    {
        List<T> objectsInPosition = new List<T>();
        foreach (T currentObject in _objectsStorageList)
        {
            if (currentObject.ObjectType == ObjectTypeEnum.Free && GetAllGameObjectByPosition(currentObject.Coordinates).Count == 1)
            {
                var targetPos = new Vector2(targetObject.CoordinatesFloat.X, targetObject.CoordinatesFloat.Y);
                var currentPos = new Vector2(currentObject.CoordinatesFloat.X, currentObject.CoordinatesFloat.Y);
                if (Vector2.Distance(targetPos, currentPos) < maxDistance)
                {
                    objectsInPosition.Add(currentObject);
                }
            }
        }

        return objectsInPosition;
    }

    public List<T> GetListOfObjectsByType(ObjectTypeEnum requiredType)
    {
        return Helper.FindAll(ObjectsStorageList, (o => o.ObjectType == requiredType));
    }

    public List<T> GetListOfObjectsByGroupe(ObjectGroupEnum requiredType)
    {
        return Helper.FindAll(ObjectsStorageList, (o => o.ObjectGroup == requiredType));
    }

    public List<T> GetNeighborsObjectByType(T requiredObject)
    {
        List<T> result = new List<T>();
        var objectFree = GetGameFreeObjectByPosition(requiredObject.CoordinatesUp);
        if (objectFree != null)
        {
            result.Add(objectFree);
        }
        objectFree = GetGameFreeObjectByPosition(requiredObject.CoordinatesDown);
        if (objectFree != null)
        {
            result.Add(objectFree);
        }
        objectFree = GetGameFreeObjectByPosition(requiredObject.CoordinatesLeft);
        if (objectFree != null)
        {
            result.Add(objectFree);
        }
        objectFree = GetGameFreeObjectByPosition(requiredObject.CoordinatesRight);
        if (objectFree != null)
        {
            result.Add(objectFree);
        }
        return result;
    }

    public List<T> GetListOfObjectsByTypeInArea(ObjectTypeEnum requiredType, AreaDimension dimension)
    {
        return GetFreeGameObjectList(Helper.FindAll(ObjectsStorageList, (o => o.ObjectType == requiredType && o.Coordinates.X > dimension.X && o.Coordinates.X < dimension.DimX &&
                                                                         o.Coordinates.Y > dimension.Y && o.Coordinates.Y < dimension.DimY)));
    }

    public List<T> GetListOfMovableObjects()
    {
        return Helper.FindAll(ObjectsStorageList, (o => o.IsAtMoving));
    }

    public List<Point> GetListOfPositionsByType(ObjectTypeEnum requiredType)
    {
        return GetPositionsList(GetListOfObjectsByType(requiredType));
    }

    public List<Point> GetListOfPositionsByGroup(ObjectGroupEnum requiredType)
    {
        return GetPositionsList(GetListOfObjectsByGroupe(requiredType));
    }

    public GridDimension Dimension()
    {
        return _gridDimension;
    }

    public int GetTotalCount()
    {
        return _objectsStorageList.Count();
    }

    public void DestroyInstance()
    {
        _instance = null;
    }

    public void RemoveGameObject(T gemGameObject)
    {
        _objectsStorageList.Remove(gemGameObject);
        var detectedType = gemGameObject.ObjectType;
        // Notify CellSuperVisorInstance instance, _cellSuperVisorInstance
        OnChanged(EventMessages.EventRemoveGameObject, gemGameObject);

    }

    #region Private Method

    private void SetStrategyConfiguration(T baseGameObject, ResourceIdentifier objectType, object initInfo)
    {
        _configurator.SetConfiguration(baseGameObject, objectType, initInfo);
    }

    private List<Point> GetPositionsList(List<T> listOfObjects)
    {
        if (listOfObjects.Count == 0) return null;
        // =======================================
        var listOfPositions = new List<Point>();
        foreach (var currentObject in listOfObjects)
        {
            listOfPositions.Add(currentObject.Coordinates);
        }
        return listOfPositions;
    }

    private List<T> GetFreeGameObjectList(List<T> list)
    {
        List<T> OnlyFreeGameObjectOnPoint = new List<T>();
        foreach (var obj in list)
        {
            if (GetAllGameObjectByPosition(obj.Coordinates).Count == 1)
            {
                OnlyFreeGameObjectOnPoint.Add(obj);
            }
        }
        return OnlyFreeGameObjectOnPoint;
    }

    #endregion

    #region Custom Event

    public event EventHandler<EventArgsGeneric<Object>> Changed;

    private void OnChanged(string eventInfo, Object someObject)
    {
        EventHandler<EventArgsGeneric<Object>> handler = Changed;
        if (handler != null)
        {
            var args = new EventArgsGeneric<Object>()
            {
                Info = eventInfo,
                SomeObject = someObject
            };
            // Raise custom game object event with specific info
            // and reference to Active Game Object for adding
            handler(this, args);
        }
    }






    #endregion
}
