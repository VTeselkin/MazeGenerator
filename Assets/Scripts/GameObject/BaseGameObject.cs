﻿using System;
using System.Collections;
using Assets.Scripts.Helpers;
using Assets.Scripts.Shared;
using UnityEngine;
using Assets.Scripts.GUI.Managers;
using Assets.Scripts.GUI;

public abstract class BaseGameObject : MonoBehaviour
{
    #region Fields

    public static float DefaultZPosition = -2f;
    public const float ElementsDistance = 1.0f;

    protected int DefaultStepEnergy;
    protected float DefaultSpeed = 2;

    protected int _CoordinateX;
    protected int _CoordinateY;

    protected float _CoordinateXFloat;
    protected float _CoordinateYFloat;

    protected Point _Coordinates = Point.Default;
    protected FloatPoint _CoordinatesFloat;

    protected bool _isInfection;
    protected bool _isDead;
    protected bool _isBleeding;

    public float DelayBeforeUpdate { get; protected set; }
    public float DelayBeforAnimation { get; protected set; }



    private float _currentSpeed;
    private int _currentStepEnergy;
    private int _currentEnergy;
    private IStrategyConfiguration<BaseGameObject> _configuration;

    #endregion

    public abstract float FixedZPosition { get; set; }
    public abstract void ChangeUIHandlers();
    public abstract void SetVisibleSelect(bool isSelect);
    public abstract void SetVisibleEyes(bool isSelect);
    public abstract void SetAnimationMove(SideDirectional directional);
    public abstract void SetAnimationDamage();
    public abstract void SetAnimationDestroy();

    #region Init Method

    public virtual void Initialize(object info)
    {
        FixedZPosition = DefaultZPosition;
        StepByEnergy = DefaultStepEnergy;
        Speed = DefaultSpeed;
        Coordinates = _Coordinates;
        ChangeUIHandlers();
    }

    #endregion

    #region Coordinates

    public virtual int CoordinateX
    {
        get { return _CoordinateX; }
        set
        {
            _CoordinateX = value;
            var currentPosition = transform.position;
            transform.position = new Vector3(_CoordinateX * ElementsDistance, currentPosition.y, FixedZPosition);
            // Refreshing value for Coordinates property
            _Coordinates.X = _CoordinateX;
        }
    }

    public float CoordinateXFloat
    {
        get { return _CoordinateXFloat; }
        set
        {
            _CoordinateXFloat = value;
            Vector3 currentPosition = transform.position;
            transform.position = new Vector3(_CoordinateXFloat * ElementsDistance, currentPosition.y, FixedZPosition);
            // Refreshing value for Coordinates property
            _CoordinatesFloat.X = _CoordinateXFloat;
        }
    }


    public virtual int CoordinateY
    {
        get { return _CoordinateY; }
        set
        {
            _CoordinateY = value;
            Vector3 currentPosition = transform.position;
            transform.position = new Vector3(currentPosition.x, _CoordinateY * ElementsDistance, FixedZPosition);
            _Coordinates.Y = _CoordinateY;
        }
    }

    public float CoordinateYFloat
    {
        get { return _CoordinateYFloat; }
        set
        {
            _CoordinateYFloat = value;
            Vector3 currentPosition = transform.position;
            transform.position = new Vector3(currentPosition.x, _CoordinateYFloat * ElementsDistance, FixedZPosition);
            _CoordinatesFloat.Y = _CoordinateYFloat;
        }
    }

    public virtual Point Coordinates
    {
        get { return _Coordinates; }
        set
        {
            _Coordinates = value;
            transform.position = new Vector3(_Coordinates.X * ElementsDistance, _Coordinates.Y * ElementsDistance, FixedZPosition);
            _CoordinateX = _Coordinates.X;
            _CoordinateY = _Coordinates.Y;
            _CoordinatesFloat = Helper.AdeptPosition(_Coordinates);
        }
    }

    public FloatPoint CoordinatesFloat
    {
        get { return _CoordinatesFloat; }
        set
        {
            _CoordinatesFloat = value;
            transform.position = new Vector3(_CoordinatesFloat.X * ElementsDistance, _CoordinatesFloat.Y * ElementsDistance, FixedZPosition);
            _CoordinateXFloat = _CoordinatesFloat.X;
            _CoordinateYFloat = _CoordinatesFloat.Y;
        }
    }

    public Point CoordinatesUp { get { return new Point(Coordinates.X, Coordinates.Y + 1); } }
    public Point CoordinatesDown { get { return new Point(Coordinates.X, Coordinates.Y - 1); } }
    public Point CoordinatesLeft { get { return new Point(Coordinates.X - 1, Coordinates.Y); } }
    public Point CoordinatesRight { get { return new Point(Coordinates.X + 1, Coordinates.Y); } }

    #endregion

    #region Properties

    public int Energy
    {
        get { return _currentEnergy; }
        set
        {
            _currentEnergy = value;
            OnChanged(EventMessages.EventGameObjectChangeEnergy);
        }
    }

    public int StepByEnergy
    {
        get
        {
            return _currentStepEnergy;
        }
        set
        {
            _currentStepEnergy = value;
            OnChanged(EventMessages.EventGameObjectChangeEnergyByStep);
        }
    }

    public float Speed
    {
        get { return _currentSpeed; }
        set
        {
            _currentSpeed = value;
            OnChanged(EventMessages.EventGameObjectChangeSpeed);
        }
    }

    public ColorEnum Color
    {
        get { return Configuration.Colorable.GetColor(); }
        set
        {
            Configuration.Colorable.SetColor(value);
        }
    }

    public bool IsAtMoving
    {
        get
        {
            if (Configuration.Movable != null)
            {
                return Configuration.Movable.IsAtMoving;
            }
            return false;
        }
    }

    public bool IsInfection
    {
        get
        {
            return _isInfection;
        }
        set { _isInfection = value; }
    }

    public bool IsBleeding
    {
        get { return _isBleeding; }
        set { _isBleeding = value; }
    }

    public bool IsDead
    {
        get { return _isDead; }
        set { _isDead = value; }
    }
    public ObjectGroupEnum ObjectGroup
    {
        get { return Configuration.Groupable.ObjectGroup; }
    }

    public ObjectTypeEnum ObjectType
    {
        get { return Configuration.ObjectType; }
    }

    internal IStrategyConfiguration<BaseGameObject> Configuration
    {
        get { return _configuration; }
        set { _configuration = value; }
    }

    public virtual Color ColorText
    {
        get { return Configuration.Textable.Color; }
        set { Configuration.Textable.Color = value; }
    }


    #endregion

    #region Public Methods

    public void SetName()
    {
        gameObject.name = ObjectType.ToString() + "[" + Coordinates.X + "],[" + Coordinates.Y + "]";
    }

    public virtual void CustomUpdate()
    {
        Configuration.Movable.CustomUpdate();
    }

    public void MoveToPosition(Point point)
    {
        Configuration.Movable.MoveToPosition(point);
    }

    public void SetInfectionForObject()
    {
        if (!IsInfection)
        {
            StartCoroutine(InfectionForObject(10.0f));
        }
    }

    public void SetBleedingForObject()
    {
        if (!IsBleeding)
        {
            StartCoroutine(BleedingForObject(10, 2.0f));
        }
    }

    public void IncrementEnergy(int powerAdd)
    {
        OnChanged(EventMessages.EventGameObjectIncrementStarted);
        OnChangedObject(EventMessages.EventGameObjectIncrementStarted, this);
        Energy += powerAdd;
        ChangeUIHandlers();
        OnChanged(EventMessages.EventGameObjectIncrementFinished);
        OnChangedObject(EventMessages.EventGameObjectIncrementFinished, this);
    }

    public virtual void DamageObject(int powerDamage)
    {

        OnChanged(EventMessages.EventGameObjectDamageStarted);
        OnChangedObject(EventMessages.EventGameObjectDamageStarted, this);
        Energy -= powerDamage;
        ChangeUIHandlers();
        ShowScores();
        OnChanged(EventMessages.EventGameObjectDamageCompleted);
        OnChangedObject(EventMessages.EventGameObjectDamageStarted, this);
        if (Energy <= 0)
        {
            DestroyObject();
        }
    }

    public virtual void DestroyObject()
    {
        if (ObjectType == ObjectTypeEnum.Player)
        {
            UIManager.Instance.CreateNewUIElement<PopupLose>(UIElementType.PopupLose);

        }
        DestroyConfiguration();
        if (gameObject != null)
        {
            OnChanged(EventMessages.EventGameObjectDestroyed);
            DestroyObject(gameObject);
        }
    }

    public void OnDestroy()
    {
        Destroy(gameObject);
    }

    #endregion

    protected void DestroyConfiguration()
    {
        if (Configuration != null)
        {
            Configuration.Destroy();
        }
    }

    private void ShowScores()
    {

    }

    private void OnTriggerEnter(Collider other)
    {
        BaseGameObject otherGameObject = other.GetComponent<BaseGameObject>();
        if (otherGameObject != null && Configuration != null)
        {
            Configuration.Interactionable.OnTriggerEnter2D(otherGameObject);
        }
    }

    private IEnumerator InfectionForObject(float delay)
    {
        IsInfection = true;
        Speed = 0.75f;
        StepByEnergy = 2;
        yield return new WaitForSeconds(delay);
        IsInfection = false;
        Speed = DefaultSpeed;
        StepByEnergy = DefaultStepEnergy;
    }

    private IEnumerator BleedingForObject(int count, float delay)
    {
        IsBleeding = true;
        for (int i = 0; i < count; i++)
        {
            yield return new WaitForSeconds(delay);
            SetAnimationDamage();
            DamageObject(1);
            BaseGameObject baseObject;
            ObjectsStorage<BaseGameObject>.Instance.AddScoreObject(ScoreTypeEnum.Damage, Coordinates, out baseObject);
            (baseObject as ObjectiveText).SetText("1");
        }
        IsBleeding = false;
    }

    #region Custom Event

    public event EventHandler<EventArgsSimple> Changed;

    public void OnChanged(string eventInfo)
    {
        EventHandler<EventArgsSimple> handler = Changed;
        if (handler != null)
        {
            var args = new EventArgsSimple { Info = eventInfo };
            // Raise custom game object event with specific info
            handler(this, args);
        }
    }

    public event EventHandler<BaseGameObjectEventArgs> ChangedObject;

    public void OnChangedObject(string eventInfo, BaseGameObject obj)
    {
        EventHandler<BaseGameObjectEventArgs> handler = ChangedObject;
        if (handler != null)
        {
            var args = new BaseGameObjectEventArgs { Info = eventInfo, GemObjectSender = obj };
            // Raise custom game object event with specific info
            handler(this, args);
        }
    }
    #endregion
}
