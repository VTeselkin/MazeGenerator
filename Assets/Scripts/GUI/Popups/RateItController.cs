﻿using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.GUI;
using Assets.Scripts.GUI.Managers;
using UnityEngine;

public class RateItController : MonoBehaviour
{

    public bool IsShowRateNeeded
    {
        get
        {
            var lastRateItShowLevel = AppSettings.Instance.LastRateItShowLevel == 0
                ? -1
                : AppSettings.Instance.LastRateItShowLevel;

            return (!AppSettings.Instance.IsRated &&
                    UserStorage.Instance.MaxOpenedLevel - lastRateItShowLevel >= 5);
        }
    }

    public void ShowPopupIfNeeded()
    {
        if (IsShowRateNeeded && AppSettings.Instance.RateCounter < 5 && PopupsAutoShowHelper.Instance.CanShowPopup())
        {
            UIManager.Instance.CreateNewUIElement<PopupRateIt>(UIElementType.RateIt);
        }
    }


    #region Singleton members

    private static readonly object _lockObject = new object();
    private static RateItController _instance;

    public static RateItController Instance
    {
        get
        {
            if (_instance == null)
            {
                lock (_lockObject)
                {
                    if (_instance == null)
                    {
                        var gameObject = new GameObject("Singleton: " + (typeof(RateItController)));
                        _instance = gameObject.AddComponent<RateItController>();
                    }
                }
            }

            return _instance;
        }
    }

    private void Awake()
    {
        if (_instance)
        {
            DestroyImmediate(gameObject);
        }
        else
        {
            _instance = this.GetComponent<RateItController>();
            DontDestroyOnLoad(gameObject);
        }
        LateAwake();
    }

    private void LateAwake()
    {

    }


    #endregion
}
