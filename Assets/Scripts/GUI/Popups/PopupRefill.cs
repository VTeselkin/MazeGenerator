﻿using System;
using Assets.Scripts.GUI;
using Assets.Scripts.GUI.Managers;
using Assets.Scripts.GUI.Popups;
using UnityEngine;

internal class PopupRefill : PopupBase
{

	[SerializeField] private UIButton _btnBuy;
	[SerializeField] private UISprite _energySprite;
	[SerializeField] private UISprite _speedSprite;
	[SerializeField] private UISprite _noAdsSprite;
	[SerializeField] private UILabel _costLabel;
	[SerializeField] private UILabel _countLabel;
	[SerializeField] private UILabel _titleLabel;
	private LevelData _levelData;
	private int _cost = 100;
	private Player _player;
	private RefillType _type;

	public enum RefillType
	{
		Energy,
		Speed,
		Ads
	}
	public void Init(RefillType type)
	{
		_levelData = GameManager<BaseGameObject>.Instance.LevelData;
		_player = GameManager<BaseGameObject>.Instance.Player;
		_type = type;

		switch (type)
		{
			case RefillType.Energy:
				_energySprite.enabled = true;
				_cost = _levelData.PlayerEnergy + _player.Energy;
				_countLabel.text = "+ " + _levelData.PlayerEnergy / 2;
				break;
			case RefillType.Speed:
				_speedSprite.enabled = true;
				_cost = (int)((_player.Speed + 1) * 50);
				_countLabel.text = "+ 1";
				break;
			case RefillType.Ads:
				_titleLabel.text = "No ADS!";
				_noAdsSprite.enabled = true;
				_cost = GameStorage.Instance.Data.CostNoBanner;
				_countLabel.text = "Remove Banner!";
				break;
			default:
				throw new ArgumentOutOfRangeException("type", type, null);
		}
		_costLabel.text = _cost.ToString();
	}
	protected override void FillUIElementsInfo()
	{
		base.FillUIElementsInfo();
	}

	protected override void InitUIHandlers()
	{
		base.InitUIHandlers();
		Commands.Add(_btnBuy.name, new SimpleActionClickCommand(ButtonBuyClick));
		_costLabel.text = _cost.ToString();
	}

	private void ButtonBuyClick()
	{

		if (UserStorage.Instance.BuyByCoins(_cost))
		{
			switch (_type)
			{
				case RefillType.Energy:
					_player.Energy += _levelData.PlayerEnergy / 2;
					AnalyticsWrapper.Instance.EventByEnergyInGame(_player.Energy, _cost);
					break;
				case RefillType.Speed:
					_player.Speed += 1;
					AnalyticsWrapper.Instance.EventBySpeedInGame(_player.Speed, _cost);
					break;
				case RefillType.Ads:
					AnalyticsWrapper.Instance.EventPressBtn(AnalyticsHelper.EventKey.UserDisableBanner);
					AppSettings.Instance.NoBanner = true;
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}
			Close();
		}
		else
		{
			UIManager.Instance.CreateNewUIElement<PopupStore>(UIElementType.PopupStore, true).Init(false);
		}
	}
}
