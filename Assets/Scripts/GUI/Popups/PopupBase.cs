﻿#region Usings

using System;
using Assets.Scripts.GUI.Managers;
using Assets.Scripts.GUI.RootElements;
using UnityEngine;

#endregion

namespace Assets.Scripts.GUI.Popups
{
	internal abstract class PopupBase : UIContainerElement
	{
		[Header("Base")]
		public UIButton BtnClose;

		[Header("Sound")]
		[SerializeField]
		private bool isNeedPlayOpenClip = true;
		[SerializeField]
		protected SoundConstants.UIAudioClipTypes ClipOpenPopupType = SoundConstants.UIAudioClipTypes.PopupLevel;
		[SerializeField]
		private bool isNeedPlayCloseClip = true;
		[SerializeField]
		protected SoundConstants.UIAudioClipTypes ClipClosePopupType = SoundConstants.UIAudioClipTypes.PopupClose;

		public Action OnClosePopup;
		public UIContainerElement Parent { get; protected set; }

		public void Init(UIContainerElement parent)
		{
			Parent = parent;
		}

		protected override void Start()
		{
			base.Start();

		}

		/// <summary>
		///     Inits all click handlers.
		/// </summary>
		protected override void InitUIHandlers()
		{
			if (BtnClose != null)
			{
				Commands.Add(BtnClose.name, new SimpleActionClickCommand(Close));
			}


			GetText();
			//LocalizationManager.Instance.LanguageChanged += OnLanguageChanged;
		}

		protected override void FillUIElementsInfo()
		{
			//       transform.parent = Camera.main.transform;
			transform.localScale = Vector3.one;
			transform.transform.position = Vector3.zero;

			if (isNeedPlayOpenClip)
			{
				AudioManager.Instance.PlaySoundUi(ClipOpenPopupType);
			}
		}

		public virtual void Close()
		{
			//LocalizationManager.Instance.LanguageChanged -= OnLanguageChanged;
			if (OnClosePopup != null)
			{
				OnClosePopup();
			}
			if (isNeedPlayCloseClip)
			{
				AudioManager.Instance.PlaySoundUi(ClipClosePopupType);
			}
			else
			{
				AudioManager.Instance.PlaySoundUi(ClipClosePopupType);
			}

			UIManager.Instance.RemoveElement(this);
		}

		protected virtual void CloseWithoutEventRaising()
		{
			//LocalizationManager.Instance.LanguageChanged -= OnLanguageChanged;
			UIManager.Instance.RemoveElement(this);
		}

		public virtual void BlockButtons(bool val)
		{
			var buttons = GetComponentsInChildren<UIButton>();
			foreach (var button in buttons)
			{
				if (button != null)
					button.isEnabled = !val;
			}
		}

		protected override void Awake()
		{
			base.Awake();

			//ScalePopup();
		}

		//protected virtual void ScalePopup()
		//{
		//    if (_content == null)
		//    {
		//        return;
		//    }

		//    var scale = ScreenHelper.MinScale / ScreenHelper.ScaleX;
		//    _content.transform.localScale = new Vector3(
		//        _content.transform.localScale.x * scale,
		//        _content.transform.localScale.y * scale,
		//        _content.transform.localScale.z * scale);
		//}

		//public Vector3 GetContentScale()
		//{
		//    return _content.transform.localScale;
		//}

		protected override void OnDestroy()
		{
			base.OnDestroy();

			//LocalizationManager.Instance.LanguageChanged -= OnLanguageChanged;
		}



		#region Localization

		protected virtual void GetText() { }

		protected virtual void OnLanguageChanged()
		{
			GetText();
		}

		#endregion
	}
}