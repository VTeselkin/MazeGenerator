﻿using Assets.Scripts.GUI.Popups;
using Assets.Scripts.Server;
using UnityEngine;

internal class PopupSync : PopupBase
{
	[SerializeField] private UIButton _btnFBSave;
	[SerializeField] private UIButton _btnLocalSave;

	protected override void InitUIHandlers()
	{
		base.InitUIHandlers();
		Commands.Add(_btnFBSave.name, new SimpleActionClickCommand(ButtonFBSaveClick));
		Commands.Add(_btnLocalSave.name, new SimpleActionClickCommand(ButtonLocalSaveClick));
	}



	private void ButtonLocalSaveClick()
	{
		UserStorage.Instance.SendUserDataToServer();
		Close();
	}

	private void ButtonFBSaveClick()
	{
		GameSparkManager.Instance.GetUserProgress((string json) =>
		{
			UserStorage.Instance.RestoreFromServer(json);
			UserStorage.Instance.Save();
			Close();
		}, (string mess) =>
		{
			UserStorage.Instance.SendUserDataToServer();
			UserStorage.Instance.Save();
			Close();
		});


	}

	protected override void FillUIElementsInfo()
	{
		base.FillUIElementsInfo();
	}
}
