﻿using UnityEngine;
using Assets.Scripts.GUI.Popups;
using UnityEngine.Purchasing;
using System;
using Assets.Scripts.Helpers;
using Assets.Scripts.Shared;

internal class PopupStore : PopupBase
{
    [SerializeField] private UIButton _buyCoinsMini;
    [SerializeField] private UIButton _buyCoinsMedium;
    [SerializeField] private UIButton _buyCoinsHard;
    [SerializeField] private UIButton _btnUpgradesPanel;
    [SerializeField] private UIButton _btnCoinsPanel;
    [SerializeField] private GameObject _upgradesPanel;
    [SerializeField] private GameObject _coinsPanel;

    [SerializeField] private UIButton _buyEnergy;
    [SerializeField] private UIButton _buySpeed;
    [SerializeField] private UIButton _buyDistance;

    [SerializeField] private UILabel _priceEnergy;
    [SerializeField] private UILabel _priceSpeed;
    [SerializeField] private UILabel _priceDistance;

    [SerializeField] private UILabel _descriptionEnergy;
    [SerializeField] private UILabel _descriptionSpeed;
    [SerializeField] private UILabel _descriptionDistance;

    [SerializeField] private UILabel _countDistance;

    [SerializeField] private UISprite _completeEnergy;
    [SerializeField] private UISprite _completeSpeed;
    [SerializeField] private UISprite _completeDistance;

    protected override void InitUIHandlers()
    {
        base.InitUIHandlers();

        Commands.Add(_buyCoinsMini.name, new SimpleActionClickCommand(ButtonCoinsMiniClick));
        Commands.Add(_buyCoinsMedium.name, new SimpleActionClickCommand(ButtonCoinsMediumClick));
        Commands.Add(_buyCoinsHard.name, new SimpleActionClickCommand(ButtonCoinsHardClick));

        Commands.Add(_btnUpgradesPanel.name, new SimpleActionClickCommand(ButtonUpgradesClick));
        Commands.Add(_btnCoinsPanel.name, new SimpleActionClickCommand(ButtonCoinsClick));

        Commands.Add(_buyEnergy.name, new SimpleActionClickCommand(ButtonEnergyClick));
        Commands.Add(_buySpeed.name, new SimpleActionClickCommand(ButtonSpeedClick));
        Commands.Add(_buyDistance.name, new SimpleActionClickCommand(ButtonDistanceClick));

        ButtonUpgradesClick();
        UserStorage.Instance.ChangeUserData += ChangePriceBoost;
        UserStorage.Instance.ChangeUserData += SetBoostEnable;
        ChangePriceBoost(TypeBoostStore.Undefined);
        SetBoostEnable(TypeBoostStore.Undefined);
    }

    private void SetBoostEnable(TypeBoostStore type)
    {
        var userStorage = UserStorage.Instance;
        if (userStorage.EnergyBoost >= UserStorage.MaxIndex)
        {
            _completeEnergy.enabled = true;
            _buyEnergy.enabled = false;
            _buyEnergy.GetComponent<Collider>().enabled = false;
        }
        if (userStorage.SpeedBoost >= UserStorage.MaxIndex)
        {
            _completeSpeed.enabled = true;
            _buySpeed.enabled = false;
            _buySpeed.GetComponent<Collider>().enabled = false;
        }
        if (userStorage.DistanceBoost >= UserStorage.MaxIndex)
        {
            _completeDistance.enabled = true;
            _buyDistance.enabled = false;
            _buyDistance.GetComponent<Collider>().enabled = false;
        }
    }
    private void ChangePriceBoost(TypeBoostStore obj)
    {
        _priceEnergy.text = "";
        _priceSpeed.text = "";
        _priceDistance.text = "";
        _descriptionEnergy.text = "Increase energy by ";
        _descriptionSpeed.text = "Increase speed by ";
        _descriptionDistance.text = "Increase visibility by ";
        _priceEnergy.text = ((UserStorage.Instance.EnergyBoost + 1) * GameStorage.Instance.Data.CostIndexByEnergy).ToString();
        _priceSpeed.text = ((UserStorage.Instance.SpeedBoost + 1) * GameStorage.Instance.Data.CostIndexBySpeed).ToString();
        _priceDistance.text = ((UserStorage.Instance.DistanceBoost + 1) * GameStorage.Instance.Data.CostIndexByDistance).ToString();
        _descriptionEnergy.text += ((UserStorage.Instance.EnergyBoost + 1) * GameStorage.Instance.Data.PercentByIndex) + "%";
        _descriptionSpeed.text += ((UserStorage.Instance.SpeedBoost + 1) * GameStorage.Instance.Data.PercentByIndex) + "%";
        _descriptionDistance.text += ((UserStorage.Instance.DistanceBoost + 1) * GameStorage.Instance.Data.PercentByIndex) + "%";
        _countDistance.text = (5 + UserStorage.Instance.DistanceBoost).ToString() + "X";
    }

    public void Init(bool isFirstPanel)
    {
        if (isFirstPanel)
        {
            ButtonUpgradesClick();
        }
        else
        {
            ButtonCoinsClick();
        }
    }

    private void ButtonCoinsClick()
    {
        _btnUpgradesPanel.normalSprite = "ui_shop_btn_left_enable";
        _btnUpgradesPanel.hoverSprite = "ui_shop_btn_left_enable";
        _btnUpgradesPanel.pressedSprite = "ui_shop_btn_left_enable";
        _btnUpgradesPanel.disabledSprite = "ui_shop_btn_left_enable";

        _btnCoinsPanel.normalSprite = "ui_shop_btn_right_disable";
        _btnCoinsPanel.hoverSprite = "ui_shop_btn_right_disable";
        _btnCoinsPanel.pressedSprite = "ui_shop_btn_right_disable";
        _btnCoinsPanel.disabledSprite = "ui_shop_btn_right_disable";

        _upgradesPanel.SetActive(false);
        _coinsPanel.SetActive(true);
    }

    private void ButtonUpgradesClick()
    {
        _btnUpgradesPanel.normalSprite = "ui_shop_btn_left_disable";
        _btnUpgradesPanel.hoverSprite = "ui_shop_btn_left_disable";
        _btnUpgradesPanel.pressedSprite = "ui_shop_btn_left_disable";
        _btnUpgradesPanel.disabledSprite = "ui_shop_btn_left_disable";

        _btnCoinsPanel.normalSprite = "ui_shop_btn_right_enable";
        _btnCoinsPanel.hoverSprite = "ui_shop_btn_right_enable";
        _btnCoinsPanel.pressedSprite = "ui_shop_btn_right_enable";
        _btnCoinsPanel.disabledSprite = "ui_shop_btn_right_enable";

        _upgradesPanel.SetActive(true);
        _coinsPanel.SetActive(false);
    }

    protected override void FillUIElementsInfo()
    {
        base.FillUIElementsInfo();


    }

    private void ButtonCoinsHardClick()
    {

        AudioManager.Instance.PlaySoundUi(SoundConstants.UIAudioClipTypes.Button_Buy);
#if UNITY_EDITOR
        UserStorage.Instance.IncCoins(GameStorage.Instance.Data.CountCoinsHard);
#else
        if (InternetReachabilityController.Instance.ShowPopupIfNeeded() == null)
        {
            DebugLogger.Log("OnPurchase ButtonCoinsHardClick");
            PurchaseManager.Instance.BuyProduct(PurchaseManager.PurchaseType.CoinsHard);
        }
#endif
    }

    private void ButtonCoinsMediumClick()
    {

        AudioManager.Instance.PlaySoundUi(SoundConstants.UIAudioClipTypes.Button_Buy);
#if UNITY_EDITOR
        UserStorage.Instance.IncCoins(GameStorage.Instance.Data.CountCoinsMedium);
#else
        if (InternetReachabilityController.Instance.ShowPopupIfNeeded() == null)
        {
            DebugLogger.Log("OnPurchase ButtonCoinsMediumClick");
            PurchaseManager.Instance.BuyProduct(PurchaseManager.PurchaseType.CoinsMedium);
        }
#endif
    }

    private void ButtonCoinsMiniClick()
    {

        AudioManager.Instance.PlaySoundUi(SoundConstants.UIAudioClipTypes.Button_Buy);
#if UNITY_EDITOR
        UserStorage.Instance.IncCoins(GameStorage.Instance.Data.CountCoinsMini);
#else
        if (InternetReachabilityController.Instance.ShowPopupIfNeeded() == null)
        {
            DebugLogger.Log("OnPurchase ButtonCoinsMiniClick");
            PurchaseManager.Instance.BuyProduct(PurchaseManager.PurchaseType.CoinsMini);
        }
#endif
    }

    private void ButtonDistanceClick()
    {
        UserStorage.Instance.IncDistanceBoost();
    }

    private void ButtonSpeedClick()
    {
        UserStorage.Instance.IncSpeedBoost();
    }

    private void ButtonEnergyClick()
    {
        UserStorage.Instance.IncEnergyBoost();
    }

    protected override void OnDestroy()
    {
        UserStorage.Instance.ChangeUserData -= ChangePriceBoost;
        UserStorage.Instance.ChangeUserData -= SetBoostEnable;
        base.OnDestroy();
    }
}
