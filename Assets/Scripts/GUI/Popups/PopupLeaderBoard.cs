﻿
using System;
using System.Collections.Generic;
using Assets.Scripts.GUI.Popups;
using Assets.Scripts.Server;
using UnityEngine;
using Assets.Scripts.Shared;

internal class PopupLeaderBoard : PopupBase
{
    [SerializeField] private UIScrollView scroll;
    [SerializeField] private UIGrid grid;
    [SerializeField] private GameObject waitObject;
    [SerializeField] private UIButton btnScoreLeader;
    [SerializeField] private UIButton btnStarLeader;
    [SerializeField] private UILabel _labelNoLeader;
    public enum LeaderBoardType
    {
        Score,
        Stars
    }
    private GameObject item;
    private List<GameObject> items;
    private LeaderBoardType _type;

    protected override void InitUIHandlers()
    {
        base.InitUIHandlers();
        item = Resources.Load("Prefabs/UIGameObjects/ItemLeaderBoard", typeof(GameObject)) as GameObject;
        Commands.Add(btnScoreLeader.name, new SimpleActionClickCommand(ButtonScoreLeaderClick));
        Commands.Add(btnStarLeader.name, new SimpleActionClickCommand(ButtonStarLeaderClick));
    }

    protected override void FillUIElementsInfo()
    {
        base.FillUIElementsInfo();
        items = new List<GameObject>();

    }

    public void Init(LeaderBoardType type)
    {
        _type = type;
        switch (_type)
        {
            case LeaderBoardType.Stars:
                btnScoreLeader.gameObject.SetActive(true);
                btnStarLeader.gameObject.SetActive(false);
                GetUserStarsData();
                break;
            case LeaderBoardType.Score:
                btnScoreLeader.gameObject.SetActive(false);
                btnStarLeader.gameObject.SetActive(true);
                GetUserScoreData();
                break;
        }
    }
    private void ButtonScoreLeaderClick()
    {
        AudioManager.Instance.PlaySoundUi(SoundConstants.UIAudioClipTypes.ButtonClick);
        btnScoreLeader.gameObject.SetActive(false);
        btnStarLeader.gameObject.SetActive(true);
        GetUserScoreData();
    }

    private void ButtonStarLeaderClick()
    {
        AudioManager.Instance.PlaySoundUi(SoundConstants.UIAudioClipTypes.ButtonClick);
        btnScoreLeader.gameObject.SetActive(true);
        btnStarLeader.gameObject.SetActive(false);
        GetUserStarsData();
    }


    private void GetUserScoreData()
    {
        //ShowLoader(true);
        GameSparkManager.Instance.GetScoreLeaderBoard((List<GameSparkUser> obj) =>
        {
            ShowLoader(false);
            OnFillScrollByUser(obj, true);
        }, (string obj) =>
        {
            ShowLoader(false);
            ShowError();
        });
    }
    private void GetUserStarsData()
    {
        //ShowLoader(true);
        GameSparkManager.Instance.GetStarsLeaderBoard((List<GameSparkUser> obj) =>
        {
            ShowLoader(false);
            OnFillScrollByUser(obj, false);
        }, (string obj) =>
        {
            ShowLoader(false);
            ShowError();
        });
    }
    private void ShowLoader(bool isShow)
    {
        if (waitObject != null)
        {
            waitObject.SetActive(isShow);
        }
    }

    private void OnFillScrollByUser(List<GameSparkUser> users, bool isCoins)
    {

        if (items != null && items.Count > 0)
        {
            foreach (var go in items)
            {
                NGUITools.Destroy(go);
            }
        }
        items = new List<GameObject>();
        _labelNoLeader.gameObject.SetActive(users.Count == 0);
        foreach (var user in users)
        {
            if (user == null) continue;
            var goUser = Instantiate(item, grid.transform);
            goUser.transform.localScale = Vector3.one;
            var itemLeaderBoard = goUser.GetComponent<ItemLeaderBoard>();
            itemLeaderBoard.Init(user, isCoins);
            items.Add(goUser);
        }
        grid.enabled = true;
        grid.Reposition();
    }

    private void ShowError()
    {
        if (items != null && items.Count > 0)
        {
            foreach (var go in items)
            {
                NGUITools.Destroy(go);
            }
        }
        items = new List<GameObject>();
        _labelNoLeader.gameObject.SetActive(true);
    }
}
