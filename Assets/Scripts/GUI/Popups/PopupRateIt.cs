﻿using UnityEngine;
using System.Collections;
using Assets.Scripts.GUI.Popups;


internal class PopupRateIt : PopupBase
{
    #region Fields

    [SerializeField] private UILabel _labelTitle;
    [SerializeField] private UILabel _labelText;
    [SerializeField] private UIButton BtnRateIt;
    [SerializeField] private UIButton BtnLater;
    [SerializeField] private UIButton[] _buttons;
    private int _currentTouch;

    #endregion

    #region Methods



    protected override void InitUIHandlers()
    {
        base.InitUIHandlers();
        _currentTouch = AppSettings.Instance.RateCounter;
        foreach (var uiButton in _buttons)
            UIEventListener.Get(uiButton.gameObject).onClick += OnClickListner;
        SetViewBtn();
    }

    private void HelpShiftView()
    {
        StartCoroutine(MakeHelpShiftView());
    }

    private void RateIt()
    {
        StartCoroutine(MakeRate());
    }

    private IEnumerator MakeHelpShiftView()
    {
        yield return new WaitForSeconds(1f);
        // HelpShiftManager.Instance.ShowConversation();
        Close();
    }
    private IEnumerator MakeRate()
    {
        yield return new WaitForSeconds(1f);

#if PLATFORM_AMAZON
            Application.OpenURL("amzn://apps/android?p=com.ximad.lilquest");
#endif
#if PLATFORM_ANDROID && UNITY_ANDROID
        Application.OpenURL("https://play.google.com/store/apps/details?id=com.teamsoft.maze");
#endif

        AppSettings.Instance.IsRated = true;
        Close();

    }

    protected override void FillUIElementsInfo()
    {
        base.FillUIElementsInfo();
        AppSettings.Instance.LastRateItShowLevel = UserStorage.Instance.MaxOpenedLevel;
    }

    public override void Close()
    {
        foreach (var uiButton in _buttons)
            // ReSharper disable once DelegateSubtraction
            UIEventListener.Get(uiButton.gameObject).onClick -= OnClickListner;
        base.Close();
    }

    private void SetViewBtn()
    {
        foreach (var uiButton in _buttons)
        {
            var sprite = uiButton.gameObject.GetComponent<UISprite>();
            if (int.Parse(uiButton.name) <= _currentTouch)
            {
                sprite.spriteName = "ui_star_enable";
            }
            else
            {
                sprite.spriteName = "ui_star_disable";
            }

        }
    }

    private void OnClickListner(GameObject go)
    {
        BlockButtons(true);
        _currentTouch = int.Parse(go.name);
        AudioManager.Instance.PlaySoundUi(SoundConstants.UIAudioClipTypes.Button_Invite);
        AppSettings.Instance.RateCounter = _currentTouch;

        SetViewBtn();
        if (_currentTouch > 3)
            RateIt();
        else
        {
            HelpShiftView();
        }
    }

    #endregion
}
