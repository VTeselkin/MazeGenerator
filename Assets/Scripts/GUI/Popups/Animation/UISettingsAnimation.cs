﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Assets.Scripts.Server;

public class UISettingsAnimation : MonoBehaviour
{
	[SerializeField] private UISprite sprite;
	[SerializeField] private List<GameObject> buttons;
	[SerializeField] private GameObject logOutBtn;
	[SerializeField]
	private float m_animTouchTimeStart = 0.2f;
	[SerializeField]
	private float m_animTouchTimeFinish = 0.2f;

	[SerializeField] private float newHeight = 300;
	[SerializeField] private float oldHeight = 100;
	private bool isProcess = false;

	public void StartAnimation(bool openAnimation)
	{
		if (FaceBookManager.Instance.IsLogin())
		{
			newHeight = 380;
		}
		if (isProcess) return;

		if (openAnimation)
		{
			StartCoroutine(OpenAnimation());
		}
		else
		{
			StartCoroutine(CloseAnimation());
		}
	}


	private IEnumerator OpenAnimation()
	{
		isProcess = true;
		float timeEnd = Time.time + m_animTouchTimeStart;

		while (timeEnd > Time.time)
		{
			sprite.height = (int)Mathf.Lerp(oldHeight, newHeight, 1 - ((timeEnd - Time.time) / m_animTouchTimeStart));
			yield return null;
		}
		foreach (var button in buttons)
		{
			button.SetActive(true);
		}
		if (FaceBookManager.Instance.IsLogin())
		{
			logOutBtn.SetActive(true);
		}
		isProcess = false;
		sprite.height = (int)newHeight;

	}

	private IEnumerator CloseAnimation()
	{

		isProcess = true;
		foreach (var button in buttons)
		{
			button.SetActive(false);
		}
		logOutBtn.SetActive(false);
		float timeEnd = Time.time + m_animTouchTimeFinish;

		while (timeEnd > Time.time)
		{
			sprite.height = (int)Mathf.Lerp(newHeight, oldHeight, 1 - ((timeEnd - Time.time) / m_animTouchTimeFinish));
			yield return null;
		}
		isProcess = false;
		sprite.height = (int)oldHeight;
	}
}
