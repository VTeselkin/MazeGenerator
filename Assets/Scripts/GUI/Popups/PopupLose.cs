﻿using System;
using Assets.Scripts.GUI.Popups;
using UnityEngine;

internal class PopupLose : PopupBase
{
	[SerializeField] private UIButton _btnMenu;
	[SerializeField] private UIButton _btnReplay;


	protected override void InitUIHandlers()
	{
		base.InitUIHandlers();
		Commands.Add(_btnMenu.name, new SimpleActionClickCommand(ButtonMenuClick));
		Commands.Add(_btnReplay.name, new SimpleActionClickCommand(ButtonReplayClick));
	}

	protected override void FillUIElementsInfo()
	{
		base.FillUIElementsInfo();
		AudioManager.Instance.PauseBackgroundMusic(true);
		AnalyticsWrapper.Instance.EventLoseLevel(LevelStorage.Instance.Data[UserStorage.Instance.CurrentSelectLevel]);

	}

	private void ButtonReplayClick()
	{
		Close();
		GameManager<BaseGameObject>.Instance.RestartGame();
	}

	private void ButtonMenuClick()
	{
		Close();
		GameManager<BaseGameObject>.Instance.FinishGame();
	}



}
