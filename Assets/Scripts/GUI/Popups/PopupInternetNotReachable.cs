﻿using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.GUI.Popups;
using UnityEngine;

internal class PopupInternetNotReachable : PopupBase
{
	protected override void InitUIHandlers()
	{
		base.InitUIHandlers();
	}

	protected override void FillUIElementsInfo()
	{
		base.FillUIElementsInfo();
	}
}
