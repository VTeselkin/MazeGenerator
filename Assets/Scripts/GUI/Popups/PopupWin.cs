﻿using System.Collections.Generic;
using Assets.Scripts.GUI.Popups;
using UnityEngine;
using Assets.Scripts.Shared;
using System.Linq;
using Assets.Scripts.Server;
using System;
using System.Collections;
using Managers;
using UnityEngine.Advertisements;

internal class PopupWin : PopupBase
{

    [SerializeField] private UIButton _btnMenu;
    [SerializeField] private UIButton _btnReplay;
    [SerializeField] private UIButton _btnNext;
    [SerializeField] private UIButton _btnFBShare;
    [SerializeField] private UIButton _btnADRewarded;
    [SerializeField] private UIGrid _grid;
    [SerializeField] private UILabel _score;
    [SerializeField] private UILabel _labelFBShare;
    [SerializeField] private List<UISprite> _starView;
    [SerializeField] private ParticleSystem[] _starFlashs;
    [SerializeField] private GameObject _coinsPanel;
    [SerializeField] private UILabel _labelNoReward;
    private float _starsCount;
    private int _stars;
    private int _coinsToReward;
    private const float _delayStarsAnimation = 0.15f;
    private const float _delayFlashAnimation = 0.7f;
    private Progress _progress;
    protected override void InitUIHandlers()
    {
        base.InitUIHandlers();
        Commands.Add(_btnMenu.name, new SimpleActionClickCommand(ButtonMenuClick));
        Commands.Add(_btnReplay.name, new SimpleActionClickCommand(ButtonReplayClick));
        Commands.Add(_btnNext.name, new SimpleActionClickCommand(ButtonNextClick));
        Commands.Add(_btnFBShare.name, new SimpleActionClickCommand(ButtonFBShareClick));
        Commands.Add(_btnADRewarded.name, new SimpleActionClickCommand(ButtonADRewardedClick));
    }

    private void ButtonADRewardedClick()
    {
        _btnADRewarded.gameObject.SetActive(false);
        _grid.enabled = true;
        _grid.Reposition();
        AdsManager.Instance.ShowRewarded(result =>
        {
            if (result == UnityEngine.Monetization.ShowResult.Finished)
            {
                DebugLogger.Log("AdsWrapper.Instance.RewardedComplete start = " + _coinsToReward);
                _coinsToReward = _coinsToReward + (int)((_coinsToReward * 10) / 100f);
                DebugLogger.Log("AdsWrapper.Instance.RewardedComplete finish = " + _coinsToReward);
                CheckReward();
            }  
        });
       
    }



    private void OnApplicationPause(bool pause)
    {
        if (!pause)
        {
            string s = string.Format("{0}", _coinsToReward);
            DebugLogger.Log("AdsWrapper.Instance.RewardedComplete isUpdate = " + s);
            if (_score != null)
            {
                _score.text = s;
            }
        }
    }
    //private void Update()
    //{
    //    if (isUpdate)
    //    {
    //        string s = string.Format("{0}", _coinsToShow);
    //        DebugLogger.Log("AdsWrapper.Instance.RewardedComplete isUpdate = " + s);
    //        if (_score != null)
    //        {
    //            _score.text = s;
    //            isUpdate = false;
    //        }
    //    }
    //}

    private void ButtonFBShareClick()
    {
        //FaceBookManager.Instance.ShareScoreWihtFriend(_coinsToReward, GameManager<BaseGameObject>.Instance.LevelData.LevelNumber);
        FaceBookManager.Instance.InviteFriends((result) =>
        {
            AnalyticsWrapper.Instance.EventIniviteState(!result.Cancelled);
        });
    }

    protected override void FillUIElementsInfo()
    {
        base.FillUIElementsInfo();
        AudioManager.Instance.PauseBackgroundMusic(true);
        OnChangeFBButton(false);
        _progress = LevelProgress.Instance.LevelsProgress[UserStorage.Instance.CurrentSelectLevel];
        FaceBookManager.Instance.CompleteAutorization += OnChangeFBButton;
        _btnADRewarded.gameObject.SetActive(AdsManager.Instance.RewardedReady());
        _grid.enabled = true;
        _grid.Reposition();
        CalculateReward();
    }

    private void ButtonReplayClick()
    {
        Close();
        GameManager<BaseGameObject>.Instance.RestartGame();
    }

    private void ButtonMenuClick()
    {
        OnApplyRewards();
        Close();
        GameManager<BaseGameObject>.Instance.FinishGame();
    }

    private void ButtonNextClick()
    {
        OnApplyRewards();
        Close();
        GameManager<BaseGameObject>.Instance.StartNextLevel();
    }

    private void OnChangeFBButton(bool isLogin)
    {
        if (!FaceBookManager.Instance.IsLogin())
        {
            _labelFBShare.text = "Login To FaceBook";
        }
        else
        {
            _labelFBShare.text = "Invite Friends";
        }
    }

    private void CalculateReward()
    {

        var player = GameManager<BaseGameObject>.Instance.Player;
        var levelData = GameManager<BaseGameObject>.Instance.LevelData;
        var boostEnergy = player.UserBoostEnergy;
        var startEnergy = levelData.PlayerEnergy;
        var typeLevel = levelData.LevelTarget;
        var targetEnergy = 0;
        switch (typeLevel)
        {
            case Assets.Scripts.Helpers.LevelTargetEnum.Finish:
                break;
            case Assets.Scripts.Helpers.LevelTargetEnum.Collect:
                targetEnergy = (int)(levelData.Foods.Sum((e) => e.Energy));
                break;
            case Assets.Scripts.Helpers.LevelTargetEnum.FinishAndCollect:
                targetEnergy = (int)(levelData.Foods.Sum((e) => e.Energy));
                break;
        }

        _coinsToReward = player.Energy;
        DebugLogger.Log("Reward score = " + _coinsToReward);
        _starsCount = player.Energy / (float)(startEnergy + boostEnergy);
        DebugLogger.Log("Reward stars = " + _starsCount);
        if (_starsCount > 0.5f)
        {
            _stars = 3;
        }
        if (_starsCount <= 0.5f && _starsCount > 0.25f)
        {
            _stars = 2;
        }
        if (_starsCount < 0.25f && _starsCount > 0.15f)
        {
            _stars = 1;
        }
        else if (_starsCount <= 0.15f)
        {
            _stars = 0;
        }
        if (_stars >= 1)
        {
            _coinsToReward = _coinsToReward * _stars;

        }
        CheckReward();
        string s = string.Format("{0}", _coinsToReward);
        DebugLogger.Log("AdsWrapper.Instance.RewardedComplete isUpdate = " + s);
        if (_score != null)
        {
            _score.text = s;
        }
        RunStarsAnimation();
    }

    private void CheckReward()
    {
        if (_progress.IsFinished)
        {
            _coinsToReward = _coinsToReward - _progress.Coins;
            if (_coinsToReward < 0)
            {
                _coinsToReward = 0;
            }
            if (_coinsToReward > 0)
            {
                _coinsPanel.SetActive(true);
                _labelNoReward.enabled = false;
            }
            else
            {
                _coinsPanel.SetActive(false);
                _labelNoReward.enabled = true;
                _labelNoReward.text = "You have already received the award! Improve your result.";
            }
        }
    }

    private void OnApplyRewards()
    {
        FaceBookManager.Instance.CompleteAutorization -= OnChangeFBButton;
        AnalyticsWrapper.Instance.EventWinLevel(LevelStorage.Instance.Data[UserStorage.Instance.CurrentSelectLevel], _coinsToReward, _stars);
        SaveProgressByLevel();
    }

    private bool SaveProgressByLevel()
    {
        var levelNumber = GameManager<BaseGameObject>.Instance.LevelData.LevelNumber - 1;
        UserStorage.Instance.SetCurrentLevel(levelNumber);
        return LevelProgress.Instance.UpdateLevelProgress(levelNumber, new Progress(_coinsToReward, _stars));
    }

    private void RunStarsAnimation()
    {
        StartCoroutine(StarsActivation());
    }

    private IEnumerator StarsActivation()
    {
        for (int i = 0; i < _stars; i++)
        {
            yield return new WaitForSeconds(_delayFlashAnimation);
            _starFlashs[i].gameObject.SetActive(true);
            AudioManager.Instance.PlaySoundUi(SoundConstants.UIAudioClipTypes.Star1, i);
            yield return new WaitForSeconds(_delayStarsAnimation);
            _starView[i].enabled = true;



        }
    }
}
