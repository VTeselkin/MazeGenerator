using Assets.Scripts.GUI.Popups;
using Managers;
using UnityEngine;

internal class PopupPause : PopupBase
{
    [SerializeField] private UIButton _btnMenu;
    [SerializeField] private UIButton _btnReplay;
    private bool isRestart = false;
    private bool isClose = false;

    protected override void InitUIHandlers()
    {
        base.InitUIHandlers();
        Commands.Add(_btnMenu.name, new SimpleActionClickCommand(ButtonMenuClick));
        Commands.Add(_btnReplay.name, new SimpleActionClickCommand(ButtonReplayClick));
    }

    protected override void FillUIElementsInfo()
    {
        base.FillUIElementsInfo();
    }

    private void ButtonReplayClick()
    {
        AudioManager.Instance.PlaySoundUi(SoundConstants.UIAudioClipTypes.ButtonClick);
        if (!AppSettings.Instance.NoADS)
        {
            AdsManager.Instance.ShowVideo();
            isRestart =true;
        }
        else
        {
            isRestart = true;
        }

    }

    private void ButtonMenuClick()
    {
        AudioManager.Instance.PlaySoundUi(SoundConstants.UIAudioClipTypes.ButtonClick);
        if (!AppSettings.Instance.NoADS)
        {
            AdsManager.Instance.ShowVideo();
            isClose = true;
        }
        else
        {
            isClose = true;
        }
    }

    protected override void Update()
    {
        base.Update();
        if (isRestart)
        {
            isRestart = false;
            GameManager<BaseGameObject>.Instance.RestartGame();
            Close();
        }
        if (isClose)
        {
            isClose = false;
            GameManager<BaseGameObject>.Instance.FinishGame();
            Close();
        }
    }
}
