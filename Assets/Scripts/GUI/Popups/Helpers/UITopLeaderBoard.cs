﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Assets.Scripts.Server;
using System.Linq;

public class UITopLeaderBoard : MonoBehaviour
{

	[SerializeField] private UITexture _avatar;
	[SerializeField] private UITexture _flag;
	[SerializeField] private UILabel _userName;
	[SerializeField] private UILabel _city;
	[SerializeField] private UILabel _score;
	[SerializeField] private GameObject _content;
	[SerializeField] private GameObject _country;
	private void Start()
	{
		FaceBookManager.Instance.CompleteAutorization += Instance_CompleteAutorization;
		if (FaceBookManager.Instance.IsLogin())
		{
			Instance_CompleteAutorization(false);
		}
	}

	void Instance_CompleteAutorization(bool res)
	{
		if (FaceBookManager.Instance.IsLogin())
		{
			GameSparkManager.Instance.GetScoreLeaderBoard((List<GameSparkUser> obj) =>
			{
				if (obj != null && obj.Count > 0)
				{
					_content.SetActive(true);
					SetView(obj[0]);
				}
			}, (string obj) =>
			{
				_content.SetActive(false);
			});
		}
		else
		{
			_content.SetActive(false);
		}
	}

	void SetView(GameSparkUser gsUser)
	{
		_userName.text = gsUser.UserName;
		_city.text = gsUser.City;
		_score.text = gsUser.UserScore.ToString();
		if (gsUser.Country.Count() > 0)
		{
			var texture = Resources.Load("Textures/UI/Popup/Flags/" + gsUser.Country.ToLower(), typeof(Texture)) as Texture;
			if (texture != null)
			{
				_flag.mainTexture = texture;
			}
			else
			{
				_country.SetActive(false);
			}
		}
		else
		{
			_country.SetActive(false);
		}
		if (gsUser.Id != null && gsUser.Id.Length > 1)
		{
			_avatar.mainTexture = FacebookImageCacheManager.Instance.GetOrAddTextureForKey(gsUser.Id, true, (Texture texture) =>
			{
				_avatar.mainTexture = texture;
			});
		}
	}

	private void OnDestroy()
	{
		FaceBookManager.Instance.CompleteAutorization -= Instance_CompleteAutorization;
	}
}
