﻿using System.Collections.Generic;
using Assets.Scripts.Helpers;
using UnityEngine;

public class UIPowerLine : MonoBehaviour
{

    [SerializeField] private List<GameObject> items;
    [SerializeField] private TypeBoostStore Type;


    void Start()
    {
        UserStorage.Instance.ChangeUserData += ChangeUserData;
        ChangeUserData(Type);
    }

    private void ChangeUserData(TypeBoostStore type)
    {
        if (type != Type) return;
        var data = 0;
        switch (type)
        {
            case TypeBoostStore.EnergyBoost:
                data = UserStorage.Instance.EnergyBoost;
                break;
            case TypeBoostStore.SpeedBoost:
                data = UserStorage.Instance.SpeedBoost;
                break;
            case TypeBoostStore.DistanceBoost:
                data = UserStorage.Instance.DistanceBoost;
                break;
        }

        for (int i = 0; i < items.Count; i++)
        {
            if (i < data)
            {
                items[i].SetActive(true);
            }
            else
            {
                items[i].SetActive(false);
            }
        }
    }

    void OnDestroy()
    {
        UserStorage.Instance.ChangeUserData -= ChangeUserData;
    }
}
