﻿using UnityEngine;

public class UISettingButtonView : MonoBehaviour
{
	[SerializeField] private UISprite sprite;
	[SerializeField] private TypeSettings type;

	private enum TypeSettings
	{
		Music,
		Sound,
		Logout
	}

	// Use this for initialization
	void Start()
	{
		switch (type)
		{
			case TypeSettings.Music:
				AppSettings.Instance.MusicChanged += OnSettinsChanged;
				break;
			case TypeSettings.Sound:
				AppSettings.Instance.SoundsChanged += OnSettinsChanged;
				break;
		}

		OnSettinsChanged();
	}

	private void OnSettinsChanged()
	{
		var isEnable = false;
		switch (type)
		{
			case TypeSettings.Music:
				isEnable = AppSettings.Instance.IsMusicOn;
				break;
			case TypeSettings.Sound:
				isEnable = AppSettings.Instance.IsSoundsOn;
				break;
		}
		if (sprite != null)
		{
			sprite.enabled = !isEnable;
		}
	}

	public void OnDestor()
	{
		switch (type)
		{
			case TypeSettings.Music:
				AppSettings.Instance.MusicChanged -= OnSettinsChanged;
				break;
			case TypeSettings.Sound:
				AppSettings.Instance.SoundsChanged -= OnSettinsChanged;
				break;
		}
	}
}
