﻿using UnityEngine;
using System.Collections.Generic;


public class UILevelGenerate : MonoBehaviour
{

    [SerializeField] private GameObject parent;
    [SerializeField] private UIGrid grid;
    [SerializeField] private UIScrollView scrollView;
    [SerializeField] private UILevelPointer pointers;
    [SerializeField] private UILabel levelTitle;
    [SerializeField] private UIRatingPanel ratingPanel;
    private List<UILevelItem> itemList;
    private const int IndexDisableTouchItem = 1;
    private void Start()
    {
        UserStorage.Instance.ChangeCurrentLevel += ChangeCurrentLevel;
        UserStorage.Instance.ChangeUserProgress += CompleteAutorization;
        LevelStorage.Instance.CompleteLoad += LevelLoadComplete;
        scrollView.onMomentumMove += ScrollView_OnMomentumMove;
        itemList = new List<UILevelItem>();
       
        if (LevelStorage.Instance.Data != null && LevelStorage.Instance.Data.Count > 0)
        {
            GenerateLevel();
            ChangeCurrentLevel();
        }
        
       
    }

    private void CompleteAutorization()
    {
        if (itemList != null)
        {
            foreach (var item in itemList)
            {
                NGUITools.Destroy(item.gameObject);
            }
        }
        itemList = new List<UILevelItem>();
        GenerateLevel();
        ChangeAnimation();
        ChangeScrollPosition();

    }

    private void ChangeCurrentLevel()
    {
        ChangeAnimation();
        ChangeScrollPosition();

    }

    private void LevelLoadComplete(bool isComplete)
    {
        LevelStorage.Instance.CompleteLoad -= LevelLoadComplete;
        GenerateLevel();
        ChangeCurrentLevel();
    }

    private void OnDestroy()
    {
        UserStorage.Instance.ChangeUserProgress -= CompleteAutorization;
        UserStorage.Instance.ChangeCurrentLevel -= ChangeCurrentLevel;
        scrollView.onMomentumMove -= ScrollView_OnMomentumMove;
    }

    private void GenerateLevel()
    {
        pointers.Init();
        pointers.ChangePageLevels(0);
        ratingPanel.Init();
        levelTitle.text = UserStorage.Instance.MaxOpenedLevel + "/" + LevelStorage.Instance.LevelCount;
        var level = Resources.Load("Prefabs/Popups/Items/LevelItem", typeof(GameObject)) as GameObject;
        for (int i = 0; i < LevelStorage.Instance.LevelCount; i++)
        {

            var go = Instantiate(level, parent.transform);
            if (i <= IndexDisableTouchItem || i > LevelStorage.Instance.LevelCount - IndexDisableTouchItem)
            {
                var center = go.GetComponent<UICenterOnChild>();
                if (center != null)
                {
                    Destroy(center);
                }

            }
            var levelItem = go.GetComponent<UILevelItem>();
            if (levelItem != null)
            {
                levelItem.Init(i, ChangeAnimation);
                itemList.Add(levelItem);
            }
        }
        grid.Reposition();

    }
    private void ChangeAnimation()
    {
        foreach (var item in itemList)
        {
            item.SetAnimationState();
        }
    }
    private int pointIndex = 0;
    void ScrollView_OnMomentumMove()
    {
        var index = (int)(scrollView.panel.clipOffset.x / 550);
        if (index != pointIndex)
        {
            pointIndex = index;
            pointers.ChangePageLevels(pointIndex);
        }
    }




    private void ChangeScrollPosition()
    {
        if (itemList.Count == 0) return;
        var item = itemList[UserStorage.Instance.CurrentSelectLevel];
        UICenterOnChild center = item.GetComponent<UICenterOnChild>();
        UIPanel panel = NGUITools.FindInParents<UIPanel>(item.gameObject);

        if (center != null)
        {
            if (center.enabled)
                center.CenterOn(item.transform);
        }
        else if (item._levelIndex >= IndexDisableTouchItem && item._levelIndex < LevelStorage.Instance.LevelCount - IndexDisableTouchItem && panel != null && panel.clipping != UIDrawCall.Clipping.None)
        {
            UIScrollView sv = panel.GetComponent<UIScrollView>();
            Vector3 offset = -panel.cachedTransform.InverseTransformPoint(item.transform.position);
            if (!sv.canMoveHorizontally) offset.x = panel.cachedTransform.localPosition.x;
            if (!sv.canMoveVertically) offset.y = panel.cachedTransform.localPosition.y;
            SpringPanel.Begin(panel.cachedGameObject, offset, 6f);
        }
    }
}
