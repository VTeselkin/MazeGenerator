﻿using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Server;
using UnityEngine;
using Assets.Scripts.Shared;

public class UIRatingPanel : MonoBehaviour
{

    [SerializeField] private UIGrid usersGrid;
    [SerializeField] private GameObject usersPanel;
    [SerializeField] private UILoading loading;
    private int _currentLevel = 0;
    public void Init()
    {
        UserStorage.Instance.ChangeCurrentLevel += ChangeCurrentLevel;
        FaceBookManager.Instance.CompleteAutorization += Instance_CompleteAutorization;
        Instance_CompleteAutorization(false);
    }

    private void ChangeCurrentLevel()
    {
        if (FaceBookManager.Instance.IsLogin())
        {
            if (_currentLevel != UserStorage.Instance.CurrentSelectLevel)
            {
                _currentLevel = UserStorage.Instance.CurrentSelectLevel;
                SetLeaderBoard();
            }
        }
    }
    void Instance_CompleteAutorization(bool obj)
    {
        bool active = FaceBookManager.Instance.IsLogin();
        usersPanel.SetActive(active);
        if (active)
        {
            SetLeaderBoard();
        }

    }

    private List<GameObject> usersList;
    private void SetLeaderBoard()
    {
        DebugLogger.Log("Start  UIRatingPanel SetLeaderBoard");
        loading.StartLoading();
        if (usersList == null)
        {
            usersList = new List<GameObject>();
        }
        else
        {
            foreach (var o in usersList)
            {
                NGUITools.Destroy(o);
            }
            usersList = new List<GameObject>();
        }
        if (InternetReachabilityController.Instance.IsInternetAvalible)
        {
            DebugLogger.Log("Start  UIRatingPanel IsInternetAvalible true");
            GameSparkManager.Instance.GetLeaderBoardByLevel(UserStorage.Instance.CurrentSelectLevel, (List<GameSparkUser> obj) =>
            {
                DebugLogger.Log("Start  UIRatingPanel AddItemRating = " + obj.Count);
                AddItemRating(obj);
            }, (string message) =>
            {
                DebugLogger.Log("UIRatingPanel GetLeaderBoardByLevel Error =  " + message);
                List<GameSparkUser> obj = new List<GameSparkUser>();
                AddItemRating(obj);
            });
        }
        else
        {
            DebugLogger.Log("Start  UIRatingPanel IsInternetAvalible false");
            List<GameSparkUser> obj = new List<GameSparkUser>();
            AddItemRating(obj);
        }

    }
    private void AddItemRating(List<GameSparkUser> obj)
    {
        loading.StopLoading();
        DebugLogger.Log("UIRatingPanel GetLeaderBoardByLevel complete. Get " + obj.Count + " items");
        var userItem = Resources.Load("Prefabs/Popups/Items/UserItem", typeof(GameObject)) as GameObject;
        var count = obj.Count;
        if (count > 5)
        {
            count = 5;
        }
        if (obj.Count < 5)
        {
            obj = AddBotRating(obj);
        }

        for (int i = 0; i < obj.Count; i++)
        {
            var go = Instantiate(userItem, usersGrid.transform);
            usersList.Add(go);
            var user = go.GetComponent<ItemLeaderBoard>();
            user.Init(obj[i], true);

        }
        usersGrid.enabled = true;
        usersGrid.Reposition();
    }
    private List<GameSparkUser> AddBotRating(List<GameSparkUser> obj)
    {
        if (obj.Count < 5)
        {
            int index = obj.Count;
            int stars = 4;
            for (int i = index; i < 5; i++)
            {
                var bot = new GameSparkUser();
                bot.isBot = true;
                bot.UserScore = GetScoreForBot(stars);
                stars--;
                obj.Add(bot);

            }
        }
        List<GameSparkUser> SortedList = obj.OrderBy(o => o.UserScore).ToList();
        SortedList.Reverse();
        for (int i = 0; i < SortedList.Count; i++)
        {
            SortedList[i].UserRank = i + 1;
        }
        return SortedList;
    }

    private void OnDestroy()
    {
        UserStorage.Instance.ChangeCurrentLevel -= ChangeCurrentLevel;
        FaceBookManager.Instance.CompleteAutorization -= Instance_CompleteAutorization;
    }

    private int GetScoreForBot(int star)
    {

        var levelData = LevelStorage.Instance.Data[UserStorage.Instance.CurrentSelectLevel];
        var startEnergy = levelData.PlayerEnergy / 2;
        if (star > 0)
        {
            return startEnergy * star;
        }
        else
        {
            return startEnergy;
        }
    }
}
