﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UILoading : MonoBehaviour
{

	[SerializeField] private UILabel _text;
	private bool isLoading = false;
	public void StartLoading()
	{
		_text.enabled = true;
		isLoading = true;
		StartCoroutine(Loading());
	}

	public void StopLoading()
	{
		_text.text = "";
		_text.enabled = false;
		isLoading = false;
		StopCoroutine(Loading());
	}

	private IEnumerator Loading()
	{
		int index = 0;
		while (isLoading)
		{
			yield return new WaitForSeconds(1f);
			if (index == 0)
			{
				_text.text = "Loading";
			}
			_text.text += ".";
			index++;
			if (index > 2)
			{
				index = 0;
			}

		}
	}
}
