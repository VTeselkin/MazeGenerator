﻿using System.Collections.Generic;
using Assets.Scripts.GUI.Managers;
using UnityEngine;
using System;
using Assets.Scripts.Helpers;

public class UILevelItem : MonoBehaviour
{
    [SerializeField] private UISprite backSprite;
    [SerializeField] private UISprite lockSprite;
    [SerializeField] private List<UISprite> stars;
    [SerializeField] private UILabel levelNumber;
    [SerializeField] private UIButton clickBtn;
    [SerializeField] private Animation anim;

    private Action _onClick;
    public int _levelIndex;

    public void Init(int index, Action click)
    {
        _onClick = click;
        _levelIndex = index;
        var levelProgress = LevelProgress.Instance.LevelsProgress[index];
        if (levelProgress.IsFinished || UserStorage.Instance.MaxOpenedLevel >= index || index == 0 || GameBuildConfig.IsDevelopBuild)
        {
            CompleteLevel(levelProgress);
        }
        else
        {
            UnCompleteLevel();
        }
        levelNumber.text = (index + 1).ToString();

    }
    public void SetAnimationState()
    {

        if (anim != null)
        {
            if (UserStorage.Instance.CurrentSelectLevel == _levelIndex)
            {
                anim.Play();
            }
            else
            {
                anim.Stop();
                anim.transform.localScale = new Vector3(1.2f, 1.2f, 1.2f);
            }
        }
    }

    private void CompleteLevel(Progress levelProgress)
    {
        UIEventListener.Get(clickBtn.gameObject).onClick += (go) =>
        {
            if (!UIManager.Instance.IsOpenedPopups)
            {
                UserStorage.Instance.SetCurrentLevel(_levelIndex);
                AudioManager.Instance.PlaySoundUi(SoundConstants.UIAudioClipTypes.ButtonPress);
                _onClick();
            }
        };

        if (UserStorage.Instance.MaxOpenedLevel == _levelIndex)
        {
            var anim = GetComponent<Animation>();
            if (anim != null)
            {
                anim.Play();
            }
        }
        for (int i = 0; i < levelProgress.Star; i++)
        {
            stars[i].spriteName = "back_level_star_active";
        }
        backSprite.spriteName = "back_level_number";
        lockSprite.enabled = false;
    }

    private void UnCompleteLevel()
    {
        clickBtn.enabled = false;
        backSprite.spriteName = "ui_popup_level_inactive";
        lockSprite.enabled = true;
    }

}
