﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Assets.Scripts.Shared;

public class ItemLeaderBoard : MonoBehaviour
{
    [SerializeField]
    private UILabel UserName;
    [SerializeField]
    private UILabel UserScore;
    [SerializeField]
    private UILabel UserRank;
    [SerializeField]
    private UITexture UserAvatar;
    [SerializeField]
    private UITexture country;
    [SerializeField]
    private GameObject countryBot;
    [SerializeField]
    private UILabel City;
    [SerializeField]
    private bool splitName = true;
    private string id;
    List<string> nameUser = new List<string>{
                {"Soupy"},
                {"Bo Bo"},
                {"Finky"},
                {"Geo"},
                {"Sammu"}};

    public void Init(GameSparkUser item, bool isCoins)
    {
        if (item == null) return;
        if (item.isBot)
        {
            SetBot(item);
            return;
        }
        if (UserName != null)
        {
            if (splitName)
            {
                UserName.text = item.UserName.Split()[0];
            }
            else
            {
                UserName.text = item.UserName;
            }

        }
        if (isCoins)
        {
            if (UserScore != null)
            {
                UserScore.text = item.UserScore.ToString();
            }
        }
        else
        {
            if (UserScore != null)
            {
                UserScore.text = item.UserStars.ToString();
            }
        }
        if (UserRank != null)
        {
            UserRank.text = item.UserRank.ToString() + ".";
        }
        if (country != null)
        {
            var texture = Resources.Load("Textures/UI/Popup/Flags/" + item.Country.ToLower(), typeof(Texture)) as Texture;
            if (texture != null)
            {
                country.mainTexture = texture;
            }
            else
            {
                country.parent.gameObject.SetActive(false);
                countryBot.SetActive(true);
            }
        }
        if (item.Id != null && item.Id.Length > 1)
        {
            UserAvatar.mainTexture = FacebookImageCacheManager.Instance.GetOrAddTextureForKey(item.Id, true, (Texture texture) =>
            {
                UserAvatar.mainTexture = texture;
            });
        }
        if (City != null)
        {
            City.text = item.City;
        }
    }

    private void SetBot(GameSparkUser item)
    {
        if (UserName != null)
        {
            string path = "";
            UserName.text = nameUser[item.UserRank - 1];
            path = "Textures/UI/Popup/Avatars/Avatar_" + item.UserRank;

            if (UserAvatar != null)
            {
                var texture = Resources.Load(path, typeof(Texture)) as Texture;
                UserAvatar.mainTexture = texture;
            }
        }
        if (UserScore != null)
        {
            UserScore.text = item.UserScore.ToString();
        }
        if (UserRank != null)
        {
            UserRank.text = item.UserRank.ToString() + ".";
        }
        if (country != null)
        {
            country.parent.gameObject.SetActive(false);
            countryBot.SetActive(true);
        }
    }
}
