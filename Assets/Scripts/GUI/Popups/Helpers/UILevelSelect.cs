﻿using Assets.Scripts.Helpers;
using UnityEngine;

public class UILevelSelect : MonoBehaviour
{

    [SerializeField] private UITexture _wallTexture;
    [SerializeField] private UILabel _levelSize;
    [SerializeField] private UILabel _energyLevel;
    [SerializeField] private UILabel _speedLevel;
    [SerializeField] private UILabel _infinityEnergy;

    private int currentLevelIndex = 0;
    private void Start()
    {
        SetView(TypeBoostStore.Undefined);
        UserStorage.Instance.ChangeUserData += (obj) => { SetView(TypeBoostStore.Undefined); };
        UserStorage.Instance.ChangeCurrentLevel += () => { SetView(TypeBoostStore.Undefined); };
        LevelStorage.Instance.CompleteLoad += (bool obj) => { SetView(TypeBoostStore.Undefined); };
    }

    private void SetView(TypeBoostStore type)
    {
        if (LevelStorage.Instance.Data != null && LevelStorage.Instance.Data.Count > 0)
        {
            currentLevelIndex = UserStorage.Instance.CurrentSelectLevel;
            SetTexture();
            SetLevelSizeLabel();
            SetEnergyLabel();
            SetSpeedLabel();
        }
    }

    private void SetTexture()
    {
        Texture texture = Resources.Load("Textures/Game/Wall/Wall_" + LevelStorage.Instance.Data[currentLevelIndex].ViewIndexWall) as Texture;
        _wallTexture.mainTexture = texture;
    }

    private void SetLevelSizeLabel()
    {
        var x = LevelStorage.Instance.Data[currentLevelIndex].Width;
        var y = LevelStorage.Instance.Data[currentLevelIndex].Height;
        _levelSize.text = x + "X" + y;
    }

    private void SetEnergyLabel()
    {
        if (LevelStorage.Instance.Data[currentLevelIndex].LevelNumber <= GameStorage.Instance.Data.MaxLevelForInfinityEnergy)
        {
            if (_infinityEnergy != null)
            {
                _infinityEnergy.enabled = true;
                _energyLevel.enabled = false;
                return;
            }
        }
        var energy = LevelStorage.Instance.Data[currentLevelIndex].PlayerEnergy;
        float userBoostEnergy = UserStorage.Instance.EnergyBoost * GameStorage.Instance.Data.PercentByIndex / 100.0f;
        _energyLevel.text = energy + "+" + userBoostEnergy * energy;
    }

    private void SetSpeedLabel()
    {
        var userBoostEnergy = UserStorage.Instance.SpeedBoost * GameStorage.Instance.Data.PercentByIndex;
        _speedLevel.text = (userBoostEnergy + 100).ToString();
    }







}
