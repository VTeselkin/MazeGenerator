﻿using System.Collections.Generic;
using UnityEngine;

public class UILevelPointer : MonoBehaviour
{
    [SerializeField] private UIGrid grid;
    private List<UISprite> listSprite;

    public void Init()
    {
        if (listSprite != null)
        {
            foreach (var item in listSprite)
            {
                NGUITools.Destroy(item.gameObject);
            }
        }
        listSprite = new List<UISprite>();
        var point = Resources.Load("Prefabs/Popups/Items/Point", typeof(GameObject)) as GameObject;
        int count = LevelStorage.Instance.LevelCount / 3;
        for (int i = 0; i < count; i++)
        {
            var go = Instantiate(point, grid.transform);
            var pointSprite = go.GetComponent<UISprite>();
            listSprite.Add(pointSprite);
        }
        grid.Reposition();

    }

    public void ChangePageLevels(int activePoint)
    {
        if (activePoint >= listSprite.Count || activePoint < 0) return;
        foreach (var point in listSprite)
        {
            point.spriteName = "back_level_dot_inactive";
            point.height = 18;

        }
        listSprite[activePoint].spriteName = "back_level_dot";
        listSprite[activePoint].height = 20;
    }
}
