﻿using System.Collections.Generic;
using UnityEngine;
using Assets.Scripts.GUI.Popups;
using System;
using Assets.Scripts.Game;

internal class PopupTutorial : PopupBase
{
    [SerializeField] private List<GameObject> tutorials;
    [SerializeField] private GameObject context;
    [SerializeField] private UIButton HideTutorial;

    private Action _action;
    protected override void InitUIHandlers()
    {
        base.InitUIHandlers();
        context.SetActive(true);
    }

    protected override void FillUIElementsInfo()
    {
        base.FillUIElementsInfo();
        UIEventListener.Get(HideTutorial.gameObject).onClick += (go) =>
        {
            Vector3 targetBarPosition = new Vector3(0, -2, 0);
            RunMoveAnimation(context, targetBarPosition, 1, "OnTargetBarAniamtionComplete");
        };
        InputHandler.BlockInput = true;
    }

    public void Init(int tutorialIndex, Action action)
    {
        _action = action;
        tutorials[tutorialIndex].SetActive(true);
    }

    private void RunMoveAnimation(GameObject gameObject, Vector3 targetPosition, float time, string completeAction)
    {
        iTween.MoveTo(gameObject, iTween.Hash("position", targetPosition, "time", time,
                      "oncomplete", completeAction, "oncompletetarget", this.gameObject, "easetype", iTween.EaseType.easeInOutQuart));
    }

    private void OnTargetBarAniamtionComplete()
    {
        if (_action != null)
        {
            _action();
        }
        InputHandler.BlockInput = false;
        Close();
    }
}
