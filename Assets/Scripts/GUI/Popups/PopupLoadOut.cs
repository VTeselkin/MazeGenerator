﻿using Assets.Scripts.GUI.Popups;
using UnityEngine;
using Assets.Scripts.Server;
using Assets.Scripts.GUI;
using Assets.Scripts.GUI.Managers;
using Assets.Scripts.Navigation;
using System.Collections.Generic;


internal class PopupLoadOut : PopupBase
{
    [SerializeField] private UIButton btnPlay;
    [SerializeField] private UIButton btnMenu;
    [SerializeField] private UIButton btnLeaderBoard;
    [SerializeField] private UIButton btnFaceBook;
    [SerializeField] private UIGrid grid;
    [SerializeField] private UILabel titleText;
    [SerializeField] private UILabel description;
    [SerializeField] private List<UISprite> stars;

    private int currentLevelIndex = 0;

    protected override void InitUIHandlers()
    {
        base.InitUIHandlers();
        Commands.Add(btnPlay.name, new SimpleActionClickCommand(ButtonPlayClick));
        Commands.Add(btnMenu.name, new SimpleActionClickCommand(ButtonMenuClick));
        Commands.Add(btnFaceBook.name, new SimpleActionClickCommand(ButtonFaceBookClick));
        Commands.Add(btnLeaderBoard.name, new SimpleActionClickCommand(ButtonLeaderBoardClick));
    }

    protected override void FillUIElementsInfo()
    {
        base.FillUIElementsInfo();
        currentLevelIndex = UserStorage.Instance.CurrentSelectLevel;
        FaceBookManager.Instance.CompleteAutorization += CompleteAutorization;
        btnFaceBook.gameObject.SetActive(!FaceBookManager.Instance.IsLogin());
        btnLeaderBoard.gameObject.SetActive(FaceBookManager.Instance.IsLogin());
        grid.Reposition();
        for (int i = 0; i < LevelProgress.Instance.LevelsProgress[currentLevelIndex].Star; i++)
        {
            stars[i].spriteName = "ui_star_enable";
        }
        titleText.text = "Start Level " + (currentLevelIndex + 1) + "!";
        SetLevelDescription();
        AnalyticsWrapper.Instance.EventLoadOut(LevelStorage.Instance.Data[currentLevelIndex]);
    }

    private void ButtonPlayClick()
    {
        Close();
        AnalyticsWrapper.Instance.EventPressBtn(AnalyticsHelper.EventKey.PressBtnMenuPlay);
        NavigationService.Navigate(new GameNavigationArgs(currentLevelIndex));
    }

    private void ButtonLeaderBoardClick()
    {
        AnalyticsWrapper.Instance.EventPressBtn(AnalyticsHelper.EventKey.PressBtnCoinsLeaderBoard);
        UIManager.Instance.CreateNewUIElement<PopupLeaderBoard>(UIElementType.LeaderBoard, true).Init(PopupLeaderBoard.LeaderBoardType.Score);
    }

    private void ButtonFaceBookClick()
    {
        AnalyticsWrapper.Instance.EventPressBtn(AnalyticsHelper.EventKey.PressBtnMenuFb);
        FaceBookManager.Instance.Login();
    }

    private void ButtonMenuClick()
    {
        Close();
    }

    private void CompleteAutorization(bool isComplete)
    {
        btnFaceBook.gameObject.SetActive(!FaceBookManager.Instance.IsLogin());
        btnLeaderBoard.gameObject.SetActive(FaceBookManager.Instance.IsLogin());
    }
    private void OnDestroy()
    {
        FaceBookManager.Instance.CompleteAutorization -= CompleteAutorization;
    }

    private void SetLevelDescription()
    {
        var target = LevelStorage.Instance.Data[currentLevelIndex].LevelTarget;
        switch (target)
        {
            case Assets.Scripts.Helpers.LevelTargetEnum.Finish:
                description.text = "Find a way out of the maze!";
                break;
            case Assets.Scripts.Helpers.LevelTargetEnum.Collect:
                description.text = "Collect all the items in the maze!";
                break;
            case Assets.Scripts.Helpers.LevelTargetEnum.FinishAndCollect:
                description.text = "Collect all the items and find a way out!";
                break;
            default:
                description.text = "Find a way out of the maze!";
                break;
        }
    }
}
