﻿using UnityEngine;
using System.Collections;

public enum UITypeHint{
    Login,
    Invite,
    SentEnergy,
    LockedBoost
}
