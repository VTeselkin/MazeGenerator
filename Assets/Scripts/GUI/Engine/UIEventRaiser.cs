﻿#region Usings

using System;
using UnityEngine;

#endregion

public class UIEventRaiser : MonoBehaviour {
    private Vector3 _downPosition;
    private Vector3 _upPosition;

    private void OnClick(){
        this.OnClick(this.gameObject.name);
    }

    private void OnMouseDown()
    {
        this.OnClick(this.gameObject.name);
        _downPosition = Input.mousePosition;
    }

    private void OnMouseUp()
    {
        _upPosition = Input.mousePosition;
        var distance = Vector3.Distance(_downPosition, _upPosition);
        this.OnClickDown(this.gameObject.name, distance);

    }
    public event EventHandler<UIEventArgs> Click; 

    public event EventHandler<UIEventArgs> ClickDown;

    private void OnClick(string info){
        EventHandler<UIEventArgs> handler = this.Click;
        if (handler != null) {
            handler(this, new UIEventArgs(info));
        }
    }
    private void OnClickDown(string info, float distance)
    {
        EventHandler<UIEventArgs> handler = this.ClickDown;
        if (handler != null)
        {
            handler(this, new UIEventArgs(info, distance));
        }
    }
}

public class UIEventArgs : EventArgs {

    public UIEventArgs(string info){
        this.Info = info;
    }

    public UIEventArgs(string info, float distance) {
        this.Info = info;
        this.Distance = distance;
    }

    public String Info { get; private set; }
    public float Distance { get; set; }
}