﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UISplashBubbel : MonoBehaviour
{
    [SerializeField] private Animator _animator;
    [SerializeField] private Rigidbody _physics;
    [SerializeField] private UI2DSprite _sprite;
    [SerializeField] private UILabel _text;
    [SerializeField] private GameObject _context;
    [SerializeField] private BoxCollider _collider;
    [SerializeField] private List<Sprite> _sprites;


    private const float AnimationUpdateDelay = 0.01f;

    private const float AnimationBubbleDelay = 0.5f;
    private bool _isInitialized;
    private float ScoreFadeSpeed = 1f;
    private int _index = 0;
    private bool isCanMove = true;

    public void Init(int index, string text, float scoreFadeSpeed)
    {
        ScoreFadeSpeed = scoreFadeSpeed;
        _index = index % 3;
        _physics.drag = 1.5f;
        _sprite.name = _sprites[_index].name;
        _text.text = text;

        StartCoroutine(FadeIn());
    }

    void Update()
    {
        if (isCanMove)
        {
            //Vector3 direction = (gameObject.transform.position - transform.position).normalized;
            //_physics.MovePosition(transform.position + direction * movementSpeed * Time.deltaTime);
            if (_physics.drag > 0.5f)
            {
                _physics.drag -= Time.deltaTime;
            }
        }
    }


    private IEnumerator FadeIn()
    {
        while (Vector3.Distance(Vector3.one * 1.5f, _context.transform.localScale) > 0.1f)
        {
            _context.transform.localScale = Vector3.Lerp(_context.transform.localScale, Vector3.one * 1.5f, ScoreFadeSpeed * Time.deltaTime);
            yield return new WaitForSeconds(AnimationUpdateDelay);
        }

        _animator.SetTrigger("BubbleDestroy" + _index);
        yield return new WaitForSeconds(AnimationBubbleDelay);
        isCanMove = false;
        _sprite.enabled = false;
        _text.enabled = false;
        _physics.isKinematic = true;
        // Destroy(gameObject, 0.1f);

    }



}
