﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class UISplashSpawner : MonoBehaviour
{
    [SerializeField] private List<GameObject> _spawnList;
    [SerializeField] private GameObject _bubbleObject;
    private float speedOneLine = 1.2f;
    private float speedTwoLine = 1.4f;
    private float speedThreeLine = 1.6f;
    private List<ItemLetter> data;

    public void StartSpawn()
    {
        data = new List<ItemLetter>{
            {new ItemLetter(0,"W", speedOneLine)},
            {new ItemLetter(1,"o", speedOneLine)},
            {new ItemLetter(2,"r", speedOneLine)},
            {new ItemLetter(3,"l", speedOneLine)},
            {new ItemLetter(4,"d", speedOneLine)},
            {new ItemLetter(2,"O", speedTwoLine)},
            {new ItemLetter(3,"f", speedTwoLine)},
            {new ItemLetter(0,"M", speedThreeLine)},
            {new ItemLetter(1,"a", speedThreeLine)},
            {new ItemLetter(2,"z", speedThreeLine)},
            {new ItemLetter(3,"e", speedThreeLine)},

    };
        StartCoroutine(CreateBubble());
    }

    private IEnumerator CreateBubble()
    {
        int index = 0;
        foreach (var item in data)
        {
            var go = Instantiate(_bubbleObject, _spawnList[item.Index].transform);
            var bubbel = go.GetComponent<UISplashBubbel>();
            bubbel.Init(index, item.Letter, item.Speed);
            index++;
            yield return new WaitForSeconds(UnityEngine.Random.Range(0.1f, 0.3f));
        }

        yield return null;
    }
    private class ItemLetter
    {
        public int Index { get; private set; }
        public string Letter { get; private set; }
        public float Speed { get; private set; }

        public ItemLetter(int index, string letter, float speed)
        {
            Index = index;
            Letter = letter;
            Speed = speed;
        }
    }
}