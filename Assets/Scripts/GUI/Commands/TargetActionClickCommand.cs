﻿using System;
using UnityEngine;

namespace Assets.Scripts.GUI.Commands
{
    internal class TargetActionClickCommand : ICommandUI
    {
        private MonoBehaviour _sender;

        private readonly Action<object> _callback;

        public TargetActionClickCommand(MonoBehaviour targetSender, Action<object> callback)
        {
            _callback = callback;
            _sender = targetSender;
        }

        public bool CanExecute()
        {
            return this._callback != null && _sender.enabled;
        }

        public void Execute()
        {
            this._callback.Invoke(_sender);
        }
    }
}
