﻿

#region Usings

using System;
using System.Collections.Generic;
using Assets.Scripts.Extensions;
using UnityEngine;
using Assets.Scripts.Shared;

#endregion

namespace Assets.Scripts.GUI.RootElements
{
	public abstract class UIContainerElement : MonoBehaviour
	{
		protected Dictionary<String, ICommandUI> Commands = new Dictionary<string, ICommandUI>();

		public UIElementType Type { get; set; }

		public Int32 LastDepthValue { get; protected set; }

		#region  PRIVATE METHODS 

		private void Subscribe()
		{
			foreach (var child in this.gameObject.GetComponentsInChildren<Transform>(true))
			{
				if (child.GetComponent<BoxCollider>() != null &&
					child.GetComponent<UIButton>() != null)
				{
					child.gameObject.AddComponent<UIEventRaiser>();
				}
			}

			foreach (var child in this.gameObject.GetComponentsInChildren<UIEventRaiser>(true))
			{
				child.Click += this.EventWorker;
			}
		}

		#endregion

		#region  PROTECTED METHODS 

		/// <summary>
		/// Events the worker.
		/// </summary>
		/// <param name="sender">Sender.</param>
		/// <param name="e">E.</param>
		protected void EventWorker(object sender, UIEventArgs e)
		{
			try
			{
				if (this.Commands[e.Info].CanExecute())
				{
					this.Commands[e.Info].Execute();
				}
			}
			catch (KeyNotFoundException)
			{
#if UNITY_EDITOR
				DebugLogger.Log("There is no click handler in " + this.Type + " e.info is " + e.Info +
								 "No command was executed");
#endif
			}
		}

		protected virtual void CalculateLastDepthValue()
		{
			if (Type != UIElementType.LoadingBar)
			{

				var maxSpriteDepthValue = GetMaxDepth(this.gameObject.GetComponentsInChildren<UISprite>());
				var maxLabelDepthValue = GetMaxDepth(this.gameObject.GetComponentsInChildren<UILabel>());

				this.LastDepthValue = maxLabelDepthValue > maxSpriteDepthValue
					? maxLabelDepthValue
					: maxSpriteDepthValue;
			}
		}

		protected int GetMaxDepth(UIWidget[] widgets)
		{
			int maxDepth = 0;
			if (widgets.Length > 0)
			{
				maxDepth = widgets[0].depth;

				foreach (UIWidget widget in widgets)
				{
					if (widget.depth > maxDepth)
						maxDepth = widget.depth;
				}
			}
			return maxDepth;
		}

		protected virtual void PreInit() { }

		protected abstract void InitUIHandlers();

		protected abstract void FillUIElementsInfo();

		#endregion

		#region  UNITY REFLECTION CALLBACKS 

		protected virtual void Awake()
		{
			this.PreInit();
			this.InitUIHandlers();
		}

		protected virtual void Start()
		{
			this.Subscribe();
			this.CalculateLastDepthValue();
			this.FillUIElementsInfo();
		}

		protected virtual void Update()
		{
		}

		protected virtual void OnDestroy()
		{
			this.Commands = null;

			foreach (var child in this.gameObject.GetComponentsInChildren<UIEventRaiser>(true))
			{
				child.Click -= this.EventWorker;
			}
		}

		#endregion

		#region PUBLIC METHODS

		public virtual void EnableInput()
		{
#if UNITY_EDITOR
			DebugLogger.Log("Enabling input in " + this.Type);
#endif
			this.gameObject.SetColliderStateInAllChildren(true);
		}

		public virtual void DisableInput()
		{
#if UNITY_EDITOR

			DebugLogger.Log("Disabling input in " + this.Type);
			this.gameObject.SetColliderStateInAllChildren(false);
#endif
		}

		public virtual void Show()
		{
			gameObject.SetActive(true);
		}

		public virtual void Hide()
		{
			gameObject.SetActive(false);
		}

		#endregion
	}

}