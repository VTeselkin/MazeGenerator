﻿using System;
using System.Collections.Generic;

namespace Assets.Scripts.GUI
{
    internal static class UiConstants
    {
        public static Dictionary<UIElementType, String> UiPrefabNames;

        static UiConstants()
        {
            UiPrefabNames = new Dictionary<UIElementType, string>()
            {
                {UIElementType.DarkAlpha, "Prefabs/Popups/DarkAlpha"},
                {UIElementType.Pause, "Prefabs/Popups/PopupPause"},
                {UIElementType.PopupWin, "Prefabs/Popups/PopupWin"},
                {UIElementType.PopupLose, "Prefabs/Popups/PopupLose"},
                {UIElementType.PopupStore, "Prefabs/Popups/PopupStore"},
                {UIElementType.LeaderBoard, "Prefabs/Popups/PopupLeaderBoard"},
                {UIElementType.Refill, "Prefabs/Popups/PopupRefill"},
                {UIElementType.LoadOut, "Prefabs/Popups/PopupLoadOut"},
                {UIElementType.PopupSync, "Prefabs/Popups/PopupSync"},
                {UIElementType.PopupTournament, "Prefabs/Popups/PopupTournament"},
                {UIElementType.InternetNotReachable, "Prefabs/Popups/PopupNoInternet"},
                {UIElementType.RateIt, "Prefabs/Popups/PopupRateIt"},
                {UIElementType.Tutorial, "Prefabs/Popups/PopupTutorial"}


            };
        }
    }
}