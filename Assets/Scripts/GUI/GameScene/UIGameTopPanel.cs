﻿using System.Collections;
using UnityEngine;

public class UIGameTopPanel : MonoBehaviour
{

    [SerializeField] private UILabel enegryLabel;
    [SerializeField] private UILabel enegryInfinityLabel;
    [SerializeField] private UILabel speedLabel;
    [SerializeField] private GameObject animationEnergy;
    [SerializeField] private GameObject animationSpeed;
    [SerializeField] private UISlider progressSlider;
    [SerializeField] private UILabel tagetCount;
    [SerializeField] private UILabel distanceCount;
    private bool isNeedAnimationEnergy;
    private bool isNeedAnimationSpeed;
    private LevelData _levelData;
    private void Start()
    {
        StartCoroutine(SubscrideToPlayer());

    }

    private IEnumerator SubscrideToPlayer()
    {
        yield return new WaitUntil(() => GameManager<BaseGameObject>.Instance != null &&
                                   GameManager<BaseGameObject>.Instance.Player != null &&
                                   GameManager<BaseGameObject>.Instance.LevelData != null);
        _levelData = GameManager<BaseGameObject>.Instance.LevelData;
        SetUIPlayerState(GameManager<BaseGameObject>.Instance.Player);
        SetProgressBar();
        GameManager<BaseGameObject>.Instance.Player.Changed += (sender, e) =>
         {
             SetUIPlayerState((BaseGameObject)sender);
         };
        ObjectSuperVisor.Instance.Changed += (sender, e) =>
        {
            SetProgressBar();
        };
    }

    private void SetUIPlayerState(BaseGameObject sender)
    {
        if (sender.Energy < 10 && !isNeedAnimationEnergy)
        {
            isNeedAnimationEnergy = true;
            StartCoroutine(AnimateEnergy());
        }
        else if (sender.Energy >= 10)
        {
            isNeedAnimationEnergy = false;
        }
        if (sender.Speed < GameStorage.Instance.Data.DefaultSpeed && !isNeedAnimationSpeed)
        {
            isNeedAnimationSpeed = true;
            StartCoroutine(AnimateSpeed());
        }
        else if (sender.Speed >= 2f)
        {
            isNeedAnimationSpeed = false;
        }
        if (_levelData.LevelNumber <= GameStorage.Instance.Data.MaxLevelForInfinityEnergy)
        {
            enegryInfinityLabel.enabled = true;
            enegryLabel.enabled = false;
        }
        else
        {
            enegryInfinityLabel.enabled = false;
            enegryLabel.enabled = true;
            enegryLabel.text = sender.Energy.ToString();
        }
        speedLabel.text = sender.Speed.ToString();
        distanceCount.text = (5 + UserStorage.Instance.DistanceBoost).ToString() + "X";
    }

    private float m_animTouchTimeStart = 0.2f;
    private float m_animTouchTimeFinish = 0.5f;

    private IEnumerator AnimateEnergy()
    {
        var currentScale = animationEnergy.transform.localScale;
        var newScale = animationEnergy.transform.localScale + animationEnergy.transform.localScale * 0.15f;
        while (isNeedAnimationEnergy)
        {

            float timeEnd = Time.time + m_animTouchTimeStart;
            while (timeEnd > Time.time)
            {
                animationEnergy.transform.localScale = Vector3.Lerp(currentScale, newScale, 1 - ((timeEnd - Time.time) / m_animTouchTimeStart));
                yield return null;
            }

            timeEnd = Time.time + m_animTouchTimeFinish;
            while (timeEnd > Time.time)
            {
                animationEnergy.transform.localScale = Vector3.Lerp(newScale, currentScale, 1 - ((timeEnd - Time.time) / m_animTouchTimeFinish));
                yield return null;
            }

            animationEnergy.transform.localScale = currentScale;
            yield return null;
        }
    }

    private IEnumerator AnimateSpeed()
    {
        var currentScale = animationSpeed.transform.localScale;
        var newScale = animationEnergy.transform.localScale + animationSpeed.transform.localScale * 0.15f;
        while (isNeedAnimationSpeed)
        {

            float timeEnd = Time.time + m_animTouchTimeStart;
            while (timeEnd > Time.time)
            {
                animationSpeed.transform.localScale = Vector3.Lerp(currentScale, newScale, 1 - ((timeEnd - Time.time) / m_animTouchTimeStart));
                yield return null;
            }

            timeEnd = Time.time + m_animTouchTimeFinish;
            while (timeEnd > Time.time)
            {
                animationSpeed.transform.localScale = Vector3.Lerp(newScale, currentScale, 1 - ((timeEnd - Time.time) / m_animTouchTimeFinish));
                yield return null;
            }

            animationSpeed.transform.localScale = currentScale;
            yield return null;
        }
    }

    private void SetProgressBar()
    {
        var levelData = GameManager<BaseGameObject>.Instance.LevelData;
        if (levelData == null) return;

        if (levelData.LevelTarget == Assets.Scripts.Helpers.LevelTargetEnum.Collect ||
           levelData.LevelTarget == Assets.Scripts.Helpers.LevelTargetEnum.FinishAndCollect
           )
        {
            progressSlider.gameObject.SetActive(true);
        }
        else
        {
            progressSlider.gameObject.SetActive(false);
            return;
        }

        var allTarger = levelData.Foods.Count;
        var needToCollect = ObjectSuperVisor.Instance.ObjectStorage.GetListOfObjectsByType(Assets.Scripts.Helpers.ObjectTypeEnum.Food).Count;
        var inProgress = allTarger - needToCollect;
        tagetCount.text = needToCollect.ToString();
        progressSlider.value = inProgress / (float)allTarger;
    }
}
