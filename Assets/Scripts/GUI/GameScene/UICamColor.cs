﻿using UnityEngine;

public class UICamColor : MonoBehaviour
{
    private string[] camColor = { "8CB886FF", "488545FF", "478927FF", "A7A370FF", "09A4FFFF", "815D42FF", "A57345FF", "7C5523FF", "DFCDAEFF", "BB9A61FF", "7D765FFF", "500103FF", "D2CCB3FF", "0F5C00FF" };

    [SerializeField] private Camera camera;

    public void Init(LevelData levelData)
    {
        var index = levelData.ViewIndexFree - 1;
        if (index < camColor.Length && index >= 0)
        {
            camera.backgroundColor = ColorFromHex(camColor[index]);
        }
    }

    private Color ColorFromHex(string hexString)
    {
        Color color = new Color();
        ColorUtility.TryParseHtmlString("#" + hexString, out color);
        return color;
    }
}
