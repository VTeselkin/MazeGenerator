﻿using UnityEngine;

public class UIFreeTrexture : MonoBehaviour
{
	[SerializeField] private UITexture freeTexture;
	[SerializeField] private int maxCountTextures;
	// Use this for initialization
	void Start()
	{
		var currentLevel = UserStorage.Instance.CurrentSelectLevel;
		var index = currentLevel - (int)(currentLevel / maxCountTextures) * maxCountTextures;
		Texture texture = Resources.Load<Texture>("Textures/Game/Free/" + index);
		freeTexture.mainTexture = texture;
	}


}
