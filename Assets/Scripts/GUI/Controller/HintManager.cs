﻿using UnityEngine;
using Assets.Scripts.Helpers;
using System.Collections.Generic;
using Assets.Scripts.Shared;

public class HintManager : MonoBehaviour
{
    private GameObject arrow;
    private static List<Indicator> listIndicator;
    private int w;
    private int h;
    private int deltaHtop = Screen.height * 15 / 100;
    private int deltaHbottom = Screen.height * 10 / 100;
    private Camera cam;
    private bool _isAddFinishTarget = false;
    public void StartHintManager()
    {
        _isAddFinishTarget = false;
        w = Screen.width;
        h = Screen.height;
        cam = GameObject.Find("Main Camera").GetComponent<Camera>();
        listIndicator = new List<Indicator>();
        arrow = Resources.Load("Prefabs/UIGameObjects/ArrowItem", typeof(GameObject)) as GameObject;
        if (arrow == null) return;
        ChangeStateGame();
        ObjectSuperVisor.Instance.UpdateChanged += (object sender, System.EventArgs e) =>
        {
            foreach (var obj in listIndicator)
            {
                SetIndicatorPosition(obj);
            }
        };

        ObjectSuperVisor.Instance.Changed += (object sender, BaseGameObjectEventArgs e) =>
        {
            if (e.Info == EventMessages.EventRemoveGameObject)
            {
                var foods = ObjectSuperVisor.Instance.ObjectStorage.GetListOfObjectsByType(ObjectTypeEnum.Food);
                if (foods.Count == 0)
                {
                    if (!_isAddFinishTarget)
                    {
                        _isAddFinishTarget = true;
                        FindTarget(ObjectTypeEnum.Finish);
                    }
                }
            }
        };
    }

    private void ChangeStateGame()
    {

        switch (GameManager<BaseGameObject>.Instance.LevelData.LevelTarget)
        {
            case LevelTargetEnum.Finish:
                FindTarget(ObjectTypeEnum.Finish);
                deltaHtop = Screen.height * 10 / 100;
                break;
            case LevelTargetEnum.Collect:
                FindTarget(ObjectTypeEnum.Food);
                break;
            case LevelTargetEnum.FinishAndCollect:
                FindTarget(ObjectTypeEnum.Food);
                break;
        }

    }
    private void FindTarget(ObjectTypeEnum type)
    {
        var goList = ObjectSuperVisor.Instance.ObjectStorage.GetListOfObjectsByType(type);
        foreach (var go in goList)
        {
            var gArrowGO = Instantiate(arrow);
            gArrowGO.transform.parent = Camera.main.transform;
            gArrowGO.transform.localScale = Vector3.one;
            listIndicator.Add(new Indicator(go.gameObject, gArrowGO, go, type));
        }

    }

    private void SetIndicatorPosition(Indicator obj)
    {

        var target = obj.Target;
        var indicator = obj.PointToTarget;
        if (target == null)
        {
            indicator.SetActive(false);
            return;
        }

        if (!obj.BaseGameObject.IsDead && obj.BaseGameObject.ObjectType == ObjectTypeEnum.Finish && !indicator.activeInHierarchy)
        {
            indicator.SetActive(true);
        }


        Vector3 targetPosOnScreen = cam.WorldToScreenPoint(target.transform.position);
        if (onScreen(targetPosOnScreen))
        {
            indicator.SetActive(false);
            return;
        }

        indicator.SetActive(true);
        Vector3 center = new Vector3(w / 2f, h / 2f, 0);
        float angle = Mathf.Atan2(targetPosOnScreen.y - center.y, targetPosOnScreen.x - center.x) * Mathf.Rad2Deg;

        float angleTop = Mathf.Atan2(h - center.y - deltaHtop, w - center.x) * Mathf.Rad2Deg;
        float angleBottom = Mathf.Atan2(h - center.y - deltaHbottom, w - center.x) * Mathf.Rad2Deg;
        if (angle < 0) angle = angle + 360;
        int edgeLine;
        if (angle < angleTop) edgeLine = 0;
        else if (angle < 180 - angleTop) edgeLine = 1;
        else if (angle < 180 + angleBottom) edgeLine = 2;
        else if (angle < 360 - angleBottom) edgeLine = 3;
        else edgeLine = 0;
        indicator.transform.position = Camera.main.ScreenToWorldPoint(Intersect(edgeLine, center, targetPosOnScreen));
        indicator.transform.eulerAngles = new Vector3(0, 0, angle);
    }

    Vector3 Intersect(int edgeLine, Vector3 line2point1, Vector3 line2point2)
    {
        float[] A1 = { -h, 0, h, 0 };
        float[] B1 = { 0, -w, 0, w };
        float[] C1 = { -w * h, -w * (h - deltaHtop), 0, w * deltaHbottom };

        float A2 = line2point2.y - line2point1.y;
        float B2 = line2point1.x - line2point2.x;

        float C2 = A2 * line2point1.x + B2 * line2point1.y;

        float det = A1[edgeLine] * B2 - A2 * B1[edgeLine];
        var x = (B2 * C1[edgeLine] - B1[edgeLine] * C2) / det;
        var y = (A1[edgeLine] * C2 - A2 * C1[edgeLine]) / det;
        return new Vector3(x, y, 0);

    }

    bool onScreen(Vector2 input)
    {
        return !(input.x > w || input.x < 0 || input.y > h || input.y < 0);
    }

    public class Indicator
    {
        public GameObject Target { get; private set; }
        public GameObject PointToTarget { get; private set; }
        public BaseGameObject BaseGameObject { get; private set; }

        public Indicator(GameObject target, GameObject pointToTarget, BaseGameObject baseGameObject, ObjectTypeEnum type)
        {
            Target = target;
            PointToTarget = pointToTarget;
            BaseGameObject = baseGameObject;
            var texture = pointToTarget.GetComponentInChildren<UITexture>();
            if (texture != null)
            {
                texture.color = Helper.GetHintColor(type);
            }

        }
    }

    #region Singleton members

    private static readonly object _lockObject = new object();

    private static HintManager _instance;

    public static HintManager Instance
    {
        get
        {
            if (_instance == null)
            {
                lock (_lockObject)
                {
                    if (_instance == null)
                    {
                        var gameObject = new GameObject("Singleton: " + (typeof(HintManager)));
                        _instance = gameObject.AddComponent<HintManager>();
                    }
                }
            }

            return _instance;
        }
    }

    private void Awake()
    {
        if (_instance)
        {
            DestroyImmediate(gameObject);
        }
        else
        {
            _instance = this.GetComponent<HintManager>();
            DontDestroyOnLoad(gameObject);
        }
    }
    #endregion
}
