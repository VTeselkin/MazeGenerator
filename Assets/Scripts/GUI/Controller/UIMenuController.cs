﻿using UnityEngine;
using Assets.Scripts.GUI.RootElements;
using Assets.Scripts.GUI.Managers;
using Assets.Scripts.GUI;
using Assets.Scripts.Server;
using Assets.Scripts.Helpers;
using System.Collections;
using Assets.Scripts.Shared;

public class UIMenuController : UIContainerElement
{
    [SerializeField] private UIButton playButton;
    [SerializeField] private UIButton fbButton;
    [SerializeField] private UIButton storeButton;
    [SerializeField] private UIButton coinsBoardButton;
    [SerializeField] private UIButton starsBoardButton;
    [SerializeField] private UIButton settingsButton;
    [SerializeField] private UIButton storeTopButton;
    [SerializeField] private UILabel userCoins;
    [SerializeField] private UILabel userStars;
    [SerializeField] private UIButton btnChangeLevelDec;
    [SerializeField] private UIButton btnChangeLevelInc;
    [SerializeField] private UIButton btnMusic;
    [SerializeField] private UIButton btnSound;
    [SerializeField] private UIButton fbLogoutButton;
    [SerializeField] private UIButton moreGamesButton;
    [SerializeField] private Transform spawnSnail;
    private UISettingsAnimation settingsAnimation;
    private bool isOpenSettings = false;

    protected override void FillUIElementsInfo()
    {
        Commands.Add(playButton.name, new SimpleActionClickCommand(PlayButtonClick));
        Commands.Add(fbButton.name, new SimpleActionClickCommand(FaceBookButtonClick));
        Commands.Add(storeButton.name, new SimpleActionClickCommand(StoreButtonClick));
        Commands.Add(coinsBoardButton.name, new SimpleActionClickCommand(LeaderCoinsBoardClick));
        Commands.Add(starsBoardButton.name, new SimpleActionClickCommand(LeaderStarsBoardClick));
        Commands.Add(storeTopButton.name, new SimpleActionClickCommand(StoreTopButtonClick));
        Commands.Add(btnChangeLevelDec.name, new SimpleActionClickCommand(ChangeLevelDecClick));
        Commands.Add(btnChangeLevelInc.name, new SimpleActionClickCommand(ChangeLevelInClick));
        Commands.Add(settingsButton.name, new SimpleActionClickCommand(SettingsBoardClick));
        Commands.Add(btnMusic.name, new SimpleActionClickCommand(SettingsMusicClick));
        Commands.Add(btnSound.name, new SimpleActionClickCommand(SettingsSoundClick));
        Commands.Add(fbLogoutButton.name, new SimpleActionClickCommand(SettingsLogoutClick));
        Commands.Add(moreGamesButton.name, new SimpleActionClickCommand(ShowMoreGamesClick));
        SetUserProgress(true);
        SetButtonState(FaceBookManager.Instance.IsLogin());
        SetViewArrow();
        StartCoroutine(MoveSlimeDown(Random.Range(30, 60)));
    }

    private void ShowMoreGamesClick()
    {
        if (!UIManager.Instance.IsOpenedPopups)
        {
            if (InternetReachabilityController.Instance.ShowPopupIfNeeded() == null)
            {
                UIManager.Instance.CreateNewUIElement<PopupTournament>(UIElementType.PopupTournament);
            }
            HideSettingsMenu();
        }
    }

    protected override void InitUIHandlers()
    {
        settingsAnimation = settingsButton.GetComponent<UISettingsAnimation>();
        FaceBookManager.Instance.CompleteAutorization += SetButtonState;
        UserStorage.Instance.CompleteLoad += SetUserProgress;
        UserStorage.Instance.ChangeUserData += ChangeUserData;
        PurchaseManager.Instance.Purchase += OnPurchaseComlete;
        UIManager.Instance.PopupOpened += Instance_PopupOpened;
        UIManager.Instance.PopupClosed += Instance_PopupOpened;
    }

    void Instance_PopupOpened(UIElementType obj)
    {
        if (!UIManager.Instance.IsOpenedPopups)
        {
            EnableInput();
        }
        else
        {
            DisableInput();
        }
    }


    private void ChangeLevelInClick()
    {
        AudioManager.Instance.PlaySoundUi(SoundConstants.UIAudioClipTypes.Button_R);
        UserStorage.Instance.ChangeCurrentLevelInc();
        SetViewArrow();
        btnChangeLevelDec.GetComponent<Collider>().enabled = true;
        btnChangeLevelDec.state = UIButtonColor.State.Normal;
        HideSettingsMenu();

    }

    private void ChangeLevelDecClick()
    {
        AudioManager.Instance.PlaySoundUi(SoundConstants.UIAudioClipTypes.Button_L);
        UserStorage.Instance.ChangeCurrentLevelDec();
        SetViewArrow();
        btnChangeLevelInc.GetComponent<Collider>().enabled = true;
        btnChangeLevelInc.state = UIButtonColor.State.Normal;
        HideSettingsMenu();
    }

    private void SetViewArrow()
    {
        if (UserStorage.Instance.CurrentSelectLevel == UserStorage.Instance.MaxOpenedLevel)
        {
            btnChangeLevelInc.GetComponent<Collider>().enabled = false;
            btnChangeLevelInc.state = UIButtonColor.State.Disabled;
        }
        if (UserStorage.Instance.CurrentSelectLevel == 0)
        {
            btnChangeLevelDec.GetComponent<Collider>().enabled = false;
            btnChangeLevelDec.state = UIButtonColor.State.Disabled;
        }
    }
    private void ChangeUserData(TypeBoostStore obj)
    {
        SetUserProgress(true);
    }

    private void OnPurchaseComlete(bool res, PurchaseManager.PurchaseType type)
    {
        SetUserProgress(res);
    }

    private void SetUserProgress(bool res)
    {
        if (res)
        {
            userCoins.text = UserStorage.Instance.Coins.ToString();
            userStars.text = UserStorage.Instance.Stars.ToString();
        }
    }
    private void SettingsBoardClick()
    {
        if (!UIManager.Instance.IsOpenedPopups)
        {
            AudioManager.Instance.PlaySoundUi(SoundConstants.UIAudioClipTypes.Pop_up_Settings);
            AnalyticsWrapper.Instance.EventPressBtn(AnalyticsHelper.EventKey.PressBtnMenuSettings);
            ShowSettingsMenu();
        }
    }
    private void ShowSettingsMenu()
    {
        isOpenSettings = !isOpenSettings;
        settingsAnimation.StartAnimation(isOpenSettings);
    }
    private void HideSettingsMenu()
    {
        if (isOpenSettings)
        {
            isOpenSettings = !isOpenSettings;
            settingsAnimation.StartAnimation(isOpenSettings);
        }
    }
    private void LeaderCoinsBoardClick()
    {
        if (!UIManager.Instance.IsOpenedPopups)
        {
            if (InternetReachabilityController.Instance.ShowPopupIfNeeded() == null)
            {
                AudioManager.Instance.PlaySoundUi(SoundConstants.UIAudioClipTypes.Button_claim);
                AnalyticsWrapper.Instance.EventPressBtn(AnalyticsHelper.EventKey.PressBtnCoinsLeaderBoard);
                UIManager.Instance.CreateNewUIElement<PopupLeaderBoard>(UIElementType.LeaderBoard).Init(PopupLeaderBoard.LeaderBoardType.Score);
            }
            HideSettingsMenu();
        }
    }


    private void LeaderStarsBoardClick()
    {
        if (!UIManager.Instance.IsOpenedPopups)
        {
            if (InternetReachabilityController.Instance.ShowPopupIfNeeded() == null)
            {
                AudioManager.Instance.PlaySoundUi(SoundConstants.UIAudioClipTypes.Button_claim);
                AnalyticsWrapper.Instance.EventPressBtn(AnalyticsHelper.EventKey.PressBtnStarsLeaderBoard);
                UIManager.Instance.CreateNewUIElement<PopupLeaderBoard>(UIElementType.LeaderBoard).Init(PopupLeaderBoard.LeaderBoardType.Stars);
            }
            HideSettingsMenu();
        }
    }
    private void StoreButtonClick()
    {
        if (!UIManager.Instance.IsOpenedPopups)
        {
            AudioManager.Instance.PlaySoundUi(SoundConstants.UIAudioClipTypes.Button_Deals);
            AnalyticsWrapper.Instance.EventPressBtn(AnalyticsHelper.EventKey.PressBtnMenuStore);
            UIManager.Instance.CreateNewUIElement<PopupStore>(UIElementType.PopupStore).Init(true);
            HideSettingsMenu();
        }
    }
    private void StoreTopButtonClick()
    {
        if (!UIManager.Instance.IsOpenedPopups)
        {
            AudioManager.Instance.PlaySoundUi(SoundConstants.UIAudioClipTypes.Button_Coins);
            AnalyticsWrapper.Instance.EventPressBtn(AnalyticsHelper.EventKey.PressBtnMenuStoreTop);
            UIManager.Instance.CreateNewUIElement<PopupStore>(UIElementType.PopupStore).Init(false);
            HideSettingsMenu();
        }
    }
    private void FaceBookButtonClick()
    {
        if (!UIManager.Instance.IsOpenedPopups)
        {
            if (InternetReachabilityController.Instance.ShowPopupIfNeeded() == null)
            {
                AudioManager.Instance.PlaySoundUi(SoundConstants.UIAudioClipTypes.Button_FB);
                AnalyticsWrapper.Instance.EventPressBtn(AnalyticsHelper.EventKey.PressBtnMenuFb);
                FaceBookManager.Instance.Login();
            }
            HideSettingsMenu();
        }
    }

    private void PlayButtonClick()
    {
        if (!UIManager.Instance.IsOpenedPopups)
        {
            AudioManager.Instance.PlaySoundUi(SoundConstants.UIAudioClipTypes.Button_Play);
            AnalyticsWrapper.Instance.EventPressBtn(AnalyticsHelper.EventKey.PressBtnMenuPlay);
            UIManager.Instance.CreateNewUIElement<PopupLoadOut>(UIElementType.LoadOut);
        }
    }



    private void SetButtonState(bool isLogin)
    {
        HideSettingsMenu();
        starsBoardButton.gameObject.SetActive(isLogin);
        fbButton.gameObject.SetActive(!isLogin);
    }


    protected override void OnDestroy()
    {
        base.OnDestroy();
        FaceBookManager.Instance.CompleteAutorization -= SetButtonState;
        UserStorage.Instance.CompleteLoad -= SetUserProgress;
        UserStorage.Instance.ChangeUserData -= ChangeUserData;
        PurchaseManager.Instance.Purchase -= OnPurchaseComlete;
        UIManager.Instance.PopupOpened -= Instance_PopupOpened;
        UIManager.Instance.PopupClosed -= Instance_PopupOpened;
    }

    private void SettingsMusicClick()
    {
        if (!UIManager.Instance.IsOpenedPopups)
        {
            AppSettings.Instance.IsMusicOn = !AppSettings.Instance.IsMusicOn;
            AudioManager.Instance.PlaySoundUi(SoundConstants.UIAudioClipTypes.ButtonClick);
        }
    }

    private void SettingsSoundClick()
    {
        if (!UIManager.Instance.IsOpenedPopups)
        {
            AppSettings.Instance.IsSoundsOn = !AppSettings.Instance.IsSoundsOn;
            AudioManager.Instance.PlaySoundUi(SoundConstants.UIAudioClipTypes.ButtonClick);
        }
    }

    private void SettingsLogoutClick()
    {
        if (!UIManager.Instance.IsOpenedPopups)
        {
            AudioManager.Instance.PlaySoundUi(SoundConstants.UIAudioClipTypes.ButtonClick);
            FaceBookManager.Instance.Logout();
        }
    }
    #region Animation Menu

    IEnumerator MoveSlimeDown(float delay)
    {
        var snail = Resources.Load("Prefabs/UIGameObjects/TutourSnailGreen", typeof(GameObject)) as GameObject;
        var go = Instantiate(snail, spawnSnail);
        var enemySnailGreen = go.GetComponent<EnemySnailGreen>();
        enemySnailGreen.Initialize(null);
        go.transform.position = spawnSnail.transform.position;
        var target = go.transform.position + new Vector3(Screen.width, 0, 0);
        var journeyLength = Screen.width;
        float distCovered = Time.deltaTime * 0.08f;
        float fracJourney = distCovered / journeyLength;
        float time = 0;
        while (time < 30f && !enemySnailGreen.IsDead)
        {
            go.transform.position = Vector3.Lerp(go.transform.position, target, fracJourney);
            time += Time.deltaTime;
            yield return new WaitForSeconds(0.01f);
            enemySnailGreen.SetAnimationMove(SideDirectional.Right);
        }
        if (!enemySnailGreen.IsDead)
        {
            DestroyObject(go);
        }
        go = null;
        yield return new WaitForSeconds(delay);

        StartCoroutine(MoveSlimeDown(Random.Range(30, 60)));
    }


    #endregion
}
