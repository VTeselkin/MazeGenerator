﻿using System;
using System.Collections;
using Assets.Scripts.GUI;
using Assets.Scripts.GUI.Managers;
using Assets.Scripts.GUI.RootElements;
using UnityEngine;

public class UIGameController : UIContainerElement
{
    [SerializeField] private UIButton energyButton;
    [SerializeField] private UIButton speedButton;
    [SerializeField] private UIButton noAdsButton;
    [SerializeField] private UIAnchor bottomAnchor;
    [SerializeField] private UIButton pauseButton;
    [SerializeField] private GameObject adsPanel;
    private Vector3 currentScale;
    private Vector3 newScale;

    protected override void FillUIElementsInfo()
    {
        Commands.Add(energyButton.name, new SimpleActionClickCommand(EnergyButtonClick));
        Commands.Add(speedButton.name, new SimpleActionClickCommand(SpeedButtonClick));
        Commands.Add(pauseButton.name, new SimpleActionClickCommand(PauseButtonClick));
        Commands.Add(noAdsButton.name, new SimpleActionClickCommand(NoAdsButtonClick));
        SetViewScene();
    }
    private void SetViewScene()
    {
        if (!GameStorage.Instance.Data.IsBannerEnable || AppSettings.Instance.NoBanner)
        {
            bottomAnchor.relativeOffset = new Vector2(bottomAnchor.relativeOffset.x, -0.06f);
            bottomAnchor.enabled = true;
            adsPanel.SetActive(false);
        }
    }
    private void NoAdsButtonClick()
    {
        if (!UIManager.Instance.IsOpenedPopups)
        {
            AudioManager.Instance.PlaySoundUi(SoundConstants.UIAudioClipTypes.Button_Buy);
            AnalyticsWrapper.Instance.EventPressBtn(AnalyticsHelper.EventKey.PressBtnGameNoAds);
            UIManager.Instance.CreateNewUIElement<PopupRefill>(UIElementType.Refill).Init(PopupRefill.RefillType.Ads);
        }
    }

    private void EnergyButtonClick()
    {
        if (!UIManager.Instance.IsOpenedPopups)
        {
            AudioManager.Instance.PlaySoundUi(SoundConstants.UIAudioClipTypes.Pop_up_Store);
            AnalyticsWrapper.Instance.EventPressBtn(AnalyticsHelper.EventKey.PressBtnGameEnergy);
            UIManager.Instance.CreateNewUIElement<PopupRefill>(UIElementType.Refill).Init(PopupRefill.RefillType.Energy);
        }
    }
    private void SpeedButtonClick()
    {
        if (!UIManager.Instance.IsOpenedPopups)
        {
            AudioManager.Instance.PlaySoundUi(SoundConstants.UIAudioClipTypes.Pop_up_Store);
            AnalyticsWrapper.Instance.EventPressBtn(AnalyticsHelper.EventKey.PressBtnGameSpeed);
            UIManager.Instance.CreateNewUIElement<PopupRefill>(UIElementType.Refill).Init(PopupRefill.RefillType.Speed);
        }
    }

    private void PauseButtonClick()
    {
        if (!UIManager.Instance.IsOpenedPopups)
        {
            AudioManager.Instance.PlaySoundUi(SoundConstants.UIAudioClipTypes.ButtonClick);
            AnalyticsWrapper.Instance.EventPressBtn(AnalyticsHelper.EventKey.PressBtnGamePause);
            UIManager.Instance.CreateNewUIElement<PopupPause>(UIElementType.Pause);
        }
    }
    protected override void InitUIHandlers()
    {
        AppSettings.Instance.NoADSChanged += ReInit;
        AppSettings.Instance.NoBannerChanged += ReInit;
    }

    private void ReInit()
    {
        SetViewScene();
    }

    private void OnDestroy()
    {
        AppSettings.Instance.NoADSChanged -= ReInit;
        AppSettings.Instance.NoBannerChanged -= ReInit;
    }
}
