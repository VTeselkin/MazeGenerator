﻿namespace Assets.Scripts.GUI
{
    public enum UIElementType
    {

        DarkAlpha,
        PopupSample,
        PopupWin,
        PopupLose,
        PopupStore,
        Tutorial,
        LoadingBar,
        Pause,
        InternetNotReachable,
        LeaderBoard,
        Refill,
        LoadOut,
        PopupSync,
        PopupTournament,
        RateIt,
        PopupTutorial
    }
}