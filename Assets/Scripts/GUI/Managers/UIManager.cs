﻿

#region Usings

using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.GUI.Popups;
using UnityEngine;
using Object = UnityEngine.Object;
using Assets.Scripts.Shared;
using Assets.Scripts.GUI.RootElements;

#endregion

namespace Assets.Scripts.GUI.Managers
{

    /// <summary>
    /// Main UI management class.
    /// </summary>
    internal class UIManager
    {
        #region ##### LOCAL CONSTANTS #####
        private const Int32 MinUiElementsDepthDiff = 2;

        #endregion

        #region ##### PRIVATE FIELDS #####

        private readonly List<UIContainerElement> _uiParentElements;

        // dark background  UI to hide lower depth UI elements.
        //private GameObject _darkAlphaObj;

        private readonly List<UIElementType> _uniqueElemetTypes;

        #endregion

        #region Events

        public bool IsOpenedPopups
        {
            get
            {
                if (_uiParentElements != null)
                {
                    foreach (var ui in _uiParentElements)
                    {
                        DebugLogger.Log(ui.name);
                    }
                    return _uiParentElements.Count > 0;
                }
                else
                {
                    return false;
                }
            }
        }

        public event Action<UIElementType> PopupOpened;

        protected virtual void OnPopupOpened(UIElementType obj)
        {
            Action<UIElementType> handler = PopupOpened;
            if (handler != null) handler(obj);
        }

        public event Action<UIElementType> PopupClosed;

        protected virtual void OnPopupClosed(UIElementType obj)
        {
            Action<UIElementType> handler = PopupClosed;
            if (handler != null) handler(obj);
        }

        #endregion

        #region ##### SINGLETON MEMBERS ####

        private static readonly UIManager instance = new UIManager();

        #region Constructors
        static UIManager()
        {
        }

        private UIManager()
        {
            this._uiParentElements = new List<UIContainerElement>();
            _uniqueElemetTypes = new List<UIElementType>();
            InitUniqueElementTypes();
        }
        #endregion

        public static UIManager Instance
        {
            get { return instance; }
        }

        #endregion

        #region ##### PRIVATE METHODS #####

        private void InitUniqueElementTypes()
        {
            _uniqueElemetTypes.Add(UIElementType.Pause);
        }

        private GameObject InstantiateUiElement(UIElementType type)
        {
            try
            {
                var obj = ResourceHelper.Load(UiConstants.UiPrefabNames[type]);
                var elementNewGameObject = Object.Instantiate(obj) as GameObject;
                return elementNewGameObject;

                //if (type == UIElementType.PopupLoadingAssetsBundle)
                //    return InstantiateFormResources(type);
                //else
                //    return InstantiateFormAssetBundle(type);

            }
            catch (KeyNotFoundException)
            {
#if UNITY_EDITOR
                DebugLogger.LogError("There is no prefab for " + type + " in dictionary");
#endif
                return null;
            }

        }

        //private GameObject InstantiateFormResources(UIElementType type) {
        //    string path = UiConstants.ElementsPrefabsPath + UiConstants.UiPrefabNames[type];

        //    var elementNewGameObject = Object.Instantiate(Resources.Load(path)) as GameObject;
        //    return elementNewGameObject;
        //}

        //private GameObject InstantiateFormAssetBundle(UIElementType type) {
        //    string pathFormat = "Assets/AdditionalAssets/{0}.prefab";
        //    string path = string.Format(pathFormat, UiConstants.ElementsPrefabsPath + UiConstants.UiPrefabNames[type]);

        //    var elementNewGameObject = Object.Instantiate(AssetBundleLoader.Instance.LoadFromBundle<GameObject>(path)) as GameObject;
        //    return elementNewGameObject;
        //}

        /// <summary>
        /// Normalizes new popup's child elements depth.
        /// </summary>
        /// <param name="prevElementMaxDepthValue">Prev layer depth value.</param>
        /// <param name="newElementGameObj">New created game object.</param>
        private static void NormalizeNewСreatedPopupDepth(Int32 prevElementMaxDepthValue, GameObject newElementGameObj)
        {

            IncreaseDepthInChildrenByType<UISprite>(newElementGameObj, prevElementMaxDepthValue);
            IncreaseDepthInChildrenByType<UILabel>(newElementGameObj, prevElementMaxDepthValue);

        }

        private static void IncreaseDepthInChildrenByType<T>(GameObject gameObj, Int32 prevElementMaxDepthValue)
            where T : UIWidget
        {
            foreach (var obj in gameObj.GetComponentsInChildren<T>())
            {
                obj.depth = prevElementMaxDepthValue + MinUiElementsDepthDiff + obj.depth;
            }
        }

        /// <summary>
        ///     Normalizes dark alpha UI element depth.
        /// </summary>
        /// <param name="depthTopLayerObj">Top layer depth.</param>
        /// <param name="depthPrevLayerObj">Bottom layer depth.</param>
        //private void NormalizeDarkAlphaObjectDepth(Int32 depthTopLayerObj,
        //                                           Int32 depthPrevLayerObj){

        //    this._darkAlphaObj.gameObject.GetComponentInChildren<UISprite>().depth
        //        = (depthTopLayerObj + depthPrevLayerObj)/
        //          2;
        //}

        #endregion

        #region ##### PUBLIC METHODS #####

        /// <summary>
        ///     Instantiales UI element by type.
        /// </summary>
        /// <typeparam name="T">Type of needed object to create.</typeparam>
        /// <param name="type">UI element type.</param>
        /// <param name="parent">Parent element if exists.</param>
        /// <returns>Created UI object.</returns>
        public T CreateNewUIElement<T>(UIElementType type, /*bool addDarkBackground = true,*/ bool hidePrevios = false)
            where T : UIContainerElement
        {

            if (_uniqueElemetTypes.Contains(type))
            {
                foreach (var element in _uiParentElements)
                {
                    if (element.Type == type)
                    {
                        return element as T;
                    }
                }
            }

            var elementNewGameObject = this.InstantiateUiElement(type);

            if (elementNewGameObject != null)
            {

                var scale = elementNewGameObject.transform.localScale;

                elementNewGameObject.transform.parent = Object.FindObjectOfType<UIRoot>().transform;

                elementNewGameObject.transform.localScale = scale;

                var elementNew = elementNewGameObject.GetComponent<UIContainerElement>();

                if (elementNew == null)
                    DebugLogger.LogError("elementNew = null");

                if (elementNew is PopupBase)
                {
                    if (hidePrevios)
                    {
                        HideLastPopup();
                    }

                    _uiParentElements.Add(elementNew);
                    //Instantiate dark alpha UI element if we need it.
                    //if (this._darkAlphaObj == null && addDarkBackground){
                    //    this._darkAlphaObj = this.InstantiateUiElement(UIElementType.DarkAlpha);
                    //    this._darkAlphaObj.transform.parent = Object.FindObjectOfType<UIRoot>().transform;
                    //}
                }
                elementNew.Type = type;
            }

            OnPopupOpened(type);
            AnalyticsWrapper.Instance.EventPopUp(type, hidePrevios);
            return this._uiParentElements.LastOrDefault() as T;
        }

        private void HideLastPopup()
        {
            UIContainerElement lastPopup = null;
            if (_uiParentElements.Count > 0)
            {
                for (var i = _uiParentElements.Count - 1; i >= 0; i--)
                {
                    if (_uiParentElements[i].Type != UIElementType.Tutorial)
                    {
                        lastPopup = _uiParentElements[i];
                        break;
                    }
                }
            }

            if (lastPopup != null)
            {
                lastPopup.Hide();
            }
        }

        private void ShowLastPopup()
        {
            var lastPopup = _uiParentElements.Count == 0
                ? null
                : _uiParentElements[_uiParentElements.Count - 1];

            if (_uiParentElements.Count > 0)
            {
                for (var i = _uiParentElements.Count - 1; i >= 0; i--)
                {
                    if (_uiParentElements[i].Type != UIElementType.Tutorial)
                    {
                        lastPopup = _uiParentElements[i];
                        break;
                    }
                }
            }

            if (lastPopup != null)
            {
                lastPopup.Show();
            }
        }

        /// <summary>
        /// Add new UI element.
        /// </summary>
        /// <typeparam name="T">Type of needed object to create.</typeparam>
        /// <param name="type">UI element type.</param>
        /// <param name="elementNewGameObject">Element for adding</param>
        /// <param name="parent">Parent element if exists.</param>
        /// <returns>Created UI object.</returns>
        public T AddNewUIElement<T>(UIElementType type, UIContainerElement elementNewGameObject, /*bool addDarkBackground = true,*/ UIContainerElement parent = null)
            where T : UIContainerElement
        {

            if (_uniqueElemetTypes.Contains(type))
            {
                foreach (var element in _uiParentElements)
                {
                    if (element.Type == type)
                    {
                        return element as T;
                    }
                }
            }

            if (elementNewGameObject != null)
            {

                var scale = elementNewGameObject.transform.localScale;

                elementNewGameObject.transform.parent = Object.FindObjectOfType<UIRoot>().transform;

                elementNewGameObject.transform.localScale = scale;

                var elementNew = elementNewGameObject.GetComponent<UIContainerElement>();

                if (elementNew is PopupBase && type != UIElementType.LoadingBar)
                {
                    _uiParentElements.Add(elementNew);
                    //Inttantiate dark alpha UI element if we need it.
                    //if (this._darkAlphaObj == null && addDarkBackground){
                    //    this._darkAlphaObj = this.InstantiateUiElement(UIElementType.DarkAlpha);
                    //    this._darkAlphaObj.transform.parent = Object.FindObjectOfType<UIRoot>().transform;
                    //}
                }
                elementNew.Type = type;
            }

            OnPopupOpened(type);

            return this._uiParentElements.LastOrDefault() as T;
        }

        /// <summary>
        ///     Gets needed element by generic type.
        /// </summary>
        /// <typeparam name="T">Needed UI element type.</typeparam>
        /// <returns>The UI element.</returns>
        public T GetUiElement<T>() where T : UIContainerElement
        {
            return this._uiParentElements.FirstOrDefault(f => f is T) as T;
        }

        public bool PopupShown<T>() where T : UIContainerElement
        {
            if ((GetUiElement<T>() as T) != null)
                return true;

            return false;
        }

        public PopupBase GetTopPopup()
        {
            if (_uiParentElements.Count == 0)
            {
                return null;
            }

            UIContainerElement topElement = _uiParentElements[0];
            UIPanel topElementPanel = topElement.GetComponent<UIPanel>();

            foreach (UIContainerElement element in _uiParentElements)
            {
                UIPanel elementPanel = element.GetComponent<UIPanel>();
                if (elementPanel.depth >= topElementPanel.depth)
                    topElement = element;
            }

            return topElement as PopupBase;
        }

        /// <summary>
        ///     Removes passed UI element from game.
        /// </summary>
        /// <param name="element">UI element to remove.</param>
        public void RemoveElement(UIContainerElement element)
        {

            var popup = element as PopupBase;

            if (popup == null)
            {
                return;
            }

            _uiParentElements.Remove(popup);
            NGUITools.DestroyImmediate(element.gameObject);
            ShowLastPopup();

            OnPopupClosed(element.Type);

            //// If dark exists
            //if (_uiParentElements.Count == 0 && this._darkAlphaObj != null){
            //    NGUITools.DestroyImmediate(this._darkAlphaObj);
            //    this._darkAlphaObj = null;
            //}
        }

        /// <summary>
        ///     Clears ui collection.
        /// </summary>
        public void ClearUIElementCollection()
        {
            this._uiParentElements.Clear();
        }

        public PopupBase GetLoadingPopup()
        {
            var loading = _uiParentElements.FirstOrDefault(f => f.Type == UIElementType.LoadingBar);
            if (loading != null)
            {
                return loading as PopupBase;
            }
            return null;
        }

        public bool HaveUIElements()
        {
            return (_uiParentElements.Count > 0);
        }

        public int ShownPopupAmount
        {
            get
            {
                return _uiParentElements.Count;
            }
        }

        #endregion
    }
}