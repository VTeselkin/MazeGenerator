﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.Scripts.Shared;
using UnityEngine;

namespace Assets.Scripts.GUI.Managers
{
	internal class PopupsAutoShowHelper : UnitySingleton<PopupsAutoShowHelper>
	{

		private List<UIElementType> _popups;

		private bool _isFullScreenAdsShown;

		protected override void LateAwake()
		{
			base.LateAwake();
			_popups = new List<UIElementType>();
		}

		public bool CanShowPopup()
		{
			if (_popups.Count != 0)
			{
				return false;
			}

			if (_isFullScreenAdsShown)
			{
				return false;
			}

			return true;
		}

		public void RegisterPopup(UIElementType popupType)
		{
			_popups.Add(popupType);
		}

		public void RegisterFullScreenAdsShow()
		{
			_isFullScreenAdsShown = true;
		}

		public void Clear()
		{
			_popups.Clear();
			_isFullScreenAdsShown = false;
		}
	}
}
