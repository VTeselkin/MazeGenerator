﻿using System.IO;
using UnityEditor;
using UnityEngine;

public static class BuildSettings
{

	[MenuItem("AutoBuilder/Delete Build Data")]
	private static void DeleteBuildData()
	{
		PlayerPrefs.DeleteAll();

		string cacheDirectory = Application.dataPath + "/Cache";
		if (Directory.Exists(cacheDirectory))
		{
			Directory.Delete(cacheDirectory, true);
		}

		Debug.Log("Build Data Deleted");
	}
}
