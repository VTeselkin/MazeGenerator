﻿using Assets.Scripts.Shared;
using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Assets.Editor {

    [InitializeOnLoad]
    public class ClearSingletones {

        private static bool playButtonWasPressed;
        static ClearSingletones() {
            playButtonWasPressed = false;
            EditorApplication.playmodeStateChanged += OnPlaymodeStateChanged;
        }

        static void OnPlaymodeStateChanged() {
            if (!playButtonWasPressed) {
                playButtonWasPressed = true;
                return;
            }

            if (playButtonWasPressed && 
                !EditorApplication.isPlaying && 
                !EditorApplication.isPaused &&
                !EditorApplication.isUpdating &&
                !EditorApplication.isCompiling) {
                    Clear();
                    playButtonWasPressed = false;
            }
        }

        static void Clear() {
            DeleteSingletones();
            if (SingletonesExist()) {
                DeleteSingletones();
            }

            DebugLogger.Log("Clear singletones");
        }

        static void DeleteSingletones() {
            GameObject[] objects = GameObject.FindObjectsOfType<GameObject>();
            for (int i = 0; i < objects.Length; i++) {
                if (objects[i].name.Contains("Singleton")) {
                    GameObject.DestroyImmediate(objects[i]);
                }
            }
        }

        static bool SingletonesExist() {
            GameObject[] objects = GameObject.FindObjectsOfType<GameObject>();
            for (int i = 0; i < objects.Length; i++) {
                if (objects[i].name.Contains("Singleton"))
                    return true;
            }

            return false;
        }
    }
}
