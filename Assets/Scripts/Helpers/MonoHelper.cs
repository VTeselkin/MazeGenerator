﻿using System.Collections;
using Assets.Scripts.Shared;
using UnityEngine;

namespace AssemblyCSharp.Scripts.MainManager
{
    public class MonoHelper : UnitySingleton<MonoHelper>
    {
        public void StartCourutine(IEnumerator coroutine)
        {
            StartCoroutine(coroutine);
        }

        /// <summary>
        /// Instance pattern helper function, finds or creates an instance within the scene
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public T FindOrCreate<T>() where T : Component
        {
            T result = (T)FindObjectOfType(typeof(T));
            if (result == null)
            {
                GameObject go = new GameObject();
                result = go.AddComponent<T>();
                result.name = typeof(T).ToString();
            }

            return result;
        }
        /// <summary>
        /// Finds the or create.
        /// </summary>
        /// <returns>The or create.</returns>
        /// <param name="name">Name.</param>
        /// <typeparam name="T">The 1st type parameter.</typeparam>
        public T FindOrCreate<T>(string nameObject) where T : Component
        {
            T result = (T)FindObjectOfType(typeof(T));
            if (result == null)
            {
                GameObject go = new GameObject();
                result = go.AddComponent<T>();
                go.name = nameObject;
            }

            return result;
        }
        /// <summary>
        /// Gets the or create component.
        /// </summary>
        /// <returns>The or create component.</returns>
        /// <typeparam name="T">The 1st type parameter.</typeparam>
        public T GetOrCreateComponent<T>() where T : Component
        {
            T ret = GetComponent<T>();
            if (ret == null)
            {
                ret = gameObject.AddComponent<T>();
            }
            return ret;
        }
    }
}



