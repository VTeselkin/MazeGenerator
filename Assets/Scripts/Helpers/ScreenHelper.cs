﻿using System;
using UnityEngine;

namespace Assets.Scripts.Shared
{
    class ScreenHelper
    {
        /// <summary>
        /// Standard screen width.
        /// </summary>
        public const float StandardWidth = 750f;

        /// <summary>
        /// Standard screen height.
        /// </summary>
        public const float StandardHeight = 1334f;

        /// <summary>
        /// Current screen width.
        /// </summary>
        public static float Width {get { return Screen.width; }}

        /// <summary>
        /// The current height.
        /// </summary>
        public static float Height { get { return Screen.height; } }

        /// <summary>
        /// The screen min scale.
        /// </summary>
        public static float MinScale { get; private set; }

        /// <summary>
        /// The screen max scale.
        /// </summary>
        public static float MaxScale { get; private set; }

        /// <summary>
        /// The horizontal scale value.
        /// </summary>
        public static float ScaleX { get; private set; }

        /// <summary>
        /// The vertical scale value.
        /// </summary>
        public static float ScaleY { get; private set; }

        /// <summary>
        /// The standard scale for platform.
        /// </summary>
        public static float StandardScale { get; private set; }

        public static void Initialize() {
            ScaleX = Screen.width / StandardWidth;
            ScaleY = Screen.height / StandardHeight;

            MinScale = Math.Min(ScaleX, ScaleY);
            MaxScale = Math.Max(ScaleX, ScaleY);

            StandardScale = (MinScale + MaxScale)/2;
        }

        public static void Initialize(int width, int height)
        {
            ScaleX = width / StandardWidth;
            ScaleY = height / StandardHeight;

            MinScale = Math.Min(ScaleX, ScaleY);
            MaxScale = Math.Max(ScaleX, ScaleY);

            StandardScale = (MinScale + MaxScale) / 2;
        }
    }
}
