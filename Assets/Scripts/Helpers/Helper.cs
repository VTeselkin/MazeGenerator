
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Assets.Scripts.Strategy.Interfaces;
using UnityEngine;

namespace Assets.Scripts.Helpers
{
    public enum ColorEnum
    {
        Yellow,
        Red,
        Blue,
        Green,
        Purple,
        Orange,
        Pink,
        Undefined
    }
    /// <summary>
    /// Mouse state enum.
    /// </summary>
    public enum MouseStateEnum
    {
        MouseLeftButtonPressed,
        Normal
    }

    public enum ObjectTypeEnum
    {
        Undefined = 0,
        Wall = -1,
        Free = 1,
        Player = 2,
        Finish = 3,
        Food = 4,
        EnemySimple = 5,
        EnemyZombie = 6,
        EnemySee = 7,
        EnemySeeZombie = 8,
        EnemySeeBat = 9,
        EnemySnailGreen = 10,
        ScoreText = 11

    };

    public enum ScoreTypeEnum
    {
        Damage,
        Score,
        Info
    }

    public enum TypeBoostStore { Undefined, EnergyBoost, SpeedBoost, DistanceBoost, Coins }
    public enum LevelTargetEnum { Finish, Collect, FinishAndCollect };
    public enum ObjectTypeFood { Undefined = 0, Speed = 1, Energy = 2 }
    public enum ObjectGroupEnum { Undefined, Walls, Enemies, Players, Targets, Used };
    public enum SideDirectional { Up, Down, Left, Right, Undefined };
    public enum ObjectUITypeEnum { LevelButton }

    public struct ResourceIdentifier
    {
        private ObjectTypeEnum _gameObjectType;
        private ColorEnum _color;

        public ObjectTypeEnum GameObjectType
        {
            get { return _gameObjectType; }
            set { _gameObjectType = value; }
        }
        public ColorEnum Color
        {
            get { return _color; }
            set { _color = value; }
        }
        public ResourceIdentifier(ObjectTypeEnum gameObjectType) : this()
        {
            _gameObjectType = gameObjectType;
        }
    }

    public struct ResourceIdentifierUI
    {
        private ObjectUITypeEnum _gameObjectType;


        public ObjectUITypeEnum GameObjectType
        {
            get { return _gameObjectType; }
            set { _gameObjectType = value; }
        }

        public ResourceIdentifierUI(ObjectUITypeEnum gameObjectType) : this()
        {
            _gameObjectType = gameObjectType;
        }
    }
    public struct Point
    {
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
            {
                return false;
            }
            return obj is Point && Equals((Point)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (X * 397) ^ Y;
            }
        }

        public bool Equals(Point other)
        {
            return X == other.X && Y == other.Y;
        }

        public static Point Default = new Point(0, 0);

        public int X, Y;

        public Point(int x, int y)
        {
            X = x;
            Y = y;
        }

        public static bool operator ==(Point obj1, Point obj2)
        {
            return obj1.X == obj2.X && obj1.Y == obj2.Y;
        }

        public static bool operator !=(Point obj1, Point obj2)
        {
            return !(obj1 == obj2);
        }

        public static Point operator +(Point obj1, Point obj2)
        {
            return new Point(obj1.X + obj2.X, obj1.Y + obj2.Y);
        }

        public static Point operator *(Point obj, int multiplier)
        {
            return new Point(obj.X * multiplier, obj.Y * multiplier);
        }

        public Point Clone()
        {
            return (Point)this.MemberwiseClone();
        }

    }

    public struct FloatPoint
    {
        public bool Equals(FloatPoint other)
        {
            return X.Equals(other.X) && Y.Equals(other.Y);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
            {
                return false;
            }
            return obj is FloatPoint && Equals((FloatPoint)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (X.GetHashCode() * 397) ^ Y.GetHashCode();
            }
        }

        public static FloatPoint Default = new FloatPoint(0f, 0f);

        public float X;
        public float Y;

        public FloatPoint(float x, float y)
        {
            X = x;
            Y = y;
        }

        public static bool operator ==(FloatPoint obj1, FloatPoint obj2)
        {
            return obj1.X == obj2.X && obj1.Y == obj2.Y;
        }

        public static bool operator !=(FloatPoint obj1, FloatPoint obj2)
        {
            return !(obj1 == obj2);
        }
    }

    public struct GridDimension
    {
        public readonly int DimX, DimY;

        public GridDimension(int dimX, int dimY)
        {
            DimX = dimX;
            DimY = dimY;
        }
    }
    public struct AreaDimension
    {
        public readonly int X, Y, DimX, DimY;

        public AreaDimension(int x, int y, int dimX, int dimY)
        {
            X = x;
            Y = y;
            DimX = dimX;
            DimY = dimY;
        }
    }
    public class EventArgsGeneric<T> : EventArgs
    {
        public string Info { get; set; }
        public T SomeObject { get; set; }
    }

    public class BaseGameObjectEventArgs : EventArgs
    {
        public string Info { get; set; }
        public BaseGameObject GemObjectSender { get; set; }
    }

    // todo Remove at refactoring
    public class EventArgsSimple : EventArgs
    {
        public string Info { get; set; }
    }

    /// <summary>
    /// Helper.
    /// </summary>
    public static class Helper
    {
        /// <summary>
        /// Adepts the position.
        /// </summary>
        /// <returns>The position.</returns>
        /// <param name="coordinates">Coordinates.</param>
        public static FloatPoint AdeptPosition(Point coordinates)
        {
            return new FloatPoint(coordinates.X, coordinates.Y);
        }

        /// <summary>
        /// Finds all.
        /// </summary>
        /// <returns>The all.</returns>
        /// <param name="source">Source.</param>
        /// <param name="validationRule">Validation rule.</param>
        /// <typeparam name="T">The 1st type parameter.</typeparam>
        public static List<T> FindAll<T>(List<T> source, Func<T, bool> validationRule)
        {
            var result = new List<T>();
            foreach (var sourceItem in source)
            {
                if (validationRule(sourceItem))
                {
                    result.Add(sourceItem);
                }
            }

            return result;
        }

        /// <summary>
        /// Gets the neighbors object.
        /// </summary>
        /// <returns>The neighbors object.</returns>
        /// <param name="baseGameObject">Base game object.</param>
        /// <param name="side">Side.</param>
        public static Point GetNeighborsObject(BaseGameObject baseGameObject, SideDirectional side)
        {
            switch (side)
            {
                case SideDirectional.Up:
                    return baseGameObject.CoordinatesUp;
                case SideDirectional.Down:
                    return baseGameObject.CoordinatesDown;
                case SideDirectional.Left:
                    return baseGameObject.CoordinatesLeft;
                case SideDirectional.Right:
                    return baseGameObject.CoordinatesRight;
            }
            return baseGameObject.Coordinates;
        }

        /// <summary>
        /// Checks the position by player.
        /// </summary>
        /// <returns><c>true</c>, if position by player was checked, <c>false</c> otherwise.</returns>
        /// <param name="position">Position.</param>
        public static bool CheckPositionByPlayer(Point position)
        {
            Point playerPosition = GameManager<BaseGameObject>.Instance.Player.Coordinates;
            return playerPosition == position;
        }
        /// <summary>
        /// Checks the position by enemy.
        /// </summary>
        /// <returns><c>true</c>, if position by enemy was checked, <c>false</c> otherwise.</returns>
        /// <param name="position">Position.</param>
        public static bool CheckPositionByMoveObjectWithoutPlayer(Point position)
        {
            List<BaseGameObject> moveObjects = ObjectsStorage<BaseGameObject>.Instance.GetListOfMovableObjects();
            Point playerPosition = GameManager<BaseGameObject>.Instance.Player.Coordinates;
            foreach (var obj in moveObjects)
            {

                if (obj.Coordinates == position && obj.Coordinates != playerPosition)
                {
                    return true;
                }
            }
            return false;
        }
        /// <summary>
        /// Checks the point inside grid.
        /// </summary>
        /// <returns><c>true</c>, if point inside grid was checked, <c>false</c> otherwise.</returns>
        /// <param name="point">Point.</param>
        /// <param name="gridDimension">Grid dimension.</param>
        public static bool CheckPointInsideGrid(Point point, GridDimension gridDimension)
        {
            return CheckPointInsideGrid(point, gridDimension.DimX, gridDimension.DimY);
        }

        /// <summary>
        /// Checks the point inside grid.
        /// </summary>
        /// <returns><c>true</c>, if point inside grid was checked, <c>false</c> otherwise.</returns>
        /// <param name="point">Point.</param>
        /// <param name="dimX">Dim x.</param>
        /// <param name="dimY">Dim y.</param>
        public static bool CheckPointInsideGrid(Point point, int dimX, int dimY)
        {
            // ==================
            if (point.X < 0 || point.X > dimX - 1) return false;
            // ==================
            if (point.Y < 0 || point.Y > dimY - 1) return false;
            // ==================
            return true;
        }

        /// <summary>
        /// Storages the file path.
        /// </summary>
        /// <returns>The file path.</returns>
        /// <param name="fileName">File name.</param>
        public static string StorageFilePath(string fileName)
        {
#if UNITY_EDITOR
            return AppSettings.StorageDataPath + "/Cache/GameData/" + fileName;
#else
			return AppSettings.StorageDataPath + "Cache/GameData/" + fileName;
#endif
        }

        /// <summary>
        /// Storages the local file path.
        /// </summary>
        /// <returns>The local file path.</returns>
        /// <param name="fileName">File name.</param>
        public static string StorageLocalFilePath(string fileName)
        {
#if UNITY_EDITOR
            return Path.Combine(Application.streamingAssetsPath, fileName);
#else
			return "jar:file://" + UnityEngine.Application.dataPath + "!/assets/" + fileName;
#endif
        }

        /// <summary>
        /// Gets the color of the hint.
        /// </summary>
        /// <returns>The hint color.</returns>
        /// <param name="type">Type.</param>
        public static Color GetHintColor(ObjectTypeEnum type)
        {
            switch (type)
            {
                case ObjectTypeEnum.Undefined:
                case ObjectTypeEnum.Free:
                    return Color.white;
                case ObjectTypeEnum.Wall:
                    return Color.gray;
                case ObjectTypeEnum.Player:
                    return Color.red;
                case ObjectTypeEnum.Finish:
                    return Color.green;
                case ObjectTypeEnum.Food:
                    return Color.yellow;
                case ObjectTypeEnum.EnemySimple:
                    return Color.blue;
                case ObjectTypeEnum.EnemyZombie:
                    return Color.magenta;
                case ObjectTypeEnum.EnemySee:
                    return Color.blue;
                case ObjectTypeEnum.EnemySeeZombie:
                    return Color.magenta;
            }
            return Color.white;
        }

        /// <summary>
        /// Gets the color of the score.
        /// </summary>
        /// <returns>The score color.</returns>
        /// <param name="type">Type.</param>
        public static Color GetScoreColor(ScoreTypeEnum type)
        {
            switch (type)
            {
                case ScoreTypeEnum.Damage:
                    return Color.red;
                case ScoreTypeEnum.Score:
                    return Color.white;
                case ScoreTypeEnum.Info:
                    return Color.white;
            }
            return Color.white;
        }

        /// <summary>
        /// Gets the score move position.
        /// </summary>
        /// <returns>The score move position.</returns>
        /// <param name="type">Type.</param>
        /// <param name="baseGameObject">Base game object.</param>
        public static Point GetScoreMovePosition(ScoreTypeEnum type, BaseGameObject baseGameObject)
        {
            switch (type)
            {
                case ScoreTypeEnum.Damage:
                    return baseGameObject.CoordinatesUp;
                case ScoreTypeEnum.Score:
                    return baseGameObject.CoordinatesUp;
                case ScoreTypeEnum.Info:
                    return baseGameObject.Coordinates;
            }
            return baseGameObject.Coordinates;
        }
    }

    /// <summary>
    /// Game build config.
    /// </summary>
    public static class GameBuildConfig
    {
        public static ScriptableObject GetBuildConfig()
        {
            ScriptableObject currentSettings = null;
#if UNITY_ANDROID
            currentSettings = Resources.Load<GPPlatformBuildSettings>("GPPlatformBuildSettings");
#else
            currentSettings = Resources.Load<IOSPlatformBuildSettings>("IOSPlatformBuildSettings");
#endif
            return currentSettings;
        }

        public static bool IsDevelopBuild
        {
            get
            {
#if UNITY_IOS
                IOSPlatformBuildSettings build = (IOSPlatformBuildSettings)GetBuildConfig();
                return build.Development;
#else
                return true;
                GPPlatformBuildSettings build = (GPPlatformBuildSettings)GetBuildConfig();
                return build.Development;
#endif
               
            }
        }
    }
}

