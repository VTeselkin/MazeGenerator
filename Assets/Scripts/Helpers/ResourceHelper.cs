﻿using UnityEngine;

namespace Assets.Scripts.Shared
{
    public class ResourceHelper
    {
        private const string AdditionalAssetsFolder = "Assets/AdditionalAssets/";

        public static Object Load(string path)
        {
            if (path != null)
            {
                if (!path.StartsWith(AdditionalAssetsFolder))
                {
                    return Resources.Load(path);
                }
                else
                {
                    //return AssetBundleLoader.Instance.LoadFromBundle<Object>(path);
                }
            }
            return null;
        }
    }
}