using UnityEngine;
using System.Collections;
using System;
using Assets.Scripts.GUI.Managers;
using Assets.Scripts.GUI.Popups;
using Assets.Scripts.GUI;

internal class InternetReachabilityController : MonoBehaviour
{
    private Action<NetworkReachability> InternetStatusChanged;
    private NetworkReachability _currentNetworkReachabilityStatus;
    private Coroutine _checkInternetStatusCoroutine = null;

    #region Singleton

    private static InternetReachabilityController _instance;

    public static InternetReachabilityController Instance
    {
        get
        {
            if (_instance == null)
            {
                GameObject go = new GameObject("InternetReachabilityController");
                DontDestroyOnLoad(go);
                _instance = go.AddComponent<InternetReachabilityController>();

            }
            return _instance;
        }
    }

    private void Awake()
    {
        if (_instance == this)
        {
            DestroyImmediate(this.gameObject);
        }
        else
        {
            _instance = this;
        }

    }

    #endregion

    private void OnInternetStatusChanged(NetworkReachability status)
    {
        if (InternetStatusChanged != null)
            InternetStatusChanged(status);
    }
    private IEnumerator CheckInternetStatus()
    {
        while (true)
        {
            yield return new WaitForSeconds(2.0f);

            if (_currentNetworkReachabilityStatus != Application.internetReachability)
            {
                _currentNetworkReachabilityStatus = Application.internetReachability;
                OnInternetStatusChanged(_currentNetworkReachabilityStatus);
            }
        }
    }

    public void SubscribeToInternetStatusChange(Action<NetworkReachability> action)
    {
        _currentNetworkReachabilityStatus = Application.internetReachability;
        InternetStatusChanged += action;

        _checkInternetStatusCoroutine = StartCoroutine(CheckInternetStatus());
    }

    public void UnsubscribeFromInternetStatusChange(Action<NetworkReachability> action)
    {
        if (_checkInternetStatusCoroutine != null)
        {
            StopCoroutine(_checkInternetStatusCoroutine);
        }

        if (InternetStatusChanged != null)
        {
            InternetStatusChanged -= action;
        }
    }

    public bool IsInternetAvalible
    {
        get
        {
            return Application.internetReachability != NetworkReachability.NotReachable;
        }
    }

    public PopupBase ShowPopup()
    {
        BlockTopPopup();
        return UIManager.Instance.CreateNewUIElement<PopupNoInternet>(UIElementType.InternetNotReachable);
    }

    public PopupBase ShowPopupIfNeeded()
    {
        if (!IsInternetAvalible)
        {
            BlockTopPopup();
            return
                UIManager.Instance.CreateNewUIElement<PopupNoInternet>(UIElementType.InternetNotReachable);
        }
        return null;
    }

    private void BlockTopPopup()
    {
        var topPopup = UIManager.Instance.GetTopPopup();
        if (topPopup != null)
        {
            topPopup.BlockButtons(true);
        }
    }
}
