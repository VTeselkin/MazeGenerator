﻿using System;
using System.IO;
using Assets.Scripts.Engine.UserFileSystem.ImplementationPlatforms;

namespace Assets.Scripts.Engine.UserFileSystem {
    public static class File {
        private static IFilePlatform _filePlatform {
            get {
#if NETFX_CORE
                return new FileWinStorePlatform();
#elif UNITY_WP8 && !UNITY_EDITOR
                return new FileWinPhonePlatform();
#elif UNITY_WEBPLAYER
                return new FileWebPlatform();                
#else
                return new FileOtherPlatforms();
#endif
            }
        }

        internal static void WriteAllText(string newFilePath, string contents) {
            _filePlatform.WriteAllText(newFilePath, contents);
        }
		
		internal static void WriteAllBytes(string newFilePath, byte[] contents) {
			_filePlatform.WriteAllBytes(newFilePath, contents);
		}

        internal static string ReadAllText(string p) {
           return _filePlatform.ReadAllText(p);
        }

        internal static byte[] ReadAllBytes(string p) {
            return _filePlatform.ReadAllBytes(p);
        }

        public static bool Exists(string filePath) {
            return _filePlatform.Exists(filePath);
        }

        public static void Delete(string filePath) {
            _filePlatform.Delete(filePath);
        }

        //public static Stream OpenRead(string filePath) {
        //    return _filePlatform.OpenRead(filePath);
        //}
    }
}
