﻿
using System;
using System.IO;

namespace Assets.Scripts.Engine.UserFileSystem {
    public interface IFilePlatform {
        void WriteAllText(string newFilePath, string contents);
		
		void WriteAllBytes(string newFilePath, byte[] contents);

        string ReadAllText(string p);

        byte[] ReadAllBytes(string p);

        bool Exists(string filePath);

        void Delete(string filePath);

        //Stream OpenRead(string filePath);
    }
}
