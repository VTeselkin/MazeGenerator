﻿
namespace Assets.Scripts.Engine.UserFileSystem {
   public interface IDirectoryPlatform {
       bool Exists(string filePath);

       void CreateDirectory(string filePath);
       
       void DeleteDirectory(string filePath);

       string[] GetFiles(string path, string searchPattern);
   }
}
