﻿namespace Assets.Scripts.Engine.UserFileSystem {
    public interface IUserFile {
        void SetString(string fileName, string data);
        string GetString(string fileName);
        string GetString(string fileName, string defaultData);

        void SetFloat(string fileName, float data);
        float GetFloat(string fileName);
        float GetFloat(string fileName, float defaultData);

        void SetInt(string fileName, int data);
        int GetInt(string fileName);
        int GetInt(string fileName, int defaultData);

        void SetJSONObject(string fileName, JSONObject data);
        JSONObject GetJSONObject(string fileName);

        void Save();
        void DeleteAll();
        void Delete(string fileName);
    }
}