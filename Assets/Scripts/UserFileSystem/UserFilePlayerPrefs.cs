﻿using UnityEngine;

namespace Assets.Scripts.Engine.UserFileSystem {
    internal class UserFilePlayerPrefs : IUserFile {
        public void SetString(string fileName, string data) {
            PlayerPrefs.SetString(fileName, data);
        }

        public string GetString(string fileName) {
            return PlayerPrefs.GetString(fileName);
        }

        public string GetString(string fileName, string defaultData) {
            return PlayerPrefs.GetString(fileName, defaultData);
        }

        public void SetFloat(string fileName, float data) {
            PlayerPrefs.SetFloat(fileName, data);
        }

        public float GetFloat(string fileName) {
            return PlayerPrefs.GetFloat(fileName);
        }

        public float GetFloat(string fileName, float defaultData) {
            return PlayerPrefs.GetFloat(fileName, defaultData);
        }

        public void SetInt(string fileName, int data) {
            PlayerPrefs.SetInt(fileName, data);
        }

        public int GetInt(string fileName) {
            return PlayerPrefs.GetInt(fileName);
        }

        public int GetInt(string fileName, int defaultData) {
            return PlayerPrefs.GetInt(fileName, defaultData);
        }

        public void DeleteAll() {
            PlayerPrefs.DeleteAll();
        }

        public void SetJSONObject(string fileName, JSONObject data) {
            SetString(fileName, data.ToString());
        }

        public JSONObject GetJSONObject(string fileName) {
            string defaultValue = "";
            string res = GetString(fileName, defaultValue);
            if (res != defaultValue)
                return new JSONObject(res);
            return null;
        }

        public void Save() {
            PlayerPrefs.Save();
        }

        public void Delete(string fileName) {
            PlayerPrefs.DeleteKey(fileName);
        }
    }
}