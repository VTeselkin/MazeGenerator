﻿
using Assets.Scripts.Engine.UserFileSystem.ImplementationPlatforms;

namespace Assets.Scripts.Engine.UserFileSystem {
    public static class Directory {

        private static IDirectoryPlatform _directoryPlatform {
            get {
#if NETFX_CORE
                return new DirectoryWinStorePlatform();

#elif UNITY_WP8 && !UNITY_EDITOR
                return new DirectoryWinPhonePlatform();
#elif UNITY_WEBPLAYER
                return new DirectoryWebPlayerPlatform();
#else
                return new DirectoryOtherPlatforms();
#endif
            }
        }

        internal static bool Exists(string filePath) {
            return _directoryPlatform.Exists(filePath);
        }

        internal static void CreateDirectory(string filePath) {
            _directoryPlatform.CreateDirectory(filePath);
        }

        public static void DeleteDirectory(string filePath) {
            _directoryPlatform.DeleteDirectory(filePath);
        }

        public static string[] GetFiles(string path, string searchPattern) {
            return _directoryPlatform.GetFiles(path, searchPattern);
        }
    }
}
