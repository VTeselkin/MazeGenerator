﻿using Assets.Scripts.Shared;

namespace Assets.Scripts.Engine.UserFileSystem {
    public class UserFile : UnitySingleton<UserFile>, IUserFile {
        private readonly IUserFile _file = new UserFilePlayerPrefs();

        public void SetString(string fileName, string data) {
            _file.SetString(fileName, data);
			_file.Save();
        }

        public string GetString(string fileName) {
            return _file.GetString(fileName);
        }

        public string GetString(string fileName, string defaultData) {
            return _file.GetString(fileName, defaultData);
        }

        public void SetFloat(string fileName, float data) {
            _file.SetFloat(fileName, data);
			_file.Save();
        }

        public float GetFloat(string fileName) {
            return _file.GetFloat(fileName);
        }

        public float GetFloat(string fileName, float defaultData) {
            return _file.GetFloat(fileName, defaultData);
        }

        public void SetInt(string fileName, int data) {
            _file.SetInt(fileName, data);
			_file.Save();
        }

        public int GetInt(string fileName) {
            return _file.GetInt(fileName);
        }

        public int GetInt(string fileName, int defaultData) {
            return _file.GetInt(fileName, defaultData);
        }

        public void SetJSONObject(string fileName, JSONObject data) {
            _file.SetJSONObject(fileName, data);
			_file.Save();
        }

        public JSONObject GetJSONObject(string fileName) {
            return _file.GetJSONObject(fileName);
        }

        public void DeleteAll() {
            _file.DeleteAll();
        }

        public void Delete(string fileName) {
            _file.Delete(fileName);
        }

        public void Save() {
            _file.Save();
        }
    }
}