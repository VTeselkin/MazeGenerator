﻿
#if UNITY_EDITOR && !UNITY_WEBPLAYER || !NETFX_CORE && !UNITY_WP8 && !UNITY_WEBPLAYER

namespace Assets.Scripts.Engine.UserFileSystem.ImplementationPlatforms {
    internal class FileOtherPlatforms : IFilePlatform {
        public void WriteAllText(string newFilePath, string contents) {
            System.IO.File.WriteAllText(newFilePath, contents);
        }

        public string ReadAllText(string p) {
            return System.IO.File.ReadAllText(p);
        }

        public byte[] ReadAllBytes(string p) {
            return System.IO.File.ReadAllBytes(p);
        }

        public void WriteAllBytes(string newFilePath, byte[] contents) {
            System.IO.File.WriteAllBytes(newFilePath, contents);
        }

        public bool Exists(string filePath) {
            return System.IO.File.Exists(filePath);
        }

        public void Delete(string filePath) {
            System.IO.File.Delete(filePath);
        }

        //public System.IO.Stream OpenRead(string filePath) {
        //    return System.IO.File.OpenRead(filePath);
        //}
    }
}

#endif