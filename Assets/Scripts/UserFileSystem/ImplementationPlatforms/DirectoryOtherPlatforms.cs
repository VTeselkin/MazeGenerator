﻿#if UNITY_EDITOR && !UNITY_WEBPLAYER || !NETFX_CORE && !UNITY_WP8 && !UNITY_WEBPLAYER
namespace Assets.Scripts.Engine.UserFileSystem.ImplementationPlatforms {
   internal class DirectoryOtherPlatforms :IDirectoryPlatform {
       public bool Exists(string filePath) {
           return System.IO.Directory.Exists(filePath);
       }

       public void CreateDirectory(string filePath) {
           System.IO.Directory.CreateDirectory(filePath);
       }

       public void DeleteDirectory(string filePath) {
           System.IO.Directory.Delete(filePath, true);
       }

       public string[] GetFiles(string path, string searchPattern) {
           return System.IO.Directory.GetFiles(path, searchPattern);
       }
   }
}
#endif