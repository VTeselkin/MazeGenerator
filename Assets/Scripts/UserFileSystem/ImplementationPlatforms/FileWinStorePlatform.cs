﻿#if NETFX_CORE
using System;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.Storage;
using Windows.Storage.Streams;
using System.IO;

namespace Assets.Scripts.Engine.UserFileSystem.ImplementationPlatforms {
   internal class FileWinStorePlatform:IFilePlatform {

        public void WriteAllText(string newFilePath, string contents) {
            newFilePath = NormalizePath(newFilePath);            

            var localFolder = ApplicationData.Current.LocalFolder;
            var file = localFolder.CreateFileAsync(newFilePath, CreationCollisionOption.ReplaceExisting).AsTask().GetAwaiter().GetResult();
            var fileStream = file.OpenAsync(FileAccessMode.ReadWrite).AsTask().GetAwaiter().GetResult();

            IOutputStream outputStream = fileStream.GetOutputStreamAt(0);
            DataWriter dataWriter = new DataWriter(outputStream);

            dataWriter.WriteString(contents);
            dataWriter.StoreAsync().AsTask().GetAwaiter().GetResult();

            outputStream.FlushAsync();
    
            dataWriter.Dispose();
            outputStream.Dispose();
            fileStream.Dispose();
        }
		
		public void WriteAllBytes(string newFilePath, byte[] contents) {
            newFilePath = NormalizePath(newFilePath);            

			var localFolder = ApplicationData.Current.LocalFolder;
			var file = localFolder.CreateFileAsync(newFilePath, CreationCollisionOption.ReplaceExisting).AsTask().GetAwaiter().GetResult();
			var fileStream = file.OpenAsync(FileAccessMode.ReadWrite).AsTask().GetAwaiter().GetResult();
			
			IOutputStream outputStream = fileStream.GetOutputStreamAt(0);
			DataWriter dataWriter = new DataWriter(outputStream);
			
			dataWriter.WriteBytes(contents);
			dataWriter.StoreAsync().AsTask().GetAwaiter().GetResult();
			
			outputStream.FlushAsync();
			
			dataWriter.Dispose();
			outputStream.Dispose();
			fileStream.Dispose();
		}

        public byte[] ReadAllBytes(string p) {
            p = NormalizePath(p);

            var localFolder = ApplicationData.Current.LocalFolder;
            var file = localFolder.GetFileAsync(p).AsTask().GetAwaiter().GetResult();
            var fileStream = file.OpenAsync(FileAccessMode.Read).AsTask().GetAwaiter().GetResult();

            Stream stream = fileStream.AsStream();
            byte[] byteArray = new byte[stream.Length];
            stream.Read(byteArray, 0, (int) stream.Length);

            stream.Dispose();
            fileStream.Dispose();

            return byteArray;
        }

       public string ReadAllText(string p) {
            p = NormalizePath(p);

            var localFolder = ApplicationData.Current.LocalFolder;
            var file = localFolder.GetFileAsync(p).AsTask().GetAwaiter().GetResult();
            var fileStream = file.OpenAsync(FileAccessMode.Read).AsTask().GetAwaiter().GetResult();

            StreamReader streamReader = new StreamReader(fileStream.AsStream());
            var readedData = streamReader.ReadToEnd();

            streamReader.Dispose();
            fileStream.Dispose();

            return readedData;
        }  

       public bool Exists(string filePath) {
           try {
                filePath = NormalizePath(filePath);

                var folder = ApplicationData.Current.LocalFolder;
                var file = folder.GetFileAsync(filePath).AsTask().GetAwaiter().GetResult();
                return true;
            }
            catch (Exception ex) {
                return false;
            }
       }

       public void Delete(string filePath) {
            filePath = NormalizePath(filePath);

            var localFolder = ApplicationData.Current.LocalFolder;
            var file = localFolder.GetFileAsync(filePath).AsTask().GetAwaiter().GetResult();
            file.DeleteAsync(StorageDeleteOption.PermanentDelete).AsTask().GetAwaiter().GetResult();
       }

       //public Stream OpenRead(string filePath) {
       //     filePath = NormalizePath(filePath);

       //     var localFolder = ApplicationData.Current.LocalFolder;
       //     var file = localFolder.GetFileAsync(filePath).AsTask().GetAwaiter().GetResult();
       //     var fileStream = file.OpenAsync(FileAccessMode.ReadWrite).AsTask().GetAwaiter().GetResult();

       //     return fileStream.AsStream();
       //}

        private string NormalizePath(string path) {
            if (path[0].Equals('/')) {
                path = path.Remove(0, 1);
            }

            path = path.Replace('/', '\\');
            return path;
        }
   }
}
#endif
