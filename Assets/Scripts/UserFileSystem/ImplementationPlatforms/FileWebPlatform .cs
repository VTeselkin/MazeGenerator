﻿#if UNITY_WEBPLAYER
using UnityEngine;
using System.Collections.Generic;
using System.Text;

namespace Assets.Scripts.Engine.UserFileSystem.ImplementationPlatforms {
	internal class FileWebPlatform : IFilePlatform {
		
		//private Dictionary<string, string> cash = new Dictionary<string, string>();

		public void WriteAllText(string newFilePath, string contents) {
            //cash[newFilePath] = contents;            

            PlayerPrefs.SetString(newFilePath, contents);
            PlayerPrefs.Save();
		}

        public void WriteAllBytes(string newFilePath, byte[] contents) {
            //cash[newFilePath] = UTF8Encoding.UTF8.GetString(contents);            

            string bytes = UTF8Encoding.UTF8.GetString(contents);

            PlayerPrefs.SetString(newFilePath, bytes);
            PlayerPrefs.Save();
        }
		
		public string ReadAllText(string p) {
            //if (!cash.ContainsKey(p))
            //    return null;
            //return cash[p];

            if (!PlayerPrefs.HasKey(p))
                return null;

            return PlayerPrefs.GetString(p);
		}
		
		public byte[] ReadAllBytes(string p) {
            //if (!cash.ContainsKey(p))
            //    return null;
            //return UTF8Encoding.UTF8.GetBytes( cash[p]);

            if (!PlayerPrefs.HasKey(p))
                return null;

            return UTF8Encoding.UTF8.GetBytes(PlayerPrefs.GetString(p));
		}	

        public bool Exists(string filePath) {
            //return cash.ContainsKey(filePath);

            return PlayerPrefs.HasKey(filePath);
		}

        public void Delete(string filePath) {
            //if (Exists(filePath)) {
            //    cash.Remove(filePath);            
            //}         

            PlayerPrefs.DeleteKey(filePath);
            PlayerPrefs.Save();
        }
	}
}
#endif