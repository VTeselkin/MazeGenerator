﻿#if NETFX_CORE
using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.Storage;

namespace Assets.Scripts.Engine.UserFileSystem.ImplementationPlatforms {
    internal class DirectoryWinStorePlatform : IDirectoryPlatform {

       public bool Exists(string filePath) {            
            try {
                filePath = NormalizePath(filePath);
                var localFolder = ApplicationData.Current.LocalFolder;
                var folder = localFolder.GetFolderAsync(filePath).AsTask().GetAwaiter().GetResult();
                return true;
            }
            catch {
                return false;
            }
       }

       public void CreateDirectory(string filePath) {
        try {
            filePath = NormalizePath(filePath);
            var localFolder = ApplicationData.Current.LocalFolder;
            var folder = localFolder.CreateFolderAsync(filePath).AsTask().GetAwaiter().GetResult();
        }
            catch {
                
            }
       }

        public string[] GetFiles(string path, string searchPattern) {
            path = NormalizePath(path);

            var localFolder = ApplicationData.Current.LocalFolder;
            var folder = localFolder.GetFolderAsync(path).AsTask().GetAwaiter().GetResult();
            var folderItems = folder.GetItemsAsync().AsTask().GetAwaiter().GetResult();

            var correctFolderItems = new List<string>();
            foreach (var storageItem in folderItems) {
                if (storageItem.Path.EndsWith(searchPattern)) {
                    correctFolderItems.Add(storageItem.Path);
                }
            }

            var result = correctFolderItems.ToArray();
            for (var i = 0; i < result.Length; i++) {
                result[i] = Path.GetFileName(result[i]);
            }

            return result;
        }

        private string NormalizePath(string path) {
            if (path[0].Equals('/')) {
                path = path.Remove(0, 1);
            }

            while (path[0].Equals('\\')) {
                path = path.Remove(0, 1);
            }

            path = path.Replace('/', '\\');
            return path;
        }
    }
}
#endif
