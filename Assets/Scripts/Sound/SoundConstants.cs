﻿using System.Collections.Generic;
using Assets.Scripts.Helpers;

public static class SoundConstants
{
    public const string MusicLocalPath = "Sounds/";
    public const string SoundPathMenu = "Menu/MainMenu";

    public static Dictionary<int, string> LocationMusic;
    public static List<string> ListSoundPathTrackSelect;


    public static Dictionary<ObjectTypeEnum, string> ListSoundPathObjectsDamage;
    public static Dictionary<ObjectTypeEnum, string> ListSoundPathObjectsDestroy;
    public static Dictionary<ObjectTypeEnum, string> ListSoundPathObjectsCreate;
    public static Dictionary<ObjectTypeEnum, string> ListSoundPathObjectsAction;

    public static Dictionary<UIAudioClipTypes, string> ListSoundUi;
    public static Dictionary<GameAudioClipType, string> ListSoundGame;

    public enum UIAudioClipTypes
    {
        ButtonClick = 0,
        ButtonPress = 1,
        LevelLose = 2,
        LevelVictory = 3,
        PopupClose = 4,
        PopupLevel = 5,
        PopupOpen = 6,
        Star1 = 7,
        Star2 = 8,
        Star3 = 9,

        Button_Buy,
        Button_claim,
        Button_Coins,
        Button_Deals,
        Button_FB,
        Button_Gems,
        Button_Invite,
        Button_L,
        Button_next,
        Button_Off,
        Button_ok,
        Button_On,
        Button_Play,
        Button_R,
        Checkbox_Off,
        Checkbox_On,
        Pop_up_ConnectToFB,
        Pop_up_NotEnoughMoves,
        Pop_up_RefillEnergy,
        Pop_up_Settings,
        Pop_up_Small_Error,
        Pop_up_Small_OK,
        Pop_up_Store,
        Pop_up_YourMessages,
        Popup_Daily_Event,
        Popup_Daily_Event_Rewards
    }

    public enum GameAudioClipType
    {
        PlayerStep = 0
    }
    static SoundConstants()
    {
        InitSoundTrackSelect();
        InitSoundUi();
        InitListSoundGame();
        InitLocationMusic();
        InitObjectsDestroy();
        InitObjectsDamage();
    }
    static void InitListSoundGame()
    {
        ListSoundGame = new Dictionary<GameAudioClipType, string>();
        ListSoundGame[GameAudioClipType.PlayerStep] = "Game/player_step";
    }
    static void InitSoundTrackSelect()
    {
        ListSoundPathTrackSelect = new List<string>(){
                "Track/Select_1",
                "Track/Select_2",
                "Track/Select_3",
                "Track/Select_4",
                "Track/Select_5",
                "Track/Select_6",
                "Track/Select_7",
                "Track/Select_8",
                "Track/Select_9",
                "Track/Select_10",
                "Track/Select_11",
                "Track/Select_12",
                "Track/Select_13",
                "Track/Select_14",
                "Track/Select_15"
            };
    }

    static void InitSoundUi()
    {
        ListSoundUi = new Dictionary<UIAudioClipTypes, string>();
        ListSoundUi[UIAudioClipTypes.ButtonClick] = "UI/Button_Click";
        ListSoundUi[UIAudioClipTypes.ButtonPress] = "UI/Button_Press";
        ListSoundUi[UIAudioClipTypes.LevelLose] = "UI/Level_Lose";
        ListSoundUi[UIAudioClipTypes.LevelVictory] = "UI/Level_Victory";
        ListSoundUi[UIAudioClipTypes.PopupClose] = "UI/Pop-up_close";
        ListSoundUi[UIAudioClipTypes.PopupLevel] = "UI/Pop-up_Level";
        ListSoundUi[UIAudioClipTypes.PopupOpen] = "UI/Pop-up_open";
        ListSoundUi[UIAudioClipTypes.Star1] = "UI/Star01";
        ListSoundUi[UIAudioClipTypes.Star2] = "UI/Star02";
        ListSoundUi[UIAudioClipTypes.Star3] = "UI/Star03";

        ListSoundUi[UIAudioClipTypes.Button_Buy] = "UI/Button_Buy";
        ListSoundUi[UIAudioClipTypes.Button_claim] = "UI/Button_claim";
        ListSoundUi[UIAudioClipTypes.Button_Coins] = "UI/Button_Coins";
        ListSoundUi[UIAudioClipTypes.Button_Deals] = "UI/Button_Deals";
        ListSoundUi[UIAudioClipTypes.Button_FB] = "UI/Button_FB";
        ListSoundUi[UIAudioClipTypes.Button_Gems] = "UI/Button_Gems";
        ListSoundUi[UIAudioClipTypes.Button_Invite] = "UI/Button_Invite";
        ListSoundUi[UIAudioClipTypes.Button_L] = "UI/Button_L";
        ListSoundUi[UIAudioClipTypes.Button_next] = "UI/Button_next";
        ListSoundUi[UIAudioClipTypes.Button_Off] = "UI/Button_Off";
        ListSoundUi[UIAudioClipTypes.Button_ok] = "UI/Button_ok";
        ListSoundUi[UIAudioClipTypes.Button_On] = "UI/Button_On";
        ListSoundUi[UIAudioClipTypes.Button_Play] = "UI/Button_Play";
        ListSoundUi[UIAudioClipTypes.Button_R] = "UI/Button_R";
        ListSoundUi[UIAudioClipTypes.Checkbox_Off] = "UI/Checkbox_Off";
        ListSoundUi[UIAudioClipTypes.Checkbox_On] = "UI/Checkbox_On";
        ListSoundUi[UIAudioClipTypes.Pop_up_ConnectToFB] = "UI/Pop-up_ConnectToFB";
        ListSoundUi[UIAudioClipTypes.Pop_up_NotEnoughMoves] = "UI/Pop-up_NotEnoughMoves";
        ListSoundUi[UIAudioClipTypes.Pop_up_RefillEnergy] = "UI/Pop-up_RefillEnergy";
        ListSoundUi[UIAudioClipTypes.Pop_up_Settings] = "UI/Pop-up_Settings";
        ListSoundUi[UIAudioClipTypes.Pop_up_Small_Error] = "UI/Pop-up_Small_Error";
        ListSoundUi[UIAudioClipTypes.Pop_up_Small_OK] = "UI/Pop-up_Small_OK";
        ListSoundUi[UIAudioClipTypes.Pop_up_Store] = "UI/Pop-up_Store";
        ListSoundUi[UIAudioClipTypes.Pop_up_YourMessages] = "UI/Pop-up_YourMessages";
        ListSoundUi[UIAudioClipTypes.Popup_Daily_Event] = "UI/Popup-Daily_Event";
        ListSoundUi[UIAudioClipTypes.Popup_Daily_Event_Rewards] = "UI/Popup-Daily_Event_Rewards";
    }

    static void InitLocationMusic()
    {
        LocationMusic = new Dictionary<int, string>();
        LocationMusic[0] = "Location/1";
        LocationMusic[1] = "Location/2";
    }

    static void InitObjectsDestroy()
    {
        ListSoundPathObjectsDestroy = new Dictionary<ObjectTypeEnum, string>();
        ListSoundPathObjectsDestroy[ObjectTypeEnum.Finish] = "GameObjects/Door";
        ListSoundPathObjectsDestroy[ObjectTypeEnum.Food] = "GameObjects/Food";

    }

    static void InitObjectsDamage()
    {
        ListSoundPathObjectsDamage = new Dictionary<ObjectTypeEnum, string>();
        ListSoundPathObjectsDamage[ObjectTypeEnum.EnemySimple] = "GameObjects/EnemySimple";
        ListSoundPathObjectsDamage[ObjectTypeEnum.EnemySee] = "GameObjects/DamageHands1";
        ListSoundPathObjectsDamage[ObjectTypeEnum.EnemySeeBat] = "GameObjects/DamageHands2";
        ListSoundPathObjectsDamage[ObjectTypeEnum.EnemySnailGreen] = "GameObjects/DamageHands3";
        ListSoundPathObjectsDamage[ObjectTypeEnum.EnemyZombie] = "GameObjects/SwordCrit1";
        ListSoundPathObjectsDamage[ObjectTypeEnum.EnemySeeZombie] = "GameObjects/SwordCrit2";
    }
}

